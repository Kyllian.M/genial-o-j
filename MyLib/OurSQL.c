#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include "OurSQL.h"
#include <assert.h>
#include "../GfxLib/ESLib.h"			//Pour valeurAleatoire()
#include "../MyLib/consoletools.h"
#include "../MyLib/KyllianToolsLib.h"	//outils generaux printdebug() betterFree() bigMalloc() etc ...
#include <unistd.h>

// GLOBALES

char** str2_split(char* a_str, const char a_delim);
int getDb2Text (char* fcontents,char* filename);

// FONCTIONS

//Convertion d'une chaine de types ("str,int,flo") en tableau de types (typeSTR,typeINT,typeFLO)
int* identifyTypeList (char* typelist){
	int nbtype = 1;
	for(int i=0;i<strlen(typelist);i++){
		if(typelist[i] == ','){
			nbtype++;
		}
	}
	char** types= str2_split(typelist,',');
	int* typeint = malloc(sizeof(int)*nbtype);
	for(int i=0; i< nbtype;i++){
		if(str_same(types[i],"int")){
			typeint[i] = typeINT;
		}
		if(str_same(types[i],"flo")){
			typeint[i] = typeFLO;
		}
		if(str_same(types[i],"str")){
			typeint[i] = typeSTR;
		}
	}
	return typeint;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//convertit le texte d'un fichier osql en table osql
Table getTableFromFile (char* dbname){
	Table T;
	char* fcontents = fileGetText(dbname);
	if(fcontents != NULL){ 	//SI LE FICHIER SPECIFIE EXISTE
		printdebug(fcontents);
		char* backupFC = malloc(sizeof(char)*(strlen(fcontents)+1));
		strcpy(backupFC,fcontents);
		char** contentbyline;
		contentbyline = str_split(fcontents,'?');
		if(!strcmp(contentbyline[0],"<BDD-START>")){
			char** OSEF = str2_split(contentbyline[1],'"');
			T.NomColonnes = str2_split(OSEF[1],',');
			T.typelist = identifyTypeList(OSEF[3]);
			for(int i = 0;T.NomColonnes[i];i++){
				T.nbcolonne = i;
			}
			int j = 0;
			(T.nbcolonne) += 1;
			T.T = malloc(sizeof(char**)*1);
			for(int i = 2;!str_same(contentbyline[i],"<BDD-END>");i++){
				T.T = realloc(T.T,sizeof(char**)*(i+1));
				removeChar(contentbyline[i],'{');
				removeChar(contentbyline[i],'}');
				removeChar(contentbyline[i],'"');
				(T.T)[j] = str2_split(contentbyline[i],',');
				j++;
				T.nbligne = i-1;
			}
		}
	}
	else{
		printf(TXT_RED);
		printf("nope\n"); // BDD N EXISTE PAS
		printf(TXT_END_COLOR);
	}
	return T;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//Reduit la table en ne gardant que les colonnes correspondent aux chaines du deuxieme argument
Table shrinkTable (Table TS,char** selected){
	bool* keep = malloc(sizeof(bool)*TS.nbcolonne);
	int nbkeep = 0;
	for(int i=0;i<TS.nbcolonne;i++){
		if(selected == ALL_SELECTED){
			keep[i] = true;
			nbkeep++;
			return TS;
			continue;
		}
		else{
			keep[i] = false;
			for(int i2=0;selected[i2];i2++){
				if(str_same(TS.NomColonnes[i],selected[i2])){
					keep[i] = true;
					nbkeep++;
				}
			}
		}
	}
	Table TR;
	TR.nbligne = TS.nbligne;
	TR.nbcolonne = nbkeep;
	TR.NomColonnes = malloc(sizeof(char*)*nbkeep);
	TR.typelist = malloc(sizeof(int)*nbkeep);
	int j = 0;
	for(int i=0;i<TS.nbcolonne;i++){
		if(keep[i]){
			TR.NomColonnes[j] = malloc(sizeof(char)*strlen(TS.NomColonnes[i]));
			strcpy(TR.NomColonnes[j],TS.NomColonnes[i]);
			TR.typelist[j] = TS.typelist[i];
			j++;
		}
	}
	TR.T = malloc(sizeof(char**)*TR.nbligne);
	for(int i=0;i<TS.nbligne;i++){
		TR.T[i] = malloc(sizeof(char*)*TR.nbcolonne);
		int j2=0;
		for(int i2=0;i2<TS.nbcolonne;i2++){
			if(keep[i2]){
				(TR.T)[i][j2] = malloc(sizeof(char)*strlen((TS.T)[i][i2]));
				strcpy((TR.T)[i][j2],(TS.T)[i][i2]);
				j2++;
			}
		}
	}
	return TR;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//Retourne l'index d'une chaine de caractere donnée dans un tableau de chaines de caracteres
int getIndexOfString (char** src,char* search,int size){
	for(int i=0;i < size;i++){
		if(str_same(src[i],search)){
			return i;
		}
	}
	return -1;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//quitte le programme en affichant un message d'erreur
void quit (char* escapeMessage){
	printf(TXT_RED);
	printf("%s\n",escapeMessage);
	printf(TXT_END_COLOR);
	char cmd[40];
	sprintf(cmd,"kill %d",getpid());
	system(cmd);
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//Fonction INNER_JOIN sur 2 tables en specifiant la colonne clé primaire
Table joinTables (Table T1, Table T2,char* column){
	int nbcolonnecommune = -1;
	int* columnmap = malloc(sizeof(int)*T1.nbcolonne);
	for(int i=0;i<T1.nbcolonne;i++){
		columnmap[i] = -1;
		for(int i2=0;i2<T2.nbcolonne;i2++){
			if(str_same(T1.NomColonnes[i],T2.NomColonnes[i2])){
				columnmap[i] = i2;
				nbcolonnecommune++;
			}
		}
	}

	int nbcolonne;
	nbcolonne = T1.nbcolonne + T2.nbcolonne -1 -nbcolonnecommune;
	Table T3;
	T3.nbcolonne = nbcolonne;
	T3.NomColonnes = malloc(sizeof(char*)*nbcolonne);
	T3.typelist = malloc(sizeof(int)*nbcolonne);
	T3.T = malloc(sizeof(char**));
	int indexT1 = getIndexOfString(T1.NomColonnes,column,T1.nbcolonne);
	int indexT2 = getIndexOfString(T2.NomColonnes,column,T2.nbcolonne);
	if(indexT1 == -1 || indexT2 == -1){
		quit("ERROR : INVALID INNER JOIN ON NON-COMMON COLUMN");
	}
	int j=1;
	T3.NomColonnes[0] = malloc(sizeof(char)*strlen(T1.NomColonnes[indexT1]));
	strcpy(T3.NomColonnes[0],T1.NomColonnes[indexT1]);
	T3.typelist[0] = T1.typelist[indexT1];
	for(int i=0;i<T1.nbcolonne;i++){
		if(i != indexT1 && columnmap[i] == -1){
			T3.NomColonnes[j] = malloc(sizeof(char)*strlen(T1.NomColonnes[i]));
			strcpy(T3.NomColonnes[j],T1.NomColonnes[i]);
			T3.typelist[j] = T1.typelist[i];
			j++;
		}
	}
	for(int i=0;i<T2.nbcolonne;i++){
		if(i != indexT2){
			T3.NomColonnes[j] = malloc(sizeof(char)*strlen(T2.NomColonnes[i]));
			strcpy(T3.NomColonnes[j],T2.NomColonnes[i]);
			T3.typelist[j] = T2.typelist[i];
			j++;
		}
	}
	orderTableBy(T1,column);
	orderTableBy(T2,column);
	int j1 = 0;
	int j2 = 0;
	for(int i=0;j1 < T1.nbligne || j2 < T2.nbligne;i++){	//Jusqu'a ce qu il n y ai plus de ligne a copier dans t1 et t2
		T3.T = realloc(T3.T,sizeof(char**)*(i+1));			//Ajout d une ligne a la table T3
		T3.T[i] = malloc(sizeof(char*)*nbcolonne);
		int cpt = 0;
		if(j2<T2.nbligne && (j1>= T1.nbligne || ( strcmp(T1.T[j1][indexT1],T2.T[j2][indexT2]) > 0))){	//si il reste des lignes a copier dans t2 et ligne de t2 est plus petite
			T3.T[i][0] = malloc(sizeof(char)*strlen(T2.T[j2][indexT2]));
			strcpy(T3.T[i][0],T2.T[j2][indexT2]);
			T3.T[i][0] = T2.T[j2][indexT2];
			for(int i2 = 0;i2< T1.nbcolonne;i2++){
				if(i2 != indexT1){
					if(columnmap[i2] == -1){
						T3.T[i][cpt] = malloc(sizeof(char)*strlen("NULL"));
						strcpy(T3.T[i][cpt],"NULL");
						cpt++;
					}
				}
				else{
					cpt++;
				}
			}
			for(int i2 = T1.nbcolonne-nbcolonnecommune;i2 < T1.nbcolonne + T2.nbcolonne - nbcolonnecommune;i2++){
				if(i2-(T1.nbcolonne-nbcolonnecommune) != indexT2){
					T3.T[i][i2-1] = malloc(sizeof(char)*strlen(T2.T[j2][i2-(T1.nbcolonne-nbcolonnecommune)]));
					strcpy(T3.T[i][i2-1],T2.T[j2][i2-(T1.nbcolonne-nbcolonnecommune)]);
				}
			}
			j2++;
		}


		else {
			if(j1<T1.nbligne && (j2>= T2.nbligne || (strcmp(T1.T[j1][indexT1],T2.T[j2][indexT2]) < 0))){
				T3.T[i][0] = malloc(sizeof(char)*strlen(T1.T[j1][indexT1]));
				strcpy(T3.T[i][0],T1.T[j1][indexT1]);
				for(int i2 = 0;i2< T1.nbcolonne;i2++){
					if(i2 != indexT1){
						if(columnmap[i2] == -1){
							T3.T[i][cpt] = malloc(sizeof(char)*strlen(T1.T[j1][i2]));
							strcpy(T3.T[i][cpt],T1.T[j1][i2]);
							cpt++;
						}
					}
					else{
						cpt++;
					}
				}
				for(int i2 = T1.nbcolonne - nbcolonnecommune;i2 < T1.nbcolonne + T2.nbcolonne - nbcolonnecommune;i2++){
					if(i2 - (T1.nbcolonne-nbcolonnecommune) != indexT2){
						bool copied = false;
						for(int i3=0;i3<T1.nbcolonne;i3++){
							if(columnmap[i3] == i2 - (T1.nbcolonne-nbcolonnecommune)){
								T3.T[i][i2-1] = malloc(sizeof(char)*strlen(T1.T[j1][i3]));
								strcpy(T3.T[i][i2-1],T1.T[j1][i3]);
								copied = true;
							}
						}
						if(!copied){
							T3.T[i][i2-1] = malloc(sizeof(char)*strlen("NULL"));
							strcpy(T3.T[i][i2-1],"NULL");
						}
					}
				}
				j1++;
			}

			else{
				if(strcmp(T1.T[j1][indexT1],T2.T[j2][indexT2]) == 0){
					T3.T[i][0] = malloc(sizeof(char)*strlen(T2.T[j2][indexT2]));
					strcpy(T3.T[i][0],T2.T[j2][indexT2]);
					T3.T[i][0] = T2.T[j2][indexT2];
					for(int i2 = 0;i2< T1.nbcolonne;i2++){
						if(i2 != indexT1){
							T3.T[i][i2] = malloc(sizeof(char)*strlen(T1.T[j1][i2]));
							strcpy(T3.T[i][i2],T1.T[j1][i2]);
						}
					}
					for(int i2 = T1.nbcolonne;i2 < T1.nbcolonne + T2.nbcolonne;i2++){
						if(i2 - T1.nbcolonne != indexT2){
							T3.T[i][i2-1] = malloc(sizeof(char)*strlen(T2.T[j2][i2 - T1.nbcolonne]));
							strcpy(T3.T[i][i2-1],T2.T[j2][i2 - T1.nbcolonne]);
						}
					}
					j1++;
					j2++;
				}
			}
		}
		T3.nbligne = i+1;
	}
	return T3;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//Fonction d ecriture du texte contenu dans un fichier osql a partir d une table
char* writeInnerTextFromTable(Table T){
	int size = 60;
	char* fcontents = malloc(sizeof(char)*60);
	strcpy(fcontents,"<BDD-START>?<DESC = \"");
	for(int i=0;i<T.nbcolonne;i++){
		size+= strlen(T.NomColonnes[i]) + 1;
		fcontents = realloc(fcontents,sizeof(char)*(size));
		strcat(fcontents,T.NomColonnes[i]);
		if(i < T.nbcolonne - 1){
			strcat(fcontents,",");
		}
	}
	strcat(fcontents,"\" TYPE = \"");
	for(int i=0;i<T.nbcolonne;i++){
		size+= 4;
		fcontents = realloc(fcontents,sizeof(char)*(size));
		switch(T.typelist[i]){
			case typeINT:
				strcat(fcontents,"int");
			break;
			case typeFLO:
				strcat(fcontents,"flo");
			break;
			case typeSTR:
				strcat(fcontents,"str");
			break;
		}
		if(i < T.nbcolonne - 1){
			strcat(fcontents,",");
		}
	}
	strcat(fcontents,"\" >?");
	for(int i=0;i<T.nbligne;i++){
		size+= 3;
		fcontents = realloc(fcontents,sizeof(char)*(size));
		strcat(fcontents,"{");
		for(int i2=0;i2<T.nbcolonne;i2++){
			size+= strlen(T.T[i][i2]) + 1;
			fcontents = realloc(fcontents,sizeof(char)*(size));
			strcat(fcontents,T.T[i][i2]);
			if(i2 < T.nbcolonne - 1){
				strcat(fcontents,",");
			}
		}
		strcat(fcontents,"}?");
	}
	strcat(fcontents,"<BDD-END>?");
	return fcontents;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//UNUSED Fonction d affichage simple d une table
void afficheTable (Table T){
	for(int i=0;i<T.nbcolonne;i++){
		printf("%s\t\t|",T.NomColonnes[i]);
	}
	printf("\n");
	for(int i=0;i<T.nbligne;i++){
		for(int i2=0;i2< T.nbcolonne;i2++){
			printf("%s\t\t|",(T.T)[i][i2]);
		}
		printf("\n");
	}
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//transforme un tableau de chaines de caracteres en une chaine de caracteres contenant les chaines du tableau séparées par des virgules
char** getSelectedColumnsName (char* selection){
	char** selected = ALL_SELECTED;
	if(strcmp(selection,"ALL")){
		selected = str2_split(selection,',');
	}
	return selected;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//retourne si oui ou non l utilisateur veut selectionner toute la table
bool selectedColumnsAll (char* selection){
	bool selected = false;
	if(!strcmp(selection,"ALL")){
		selected = true;
	}
	return selected;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//retourne si deux chaines sont identiques
bool str_same (char* a,char* b){
	if(!strcmp(a,b)){
		return true;
	}
	else{
		return false;
	}
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//convertit les instructions SELECT,INSERT,WHERE ... En tableau de commandes
void getArgsValue (int argc,char* argv[],int** Args){
	*Args = malloc(sizeof(int)*1);
	bool invalid = true;
	for(int i=0;i<argc;i++){
		*Args = realloc(*Args,sizeof(int)*(i+1));
		if(!strcmp(argv[i],"SELECT")){
			(*Args)[i] = SELECT;
			invalid = false;
		}
		if(!strcmp(argv[i],"FROM")){
			(*Args)[i] = FROM;
			invalid = false;
		}
		if(!strcmp(argv[i],"ALL")){
			(*Args)[i] = ALL;
			invalid = false;
		}
		if(!strcmp(argv[i],"WHERE")){
			(*Args)[i] = WHERE;
			invalid = false;
		}
		if(!strcmp(argv[i],"ORDER_BY")){
			(*Args)[i] = ORDER_BY;
			invalid = false;
		}
		if(!strcmp(argv[i],"INSERT")){
			(*Args)[i] = INSERT;
			invalid = false;
		}
		if(!strcmp(argv[i],"VALUES")){
			(*Args)[i] = VALUES;
			invalid = false;
		}
		if(!strcmp(argv[i],"CREATE")){
			(*Args)[i] = CREATE;
			invalid = false;
		}
		if(!strcmp(argv[i],"ADD_COLUMNS")){
			(*Args)[i] = ADD_COLUMNS;
			invalid = false;
		}
		if(!strcmp(argv[i],"DELETE_COLUMNS")){
			(*Args)[i] = DELETE_COLUMNS;
			invalid = false;
		}
		if(!strcmp(argv[i],"HELP")){
			(*Args)[i] = HELP;
			invalid = false;
		}
		if(!strcmp(argv[i],"UPDATE")){
            (*Args)[i] = UPDATE;
            invalid = false;
        }
		if(!strcmp(argv[i],"DELETE")){
            (*Args)[i] = DELETE;
            invalid = false;
        }
		if(!strcmp(argv[i],"DELETE_ALL")){
            (*Args)[i] = DELETE_ALL;
            invalid = false;
        }
		if(!strcmp(argv[i],"DROP")){
            (*Args)[i] = DROP;
            invalid = false;
        }
		if(invalid){
			(*Args)[i] = INVALID;
		}
	}
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//FONCTIONS DE COMPARAISON DES DIFFERENTS TYPES DE DONNEES POUR qsort()
int columnToCompare = 0;
int compareTableLinesStr(const void* a, const void* b){
	char** stra = *(char***) a;
	char** strb = *(char***) b;
	int ret = strcasecmp(stra[columnToCompare],strb[columnToCompare]);
	return ret;
}

int compareTableLinesInt(const void* a, const void* b){
	char** stra = *(char***) a;
	char** strb = *(char***) b;
	int ret = atoi(stra[columnToCompare]) - atoi(strb[columnToCompare]);
	return ret;
}

int compareTableLinesFlo(const void* a, const void* b){
	char** stra = *(char***) a;
	char** strb = *(char***) b;
	int ret = ( atof(stra[columnToCompare]) > atof(strb[columnToCompare]) ) - ( atof(stra[columnToCompare]) < atof(strb[columnToCompare]) );
	return ret;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//Reorganise les lignes de la table pour qu'elles soient ordonnées selon la colonne donnée
void orderTableBy (Table T,char* str){
	for(int i=0;i<T.nbcolonne;i++){
		if(!strcmp(T.NomColonnes[i],str)){
			columnToCompare = i;
		}
	}
	int (*compareFunc) (const void* a,const void* b);
	switch(T.typelist[columnToCompare]){
		case typeINT:
			compareFunc = compareTableLinesInt;
		break;
		case typeFLO:
			compareFunc = compareTableLinesFlo;
		break;
		case typeSTR:
			compareFunc = compareTableLinesStr;
		break;
	}
	qsort(T.T,T.nbligne,sizeof(char*),compareFunc);
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//retourne la liste des colonnes d'une table sauf celles données
char** getSelectedColumnsNameExcept (Table T,char* selectionn){
	char** selection = str2_split(selectionn,',');
	char** selectede = malloc(sizeof(char*));
	int j = 1;
	bool nothingInTable = true;
	for(int i=0;i<T.nbcolonne;i++){
		bool copy = true;
		for(int i2=0;selection[i2];i2++){
			if(str_same(selection[i2],T.NomColonnes[i])){
				copy = false;
			}
		}
		if(copy){
			nothingInTable = false;
			selectede = realloc(selectede,sizeof(char*)*j);
			selectede[j-1] = malloc(sizeof(char)*strlen(T.NomColonnes[i]));
			strcpy(selectede[j-1],T.NomColonnes[i]);
			j++;
		}
	}
	if(nothingInTable){
		printf(TXT_RED);
		printf("You can't delete every column in the table !\n Use DROP instead \n");
		printf(TXT_END_COLOR);
		return T.NomColonnes;
	}
	selectede[j-1] = false;
	return selectede;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//fonction de split d'une chaine en tableau de chaines selon le delimiteur
char** str2_split(char* a_str, const char a_delim){
    return str_split(a_str,a_delim);
}

//remplit fcontents avec le contenu du texte d un fichier, retourne si l operation a reussi ou non
int getDb2Text (char* fcontents,char* filename){
	size_t nread;
	FILE* f = fopen(filename,"r");
	if( f!= NULL){
		do{
			nread = fread(fcontents,1,DBSIZE*sizeof(char),f);
		}
		while(nread > 0);
		int lastchar;
		for(lastchar=0;fcontents[lastchar] != '\0';lastchar++);
		fcontents[lastchar - 1] = '\0';
		fclose(f);
		return 0;
	}
	else{
		printf(TXT_RED);
		printf("ERROR : FILE \"%s\" NOT FOUND\n",filename);
		printf(TXT_END_COLOR);
		return 1;
	}
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//enleve toutes les occurences d un caractere dans une chaine
void removeChar(char *str, char garbage) {
    char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != garbage) dst++;
    }
    *dst = '\0';
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//explicite 
int compareChar (const void * a,const void * b){
	return *((char*)a)-*((char*)b);
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//ecris toutes les lignes de contentbyLine dans un fichier
void writeBdd (char* filename,char** contentByLine,int nbLine){
	FILE* f = fopen(filename,"w");
	if( f!= NULL){
		for(int i = 0;i<nbLine;i++){
			fprintf(f,"%s?",contentByLine[i]);
		}
		fclose(f);
	}
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//UNUSED marche pas
int sizeinarray (void* ptr){
	int i=0;
	for(;((char**)ptr)[i];i++);
	return i;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//ajoute une colonne dans une table et remplis les lignes existantes avec NULL pour la nouvelle colonne
Table enlargeTable (Table TS,char** ToAdd,int* type){
	Table TR;
	int nbtoadd = sizeinarray(ToAdd);
	TR.nbcolonne = TS.nbcolonne + nbtoadd;
	TR.nbligne = TS.nbligne;
	TR.NomColonnes = malloc(sizeof(char*)*TR.nbcolonne);
	int j = 0;
	for(int i=0;i<TR.nbcolonne;i++){
		if(i < TS.nbcolonne){
			TR.NomColonnes[i] = malloc(sizeof(char)*strlen(TS.NomColonnes[i]));
			strcpy(TR.NomColonnes[i],TS.NomColonnes[i]);
		}
		else{
			TR.NomColonnes[i] = malloc(sizeof(char)*strlen(ToAdd[j]));
			strcpy(TR.NomColonnes[i],ToAdd[j]);
			j++;
		}
	}
	TR.T = malloc(sizeof(char**)*TR.nbligne);
	for(int i=0;i<TS.nbligne;i++){
		TR.T[i] = malloc(sizeof(char*)*TR.nbcolonne);
		for(int i2=0;i2<TR.nbcolonne;i2++){
			if(i2 < TS.nbcolonne){
				(TR.T)[i][i2] = malloc(sizeof(char)*strlen((TS.T)[i][i2]));
				strcpy((TR.T)[i][i2],(TS.T)[i][i2]);
			}
			else{
				(TR.T)[i][i2] = malloc(sizeof(char)*5);
				strcpy((TR.T)[i][i2],"NULL");
			}
		}
	}
	int j2 = 0;
	TR.typelist = malloc(sizeof(int)*TR.nbcolonne);
	for(int i=0;i<TR.nbcolonne;i++){
		if(i<TS.nbcolonne){
			TR.typelist[i] = TS.typelist[i];
		}
		else{
			TR.typelist[i] = type[j2];
			j2++;
		}
	}
	return TR;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

//retourne le tabeau de chaine d entree avec une nouvelle ligne inserée
char** putLineAt (char **contentByLine,char* formatedLine,int line,int nbLine){
	char** result = malloc(sizeof(char*)*nbLine+1);
	int i2 = 0;
	for(int i=0;i<nbLine+1;i++){
		if(i == line){
			result[i] = malloc(sizeof(char)*strlen(formatedLine));
			strcpy(result[i],formatedLine);
		}
		else{
			result[i] = malloc(sizeof(char)*strlen(contentByLine[i2]));
			strcpy(result[i],contentByLine[i2]);
			i2++;
		}
	}
	return result;
}
//Cree par Kyllian MARIE en CIR2 A l'ISEN TOULON

void afficheBDD (Table T){ //Affiche la bdd
	int nbchar = 0;
	
	float charmax =  90./(T.nbcolonne); //Pour améliorer la visiblité on défini un nombre de caractères max par case
	
	int i,j,k;
	float charrest;
	int charspecial=0; //Contient le nombre de caractères spéciaux qui ont une taille de deux char (é,è...)
	
	//Présentation
	printf(TXT_BLUE);
	for(i=0;i<(T.nbcolonne)*(charmax+1);i++){
		printf("-");
		if(T.nbcolonne == 1){
			printf("-");
		}
		
	}
	
	//Présentation
	for(i=0;i<T.nbcolonne-1;i++){
		printf("-");
	}
	printf("\n");
	
	//Affichage des noms des colonnes
	printf("| ");
	for(i=0;i<T.nbcolonne;i++){
		charrest = (int)charmax;
		printf("%s",T.NomColonnes[i]);
		charrest=charrest - strlen(T.NomColonnes[i]);
		while(charrest != 0){
			printf(" ");
			charrest --;
		}
		printf("| ");
	}
	printf("\n");
	
	//Présentation
	for(i=0;i<(T.nbcolonne)*(charmax+1);i++){
		printf("-");
		if(T.nbcolonne == 1){
			printf("-");
		}
		
	}
	for(i=0;i<T.nbcolonne-1;i++){
		printf("-");
	}
	printf("\n");
	printf(TXT_CYAN);
	
	//Affichage du contenu de la bdd
	for(i=0;i<T.nbligne;i++){
		j = 0;
		k = 0;
		while(T.T[i][j][k] != '\0'){
			nbchar++;
			k++;
		}
		
		if(nbchar <= 90){ //Affichage de la bdd en prenant en compte la taille des chaine et en remplaçant les termes en trop par des "..."
			printf(TXT_WHITE);
			printf("| ");
			printf(TXT_CYAN);
			for(j=0;j<T.nbcolonne;j++){
				charspecial = 0;
				while(T.T[i][j][k] != '\0'){
					k++;
				if(T.T[i][j][k] == -61 || T.T[i][j][k] == -62){
					charspecial++;
					
				}
				}
				charrest = (int)charmax;
			 	if(strlen(T.T[i][j]) > (int)charmax){
					T.T[i][j][(int)charmax - 4] = '.';
					T.T[i][j][(int)charmax - 3] = '.';
					T.T[i][j][(int)charmax - 2] = '.';
					T.T[i][j][(int)charmax - 1] = '\0';
				}
				printf("%s",T.T[i][j]);
				
				charrest = charrest - strlen(T.T[i][j]);
				charrest = charrest + charspecial;
				
				while(charrest > 0){
					printf(" ");
					charrest --;
				}
				printf(TXT_WHITE);
				printf("| ");
				printf(TXT_CYAN);
			}
		}else{
			printf(TXT_WHITE);
			printf("| ");
			printf(TXT_CYAN);
			for(j=0;j<T.nbcolonne;j++){
				charspecial = 0;
				while(T.T[i][j][k] != '\0'){
					k++;
				if(T.T[i][j][k] == -61 || T.T[i][j][k] == -62){
					charspecial++;
					
				}
				}
				charrest = (int)charmax;
				if(strlen(T.T[i][j]) > (int)charmax){
					T.T[i][j][(int)charmax - 4] = '.';
					T.T[i][j][(int)charmax - 3] = '.';
					T.T[i][j][(int)charmax - 2] = '.';
					T.T[i][j][(int)charmax - 1] = '\0';
				}
				
				printf("%s",T.T[i][j]);
				charrest=charrest - strlen(T.T[i][j]);
				charrest = charrest + charspecial;
				while(charrest > 0){
					printf(" ");
					charrest --;
				}
				printf(TXT_WHITE);
				printf("| ");
				printf(TXT_CYAN);
			}
		}
		printf("\n");
	}
	printf(TXT_COLOR_END);
	
}

Table DeletefromBDD(Table T,char *col, char *condition){
	Table T2; //Table qui contiendra la premiere sans les lignes à DELETE
	T2.nbcolonne = T.nbcolonne;
	T2.nbligne = T.nbligne -1;
	T2.T = malloc(sizeof(char**)*T2.nbligne);
	for(int i=0;i<T2.nbligne;i++){
		T2.T[i]=malloc(sizeof(char*)*T2.nbcolonne);
	}
	
	T2.NomColonnes = T.NomColonnes;
	T2.typelist = T.typelist;
	int repere = 99;
	int oui = 0; //Il y a des lignes a DELETE ou non
	
	for(int i=0;i<T.nbcolonne;i++){
		if(!strcmp(col,T.NomColonnes[i])){ //On cherche la colonne a tester
			repere = i;
			
		}
	}
	
	int repereLigne;
	
	for(int i=0;i<T.nbligne;i++){
		if(!strcmp(condition,T.T[i][repere])){ //On cherche les lignes qui verifient les conditions
			oui = 1; //il y a des lignes a DELETE
			repereLigne = i;
		}
	}
	
	
	if(oui == 1){ //si il y a des lignes a DELETE 
		int cpt = 0;
		for(int i=0;cpt<T2.nbligne; i++){
			if(i != repereLigne){
				for(int j=0;j<T.nbcolonne;j++){ //On recopie la premiere table dans une autre sans les lignes à DELETE
					T2.T[cpt][j] = malloc(sizeof(char)*strlen(T.T[i][j])); 
					strcpy(T2.T[cpt][j],T.T[i][j]);	
				}
				cpt++;
			}
		}
		return T2; //On renvoie la table modifiée
	}
	printf(TXT_RED);
	printf("Pas de modif apres DELETE\n");
	printf(TXT_END_COLOR);
	
	return T; //On renvoie la table non modifiée
}

//Fonction qui supprime les données d'une bdd en gardant les noms des colonnes
void DeleteAllFromBDD(Table T,char *bddname){
	FILE *f;
	f = fopen(bddname,"w");
	fprintf(f,"<BDD-START>\n");
	
	
	int taille = 14;
	
	for(int i=0;i<T.nbcolonne;i++){
		taille = taille + strlen(T.NomColonnes[i]) + 1;
	}
	
	char *NomCol;
	NomCol = malloc(sizeof(char)*taille);
	strcpy(NomCol,"<DESC = \"");
	for(int i=0;i<T.nbcolonne;i++){
		strcat(NomCol,T.NomColonnes[i]);
		strcat(NomCol,",");
	}
	strcat(NomCol,"\" >\n");
	fprintf(f,"%s",NomCol);
	fprintf(f,"<BDD-END>");
	fclose(f);
}

//FONCTION DE LIBERATION DE LA MEMOIRE OCCUPEE PAR UNE TABLE
void libereTable (Table T){
	for(int i=0;i<T.nbcolonne;i++){
		betterFree((void**)&(T.NomColonnes[i]));
	}
	betterFree((void**)&(T.NomColonnes));
	betterFree((void**)&(T.typelist));
	for(int i=0;i<T.nbligne;i++){
		for(int i2=0;i2<T.nbcolonne;i2++){
			betterFree((void**)&(T.T[i][i2]));
		}
		betterFree((void**)&(T.T[i]));
	}
	betterFree((void**)&(T.T));
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//Fonction qui permet d'identifier les lignes correspondants aux conditions sur une colonne
where fctWHERE(Table T, char *col,char *condition, char *valeur){
	
	int repere=0; 
	int operateur;
	where w;
	w.lignes = NULL;
	w.nbligne = 0;
	
	
	for(int i=0;i<T.nbcolonne;i++){
		if(!strcmp(col,T.NomColonnes[i])){
			repere = i;
		}
	}

	//Identification de l'opérateur
	if(!strcmp(condition,">")){
		operateur = 1;
	}
	if(!strcmp(condition,"<")){
		operateur = 2;
	}
	if(!strcmp(condition,"=")){
		operateur = 3;
	}
	

	switch(operateur){ //Identification des lignes correspondant aux conditions 
		
		case 1 :
			for(int i=0;i<T.nbligne;i++){
				switch(T.typelist[repere]){
					case typeFLO :
						if((atof(T.T[i][repere]) > atof(valeur) ) - ( atof(T.T[i][repere]) < atof(valeur))>0){
							w.nbligne++;
						}
					break;
					case typeINT :
						if(atoi(T.T[i][repere]) > atoi(valeur)){
							w.nbligne++;
						}
					break;
					case typeSTR :
						if(strcmp(T.T[i][repere],valeur)>0){
							w.nbligne++;
						}
					break;
				}
			}
			
			w.lignes = malloc(sizeof(int)*T.nbligne);
			w.nbligne = 0;
			for(int i=0;i<T.nbligne;i++){
				switch(T.typelist[repere]){
					case typeFLO :
						if((atof(T.T[i][repere]) > atof(valeur) ) - ( atof(T.T[i][repere]) < atof(valeur))>0){
							w.lignes[w.nbligne] = i;
							w.nbligne++;
						}
					break;
					case typeINT :
						if(atoi(T.T[i][repere]) > atoi(valeur)){
							w.lignes[w.nbligne] = i;
							w.nbligne++;
						}
					break;
					case typeSTR :
						if(strcmp(T.T[i][repere],valeur)>0){
							w.lignes[w.nbligne] = i;
							w.nbligne++;
						}
					break;
				}
				
			}
			return w;
		break;
		
		case 2 :
			for(int i=0;i<T.nbligne;i++){
				switch(T.typelist[repere]){
					case typeFLO :
						if((atof(T.T[i][repere]) > atof(valeur) ) - ( atof(T.T[i][repere]) < atof(valeur))<0){
							w.nbligne++;
						}
					break;
					case typeINT :
						if(atoi(T.T[i][repere]) < atoi(valeur)){
							w.nbligne++;
						}
					break;
					case typeSTR :
						if(strcmp(T.T[i][repere],valeur)<0){
							w.nbligne++;
						}
					break;
				}
			}
			
			w.lignes = malloc(sizeof(int)*T.nbligne);
			w.nbligne = 0;
			for(int i=0;i<T.nbligne;i++){
				switch(T.typelist[repere]){
					case typeFLO :
						if((atof(T.T[i][repere]) > atof(valeur) ) - ( atof(T.T[i][repere]) < atof(valeur))<0){
							w.lignes[w.nbligne] = i;
							w.nbligne++;
						}
					break;
					case typeINT :
						if(atoi(T.T[i][repere]) < atoi(valeur)){
							w.lignes[w.nbligne] = i;
							w.nbligne++;
						}
					break;
					case typeSTR :
						if(strcmp(T.T[i][repere],valeur)<0){
							w.lignes[w.nbligne] = i;
							w.nbligne++;
						}
					break;
				}
			}
			return w;			
		break;
		
		case 3 :
			for(int i=0;i<T.nbligne;i++){
				if(strcmp(T.T[i][repere],valeur)==0){
					w.nbligne++;
				}
			}
			
			w.lignes = malloc(sizeof(int)*T.nbligne);
			w.nbligne = 0;
			for(int i=0;i<T.nbligne;i++){
				if(strcmp(T.T[i][repere],valeur)==0){
					w.lignes[w.nbligne] = i;
					w.nbligne++;
				}
			}
			return w;
		break;
	}
	w.lignes = realloc(w.lignes,sizeof(int)*w.nbligne);
	return w;
}

//Modifie certaines case de la bdd correspondant aux conditions
void fctUPDATE(Table T,where w,char *col){
	int k=0;
	int repere = 0;
	
	for(int i=0;i<T.nbcolonne;i++){
		if(!strcmp(w.colAmodif,T.NomColonnes[i])){
			repere = i;
		}
	}
	
	for(int i=0;i<T.nbligne;i++){
		if(k<=w.nbligne){
			if(i == w.lignes[k]){
				T.T[i][repere] = realloc(T.T[i][repere],sizeof(char)*strlen(col));
				strcpy(T.T[i][repere],col);
				k++;
			}
		}
	}
	
} 

//Creer une Table contenant les lignes correspondant aux conditions
Table selectWhere(Table T,where w){
	int k=0;
	int cpt=0;
	Table T2;
	T2.nbcolonne = T.nbcolonne;
	T2.nbligne = w.nbligne;
	T2.T = malloc(sizeof(char**)*T2.nbligne);
	for(int i=0;i<T2.nbligne;i++){
		T2.T[i]=malloc(sizeof(char*)*T2.nbcolonne);
	}
	T2.NomColonnes = T.NomColonnes;
	T2.typelist = T.typelist;
	
	for(int i=0;i<w.nbligne;i++){
		k=w.lignes[cpt];
		cpt++;
		for(int j=0;j<T2.nbcolonne;j++){
			T2.T[i][j]=malloc(sizeof(char)*strlen(T.T[k][j])+1);
			strcpy(T2.T[i][j],T.T[k][j]);
		}
	}
	
	return T2;
}

//Fonction principale gerant la commande de l utilisateur
Table databaseQuery(char* request){
	//conversion de la commande en tableau de chaines contenant chaque arguments
	char **argv = malloc(sizeof(char*));
	argv[0] = malloc(sizeof(char));
	int j=0;
	int j2=0;
	for(int i=0;i<strlen(request);i++){
		if(request[i] == ' '){
			argv[j] = realloc(argv[j],sizeof(char)*(j2+1));
			argv[j][j2] = '\0';
			
			j++;
			argv = realloc(argv,sizeof(char*)*(j+1));
			j2 = 0;

			argv[j] = malloc(sizeof(char));
		}
		else{
			argv[j] = realloc(argv[j],sizeof(char)*(j2+1));
			argv[j][j2] = request[i];
			j2++;
		}
		
	}
	argv[j] = realloc(argv[j],sizeof(char)*(j2+1));
	argv[j][j2] = '\0';
	int argc = j+1;
	// ------------------------------------------------------- //
	printf(TXT_RED);
	printf("Our-SQL\n");
	printf(TXT_END_COLOR);
	bool invalidRequest = true;
	getArgsValue(argc,argv,&args);
	//char *fcontents;
	char *dbname;
	Table T;
	Table T2;
	char **selected;
	char chemin[50];
	where w = (where){NULL,0,NULL};


	//SWITCH SUR TYPES DE COMMANDES PRINCIPALES UPDATE SELECT INSERT DELETE
	switch(args[1]){
		case SELECT:
			//SELECT [col1,col2.../ALL] FROM [dbname.ext] (WHERE [col] [=/\\>/\\<] [value]) (INNER_JOIN [db2.ext] ON [col]) (ORDER_BY [col]) ...
		
			dbname = argv[4];
			int curseur = 2;
			selected = getSelectedColumnsName(argv[2]);
			T = getTableFromFile(dbname);
			T = shrinkTable(T,selected);
			curseur = 5;
			while(curseur < argc){	// boucle tant qu'il reste des arguments a traiter
				char* ordercol = false;
				char* bddToJoin = false;
				char* columnToJoin = false;
				char* whereValue = false;
				char* whereColumn = false;
				char* whereComparator = false;
				if(str_same("ORDER_BY",argv[curseur])){
					ordercol = argv[curseur+1];
					curseur++;
				}
				if(str_same("INNER_JOIN",argv[curseur])){
					bddToJoin = argv[curseur+1];
					columnToJoin = argv[curseur+3];
					curseur+= 3;
				}
				if(str_same("WHERE",argv[curseur])){
					whereColumn = argv[curseur+1];
					whereComparator = argv[curseur+2];
					whereValue = argv[curseur+3];
					curseur+=3;
				}
				if(bddToJoin){
					T = joinTables(T,getTableFromFile(bddToJoin),columnToJoin);
				}
				if(ordercol){
					orderTableBy(T,ordercol);
				}
				if(whereColumn){
					where w;
					w.colAmodif = NULL;
					w = fctWHERE(T,whereColumn,whereComparator,whereValue);
					w.colAmodif = malloc(sizeof(char)*(strlen(whereColumn)+1));
					strcpy(w.colAmodif,whereColumn);
					T = selectWhere(T,w);
				}
				curseur++;	//pas de free sur les pointeurs char* declarés dans la boucle car ils pointent sur argv
			}
			libereWhere(w);
			return T;
			invalidRequest = false;
		break;
		
		case INSERT:
			//INSERT IN [dbname.ext] VALUES [value1,value2...]
			dbname = argv[3];
			char **values = str2_split(argv[5],',');
			T = getTableFromFile(dbname);
			T.nbligne++;
			T.T = realloc(T.T,sizeof(char**)*T.nbligne);
			T.T[T.nbligne-1] = malloc(sizeof(char*)*T.nbcolonne);
			for(int i=0;i<T.nbcolonne;i++){
				T.T[T.nbligne-1][i] = malloc(sizeof(char)*strlen(values[i])+1);
				strcpy(T.T[T.nbligne-1][i],values[i]);
			}
			char* innerTexxxxt = writeInnerTextFromTable(T);
			writeBdd(dbname,str2_split(innerTexxxxt,'?'),T.nbligne+3);
			invalidRequest = false;
			libereTable(T);
		break;
		
		case CREATE:;
			// CREATE bddx.txt WITH COLUMNS melles,Mickey,Daniel AS int,str,flo
			char **innerTXT = malloc(sizeof(char*)*3);
			for(int i=0;i<3;i++){
				innerTXT[i] = malloc(sizeof(char)*500);
			}
			sprintf(innerTXT[0],"<BDD-START>");
			sprintf(innerTXT[1],"<DESC = \"%s\" TYPE = \"%s\" >",argv[5],argv[7]);
			sprintf(innerTXT[2],"<BDD-END>");
			writeBdd(argv[2],innerTXT,3);
			invalidRequest = false;
		break;
		
		case ADD_COLUMNS:
			// ADD_COLUMNS truc,bidule,chose... IN bddx.txt AS int,float,str...
			dbname = argv[4];
			char** toAdd = getSelectedColumnsName(argv[2]);
			T = getTableFromFile(dbname);
			T2 = enlargeTable(T,toAdd,identifyTypeList(argv[6]));
			afficheBDD(T2);
			char* innerText = writeInnerTextFromTable(T2);
			writeBdd(dbname,str2_split(innerText,'\n'),T2.nbligne+3);
			invalidRequest = false;
			libereTable(T);
			libereTable(T2);			
		break;

		case DELETE_COLUMNS:
			//DELETE_COLUMNS truc,bidule IN bdd.txt
			dbname = argv[4];
			T = getTableFromFile(dbname);
			char** toKeep = getSelectedColumnsNameExcept(T,argv[2]);
			T2 = shrinkTable(T,toKeep);
			afficheBDD(T2);
			char* innerTexxt = writeInnerTextFromTable(T2);
			writeBdd(dbname,str2_split(innerTexxt,'\n'),T2.nbligne+3);
			invalidRequest = false;
			libereTable(T);
			libereTable(T2);
		break;

		case HELP:
			printf(TXT_YELLOW);
			printf("-_-_-_-_-_-_-_-| List Of Commands |-_-_-_-_-_-_-_-_-\n");
			printf("**\n| items in ( ) are optional \n| items in [ ] must be defined by the user \n| ... means you can put the last few terms in ( ) numerous times\n");
			printf("1 - SELECT [col1,col2.../ALL] FROM [dbname.ext] (WHERE [col] [=/\\>/\\<] [value]) (INNER_JOIN [db2.ext] ON [col]) (ORDER_BY [col]) ...\n");
			printf("2 - INSERT IN [dbname.ext] VALUES [value1,value2...]\n");
			printf("3 - CREATE [dbname.ext] WITH COLUMNS [col1,col2,...] AS [int/flo/str]\n");
			printf("4 - ADD_COLUMNS [colname1,colname2,...] IN [dbname.ext] AS [int/flo/str]\n");
			printf("5 - DELETE_COLUMNS [col1,col2] FROM [db.ext]\n");
			printf("6 - DELETE FROM [db.ext] WHERE [col] = [value]\n");
			printf("7 - DELETE_ALL FROM [db.ext]\n");
			printf("8 - DROP [db.ext]\n");
			printf("9 - UPDATE [col] = [value] IN [dbname.ext] WHERE [col] [=/\\>/\\<] [value]\n");
			invalidRequest = false;
		break;

		case DELETE:
			//DELETE FROM [db.ext] WHERE [col] = [value]
			dbname = argv[3];
			char *col = argv[5];
			char *condition = argv[7];
			T = getTableFromFile(dbname);
			Table Td = DeletefromBDD(T,col,condition);
			writeBdd(dbname,str2_split(writeInnerTextFromTable(Td),'\n'),Td.nbligne+3);
			invalidRequest = false;
			libereTable(T);
		break;
		
		case DELETE_ALL :
			//DELETE_ALL FROM [db.ext] 
			dbname = argv[3];
			T = getTableFromFile(dbname);
			DeleteAllFromBDD(T,dbname);
			invalidRequest = false;
			libereTable(T);
		break;
		
		case DROP :
			//DROP [db.ext]
			strcpy(chemin,"rm ");
			strcat(chemin,argv[2]);
			system(chemin);		
			invalidRequest = false;
		break;
		
		case UPDATE :
			//UPDATE [col] = [value] IN [dbname.ext] WHERE [col] [=/\\>/\\<] [value]
			dbname = argv[6];
			T = getTableFromFile(dbname);
			w = fctWHERE(T,argv[8],argv[9],argv[10]);
			w.colAmodif = malloc(sizeof(char)*strlen(argv[2]));
			strcpy(w.colAmodif,argv[2]);
			fctUPDATE(T,w,argv[4]);
			writeBdd(dbname,str2_split(writeInnerTextFromTable(T),'?'),T.nbligne+3);
			invalidRequest = false;
			libereTable(T);
			libereWhere(w);
		break;

	}
	if(invalidRequest){
		printf(TXT_RED);
		printf("ERROR : INVALID ARGUMENTS : ");
		printf(TXT_YELLOW);
		printf("\"%s\" ",argv[1]);
		for(int i=2;i<argc;i++){
			printf(TXT_RED);
			printf("AND ");
			printf(TXT_YELLOW);
			printf("\"%s\" ",argv[i]);
		}
		printf("\n");
		printf(TXT_END_COLOR);
	}
	// ------------------------------------------------------- //
	Table TNULL = (Table){0,0,NULL,NULL,NULL};
	return TNULL;
}
//cree par kyllian marie et gaian dourville 

//Libere la mémoire allouée à la structure where
void libereWhere(where w){
	if(w.lignes != NULL){
		free(w.lignes);
	}
}