#ifndef SDLGLUTILS_H
#define SDLGLUTILS_H

#include <GL/gl.h>
#include <SDL/SDL.h>

#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 0x812F
#endif

GLuint loadTexture(const char * filename,bool useMipMap);
int takeScreenshot(const char * filename);
void drawAxis(double scale);
int initFullScreen(unsigned int * width,unsigned int * height);
int XPMFromImage(const char * imagefile, const char * XPMfile);
SDL_Cursor * cursorFromXPM(const char * xpm[]);

#endif


typedef struct Image{
	int width;
	int height;
	float zoom;
	float static_width;
	float static_height;
	GLint donneesRGB;
}Image;

Image* new_Image (char* filename,bool antialiasing);
void ecrisImageTexture (Image* I,int x,int y);
void ecrisImageTextureCustom (Image* I,int x,int y,int width,int height);
void fitImageToRectangle (Image* img,int max_width,int max_height);