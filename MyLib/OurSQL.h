// DEFINES

#define DBSIZE 6000
#define SELECT 0
#define FROM 1
#define ALL 2
#define WHERE 3
#define ORDER_BY 4
#define INSERT 5
#define VALUES 6
#define CREATE 7
#define ADD_COLUMNS 8
#define DELETE 9
#define DELETE_ALL 10
#define DROP 11
#define UPDATE 12
#define DELETE_COLUMNS 21
#define HELP 66
#define INVALID 99

#define ALL_SELECTED NULL

#define TXT_BLACK "\033[1;30m"
#define TXT_RED "\033[1;31m"
#define TXT_GREEN "\033[1;32m"
#define TXT_YELLOW "\033[1;33m"
#define TXT_BLUE "\033[1;34m"
#define TXT_MAGENTA "\033[1;35m"
#define TXT_CYAN "\033[1;36m"
#define TXT_WHITE "\033[1;37m"
#define TXT_END_COLOR "\033[0m"

#define typeINT 0
#define typeFLO 1
#define typeSTR 2


// TYPEDEFS

int* args;

typedef struct Table{
	int nbcolonne;
	int nbligne;
	char** NomColonnes;
	char*** T;
	int* typelist;
}Table;

typedef struct where{
	int *lignes;
	int nbligne;
	char *colAmodif;
}where;

//FONCTIONS

char* writeInnerTextFromTable(Table T);
char** getSelectedColumnsName (char* selection);
bool str_same (char* a,char* b);
char** putLineAt (char **contentByLine,char* formatedLine,int line,int nbLine);
void getArgsValue (int argc,char* argv[],int** Args);
char** str_split(char* a_str, const char a_delim);
int getDbText (char* fcontents,char* filename);
void removeChar(char *str, char garbage);
int compareChar (const void * a,const void * b);
void writeBdd (char* filename,char** contentByLine,int nbLine);
Table shrinkTable (Table TS,char** selected);
Table getTableFromFile (char* dbname);
void afficheTable (Table T);
bool selectedColumnsAll (char* selection);
void afficheBDD (Table T);
Table DeletefromBDD(Table T,char *col, char *condition);
void DeleteAllFromBDD(Table T,char *bddname);
where fctWHERE(Table T, char *col,char *condition, char *valeur);
void fctUPDATE(Table T,where w,char *col);
Table selectWhere(Table T,where w);
Table enlargeTable (Table TS,char** ToAdd,int* type);
char** getSelectedColumnsNameExcept (Table T,char* selectionn);
int compareTableLines(const void* a, const void* b);
void orderTableBy (Table T,char* str);
Table joinTables (Table T1, Table T2,char* column);
Table databaseQuery(char* request);
int* identifyTypeList (char* typelist);
void libereTable (Table T);
void libereWhere(where w);