#ifdef _WIN32
	#include <windows.h>
#endif

#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "../GfxLib/GfxLib.h"
#include "../GfxLib/BmpLib.h"
#include "../GfxLib/ESLib.h"
#include "../MyLib/KyllianToolsLib.h"
#include "../MyLib/KToolsLibForGfxLib.h"
#include "../GfxLib/commonIO.h"
#include "../MyLib/consoletools.h"
#include "geometry.h"
#include "Cwing.h"

// coms dans le .h

// FONCTIONS

void setFloatModeSlider(Slider* s){
	s->float_mode = true;
}

void setIntModeSlider(Slider* s){
	s->float_mode = false;
}

void afficheSlider(Slider *s,int x,int y,int width){
	s->x = x;
	s->y = y;
	s->width = width;
	couleurCouranteRGB(s->color);
	rectangle(x-2,y+5,x,y-5);
	rectangle(x,y+1,x+width,y-1);
	rectangle(x+width+2,y+5,x+width,y-5);
	int 
	x1 = (int) ((s->value- s->min)*1. / fabs(s->max-s->min)*1.*width) + x - 5,
	x2 = x1 + 10,
	x3 = x1 + 5,
	y1 = y + 13,
	y2 = y + 13,
	y3 = y + 3;
	triangle(x1,y1,x2,y2,x3,y3);
	epaisseurDeTrait(1);
	if(s->float_mode){
		printGfx(x3-tailleChaine(str_format("%.2f",s->value),12)/2,y+15,12,"%.2f",s->value);
	}
	else{
		printGfx(x3-tailleChaine(str_format("%d",(int)s->value),12)/2,y+15,12,"%d",(int)s->value);
	}
}

void onChangeSlider(Slider* s,void (*callback) (void)){
	s->callback = callback;
}

void updateSlider (Slider *s){
	if(mouseState == MOUSE_LEFT_DOWN){
		if(!s->selected){
			if(pointIsInRectangle(AS,OS,s->x,s->y-5,s->width,10)){
				s->selected = true;
			}
		}
		if(s->selected){
			if(AS >= s->x && AS <= s->x + s->width){
				s->value = (AS - s->x)*1. / s->width * fabs(s->max-s->min) + s->min;
				if(s->callback != NULL)
					s->callback();
			}
		}
	}
	if(mouseState == MOUSE_LEFT_UP){
		s->selected = false;
	}
}

Slider new_Slider (double min,double max,double default_value){
	return (Slider) {0,0,0,min,max,default_value,(RGB){0,0,0},false,false,NULL,setFloatModeSlider,setIntModeSlider,afficheSlider,onChangeSlider,updateSlider};
}


Mover new_Mover (Point** trajectoire,bool repeat,float tps,bool reverse_loop,bool loop){
	Mover M;
	M.activated = false;
	M.trajectoire = trajectoire;
	M.repeat = repeat;
	M.tps = tps;
	M.reverse_loop = reverse_loop;
	M.reverse = false;
	M.loop = loop;
	M.elapsed_time = 0;
	M.current_pt = 0;
	return M;
}

void activateMover (Mover* M){
	M->activated = true;
	M->t0 = tempsReel();
}

void assignToMover (Mover* M,int* x,int* y){
	M->x = x;
	M->y = y;
}

void updateMover (Mover* M){
	if(M->activated){
		M->elapsed_time = tempsReel() - M->t0;
		bool finished_loop = M->elapsed_time >= M->tps;
		if(finished_loop){
			M->elapsed_time = M->tps;
			M->activated = false;
		}
		int index;
		if(M->reverse)
			index = (count(M->trajectoire)-2) * (1 - 1.*(M->elapsed_time/M->tps));
		
		else 
			index = 1.*(M->elapsed_time/M->tps)*(count(M->trajectoire)-2);
		*(M->x) = M->trajectoire[index]->x;
		*(M->y) = M->trajectoire[index]->y;
		if(M->loop && finished_loop){
			activateMover(M);
			if(M->reverse_loop)
				M->reverse = !M->reverse;
		}
	}
}

void fitTextToRectangle (char* text,int x,int y,int width,int height){
	int size;
	for(size=1;tailleChaine(text,size)< width;size++);
	if(size >= height){
		size = height - 1;
	}
	size--;
	int str_x = x + width/2 - tailleChaine(text,size)/2;
	int str_y = y + height/2 - size/2;
	printGfx(str_x,str_y,size,text);
}

int indexOfOptionDropMenu (DropMenu* DM,char* opt){
	int index = -1;
	for(int i=0;i<count(DM->option)-1;i++){
		if(!strcmp(DM->option[i],opt)){
			index = i;
			break;
		}
	}
	return index;
}

void deleteAllFromDropMenu (DropMenu* DM){
	if(DM->option != NULL){
		for(int i=0;i<count(DM->option)-1;i++){
			DM->option[i]= NULL;
		}
	}
	DM->selected_option = 0;
}

void deleteFromDropMenu (DropMenu* DM,char* opt){
	int index = DM->indexOf(DM,opt);
	if(index == -1){
		printerr("ERROR : could not delete option '%s' from DropMenu : 404 option not found (t'es un PD)",opt);
	}
	else{
		char** newopt = malloc(sizeof(char*)*(count(DM->option)-1));
		int lastindex = count(DM->option)-2;
		int cpt = 0;
		for(int i=0;DM->option[i];i++){
			if(i != index){
				newopt[cpt] = DM->option[i];
				cpt++;
			}
		}
		betterFree(&(DM->option));
		newopt[lastindex] = NULL;
		DM->option = newopt;
	}
}

void addToDropMenu (DropMenu* DM,char* opt){
	int nboption = count(DM->option) - 1;
	printdebug("here");
	DM->option = realloc(DM->option,sizeof(char*)*(nboption+2));
	printdebug("here");
	DM->option[nboption] = opt;
	printdebug("here");
	DM->option[nboption+1] = NULL;
}

void afficheDropMenu (DropMenu* DM,int x,int y){
	epaisseurDeTrait(1);
	DM->x = x;
	DM->y = y;
	if(DM->option[0] == NULL){
		couleurCourante(DM->color.r,DM->color.g,DM->color.b);
		if(DM->poly == NULL){
			rectangle(x,y,x+DM->width,y+DM->height);
		}
		else{
			dessinePolygone(*(DM->poly),x,y);
		}
		couleurCourante(DM->text_color.r,DM->text_color.g,DM->text_color.b);
	}
	else{
		couleurCourante(DM->color.r,DM->color.g,DM->color.b);
		if(DM->poly == NULL){
			rectangle(x,y,x+DM->width,y+DM->height);
		}
		else{
			dessinePolygone(*(DM->poly),x,y);
		}
		couleurCourante(DM->text_color.r,DM->text_color.g,DM->text_color.b);
		fitTextToRectangle(DM->option[DM->index_selected_option],x,y+3,DM->width,DM->height-3);
		int tsize = 9;
		if(count(DM->option)-1 > 1){
			triangle(
				DM->width + x - 20			, y + DM->height/2 + tsize/2,
				DM->width + x - 20 + tsize/2	, y + DM->height/2 - tsize/2,
				DM->width + x - 20 + tsize	, y + DM->height/2 + tsize/2
			);
		}
		//printGfx();
		if(DM->isOpened){
			int i = 0;
			int nbopt = count(DM->option) - 1;
			for(int i2=0;i2<nbopt;i2++){
				if(i2 != DM->index_selected_option){
					int x1 = x;
					int x2 = x + DM->width;
					int y1 = y - DM->height*(i+1);
					int y2 = y1 + DM->height;
					if(i2 == DM->index_option_hovered)
						couleurCouranteRGB(DM->hover_color);
					else
						couleurCouranteRGB(DM->color);
					rectangle(x1,y1,x2,y2);
					if(i2 == DM->index_option_hovered)
						couleurCouranteRGB(DM->hover_text_color);
					else
						couleurCouranteRGB(DM->text_color);
					fitTextToRectangle(DM->option[i2],x1,y1+3,x2-x1,y2-y1-3);
					i++;
				}
			}
		}
	}
}

void DropMenuRoundBorders(DropMenu* DM,float factor){
	DM->poly = malloc(sizeof(Polygone));
	*(DM->poly) = initPolygone(DM->width,DM->height,factor,60);
}

void dropMenuUpdate (DropMenu* DM){
	DM->index_option_hovered = -1;
	bool gereClic = handleSouris && mouseState == MOUSE_LEFT_UP;
	if(DM->isOpened == false){
		if(pointIsInRectangle(AS,OS,DM->x,DM->y,DM->width,DM->height)){
			//glutSetCursor(GLUT_CURSOR_HELP);
			if(gereClic){
				DM->isOpened = true;
				handleSouris = false;
			}
		}
	}
	else{
		int i = 0;
		int nbopt = count(DM->option) - 1;
		for(int i2=0;i2<nbopt;i2++){
			if(i2 != DM->index_selected_option){
				int x = DM->x;
				int width = DM->width;
				int y = DM->y - DM->height*(i+1);
				int height = DM->height;
				if(pointIsInRectangle(AS,OS,x,y,width,height)){
					DM->index_option_hovered = i2;
					if(gereClic){
						DM->index_selected_option = i2;
						DM->selected_option = DM->option[i2];
						handleSouris = false;
						if(DM->event != NULL){
							DM->event();
						}
						break;
					}
				}
				i++;
			}
		}
		if(gereClic)
			DM->isOpened = false;
	}
}

DropMenu new_DropMenu (int width,int height){
	DropMenu ret;
	ret.index_option_hovered = -1;
	ret.x=0;
	ret.y=0;
	ret.color = (RGB){255,255,255};
	ret.text_color = (RGB){60,60,60};
	ret.hover_color = (RGB){60,120,255};
	ret.hover_text_color = (RGB){255,255,255};
	ret.width = width;
	ret.height = height;
    ret.option = malloc(sizeof(char*));
	ret.option[0] = NULL;
	ret.selected_option = NULL;
	ret.index_selected_option = 0;
	ret.isOpened = false;
	ret.poly = NULL;
	ret.add = addToDropMenu;
	ret.indexOf = indexOfOptionDropMenu;
	ret.delete = deleteFromDropMenu;
	ret.update = dropMenuUpdate;
	ret.show = afficheDropMenu;
	ret.roundBorders = DropMenuRoundBorders;
	ret.deleteAll = deleteAllFromDropMenu;
	ret.event = NULL;
	return ret;
}

void afficheClickMenu (ClickMenu* C,int x,int y){
	if(C->is_shown){
		couleurCourante(100,100,100);
		int width = C->option[0].width, height = C->option[0].height;
		rectangle(x-1,y+1,x+width+1,y-height*C->nboption-1);
		for(int i=0;i<C->nboption;i++){
			epaisseurDeTrait(1);
			afficheBouton(x,y-(i+1)*height,&(C->option[i]));
		}
		for(int i=0;i<C->nbseparateurs;i++){
			couleurCourante(0,0,0);
			rectangle(x,y-(C->separateurs[i])*height,x+width,y-(C->separateurs[i])*height - 2);
		}
	}
}

void executeActionClickMenu (ClickMenu* C){
	for(int i=0;i<C->nboption;i++){
		executeActionBouton(C->option[i]);
	}
}

ClickMenu new_ClickMenu (bouton* option,int* separateurs,int nboption,int nbseparateurs){
	return (ClickMenu) {option,separateurs,nboption,nbseparateurs,false,afficheClickMenu,executeActionClickMenu};
}

textField initTextField(int xx,int yy,int width,int height,RGB couleur,char* chaine){
	textField B;
	B.width = width;
	B.height = height;
	B.couleur = couleur;
	B.skin = NULL;
	B.texte = malloc(sizeof(char)*100);
	B.tailleTexte = height*0.7;
	B.editing = false;
	strcpy(B.texte,chaine);
	B.x = xx;
	B.poly = NULL;
	B.y = yy;
	return B;
}

void afficheTextField(textField *B,int x,int y){
	B->x = x;
	B->y = y;
	if(B->skin == NULL){
		couleurCourante(0,0,0);
		epaisseurDeTrait(2);
		if(B->poly == NULL)
			rectangle(x,y,x+B->width,y+B->height);
		if(B->editing){
			couleurCourante(B->couleur.r + 127,B->couleur.g+127,B->couleur.b+127);
		}
		else{
			couleurCourante(B->couleur.r,B->couleur.g,B->couleur.b);
		}
		if(B->poly == NULL)
			rectangle(x+2,y+2,x+B->width-2,y+B->height-2);
		else 
			dessinePolygone(*(B->poly),x,y);
		couleurCourante(0,0,0);
		afficheChaine(B->texte,B->tailleTexte,x+B->width/2-tailleChaine(B->texte,B->tailleTexte)/2,y+B->height/2-B->tailleTexte/2);
	}
	if(B->skin != NULL){
		
	}
	if(tailleChaine(B->texte,B->tailleTexte) > B->width*0.9){
		int i;
		for(i= B->height*0.7;tailleChaine(B->texte,i) > B->width*0.9;i--){}
		B->tailleTexte = i;
	}
	if(tailleChaine(B->texte,B->tailleTexte) < B->width*0.9 && B->tailleTexte < B->height*0.7){
		int i;
		for(i= B->height*0.7;tailleChaine(B->texte,i) > B->width*0.9;i--){}
		B->tailleTexte = i;
	}
	/*if(B->showCursor && B->editing){
		x1cursor = B->x + B->width/2 + tailleChaine(B->texte,B->tailleTexte)/2 -x;
		y1cursor = B->y + B->height/2 - B->tailleTexte/2 -y - 5;
		x2cursor = B->x + B->width/2 + tailleChaine(B->texte,B->tailleTexte)/2 + 10-x;
		y2cursor = B->y + B->height/2 - B->tailleTexte/2 + B->tailleTexte-y + 5;
	}*/
}

void textFieldRoundBorders (textField *T,float coef){
	T->poly = malloc(sizeof(Polygone));
	*(T->poly) = initPolygone(T->width,T->height,coef,65);
}

void textFieldClickListener (textField *B){
	if(handleSouris){
		if (mouseState == MOUSE_LEFT_UP){
			if(AS > B->x && AS < B->x+B->width && OS> B->y && OS < B->y+B->height){
				B->editing = true;
			}
			else{
				B->editing = false;
			}
		}
	}
	if(B->editing){
		for(int i=0;i<255;i++){
			ifkeyup(i){
				switch(i){
					case 13:
						B->editing = false;
					break;
					case 8:
						B->texte[strlen(B->texte)-1] = '\0';
						B->texte[strlen(B->texte)] = '\0';
					break;
					default:;
						char stra[2];
						stra[0] = i;
						stra[1] = '\0';
						strcat(B->texte,stra);
					break;
				}
			}
		}
	}
}

//BOUTONS


void addEventListener (bouton *B,char* listener,void (*event) (void)){
	B->event = event;
	strcpy(B->listener,listener);
}

bouton initBouton(int width,int height,char* texte,RGB couleur){
	bouton B;
	B.width = width;
	B.height = height;
	B.couleur = couleur;
	B.skin = NULL;
	B.texte = texte;
	B.show = true;
	int i;
	for(i= height*0.7;tailleChaine(B.texte,i) > width*0.9;i--){}
	B.tailleTexte = i;
	B.addEventListener = addEventListener;
	B.hover = false;
	B.poly = NULL;
	return B;
}

bouton initBoutonFromImg(char* chemin,RGB fondVert){
	bouton B;
	B.skin = lisBMPRGB(chemin);
	B.couleur = fondVert;
	B.width = B.skin->largeurImage;
	B.height = B.skin->hauteurImage;
	B.addEventListener = addEventListener;
	return B;
}

void roundBorders (bouton* B,float coef){
	DEBUGSTART;
	B->poly = malloc(sizeof(Polygone));
	*(B->poly) = initPolygone(B->width,B->height,coef,65);
	DEBUGEND;
}

void afficheBouton(int x,int y,bouton *B){
	if(B->show){
		if(B->skin == NULL){
			if(pointIsInRectangle(AS,OS,B->x,B->y,B->width,B->height)){
				couleurCourante(B->couleur.r + 100,B->couleur.g + 100,B->couleur.b + 100);
			}
			else{
				couleurCourante(B->couleur.r,B->couleur.g,B->couleur.b);
			}
			if(B->poly == NULL){
				rectangle(x,y,x+B->width,y+B->height);
			}
			else{
				dessinePolygone(*(B->poly),x,y);
			}
			couleurCourante(0,0,0);
			afficheChaine(B->texte,B->tailleTexte,x+B->width/2-tailleChaine(B->texte,B->tailleTexte)/2,y+B->height/2-B->tailleTexte/2);
		}
		if(B->skin != NULL){
			ecrisImageSFShort(B->skin,x,y,B->couleur);
		}
		B->x = x;
		B->y = y;
	}
}

void executeActionBouton (bouton B){
	if(B.show){
		if(B.listener[0] == 'c' && B.listener[1] == 'l' && B.listener[2] == 'i'){
			if(handleSouris){
				if (mouseState == MOUSE_LEFT_UP){
					if(AS > B.x && AS < B.x+B.width && OS> B.y && OS < B.y+B.height){
						B.event();
						handleSouris = false;
					}
				}
			}
		}
	}
}

RGB rainbowRGB (int x,int y,int width,int height){
	int r=0,g=0,b=0;
	float a = 255/(width/6);
	int w = width;

	if(x >= 0 && x < 1*w/6){
		r = 255;
		b = 0;
		g = a*x - 0;
	}
	if(x >= 1*w/6 && x < 2*w/6){
		g = 255;
		b = 0;
		r = -a*x + 2*255;
	}
	if(x >= 2*w/6 && x < 3*w/6){
		g = 255;
		r = 0;
		b = a*x - 2*255;
	}
	if(x >= 3*w/6 && x < 4*w/6){
		b = 255;
		r = 0;
		g = -a*x + 4*255;
	}
	if(x >= 4*w/6 && x < 5*w/6){
		b = 255;
		g = 0;
		r = a*x - 4*255;
	}
	if(x >= 5*w/6 && x <= w){
		r = 255;
		g = 0;
		b = -a*x + 6*255;
	}

	//correction des cas limites du aux float
	if(r < 0){
		r = 0;
	}
	if(g < 0){
		g = 0;
	}
	if(b < 0){
		b = 0;
	}
	if(r > 255){
		r = 255;
	}
	if(g > 255){
		g = 255;
	}
	if(b > 255){
		b = 255;
	}

	RGB couleur = (RGB) {r,g,b};
	if(y<19*height/20){
		int halfheight = (height-height/20)/2;
		if(y < halfheight){
			couleur.r = couleur.r*y/halfheight;
			couleur.g = couleur.g*y/halfheight;
			couleur.b = couleur.b*y/halfheight;
		}
		if(y > halfheight){
			couleur.r = couleur.r + (255-couleur.r)*(y-halfheight)/halfheight;
			couleur.g = couleur.g + (255-couleur.g)*(y-halfheight)/halfheight;
			couleur.b = couleur.b + (255-couleur.b)*(y-halfheight)/halfheight;
		}
	}
	return couleur;
}

DonneesImageRGB* new_Gradient (int width,int height){
	DonneesImageRGB* gradient = malloc(sizeof(DonneesImageRGB));
	gradient->hauteurImage = height;
	gradient->largeurImage = width;
	gradient->donneesRGB = malloc(sizeof(unsigned char)*width*height*3);
	for(int i = 0;i< height*width*3;i+=3){
		int x = i/3%width;
		int y = i/3/width;
		RGB color = rainbowRGB(x,y,width,height);
		gradient->donneesRGB[i+2] = color.r;
		gradient->donneesRGB[i+1] = color.g;
		gradient->donneesRGB[i] = color.b;
	}
	return gradient;
}

RGB** new_GradientIndexer (int width,int height){
	RGB** gradient = malloc(sizeof(RGB*)*width);
	for(int i=0;i<width;i++){
		gradient[i] = malloc(sizeof(RGB)*height);
	}
	for(int i = 0;i< width;i++){
		for(int i2=0;i2<height;i2++){
			gradient[i][i2] = rainbowRGB(i,i2,width,height);
		}
	}
	return gradient;
}

void updateColorPicker (ColorPicker* pick){
	executeActionBouton(pick->validate);
	double r = pick->r_slider.value;
	double g = pick->g_slider.value;
	double b = pick->b_slider.value;
	pick->r_slider.update(&(pick->r_slider));
	pick->g_slider.update(&(pick->g_slider));
	pick->b_slider.update(&(pick->b_slider));
	if(r != pick->r_slider.value || g != pick->g_slider.value || b != pick->b_slider.value){
		pick->xselec = -1;
		pick->selection = (RGB) {pick->r_slider.value,pick->g_slider.value,pick->b_slider.value};
	}
	if(mouseState == MOUSE_LEFT_DOWN){
		if(pointIsInRectangle(AS,OS,pick->x,pick->y,pick->width-1,pick->height-1)){
			pick->xselec = AS-pick->x;
			pick->yselec = OS-pick->y;
			pick->selected = true;
			pick->selection = pick->indexer[AS-pick->x][OS-pick->y];
		}
	}
	pick->r_slider.value = pick->selection.r;
	pick->g_slider.value = pick->selection.g;
	pick->b_slider.value = pick->selection.b;
	handleSouris = false;
}

void showColorPicker (ColorPicker* pick,int x,int y){
	pick->x = x;
	pick->y = y;
	couleurCouranteRGB(pick->selection);
	rectangle(pick->x-10,pick->y-10,pick->x+pick->width+10,pick->y+pick->height+10);
	ecrisImageShort(pick->gradient,x,y);
	if(pick->xselec != -1){
		int size = 10;
		int width = 2;
		int inter = 5;
		couleurCourante(
			255 - pick->selection.r,
			255 - pick->selection.g,
			255 - pick->selection.b
		);
		rectangle(pick->xselec+pick->x-size-inter,pick->yselec+pick->y - width/2,pick->xselec+pick->x-inter,pick->yselec+pick->y + width/2);
		rectangle(pick->xselec+pick->x+inter,pick->yselec+pick->y - width/2,pick->xselec+pick->x+inter+size,pick->yselec+pick->y + width/2);

		rectangle(pick->xselec+pick->x -width/2,pick->yselec+pick->y-size-inter,pick->xselec+pick->x+width/2,pick->yselec+pick->y-inter);
		rectangle(pick->xselec+pick->x -width/2,pick->yselec+pick->y+inter,pick->xselec+pick->x+width/2,pick->yselec+pick->y+size+inter);
	}
	pick->r_slider.show(&(pick->r_slider),pick->x+pick->width + 20,pick->y + 4*pick->height/6,pick->width/4);
	pick->g_slider.show(&(pick->g_slider),pick->x+pick->width + 20,pick->y + 3*pick->height/6,pick->width/4);
	pick->b_slider.show(&(pick->b_slider),pick->x+pick->width + 20,pick->y + 2*pick->height/6,pick->width/4);
	couleurCouranteRGB(pick->selection);
	rectangle(pick->x+pick->width + 20 + pick->width/8 - pick->height/12,pick->y + pick->height - pick->height/6,pick->x+pick->width + 20 + pick->width/8 - pick->height/12 + pick->height/6,pick->y + pick->height);
	afficheBouton(pick->x+pick->width + 20 + pick->width/8 - pick->validate.width/2,pick->y + pick->height/6,&(pick->validate));
}

void setCallbackColorPicker (ColorPicker* pick,void (*function) (void)){
	pick->validate.addEventListener(&(pick->validate),"click",function);
}

ColorPicker new_ColorPicker (int width,int height){
	ColorPicker ret;
	ret.xselec = -1;
	ret.yselec = -1;
	ret.selection = (RGB){0,0,0};
	ret.selected = false;
	ret.x = 0;
	ret.y = 0;
	ret.width = width;
	ret.height = height;
	ret.gradient = new_Gradient(width,height);
	ret.indexer = new_GradientIndexer(width,height);
	ret.show = showColorPicker;
	ret.update = updateColorPicker;
	ret.setCallback = setCallbackColorPicker;
	ret.r_slider = new_Slider(0,255,0);
	ret.g_slider = new_Slider(0,255,0);
	ret.b_slider = new_Slider(0,255,0);
	ret.validate = initBouton(70,23,"Valider",(RGB){190,190,190});
	roundBorders(&(ret.validate),0.2);
	return ret;
}

void afficheCounter (Counter* c,int x,int y,int width,int height){
	c->x = x;
	c->y = y;
	c->width = width;
	c->height = height;
	couleurCourante(c->color.r + 20,c->color.g + 20,c->color.b + 20);
	rectangle(x,y,x+width,y+height*0.2);
	rectangle(x,y+height*0.8,x+width,y+height);
	couleurCouranteRGB(c->color);
	rectangle(x,y+height*0.2,x+width,y+height*0.8);
	couleurCourante(0,0,0);
	epaisseurDeTrait(2);
	fitTextToRectangle("v",x,y,width,height*0.2);
	fitTextToRectangle("^",x,y+height*0.8,width,height*0.2);
	fitTextToRectangle(str_format("%d",c->value),x,y+height*0.2,width,height*0.6);
}

void updateCounter (Counter* c){
	if(handleSouris && mouseState == MOUSE_LEFT_UP){
		if(pointIsInRectangle(AS,OS,c->x,c->y,c->width,c->height*0.2)){
			if(c->value - 1 >= c->valmin){
				c->value--;
				if(c->eventListener != NULL){
					c->eventListener();
				}
			}
			handleSouris = false;
		}
		if(pointIsInRectangle(AS,OS,c->x,c->y + c->height*0.8,c->width,c->height*0.2)){
			if(c->value + 1 <= c->valmax){
				c->value++;
				if(c->eventListener != NULL){
					c->eventListener();
				}
			}
			handleSouris = false;
		}
	}
}

Counter new_Counter (int valmin,int val,int valmax){
	Counter ret;
	ret.x=0;
	ret.y = 0;
	ret.width = 0;
	ret.height = 0;
	ret.value = val;
	ret.color = (RGB){160,160,160};
	ret.valmax = valmax;
	ret.valmin = valmin;
	ret.eventListener = NULL;
	return ret;
}