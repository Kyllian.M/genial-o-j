#ifdef _WIN32
	#include <windows.h>
#endif
#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "../GfxLib/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "../GfxLib/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "../GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <string.h>
#include "consoletools.h"
#include "../GfxLib/commonIO.h"
#include "KyllianToolsLib.h"
#include "KToolsLibForGfxLib.h"
#include "sdlglutils.h"
#include "Timer.h"
#include "images.h"
#include "geometry.h"
#include "imageCropper.h"

void updateImageCropper (ImageCropper* crop){
	if(crop->s_x < 0)
		crop->s_x = 0;
	if(crop->s_x > crop->src->static_width)
		crop->s_x = crop->src->static_width - 3;
	if(crop->s_w < 3)
		crop->s_w = 3;
	if(crop->s_w + crop->s_x > crop->src->static_width)
		crop->s_w = crop->src->static_width - crop->s_x;
	int x = crop->x;
	int y = crop->y;
	int s_x = crop->s_x;
	int s_y = crop->s_y;
	int s_w = crop->s_w;
	int s_h = crop->s_h;
	border_state up = crop->up;
	border_state down = crop->down;
	border_state left = crop->left;
	border_state right = crop->right;
	bool selected = crop->selected;
	if(crop->selected){
		crop->s_x = AS - crop->clicked.x;
		crop->s_y = OS - crop->clicked.y;
		if(crop->s_x < 0){
			crop->s_x = 0;
		}
		if(crop->s_x + crop->s_w > crop->src->static_width){
			crop->s_x = crop->src->static_width - crop->s_w;
		}
		if(crop->s_y < 0){
			crop->s_y = 0;
		}
		if(crop->s_y + crop->s_h > crop->src->static_height){
			crop->s_y = crop->src->static_height - crop->s_h;
		}
	}
	if(crop->down == sel){
		if(OS > y){
			if(OS < crop->s_h + crop->s_y + y){
				int old = crop->s_y;
				crop->s_y = OS-y;
				crop->s_h += old-crop->s_y;
			}
			else{
				crop->s_y = crop->s_y + crop->s_h - 1;
				crop->s_h = 1;
			}
		}
		else{
			int old = crop->s_y;
			crop->s_y = 0;
			crop->s_h += old-crop->s_y;
		}
	}
	if(crop->left == sel){
		if(AS > x){
			if(AS < crop->s_w + crop->s_x + x){
				int old = crop->s_x;
				crop->s_x = AS-x;
				crop->s_w += old-crop->s_x;
			}
			else{
				crop->s_x = crop->s_x + crop->s_w - 1;
				crop->s_w = 1;
			}
		}
		else{
			int old = crop->s_x;
			crop->s_x = 0;
			crop->s_w += old-crop->s_x;
		}
	}
	if(crop->up == sel){
		if(OS < crop->src->static_height + y){
			if(OS > crop->s_y + y){
				crop->s_h = OS - crop->s_y - y;
			}
			else{
				crop->s_h = 1; 
			}
		}
		else{
			crop->s_h = crop->src->static_height - crop->s_y;
		}
	}
	if(crop->right == sel){
		if(AS < crop->src->static_width + x){
			if(AS > crop->s_x + x){
				crop->s_w = AS - crop->s_x - x;
			}
			else{
				crop->s_w = 1; 
			}
		}
		else{
			crop->s_w = crop->src->static_width - crop->s_x;
		}
	}

	//mousemoved

	if(down != sel){
		if(pointIsInRectangle(AS,OS,s_x-5+x,s_y-5+y,s_w+10,5)){
			crop->down = hover;
		}
		else{
			crop->down = none;
		}
	}
	if(up != sel){
		if(pointIsInRectangle(AS,OS,s_x-5+x,s_y+y + s_h,s_w+10,5)){
			crop->up = hover;
		}
		else{
			crop->up = none;
		}
	}
	if(left != sel){
		if(pointIsInRectangle(AS,OS,s_x-5+x,s_y-5+y,5,s_h+10)){
			crop->left = hover;
		}
		else{
			crop->left = none;
		}
	}
	if(right != sel){
		if(pointIsInRectangle(AS,OS,s_x+s_w+x,s_y-5+y,5,s_h+10)){
			crop->right = hover;
		}
		else{
			crop->right = none;
		}
	}

	if(handleSouris){
		if(mouseState == MOUSE_LEFT_DOWN){
			if(!selected && pointIsInRectangle(AS,OS,s_x+x,s_y+y,s_w,s_h)){
				crop->selected = true;
				crop->clicked = new_Point(AS-s_x,OS-s_y);
			}
			if(down == hover){
				crop->down = sel;
			}
			if(up == hover){
				crop->up = sel;
			}
			if(right == hover){
				crop->right = sel;
			}
			if(left == hover){
				crop->left = sel;
			}
		}
		if(mouseState == MOUSE_LEFT_UP){
			crop->selected = false;
			if(down == sel)
				crop->down = none;
			if(up == sel)
				crop->up = none;
			if(left == sel)
				crop->left = none;
			if(right == sel)
				crop->right = none;
		}
		handleSouris = false;
	}
}


void updateImageCropperSquare (ImageCropper* crop){
	if(crop->s_x < 0)
		crop->s_x = 0;
	if(crop->s_x > crop->src->static_width)
		crop->s_x = crop->src->static_width - 3;
	if(crop->s_w < 3)
		crop->s_w = 3;
	if(crop->s_w + crop->s_x > crop->src->static_width)
		crop->s_w = crop->src->static_width - crop->s_x;
	int x = crop->x;
	int y = crop->y;
	int s_x = crop->s_x;
	int s_y = crop->s_y;
	int s_w = crop->s_w;
	int s_h = crop->s_h;
	border_state up = crop->up;
	border_state down = crop->down;
	border_state left = crop->left;
	border_state right = crop->right;
	bool selected = crop->selected;
	if(crop->selected){
		crop->s_x = AS - crop->clicked.x;
		crop->s_y = OS - crop->clicked.y;
		if(crop->s_x < 0){
			crop->s_x = 0;
		}
		if(crop->s_x + crop->s_w > crop->src->static_width){
			crop->s_x = crop->src->static_width - crop->s_w;
		}
		if(crop->s_y < 0){
			crop->s_y = 0;
		}
		if(crop->s_y + crop->s_h > crop->src->static_height){
			crop->s_y = crop->src->static_height - crop->s_h;
		}
	}

	if(crop->down == sel){
		if(OS > y){
			if(OS < crop->s_h + crop->s_y + y){
				int old = crop->s_y;
				crop->s_y = OS-y;
				crop->s_h += old-crop->s_y;
				int old2 = crop->s_w;
				crop->s_w = crop->s_h;
				crop->s_x += old2-crop->s_w; 

			}
			else{
				crop->s_y = crop->s_y + crop->s_h - 1;
				crop->s_h = 1;
				crop->s_w = 1;
			}
		}
		else{
			int old = crop->s_y;
			crop->s_y = 0;
			crop->s_h += old-crop->s_y;
		}
	}
	if(crop->up == sel){
		if(OS < crop->src->static_height + y && OS - crop->s_y - y + crop->s_x < crop->src->static_width){
			if(OS > crop->s_y + y){
				crop->s_h = OS - crop->s_y - y;
				crop->s_w = crop->s_h;
			}
			else{
				crop->s_h = 1; 
				crop->s_w = 1;
			}
		}
		else{
			int mini = min(crop->src->static_height - crop->s_y,crop->src->static_width - crop->s_x);
			crop->s_h = mini;
			crop->s_w = mini;
		}
	}

	//mousemoved

	if(down != sel || left!= sel){
		if(pointIsInRectangle(AS,OS,s_x-20+x,s_y-20+y,s_w+40,20) && pointIsInRectangle(AS,OS,s_x-20+x,s_y-20+y,20,s_h+40)){
			crop->down = hover;
			crop->left = hover;
		}
		else{
			crop->down = none;
			crop->left = none;
		}
	}
	if(up != sel || right != sel){
		if(pointIsInRectangle(AS,OS,s_x-20+x,s_y+y + s_h,s_w+40,20) && pointIsInRectangle(AS,OS,s_x+s_w+x,s_y-20+y,20,s_h+40)){
			crop->up = hover;
			crop->right = hover;
		}
		else{
			crop->up = none;
			crop->right = none;
		}
	}

	if(handleSouris){
		if(mouseState == MOUSE_LEFT_DOWN){
			if(!selected && pointIsInRectangle(AS,OS,s_x+x,s_y+y,s_w,s_h)){
				crop->selected = true;
				crop->clicked = new_Point(AS-s_x,OS-s_y);
			}
			if(down == hover){
				crop->down = sel;
			}
			if(up == hover){
				crop->up = sel;
			}
			if(right == hover){
				crop->right = sel;
			}
			if(left == hover){
				crop->left = sel;
			}
		}
		if(mouseState == MOUSE_LEFT_UP){
			crop->selected = false;
			if(down == sel)
				crop->down = none;
			if(up == sel)
				crop->up = none;
			if(left == sel)
				crop->left = none;
			if(right == sel)
				crop->right = none;
		}
		if(pointIsInRectangle(AS,OS,x,y,crop->src->static_width,crop->src->static_height))
			handleSouris = false;
	}
}

void showImageCropper (int x,int y,int maxwidth, int maxheight, ImageCropper* crop){
	fitImageToRectangle(crop->src,maxwidth,maxheight);
	crop->x = x;
	crop->y = y;
	int s_x = crop->s_x;
	int s_y = crop->s_y;
	int s_w = crop->s_w;
	int s_h = crop->s_h;
	border_state up = crop->up;
	border_state down = crop->down;
	border_state left = crop->left;
	border_state right = crop->right;
	Image* src = crop->src;
	ecrisImageTexture(src,x,y);
	int W = src->static_width;
	int H = src->static_height;
	setColor4i(255,255,255,100);
	rectangle(x,y,x+W,y+s_y);
	rectangle(x,y+s_y,x+s_x,y+s_y+s_h);
	rectangle(x+s_x+s_w,y+s_y,x+W,y+s_y+s_h);
	rectangle(x,y+H,x+W,y+s_y+s_h);
	epaisseurDeTrait(1);
	switch(down){
		case none:
			setColor4i(0,0,0,180);
		break;
		case hover:
			setColor4i(255,0,0,180);
		break;
		case sel:
			setColor4i(0,255,255,180);
		break;
	}
	ligne(x+s_x,y+s_y,x+s_x+s_w,y+s_y); // DOWN
	switch(left){
		case none:
			setColor4i(0,0,0,180);
		break;
		case hover:
			setColor4i(255,0,0,180);
		break;
		case sel:
			setColor4i(0,255,255,180);
		break;
	}
	ligne(x+s_x,y+s_y,x+s_x,y+s_y+s_h);	// LEFT
	switch(up){
		case none:
			setColor4i(0,0,0,180);
		break;
		case hover:
			setColor4i(255,0,0,180);
		break;
		case sel:
			setColor4i(0,255,255,180);
		break;
	}
	ligne(s_x+x,s_h+s_y+y,s_x+s_w+x,s_y+s_h+y);	//UP
	switch(right){
		case none:
			setColor4i(0,0,0,180);
		break;
		case hover:
			setColor4i(255,0,0,180);
		break;
		case sel:
			setColor4i(0,255,255,180);
		break;
	}
	ligne(s_x+s_w+x,s_y+y,s_x+s_w+x,s_y+s_h+y);	//RIGHT
}

void captureZoneImageCropper(ImageCropper* crop,char* path){
	screenshot(crop->s_x+crop->x,crop->s_y+crop->y,crop->s_w,crop->s_h,path);
}

ImageCropper new_ImageCropper(Image* src,bool square_mode){
	ImageCropper ret;
	ret.src = src;
	fitImageToRectangle(ret.src,LF,HF);
	ret.selected = false;
	ret.s_x = 0;
	ret.s_y = 0;
	ret.s_w = 100;
	ret.s_h = 100;
	ret.x = 0;
	ret.y = 0;
	ret.up = none;
	ret.down = none;
	ret.left = none;
	ret.right = none;
	ret.show = showImageCropper;
	ret.update = (square_mode) ? updateImageCropperSquare : updateImageCropper;
	ret.saveAs = captureZoneImageCropper;
	return ret;
}