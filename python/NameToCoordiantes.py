import sys
from geopy.geocoders import Nominatim

in_str = ""
i = 1
while i < len(sys.argv) :
    in_str += " " + sys.argv[i]
    i = i+1

geolocator = Nominatim(user_agent="geolocator")
location = geolocator.geocode(in_str)
print(location.address)

file = open("../Ressources/geolocator.txt","w")
string = ""
if location.address is None :
    string = "0,0" + "\n" + "NOWHERE"
else :
    string = "" +  repr(location.longitude) + "," + repr(location.latitude) + "\n" + location.address
file.write(string.encode('utf8'))
file.close()