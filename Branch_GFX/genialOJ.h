// TYPEDEFS





//GlOBALES EXTERNES 

//premiere boucle ?
extern bool first;		
//temps reel au demarage du programme				        
extern double runtime_t0;	
//nom de la fenetre					
extern char processname[100];
//largeur initiale de la fenetre
extern int processwidth;
//hauteur initiale de la fenetre
extern int processheight;


// FONCTIONS

//callback avant création de la fenetre
void prelaunch ();

//callback apres la création de la fenetre
void initialize ();

//callback appelé a chaque frame pour le traitement
void update ();

//callback appelé pour dessiner la frame suivante
void render ();

//callback click droit appuyé
void rightClickDown ();

//callback clic droit relaché
void rightClickUp ();

//callback clic gauche appuyé
void leftClickDown ();

//callback clic gauche relaché
void leftClickUp ();

//callback scroll vers le haut
void scrollUp ();

//callback scroll vers le bas
void scrollDown ();

//callback fenetre redimensionnée
void windowResize();

//callback souris bougée
void mouseMoved ();


// FONCTIONS USUELLES



/*fonction en bordel
//LISTE FONCTIONS

void addMember (Maillon** tete,Maillon* new);
void deleteMember (Maillon** tete,Maillon* suppr);
Maillon* getSuivant (Maillon* tete,)

// fonction sur les donn�es

char * readdonnee(char * nomfichier);
void convertitTexteMaillon(char* donnee,Maillon** tete);
void convertitMaillonTexte(char**donnee,Maillon *tete);
void writedonnee(char * donnee,char * nomfichier);


// fonction sur les chaines;


maillon * getPrecedent(maillon *ptrTete,maillon M);
maillon * RechercheMaillon(Individu perso,maillon * ptrTete);
maillon * CreationArbre(void);
void supprimeArbre(maillon ** ptrTete);
void InverseArbre(maillon **ptrTete) ;

//AFFICHAGE :

void afficheProfil(Individu A)  affiche la photo, nom, et info general sur l individu
void afficheLiens(segments* S)
void afficheMainMenu();
void afficheReglages();
void afficheTextfield(textfield T);
void afficheBouton(Bouton B);

//GESTION :
void gereReglages([Globales])
void gereClicBouton(bouton* B)
void gereTextfield(textfield* T)
char* genereStringDate(Date D)
void joueListeEvenement (Individu I)    //joue une animation sur les deplacements d une personne et les evenements (mariage,enfents,mort...)

//RECHERCHE : 
Calcule le degre de probabilit� que deux personne aient un lien 
int getProbaParent (Individu I1,Individu I2)
int getProbaSibling (Individu I1,Individu I2)
int getProbaConnaissance (Individu I1,individu I2)

//setter

Individu new_Individu(char * surname,char**name,Birth birth_data,Death* death_data,bool gender, DonneesImageRGB* photo);


maillon * new_Maillon(Individu perso);

Date new_Date (int j,int m,int a){
return (Date){j,m,a};
}


Lieu new_Lieu (char * nom, double x ,double y){
	
	return (Lieu){nom,x,y};
	
}


Birth new_Birth (Date date, Lieu lieu){
	
	return (Birth){date,lieu};
}


Death new_Death (Date date, Lieu lieu){
	
	return (Death){date,lieu};
}


Mariage new_Mariage (Data date,Lieu lieu,Individu* maries[2]);


 






*/
