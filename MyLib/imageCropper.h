typedef enum border_state{
	none,hover,sel
} border_state;

typedef struct ImageCropper ImageCropper;
struct ImageCropper{
	Image* src;
	bool selected;
	Point clicked;
	int s_x;
	int s_y;
	int s_w;
	int s_h;
	int x,y;
	border_state up;
	border_state down;
	border_state left;
	border_state right;
	void (*saveAs) (ImageCropper* self,char* path);
	void (*update) (ImageCropper* self);
	void (*show) (int x,int y,int maxwidth,int maxheight,ImageCropper* self);
};

ImageCropper new_ImageCropper(Image* src,bool square_mode);