#ifdef _WIN32
	#include <windows.h>
#endif
#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "../GfxLib/GfxLib.h"
#include "../GfxLib/BmpLib.h"
#include "../GfxLib/ESLib.h"
#include <string.h>
#include "../GfxLib/commonIO.h"
#include "../MyLib/KyllianToolsLib.h"
#include "../MyLib/KToolsLibForGfxLib.h"
#include "../MyLib/geometry.h"
#include "../MyLib/Cwing.h"
#include "../MyLib/consoletools.h"
#include "../MyLib/CSVlib.h"
#include "../MyLib/Timer.h"
#include "../MyLib/images.h"
#include "../MyLib/sdlglutils.h"
#include "../MyLib/listechainee.h"
#include "../MyLib/exploFichier.h"
#include "../GfxLib/commonIO.h"
#include "../MyLib/imageCropper.h"
#include "../MyLib/googleimagelib.h"
#include "../MyLib/googlemapslib.h"
#include "../MyLib/OurSQL.h"
#include "../MyLib/genialOJ_GFX.h"
#include "../MyLib/ia.h"




// GLOBALES EXTERNES

State state = main_menu;

bool moved = false;
bool grab = false;
RechercheGFX RechGFX;

//coordoonée x inferieure gauche du plan visible		
int cam_x = 0;			

//coordoonée y inferieure gauche du plan visible
int cam_y = 0;			

//Affichage du click menu ?
bool showmenu = false;		

//ClickMenu principale
ClickMenu menu;			

//Source originale de la map
DonneesImageRGB* mapsource;	

//zoom
float zoom = 1;	

//tete de la LC
Maillon* ptrTete;

//coordonnée du clickMenu
int xmenu,ymenu;

//Individu selectionné
Maillon* toshow = NULL;

SearchMenu search_menu;

Maillon* start = NULL;

//menu settings
bouton menu_button[6];
Settings Menu_Settings;
bool show_setting;
Counter count;

//MenuEdition;
Edition edit;
char * tmpc=NULL;

//Mover bouton menu principal
Mover mov_bt1;
Mover mov_bt2;
Mover mov_bt3;
Mover mov_bt4;
Mover mov_bt5;
Mover mov_bt6;

int numerofichierind=0;
int numerofichiermari=0;

//GLOBALES SIMPLES
//Menu Edition
//char * tmpc=NULL;
int widthrec;
int heightrec;
int choixG;
//Image du Pin Google Maps
Image* pin = NULL;
Image* img_genre[2] = {NULL,NULL};
Image* ico_calendrier = NULL;
Image* ico_place = NULL;
bigScreenshotData bigScreenshot = (bigScreenshotData) {0,0,0,0,0,0,0,0,0,0,0};

void takeBigScreenshot (int x,int y,int width,int height,char* nomFichier){
	bigScreenshot.done = 1;
	bigScreenshot.oldLF = LF;
	bigScreenshot.oldHF = HF;
	bigScreenshot.oldcam_x = cam_x;
	bigScreenshot.oldcam_y = cam_y;
	bigScreenshot.oldx = glutGet((GLenum)GLUT_WINDOW_X);
	bigScreenshot.oldy = glutGet((GLenum)GLUT_WINDOW_Y);
	bigScreenshot.oldzoom = zoom;

	bigScreenshot.newLF = width;
	bigScreenshot.newHF = height;
	bigScreenshot.newcam_x = x;
	bigScreenshot.newcam_y = y;
	zoom = 1;
	strcpy(bigScreenshot.nomFichier,nomFichier);
}

void updateBigScreenshot (){
	if(bigScreenshot.done > 0 && bigScreenshot.done < 4){
		glutReshapeWindow(bigScreenshot.newLF,bigScreenshot.newHF);
		cam_x = bigScreenshot.newcam_x;
		cam_y = bigScreenshot.newcam_y;
		glutPositionWindow(999999,999999);
	}
}

void updateViewBigScreenshot (){
	if(bigScreenshot.done > 0){
		bigScreenshot.done++;
	}
	if(bigScreenshot.done == 4){
		screenshot(0,0,LF,HF,bigScreenshot.nomFichier);
		glutReshapeWindow(bigScreenshot.oldLF,bigScreenshot.oldHF);
		cam_x = bigScreenshot.oldcam_x;
		cam_y = bigScreenshot.oldcam_y;
		zoom = bigScreenshot.oldzoom;
		bigScreenshot.done = 0;
		glutPositionWindow(bigScreenshot.oldx,bigScreenshot.oldy);
	}
}


/**
 * @brief recherche la photo d'un individu a partir de son nom
 * 
 * @param ind individu recherché
 * @return GoogleSearchIMG* 
 */
GoogleSearchIMG* searchGoogleImageIndividu (Individu* ind){
	char* searchTerms = str_format("%s %s",ind->name[0],ind->surname);
	return new_GoogleSearchIMG(searchTerms,5,"../GoogleSearch");
}

/**
 * @brief sauvegarde d une image google_search d'un individu 
 * 
 * @param google recherche
 * @param numImage index de la photo voulue
 * @param ind individu
 */
void saveGoogleImageAsIndividu (GoogleSearchIMG* google,int numImage,Individu* ind){
	char* path = str_format("../individus_image/%s_%s_%d.%s",ind->surname,ind->name[0],ind->numGen,fileGetExtension(google->file_location[numImage]));
	ind->photo = google->result[numImage];
	float zoom = 160*1. / ind->photo->width;
	ind->photo->zoom = zoom;
	ind->photo->static_width = zoom*ind->photo->width;
	ind->photo->static_height = zoom*ind->photo->height;
	saveGoogleImageAs(google,numImage,path);
}

//action du bouton edit du clickmenu
void actionOfB (){
	search_menu.search = new_GoogleSearchIMG(search_menu.txt.texte,atoi(search_menu.txt_nb.texte),"../GoogleSearch");
	search_menu.manualsearch = true;
}

//action du bouton de fermeture de la recherche d image
void actionOfQuitSearch(){
	search_menu.searchMenu = false;
	search_menu.search = NULL;
	search_menu.txt.texte[0] = '\0';
	search_menu.manualsearch = false;
	search_menu.crop = NULL;
}

//action du bouton done de l editeur d image
void actionOfDone(){
	system(str_format("rm -rf ../individus_image/%s_%s_%d.*",search_menu.selec->surname,search_menu.selec->name[0],search_menu.selec->numGen));
	char* path = str_format("../individus_image/%s_%s_%d.bmp",search_menu.selec->surname,search_menu.selec->name[0],search_menu.selec->numGen);
	search_menu.crop->saveAs(search_menu.crop,path);
	search_menu.selec->photo = new_Image(path,"true");
	fitImageToRectangle(search_menu.selec->photo,160,160);
	println("clicked");
	search_menu.crop = NULL;
	actionOfQuitSearch();
}

//actualise le menu de recherche image
void updateSearchMenu (SearchMenu* sm){
	if(sm->searchMenu){
		executeActionBouton(sm->b);
		executeActionBouton(sm->quitsearch);
		textFieldClickListener(&(sm->txt_nb));
		textFieldClickListener(&(sm->txt));
	}
	updateGoogleSearchIMG(sm->search);
	if(sm->crop != NULL){
		sm->crop->update(sm->crop);
		executeActionBouton(sm->done);
	}
	if(handleSouris && mouseState == MOUSE_LEFT_UP){
		if(sm->selec != NULL && sm->search != NULL && sm->search->finished == true){
			for(int i = 0; i<sm->search->nb_resultats;i++){
				if(pointIsInRectangle(AS,OS,LF*160/1200*i + LF/2-(LF*160/1200*sm->search->nb_resultats)/2,HF/2 - LF*160/1200/2,LF*160/1200,LF*160/1200)){
					println("%d",i);
					sm->crop = malloc(sizeof(ImageCropper));
					*(sm->crop) = new_ImageCropper(sm->search->result[i],true);
					//saveGoogleImageAsIndividu(search,i,selec);
					break; 		//Si c est ca je me casse !
				}
			}
		}
	}
}

//affichage du menu de recherche image
void showSearchMenu (SearchMenu *sm){
	if(sm->searchMenu){
		setColor4i(0,0,0,90);
		rectangle(0,0,LF,HF);
		setColor4i(0,0,0,255);
		rectangle(100,100,LF-100,HF-100);
		couleurCourante(255,255,255);
		epaisseurDeTrait(3);
		if(sm->search != NULL && !sm->search->finished)
			printGfx(LF/2 - tailleChaine("Loading Images ...",27)/2,HF-100-27-80,27,"Loading Images ...");
		else if(sm->manualsearch)
			printGfx(LF/2 - tailleChaine("Images Pour XXXXXX XXXXXXXX",27)/2,HF-100-27-80,27,"Images Pour %s",sm->txt.texte);
		else
			printGfx(LF/2 - tailleChaine("Images Pour XXXXXX XXXXXXXX",27)/2,HF-100-27-80,27,"Images Pour %s %s",sm->selec->name[0],sm->selec->surname);
		if(sm->crop!=NULL){
			sm->crop->show(LF/2-sm->crop->src->static_width/2,190,LF-200,HF-200-20-27-30-190,sm->crop);
			afficheBouton(LF/2-200/2,150,&sm->done);
		}
		else{
			if(sm->search!= NULL){
				int offset = 0;
				for(int i=0;i<sm->search->nb_resultats;i++){
					if(sm->search->result[i] != NULL){
						fitImageToRectangle(sm->search->result[i],LF*160/1200,LF*160/1200);
						ecrisImageTexture(sm->search->result[i],LF/2-(LF*160/1200*sm->search->nb_resultats)/2 + offset,HF/2 - LF*160/1200/2);
						offset+= LF*160/1200;
					}
				}
			}
			afficheTextField(&(sm->txt_nb),LF/2 -150 -60,150);
			afficheTextField(&(sm->txt),LF/2 -150 + 0,150);
			afficheBouton(LF/2 -150 +310,150,&(sm->b));
		}
		afficheBouton(LF-100-50,HF-100-50,&(sm->quitsearch));
	}
}

//cree un nouveau menu de rcherche d image
SearchMenu new_SearchMenu (){
	SearchMenu ret;
	ret.selec = NULL;
	ret.crop = NULL;
	ret.searchMenu = false;
	ret.manualsearch = false;
	ret.search = NULL;
	ret.txt = initTextField(100,-200,300,30,(RGB){200,200,200},"");
	ret.txt_nb = initTextField(0,-200,50,30,(RGB){200,200,200},"5");
	ret.b = initBouton(30,30,"Go",(RGB){200,200,200});
	ret.b.addEventListener(&(ret.b),"click",actionOfB);
	ret.done = initBouton(200,30,"Done",(RGB){200,200,200});
	ret.done.addEventListener(&(ret.done),"click",actionOfDone);
	ret.quitsearch = initBouton(50,50,"X",(RGB){200,200,200});
	ret.quitsearch.addEventListener(&(ret.quitsearch),"click",actionOfQuitSearch);
	ret.update = updateSearchMenu;
	ret.show = showSearchMenu;
	return ret;
}

/**
 * @brief initialise les données d affichage des maillons
 * 
 * @param ptrTete tete de liste chainée
 */
void initMaillonTiles (Maillon* ptrTete){
	int cpt = 0;
	int cptx = 0;
	int last_gen = 0;
	int max = goToLC(ptrTete,countLC(ptrTete)-1)->data->numGen;
	Polygone* poly_tile = malloc(sizeof(Polygone));
	Polygone* poly_shadow = malloc(sizeof(Polygone));
	*poly_shadow = initPolygone(304,404,0.8,100);
	*poly_tile = initPolygone(300,400,0.8,100);
	println("%d",max);
	for(Maillon* ptr = ptrTete; ptr != NULL;ptr = ptr->next){
		if(ptr->data->numGen != last_gen){
			cptx = 0;
		}
		int x = cptx*600;
		int y = -600*(max-ptr->data->numGen);
		x = -300;
		ptr->tile_info = new_IndividuTile(x,y,poly_tile,poly_shadow);
		cpt++;
		cptx++;
		last_gen = ptr->data->numGen;
	}
	resizePhotoLC(ptrTete,160);
}

/**
 * @brief actualisation des pins de la map
 * 
 * @param ptrTete tete de liste chainee
 * @param map image de la carte
 */
void updatePinListener (Maillon* ptrTete,Image* map){
	bool selected = false;
	bool quit = false;
	for(Maillon* tmp = ptrTete; tmp != NULL ; tmp = tmp->next){
		int posx;
		for(posx = cam_x;posx < LF;posx+=map->static_width){
			float xc = map->static_width/2 + (map->static_width/2)* tmp->data->birth_data->lieu.X*1. /180 + posx,
			yc = map->static_height/2 + (map->static_height/2)* tmp->data->birth_data->lieu.Y*1. /90 + cam_y;
			if(pointIsInRectangle(AS,OS,xc-23,yc,47,73)){
				toshow = tmp;
				selected = true;
				quit = true;
				break;
			}
		}
		if(quit) break;
		for(posx = cam_x - map->static_width;posx > -map->static_width;posx-=map->static_width){
			float xc = map->static_width/2 + (map->static_width/2)* tmp->data->birth_data->lieu.X*1. /180 + posx,
			yc = map->static_height/2 + (map->static_height/2)* tmp->data->birth_data->lieu.Y*1. /90 + cam_y;
			if(pointIsInRectangle(AS,OS,xc-23,yc,47,73)){
				toshow = tmp;
				selected = true;
				quit = true;
				break;
			}
		}
		if(quit) break;
	}
	if(!selected && !moved){
		toshow = NULL;
	}
}

/**
 * @brief actualise les coordonées x_cam et y_cam pour que l on puisse se dplacer a l aide de la souris sur un plan 2D
 * 
 */
void plan2DGrabbable (){		
	static int offsetx = 0;
	static int offsety = 0;
	static int oldAS = 0;
	static int oldOS = 0;
	if(grab && (oldAS != AS || oldOS != OS)){
		offsetx = AS - oldAS;
		offsety = OS - oldOS;
		oldAS = AS;
		oldOS = OS;
		cam_x += offsetx;
		cam_y += offsety;
		moved = true;
	}
	if(handleSouris && mouseState == MOUSE_LEFT_DOWN){
		grab = true;
		oldAS = AS;
		oldOS = OS;
	}
	if(handleSouris && mouseState == MOUSE_LEFT_UP){
		grab = false;
	}
}

//action du bouton edit du clickmenu
void actionOfEdit (){
	if(!moved && !search_menu.searchMenu){
		for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
			IndividuTile *IT = tmp->tile_info; 
			if(xmenu >= IT->x*zoom+cam_x && xmenu <= IT->x*zoom+cam_x + IT->poly->width*zoom && ymenu >= IT->y*zoom+cam_y && ymenu <= IT->y*zoom+cam_y + IT->poly->height*zoom){
					
				edit.choisis=tmp;
				
				if(edit.choisis!=NULL){

					if(edit.choisis->child!=NULL){
						for(int i=0;i<count(edit.choisis->child)-1;i++){
							tmpc=malloc(sizeof(char)*50);
							strcpy(tmpc,edit.choisis->child[i]->data->name[0]);
							strcat(tmpc," ");
							strcat(tmpc,edit.choisis->child[i]->data->surname);
							edit.enfants.add(&(edit.enfants),tmpc);
		
						}
					}
					else{
						edit.enfants.add(&(edit.enfants),"Pas d'enfants");
					}
					if(edit.choisis->sibling!=NULL){
		
						for(int i=0;i<count(edit.choisis->sibling)-1;i++){
							tmpc=malloc(sizeof(char)*50);
							strcpy(tmpc,edit.choisis->sibling[i]->data->name[0]);
							strcat(tmpc," ");
							strcat(tmpc,edit.choisis->sibling[i]->data->surname);
							edit.fraterie.add(&(edit.fraterie),tmpc);
						}
					}
					else{
	
						edit.fraterie.add(&(edit.fraterie),"Pas de fraterie");
					}
					AjouteInfo(edit.choisis);
					edit.show_edit=true;
				}
				
					
				
				break; 		//Si c est ca je me casse !
			}
		}
	}
	
}

/**
 * @brief affichage des liens de mariage entre les individus
 * 
 * @param ptrTete tete de liste chainée
 */
void afficheLiensMariage (Maillon* ptrTete){
	int lasty = 1;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->tile_info->isShown && *(tmp->data->gender)==HOMME && tmp->wedding != NULL && tmp->wedding->data->maries[0] != NULL && tmp->wedding->data->maries[1] != NULL && tmp->tile_info->isShown && tmp->wedding->data->maries[1]->tile_info->isShown){
			int r=Menu_Settings.button_settings[4].couleur.r;
			int g=Menu_Settings.button_settings[4].couleur.g;
			int b=Menu_Settings.button_settings[4].couleur.b;
			couleurCourante(r,g,b);
			Maillon* m1 = tmp->wedding->data->maries[0];
			Maillon* m2 = tmp->wedding->data->maries[1];
			int x1 = cam_x + (m1->tile_info->x + m1->tile_info->poly->width/2)*zoom,
				x2 = cam_x + (m2->tile_info->x + m2->tile_info->poly->width/2)*zoom,
				y1 = cam_y + m1->tile_info->y*zoom,
				y2 = cam_y + m2->tile_info->y*zoom;
			int decalage = 0;
			if(lasty == y1){
				decalage = (30)*zoom;
			}
			else{
				decalage = 30*zoom;
			}
			lasty = y1;
			ligne(x1,y1,x1,y1-decalage);
			ligne(x1,y1-decalage,x2,y2-decalage);
			ligne(x2,y1,x2,y2-decalage);
		}
	}
}

void afficheLiensFraterie (Maillon* ptrTete){
	int lasty = 1;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->sibling != NULL && tmp->tile_info->isShown){
			for(int i=0;tmp->sibling[i]!= NULL;i++){
				if(tmp->sibling[i]->tile_info->isShown){
					couleurCourante(0,255,100);
					Maillon* m1 = tmp;
					Maillon* m2 = tmp->sibling[i];
					int x1 = cam_x + (m1->tile_info->x + m1->tile_info->poly->width/2)*zoom,
						x2 = cam_x + (m2->tile_info->x + m2->tile_info->poly->width/2)*zoom,
						y1 = cam_y + (m1->tile_info->y+m1->tile_info->poly->height)*zoom,
						y2 = cam_y + (m2->tile_info->y+m2->tile_info->poly->height)*zoom;
					int decalage = 0;
					if(lasty == y1){
						decalage = (30)*zoom;
					}
					else{
						decalage = 30*zoom;
					}
					lasty = y1;
					ligne(x1,y1,x1,y1+decalage);
					ligne(x1,y1+decalage,x2,y2+decalage);
					ligne(x2,y1,x2,y2+decalage);
				}
			}
		}
	}
}	

/**
 * @brief affichage des liens de parenté entre les individus
 * 
 * @param ptrTete tete de liste chainée
 */
void afficheLiensParents (Maillon* ptrTete){
	int r=Menu_Settings.button_settings[3].couleur.r;
	int g=Menu_Settings.button_settings[3].couleur.g;
	int b=Menu_Settings.button_settings[3].couleur.b;
	couleurCourante(r,g,b);
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->child != NULL && tmp->tile_info->isShown){
			int x1,x2,y1,y2;
			Maillon* m1 = NULL;
			Maillon* m2 = NULL;
			if(tmp->wedding != NULL && tmp->wedding->data->maries[!(*(tmp->data->gender))]->child != NULL){		// !!!!!!! Penser au cas ou ils sont mariés mais c est pas le gosse de l autre
				//x1 y1
				bool samechild = false;
				for(int i=0;tmp->wedding->data->maries[!(*(tmp->data->gender))]->child[i] != NULL;i++){
					if(tmp->wedding->data->maries[!(*(tmp->data->gender))]->child[i] == tmp->child[0]){
						samechild = true;
						break;
					}
				}
				if(samechild){
					x1 = (tmp->tile_info->x + tmp->wedding->data->maries[!(*(tmp->data->gender))]->tile_info->x + tmp->tile_info->poly->width)/2;
					y1 = tmp->tile_info->y - 30;
				}
				else{
					m1 = tmp;
					x1 = m1->tile_info->x + m1->tile_info->poly->width/2;
					y1 = m1->tile_info->y;
				}
			}
			else{
				//x1 y1
				m1 = tmp;
				x1 = m1->tile_info->x + m1->tile_info->poly->width/2;
				y1 = m1->tile_info->y;
			}
			if(tmp->child[0]->sibling != NULL){
				//calc milieu du sibling x2 y2
				int xmin = tmp->child[0]->tile_info->x;
				int xmax = tmp->child[0]->tile_info->x;
				for(int i = 0;tmp->child[0]->sibling[i] != NULL;i++){
					if(tmp->child[0]->sibling[i]->tile_info->x < xmin){
						xmin = tmp->child[0]->sibling[i]->tile_info->x;
					}
					if(tmp->child[0]->sibling[i]->tile_info->x > xmax){
						xmax = tmp->child[0]->sibling[i]->tile_info->x;
					}
				}
				xmax+=tmp->tile_info->poly->width;
				x2 = (xmax + xmin)/2;
				y2 =  tmp->child[0]->tile_info->y +  tmp->child[0]->tile_info->poly->height + 30;
			}
			else{	//pas de fraterie
				//x2 y2
				m2 = tmp->child[0];
				x2 = m2->tile_info->x + m2->tile_info->poly->width/2;
				y2 = m2->tile_info->y + m2->tile_info->poly->height;
			}
			ligne(cam_x + x1*zoom,cam_y + y1*zoom,cam_x + x2*zoom,cam_y + y2*zoom);
		}
	}
}
/*void afficheLiensParents (Maillon* ptrTete){
	int r=Menu_Settings.button_settings[3].couleur.r;
	int g=Menu_Settings.button_settings[3].couleur.g;
	int b=Menu_Settings.button_settings[3].couleur.b;
	couleurCourante(r,g,b);
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->dad || tmp->mom){
			if(tmp->dad && tmp->mom && tmp->mom->tile_info->isShown && tmp->dad->tile_info->isShown && tmp->dad->wedding && tmp->mom->wedding){
				MaillonMariage* cMdad;
				MaillonMariage* cMmom;
				for(cMdad = tmp->dad->wedding; cMdad->next; cMdad=cMdad->next){}
				for(cMmom = tmp->dad->wedding; cMmom->next; cMmom=cMmom->next){}

				//dernier mariage commun
				if(cMdad->data == cMmom->data){
					if(tmp->sibling){
						int sum = 0, i =0;
						for( i = 0; tmp->sibling[i];i++){
							if(tmp->sibling[i]->tile_info->isShown){
								println("%s %s",tmp->sibling[i]->data->surname,tmp->sibling[i]->data->name[0]);
								sum += tmp->sibling[i]->tile_info->x + tmp->sibling[i]->tile_info->poly->width/2;
							}
						}
						sum /= i;

						//tracer entre les deux
						Maillon* m1 = tmp;
						Maillon* m2 = tmp->dad;
						Maillon* m3 = tmp->mom;
						int x1 = sum ,
							x2 = ((m2->tile_info->x + m2->tile_info->poly->width/2)+(m3->tile_info->x + m3->tile_info->poly->width/2))/2,
							y1 = m1->tile_info->y + m1->tile_info->poly->height,
							y2 = m2->tile_info->y;
						ligne(cam_x + x1*zoom,cam_y + y1*zoom,cam_x + x2*zoom,cam_y + y2*zoom);
					}
					else{
						println(" 1 .%s %s",tmp->data->surname,tmp->data->name[0]);
						//tracer entre les deux
						Maillon* m1 = tmp;
						Maillon* m2 = tmp->dad;
						Maillon* m3 = tmp->mom;
						int x1 = m1->tile_info->x + m1->tile_info->poly->width/2,
							x2 = ((m2->tile_info->x + m2->tile_info->poly->width/2)+(m3->tile_info->x + m3->tile_info->poly->width/2))/2,
							y1 = m1->tile_info->y + m1->tile_info->poly->height,
							y2 = m2->tile_info->y-30;
						ligne(cam_x + x1*zoom,cam_y + y1*zoom,cam_x + x2*zoom,cam_y + y2*zoom);
					}
				}

			}
			else{
				if(tmp->dad && tmp->dad->tile_info->isShown){
					//tracer vers papa
					println("2 . %s %s",tmp->data->surname,tmp->data->name[0]);
					Maillon* m1 = tmp;
					Maillon* m2 = tmp->dad;
					int x1 = m1->tile_info->x + m1->tile_info->poly->width/2,
						x2 = m2->tile_info->x + m2->tile_info->poly->width/2,
						y1 = m1->tile_info->y + m1->tile_info->poly->height,
						y2 = m2->tile_info->y;
					ligne(cam_x + x1*zoom,cam_y + y1*zoom,cam_x + x2*zoom,cam_y + y2*zoom);
				}
				if(tmp->mom && tmp->mom->tile_info->isShown){
					println("3. %s %s",tmp->data->surname,tmp->data->name[0]);
					//tracer vers maman
					Maillon* m1 = tmp;
					Maillon* m2 = tmp->mom;
					int x1 = m1->tile_info->x + m1->tile_info->poly->width/2,
						x2 = m2->tile_info->x + m2->tile_info->poly->width/2,
						y1 = m1->tile_info->y + m1->tile_info->poly->height,
						y2 = m2->tile_info->y;
					ligne(cam_x + x1*zoom,cam_y + y1*zoom,cam_x + x2*zoom,cam_y + y2*zoom);
				}
			}
		}
	}
}*/

/*void afficheLiensParents (Maillon* ptrTete){
	int r=Menu_Settings.button_settings[3].couleur.r;
	int g=Menu_Settings.button_settings[3].couleur.g;
	int b=Menu_Settings.button_settings[3].couleur.b;
	couleurCourante(r,g,b);
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->dad != NULL){
			Maillon* m1 = tmp;
			Maillon* m2 = tmp->dad;
			int x1 = m1->tile_info->x + m1->tile_info->poly->width/2,
				x2 = m2->tile_info->x + m2->tile_info->poly->width/2,
				y1 = m1->tile_info->y + m1->tile_info->poly->height,
				y2 = m2->tile_info->y;
			ligne(cam_x + x1*zoom,cam_y + y1*zoom,cam_x + x2*zoom,cam_y + y2*zoom);
		}
		if(tmp->mom != NULL){
			Maillon* m1 = tmp;
			Maillon* m2 = tmp->mom;
			int x1 = m1->tile_info->x + m1->tile_info->poly->width/2,
				x2 = m2->tile_info->x + m2->tile_info->poly->width/2,
				y1 = m1->tile_info->y + m1->tile_info->poly->height,
				y2 = m2->tile_info->y;
			ligne(cam_x + x1*zoom,cam_y + y1*zoom,cam_x + x2*zoom,cam_y + y2*zoom);
		}
	}
}*/

/**
 * @brief
 * retourne le menu type 1 avec Edit,Delete,Copy,Paste
 * @return ClickMenu 
 */
ClickMenu new_ClickMenu1 (){
	bouton* option = malloc(sizeof(bouton)*4);
	int bwidth = 150,bheight = 17;
	RGB couleur = (RGB) {190,190,190};
	option[0] = initBouton(bwidth,bheight,"Edit",couleur);
	option[1] = initBouton(bwidth,bheight,"Delete",couleur);
	option[2] = initBouton(bwidth,bheight,"Copy",couleur);
	option[3] = initBouton(bwidth,bheight,"Paste",couleur);
	option[0].addEventListener(&(option[0]),"click",actionOfEdit);
	int* separateurs = malloc(sizeof(int));
	separateurs[0] = 2;
	return new_ClickMenu(option,separateurs,4,1);
}

/**
 * @brief
 * changement du Zoom de la carte
 * | toadd : de combien on incrémente le zoom
 */
void changeZoom(float toadd,Image* map){
	if(zoom+toadd < 90000){
		float coefx = (cam_x*1.-LF/2) / map->width/zoom;
		float coefy = (cam_y*1.-HF/2) / map->height/zoom;
		zoom+= toadd;
		map->zoom = zoom;
		map->static_width = zoom*map->width;
		map->static_height = zoom*map->height;
		int a = 1.*map->static_width * coefx;
		int o = 1.*map->static_height * coefy;
		cam_x = a + LF/2;
		cam_y = o + HF/2;
	}
}

/*float getMouseLongitude (Image* map){
	
}*/

/**
 * @brief
 * Obtenir la longitude relative a la carte d'une position en pixels sur la carte
 * |param x : position que l'on cherche 
 * |return float : longitude
 */
float getLongitude (int x,Image* map){

	int width = map->static_width;

	return (x-cam_x%width-width*1./2)*360*1./width;	//on retrouve cette equation en applicant la loi de descartes sur la loi de fresnel en raie mineure

	printdebug("%d %d\n",width,x);
	if(x > width/2){
		return ((x - width*1./2 - cam_x)*1. / (width*1./2) )*180;
	}
	else{
		return -(( x *1./ (width/2) )* 180);
	}
}

/**
 * @brief
 * Obtenir la latitude relative a la carte d'une position en pixels sur la carte
 * |param y : position que l'on cherche 
 * |return float : latitude
 */
float getLatitude (int y,Image* map){
	int height = map->static_height;
	printdebug("%d %d\n",height,y);

	return (y - height*1./2 -cam_y)*180*1./height;

	if(y > height/2){
		return ((y - height*1./2 - cam_y)*1. / (height*1./2) )*90;
	}
	else{
		return -(( y *1./ (height/2) )* 90);
	}
}

/**
 * @brief affichage des données de naissance d'un individu
 * 
 * @param data 
 * @param x 
 * @param y 
 * @param width largeur 
 * @param height hauteur
 */
void afficheBirthData (Birth* data,int x,int y,int width,int height){
	if(ico_calendrier == NULL){
		ico_calendrier = new_Image("../Ressources/calendrier.png",true);
	}
	if(ico_place == NULL){
		ico_place = new_Image("../Ressources/place.png",true);
	}
	if(data != NULL){
		couleurCourante(140,140,140);
		fitTextToRectangle("Naissance",x,y+height - 21*height/100,width,20*height/100);
		couleurCourante(255,255,255);
		ecrisImageTextureCustom(ico_calendrier,x + 30*width/300,y + height - 60*height/100,23*width/300,26*width/300);
		ecrisImageTextureCustom(ico_place,x + 30*width/300,y,23*width/300,26*width/300);
		char* date = (dateIsNULL(data->date))? "???" : dateToFancyString(data->date);
		fitTextToRectangle(date,x + 78*width/300,y + height - 60*height/100,200*width/300,26*width/300);
		if(!dateIsNULL(data->date)){
			free(date);
			date = NULL;
		}
		if(data->lieu.nom == NULL){
			fitTextToRectangle("???",x + 78*width/300,y,200*width/300,26*width/300);
		}
		else{
			fitTextToRectangle(data->lieu.nom,x + 78*width/300,y,200*width/300,26*width/300);
		}
	}
	else{
		printerr("ERROR : Affichage de birth_data NULL impossible, t'es un pd");
	}
}

/**
 * @brief affichage des données de mort d'un individu
 * 
 * @param data 
 * @param x 
 * @param y 
 * @param width largeur 
 * @param height hauteur
 */
void afficheDeathData (Death* data,int x,int y,int width,int height){
	if(ico_calendrier == NULL){
		ico_calendrier = new_Image("../Ressources/calendrier.png",true);
	}
	if(ico_place == NULL){
		ico_place = new_Image("../Ressources/place.png",true);
	}
	if(data != NULL){
		couleurCourante(140,140,140);
		fitTextToRectangle("Mort",x,y+height - 21*height/100,width,20*height/100);
		couleurCourante(255,255,255);
		ecrisImageTextureCustom(ico_calendrier,x + 30*width/300,y + height - 60*height/100,23*width/300,26*width/300);
		ecrisImageTextureCustom(ico_place,x + 30*width/300,y,23*width/300,26*width/300);
		char* date = (dateIsNULL(data->date))? "???" : dateToFancyString(data->date);
		fitTextToRectangle(date,x + 78*width/300,y + height - 60*height/100,200*width/300,26*width/300);
		if(!dateIsNULL(data->date)){
			free(date);
			date = NULL;
		}
		if(data->lieu.nom == NULL){
			fitTextToRectangle("???",x + 78*width/300,y,200*width/300,26*width/300);
		}
		else{
			fitTextToRectangle(data->lieu.nom,x + 78*width/300,y,200*width/300,26*width/300);
		}
	}
	else{
		printerr("ERROR : Affichage de death_data NULL impossible, t'es un pd");
	}
}

//action du bouton conjoint du side menu
void actionOfBtnConjoint (){
	if(toshow->wedding != NULL){
		Maillon* conjoint = (toshow->wedding->data->maries[0] == toshow) ?toshow->wedding->data->maries[1] : toshow->wedding->data->maries[0];
		toshow = conjoint; 
		cam_x = -toshow->tile_info->x*zoom + (LF/2 - toshow->tile_info->poly->width/2*zoom);
		cam_y = -toshow->tile_info->y*zoom + (HF/2 - toshow->tile_info->poly->width/2*zoom);
	}
}

//action du bouton mere du side menu
void actionOfBtnMere (){
	if(toshow->mom != NULL){
		toshow = toshow->mom; 
		cam_x = -toshow->tile_info->x*zoom + (LF/2 - toshow->tile_info->poly->width/2*zoom);
		cam_y = -toshow->tile_info->y*zoom + (HF/2 - toshow->tile_info->poly->width/2*zoom);
	}
}

//action du bouton pere du side menu
void actionOfBtnPere (){
	if(toshow->dad != NULL){
		toshow = toshow->dad; 
		cam_x = -toshow->tile_info->x*zoom + (LF/2 - toshow->tile_info->poly->width/2*zoom);
		cam_y = -toshow->tile_info->y*zoom + (HF/2 - toshow->tile_info->poly->width/2*zoom);
	}
}

//cree un nouveau sidemenu
SideMenu new_SideMenu (){
	SideMenu sm;
	RGB couleur_bt = (RGB) {40,40,40};
	(sm.bt_pere) = initBouton(200,20,"???",couleur_bt);
	(sm.bt_mere) = initBouton(200,20,"???",couleur_bt);
	(sm.bt_conjoint) = initBouton(200,20,"",couleur_bt);
	sm.bt_pere.addEventListener(&(sm.bt_pere),"click",actionOfBtnPere);
	sm.bt_mere.addEventListener(&(sm.bt_mere),"click",actionOfBtnMere);
	sm.bt_conjoint.addEventListener(&(sm.bt_conjoint),"click",actionOfBtnConjoint);
	sm.dm_enfants = new_DropMenu(200,20);
	sm.dm_fraterie = new_DropMenu(200,20);
	sm.poly_bt = malloc(sizeof(Polygone));
	*(sm.poly_bt) = initPolygone(200,20,1,50);
	sm.bt_mere.poly = sm.poly_bt; 
	sm.bt_pere.poly = sm.poly_bt;
	sm.bt_conjoint.poly = sm.poly_bt;
	sm.dm_enfants.poly = sm.poly_bt;
	sm.dm_fraterie.poly = sm.poly_bt;
	sm.ico_calendrier = new_Image("../Ressources/calendrier.png",true);
	sm.ico_place = new_Image("../Ressources/place.png",true);
	sm.img_genre[HOMME] = new_Image("../Ressources/genre_m.png",true);
	sm.img_genre[FEMME] = new_Image("../Ressources/genre_f.png",true);
	sm.M = NULL;
	sm.last_M = NULL;
	sm.last_param = NULL;
	sm.move = NULL;
	sm.x = 0;
	sm.y = 0;
	sm.reload = false;
	sm.width = 0;
	sm.height = 0; 
	return sm;
}

/**
 * @brief définit les boutons et dropmenu du side menu selon un maillon
 * 
 * @param sm 
 * @param M 
 */
void updateUISideMenu (SideMenu* sm, Maillon* M){
	char* nomPere = NULL;
	char* nomMere = NULL;
	char* nomConjoint = NULL;
	if(M->dad == NULL){
		nomPere = "";
	}
	else{
		nomPere = str_format("%s %s",M->dad->data->name[0],M->dad->data->surname);
	}
	sm->bt_pere.texte = nomPere;
	if(M->mom == NULL){
		nomMere = "";
	}
	else{
		nomMere = str_format("%s %s",M->mom->data->name[0],M->mom->data->surname);
	}
	sm->bt_mere.texte = nomMere;
	if(M->wedding == NULL){
		nomConjoint = "";
	}
	else{
		Maillon* conjoint = (M->wedding->data->maries[0] == M) ?M->wedding->data->maries[1] : M->wedding->data->maries[0];
		nomConjoint = str_format("%s %s",conjoint->data->name[0],conjoint->data->surname);
	}
	sm->bt_conjoint.texte = nomConjoint;
	sm->dm_enfants.deleteAll(&(sm->dm_enfants));
	if(M->child != NULL){
		for(int i=0;M->child[i];i++){
			char* str = str_format("%s %s",M->child[i]->data->name[0],M->child[i]->data->surname);
			sm->dm_enfants.add(&(sm->dm_enfants),str);
		}
	}
}

//actualise le sidemenu
void updateSideMenu (SideMenu* sm,Maillon* M){
	sm->M = M;
	if(sm->last_param != M){
		printdebug("chgmt param");
	}
	if(M == NULL && sm->last_param != NULL){
		printdebug("indiv to null");
		sm->reload = true;
		sm->move = malloc(sizeof(Mover));
		*(sm->move) = new_Mover(makeLine(0.75*LF,0,LF,0),false,0.5,false,false);
		activateMover(sm->move);
		assignToMover(sm->move,&(sm->x),&(sm->y));
	}
	 
	if(sm->last_M != NULL && sm->M != NULL && sm->M != sm->last_M){
		printdebug("switch indiv");
		updateUISideMenu(sm,M);
	}
	if(sm->reload && sm->move != NULL && sm->move->activated){
		sm->M = sm->last_M;
	}
	else{
		sm->reload = false;
	}
	if(sm->M != NULL){
		if(sm->last_param == NULL && !sm->reload){
			printdebug("null to indiv");
			sm->move = malloc(sizeof(Mover));
			*(sm->move) = new_Mover(makeLine(LF,0,0.75*LF,0),false,0.5,false,false);
			activateMover(sm->move);
			assignToMover(sm->move,&(sm->x),&(sm->y));
			updateUISideMenu(sm,M);
		}
		sm->x = 0.75*LF;
		sm->y = 0;
		updateMover(sm->move);
		sm->width = 0.25*LF;
		sm->height = HF;
	}
	if(sm->reload)
		sm->last_param = NULL;
	else
		sm->last_param = M;
	if(M != NULL)
		sm->last_M = M;
	sm->dm_enfants.update(&(sm->dm_enfants));
	//if(!sm->dm_enfants.isOpened){
		executeActionBouton(sm->bt_pere);
		executeActionBouton(sm->bt_mere);
		executeActionBouton(sm->bt_conjoint);
	//}
}

//affichage du sidemenu
void afficheSideMenu (SideMenu* sm){
	 
	Individu* I;
	if(sm->M == NULL || !sm->M->tile_info->isShown){
		I = NULL;
	}
	else{
	    I = sm->M->data;
	}
	 
	if(I != NULL){
		int xrel = sm->x;
		int yrel = sm->y;

		int width = 0.25*LF;
		int height = HF;

		setColor4i(0,0,0,200);
		rectangle(xrel,yrel,LF,HF);
		 
		if(I->photo != NULL){
			int photowidth = min(height*160*1./800,width*160/300);
			int photoheight = photowidth;
			int xphoto = xrel + (width)*1./2 - photowidth*1./2;
			int yphoto = height - photoheight - 57*height*1./800;
			ecrisImageTextureCustom(I->photo,xphoto,yphoto,photowidth,photoheight);
			int gensquarewidth = 20*photowidth*1./160;
			int gensquarex = xphoto + photowidth - gensquarewidth;
			int gensquarey = yphoto;
			couleurCourante(0,0,0);
			epaisseurDeTrait(1);
			rectangle(gensquarex,gensquarey,gensquarex + gensquarewidth,gensquarey + gensquarewidth);
			couleurCourante(255,255,255);
			fitTextToRectangle(str_format("%d",I->numGen),gensquarex,gensquarey,gensquarewidth,gensquarewidth);
		}
		couleurCourante(255,255,255);
		epaisseurDeTrait(3);
		fitTextToRectangle(I->name[0],xrel,height - 259*height/800,width,27*height/800);
		fitTextToRectangle(I->surname,xrel,height - 300*height/800,width,27*height/800);
		fitTextToRectangle("Sexe :",xrel + 90*width/300,height - 366*height/800,75*width/300,20*width/300);
		ecrisImageTextureCustom(sm->img_genre[*(I->gender)],xrel + 175*width/300,height - 374*height/800,45*width/300,45*width/300);
		afficheBirthData(I->birth_data,xrel,height - 490*height/800,width,100*height/800);
		afficheDeathData(I->death_data,xrel,height - 605*height/800,width,100*height/800);
		afficheBouton(xrel,yrel,&(sm->bt_pere));
		afficheBouton(xrel,yrel+25,&(sm->bt_mere));
		afficheBouton(xrel,yrel+50,&(sm->bt_conjoint));
		sm->dm_enfants.show(&(sm->dm_enfants),xrel,70);
	}
	 
}

//retourne si l un des bouton ou clickmenu est survolé
bool optionHoveredSideMenu (SideMenu* sm){
	//println("%s", (pointIsInRectangle(AS,OS,sm->bt_pere.x,sm->bt_pere.y,sm->bt_pere.width,sm->bt_pere.height) || sm->bt_mere.hover || sm->bt_conjoint.hover) ? "true" : "false" );
	return 	pointIsInRectangle(AS,OS,sm->bt_pere.x,sm->bt_pere.y,sm->bt_pere.width,sm->bt_pere.height) ||
			pointIsInRectangle(AS,OS,sm->bt_mere.x,sm->bt_mere.y,sm->bt_mere.width,sm->bt_mere.height) ||
			pointIsInRectangle(AS,OS,sm->bt_conjoint.x,sm->bt_conjoint.y,sm->bt_conjoint.width,sm->bt_conjoint.height) ||
			pointIsInRectangle(AS,OS,sm->dm_enfants.x,sm->dm_enfants.y,sm->dm_enfants.width,sm->dm_enfants.height) ;
}

/**
 * @brief 
 * Affichage d'un individu sur la map en fonction de ses coordonées X,Y 
 * Avec un Pin et la photo en 32x32 de l individu
 * |param I : Individu a afficher
 */
void afficheIndividuMap (Individu* I,Image* map){
    if(pin == NULL){
        pin = new_Image("../Ressources/Map/pin.png",true);
    }
	int posx;
	couleurCourante(255,0,0);
	epaisseurDeTrait(17);
	for(posx = cam_x;posx < LF;posx+=map->static_width){
        int x = map->static_width/2 + (map->static_width/2)* I->birth_data->lieu.X*1. /180 + posx;
        int y = map->static_height/2 + (map->static_height/2)* I->birth_data->lieu.Y*1. /90 + cam_y;
        if(pointIsInRectangle(x,y,0,0,LF,HF)){
			int a = I->photo->static_width;
			int b = I->photo->static_height;
			I->photo->static_height = 32;
			I->photo->static_width = 32;
            ecrisImageTexture(I->photo,x - pin->static_width/2 + 8,y + 34);
            ecrisImageTexture(pin,x - pin->static_width/2 ,y);
			I->photo->static_height = b;
			I->photo->static_width = a;
        }
	}
	for(posx = cam_x - map->static_width;posx > -map->static_width;posx-=map->static_width){
		int x = map->static_width/2 + (map->static_width/2)* I->birth_data->lieu.X*1. /180 + posx;
        int y = map->static_height/2 + (map->static_height/2)* I->birth_data->lieu.Y*1. /90 + cam_y;
        if(pointIsInRectangle(x,y,0,0,LF,HF)){
			int a = I->photo->static_width;
			int b = I->photo->static_height;
			I->photo->static_height = 32;
			I->photo->static_width = 32;
            ecrisImageTexture(I->photo,x - pin->static_width/2 + 8,y + 34);
            ecrisImageTexture(pin,x - pin->static_width/2 ,y);
			I->photo->static_height = b;
			I->photo->static_width = a;
        }
	}
}

/**
 * @brief 
 * Création d'une Tile Individu
 * |param I : Individu sur lequel la tile est basée
 * |param x : Coordonnée x ou l'individu est affiché
 * |param y : Coordonnée y ou l'individu est affiché
 * |return IndividuTile
 */
IndividuTile* new_IndividuTile (int x,int y,Polygone* poly,Polygone* shadow){
	IndividuTile* IT = malloc(sizeof(IndividuTile));
	*IT = (IndividuTile) {x,y,true,poly,shadow};
	return IT;
}

/**
 * @brief 
 * affichage d'une tile individu
 * @param IT 
 */
void afficheIndividuTile (Maillon* M){
	IndividuTile IT = *(M->tile_info);
	Polygone poly = *(IT.poly);
	Polygone polyshadow = *(IT.shadow);
	int xrel = IT.x*zoom + cam_x;
	int yrel = IT.y*zoom + cam_y;
	if(xrel > -poly.width*zoom && xrel < LF && yrel > -poly.height*zoom && yrel < HF){
		couleurCourante(0,0,0);
		dessinePolygoneZoom(polyshadow,xrel - 2,yrel -2,zoom);
		unsigned char r=Menu_Settings.button_settings[2].couleur.r;
		unsigned char g=Menu_Settings.button_settings[2].couleur.g;
		unsigned char b=Menu_Settings.button_settings[2].couleur.b;
	
		if(Menu_Settings.light_mode == LIGHT){
				couleurCourante(r,g,b);
		}
		else{
				couleurCourante(r,g,b);
		}
		dessinePolygoneZoom(poly,xrel,yrel,zoom);
		if(M->data->photo != NULL)
			ecrisImageTextureCustom(M->data->photo,xrel+poly.width/2*zoom - M->data->photo->static_width/2*zoom,yrel+poly.height/2*zoom,M->data->photo->static_width*zoom,M->data->photo->static_height*zoom);
		couleurCourante(0,0,0);
		epaisseurDeTrait(3*zoom);
		if(zoom > 0.2){
			afficheChaine(M->data->name[0],27*zoom,xrel - tailleChaine(M->data->name[0],27*zoom)/2 + poly.width/2*zoom,yrel + 160*zoom);
			afficheChaine(M->data->surname,27*zoom,xrel - tailleChaine(M->data->surname,27*zoom)/2 + poly.width/2*zoom,yrel + 120*zoom);
			couleurCourante(0,0,255);
			if(M->data->birth_data->lieu.nom != NULL){
				char lieu[100];
				sprintf(lieu,"De %s",M->data->birth_data->lieu.nom);
				fitTextToRectangle(lieu,xrel,yrel+35*zoom,300*zoom,29*zoom);
			}
		}
	}
}

Maillon* getLiaisonMaillon (Maillon* maille){
	Maillon* ret = NULL;
	bool sexe = *(maille->data->gender);
	if(maille->wedding != NULL){
		if(sexe == 0){ //HOMME
			if(maille->wedding->data->maries[1] && maille->wedding->data->maries[1]->tile_info->isShown)
				ret = maille->wedding->data->maries[1];
		}
		if(sexe == 1){ //FEMME
			if(maille->wedding->data->maries[1] && maille->wedding->data->maries[1]->tile_info->isShown)
				ret = maille->wedding->data->maries[0];
		}
	}
	else{
		if(sexe == 0){
			if(maille->child != NULL){
				for(int i = 0;maille->child[i] != NULL;i++){
					if(maille->child[i]->mom != NULL && maille->child[i]->mom->tile_info->isShown ){
						ret = maille->child[i]->mom;
					}
				}
			}
		}
		if(sexe == 1){
			if(maille->child != NULL){
				for(int i = 0;maille->child[i] != NULL;i++){
					if(maille->child[i]->dad != NULL && maille->child[i]->dad->tile_info->isShown){
						ret = maille->child[i]->dad;
					}
				}
			}
		}
	}
	return ret;
}

// ANCIENNE FACON DE RENDER L ARBRE
/*
void actualisePositionTiles (Maillon* ptrTete){
	int cpt = 0;
	int cptx = 0;
	int last_gen = 0;
	int max = goToLC(ptrTete,countLC(ptrTete)-1)->data->numGen;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->data->numGen != last_gen){
			cptx = 0;
		}
		tmp->tile_info->x = cptx*600;
		tmp->tile_info->y = -600*(max-tmp->data->numGen);
		cpt++;
		cptx++;
		last_gen = tmp->data->numGen;
		afficheIndividuTile(tmp);
	}
}*/

/*
void actualisePositionTiles (Maillon* ptrTete){
	int cptx = 0;
	int last_gen = 0;
	int lastx = 0;
	int xmax = 0;
	int max = goToLC(ptrTete,countLC(ptrTete)-1)->data->numGen;
				for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
					if(tmp->data->numGen != last_gen){
						cptx = 0;
						xmax = 0;
					}
					if(tmp->data->numGen == max){	//premiere gen
						if(*(tmp->data->gender) == 0 ){
							tmp->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
							cptx++;
							if(tmp->child != NULL && tmp->child[0]->mom != NULL){
								tmp->child[0]->mom->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
								cptx++;
							}
						}
						else{
							if(tmp->child == NULL || tmp->child[0]->dad == NULL){
								tmp->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
								cptx++;
							}
						}
					}
					else{	//pas la premiere gen
						if(tmp->dad != NULL){
							if(*(tmp->data->gender) == 0){
								tmp->tile_info->x = tmp->dad->tile_info->x + (tmp->tile_info->poly->width + 300)/2;
								if(tmp->child != NULL && tmp->child[0]->mom != NULL){
									tmp->child[0]->mom->tile_info->x = tmp->tile_info->x + 600;
								}
							}
							else{
								if(tmp->tile_info->x == -300){
									tmp->tile_info->x = tmp->dad->tile_info->x + (tmp->tile_info->poly->width + 300)/2 + 600;
									if(tmp->child != NULL && tmp->child[0]->dad != NULL){
										tmp->child[0]->dad->tile_info->x = tmp->tile_info->x - 600;
									}
								}
								else{
									
								}
							}
						}
						else{
							if(tmp->tile_info->x == -300){
								tmp->tile_info->x = lastx + cptx*(tmp->tile_info->poly->width + 300);
								cptx++;
							}
						}
					}
					last_gen = tmp->data->numGen;
					if(tmp->tile_info->x > xmax){
						xmax = tmp->tile_info->x;
					}
					if(tmp->next != NULL && tmp->next->data->numGen != last_gen){
						lastx = xmax;
					}
					afficheIndividuTile(tmp);
				}
				println("x = %d",superRechercheMaillon(creeCritere(3,"Beaulieu","Mathilde",NULL,NULL,NULL,NULL,NULL,"="),ptrTete)[0]->tile_info->x);
			}
//*/
//*

//sous fct de actualisePositionTiles()
int getPosxDad (Maillon* m){ 
	if(m->dad != NULL){
		return m->dad->tile_info->x;
	}
	else {
		if(m->mom != NULL){
			return m->mom->tile_info->x;
		}
		else{
			printerr("PROBLEME fc positionTiles pour indiv %s",m->data->name[0]);
			return 0;
		}
	}
}

//sous fct de actualisePositionTiles()
int getPosxMom (Maillon* m){ 
	if(m->mom != NULL){
		return m->mom->tile_info->x;
	}
	else {
		if(m->dad != NULL){
			return m->dad->tile_info->x;
		}
		else{
			printerr("PROBLEME fc positionTiles pour indiv %s",m->data->name[0]);
			return 0;
		}
	}
}

int getLastXGen (Maillon* ptrTete,int gen){
	int max = 0;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->data->numGen > gen){
			break;
		}
		if(tmp->data->numGen == gen && tmp->tile_info->x > max){
			max = tmp->tile_info->x;
		}
	}
	return max;
}

Maillon** initLcArbreAsc(Maillon* prs, int n, int const repere){
	
	Maillon** arrayArbre = malloc(sizeof(Maillon*)*2);
	arrayArbre[0] = prs;
	arrayArbre[1] = NULL;
	int size = 2;
	if(n > 0){
		if(prs->wedding){
			MaillonMariage* currentMariage = prs->wedding;
			while(currentMariage->next){
				currentMariage = currentMariage->next;
			}
			if(*(prs->data->gender) == HOMME && currentMariage->data->maries[1]){
				arrayArbre[size-1] = currentMariage->data->maries[0];
				size++;
				arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
				arrayArbre[size-1] = NULL;
				
			}
			else if(*(prs->data->gender) == FEMME && currentMariage->data->maries[0]){
				arrayArbre[size-1] = currentMariage->data->maries[0];
				size++;
				arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
				arrayArbre[size-1] = NULL;
			}
		}

		if(prs->sibling && n != repere){
			for(int i = 0; prs->sibling[i];i++){
				arrayArbre[size-1] = prs->sibling[i];
				size++;
				arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
				arrayArbre[size-1] = NULL;
			}
		}

		if(prs->mom){
			Maillon** arrayTmp = initLcArbreAsc(prs->mom, n-1,repere);
			
			for(int i = 0; arrayTmp[i]; i++){
				arrayArbre[size-1] = arrayTmp[i];
				size++;
				arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
				arrayArbre[size-1] = NULL;
			}
			
			
		}
		if(prs->dad){
			Maillon** arrayTmp = initLcArbreAsc(prs->dad, n-1,repere);

			for(int i = 0; arrayTmp[i]; i++){
				arrayArbre[size-1] = arrayTmp[i];
				size++;
				arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
				arrayArbre[size-1] = NULL;
			}

		}
	}
	return supprDoublons(arrayArbre);

}

Maillon** initLcArbreDesc(Maillon* prs, int n, int const repere){
	
	Maillon** arrayArbre = malloc(sizeof(Maillon*)*2);
	arrayArbre[0] = prs;
	arrayArbre[1] = NULL;
	int size = 2;
	if(n > 0){
		if(prs->wedding){
			MaillonMariage* currentMariage = prs->wedding;
			while(currentMariage->next){
				currentMariage = currentMariage->next;
			}
			if(*(prs->data->gender) == HOMME && currentMariage->data->maries[1]){
				arrayArbre[size-1] = currentMariage->data->maries[0];
				size++;
				arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
				arrayArbre[size-1] = NULL;
				
			}
			else if(*(prs->data->gender) == FEMME && currentMariage->data->maries[0]){
				arrayArbre[size-1] = currentMariage->data->maries[0];
				size++;
				arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
				arrayArbre[size-1] = NULL;
			}
		}

		if(prs->sibling){
			for(int i = 0; prs->sibling[i];i++){
				arrayArbre[size-1] = prs->sibling[i];
				size++;
				arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
				arrayArbre[size-1] = NULL;
			}
		}

		if(prs->child){

			for(int i = 0; prs->child[i];i++){
				Maillon** arrayTmp = initLcArbreAsc(prs->child[i], n-1,repere);
				for(int j = 0; arrayTmp[j];j++){
					arrayArbre[size-1] = arrayTmp[j];
					size++;
					arrayArbre = realloc(arrayArbre,size*sizeof(Maillon*));
					arrayArbre[size-1] = NULL;
				}

			}
			
			
		}
	}
	return supprDoublons(arrayArbre);

}


//TODO Gerer Pos Fraterie
/* void actualisePositionTiles (Maillon* ptrTete){
	int cptx = 0;
	int last_gen = 0;
	int lastx = 0;
	int max = goToLC(ptrTete,countLC(ptrTete)-1)->data->numGen;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->tile_info->isShown){
			Maillon* liaison = getLiaisonMaillon(tmp);
			bool sexe = *(tmp->data->gender);
			bool has_parent = (tmp->dad != NULL || tmp->mom!= NULL);
			bool liaison_has_parent = (liaison != NULL && (liaison->mom != NULL || liaison->dad != NULL));
			if(tmp->data->numGen != last_gen){
				cptx = 0;
			}
			if(tmp->data->numGen == max){	//premiere gen
				if(*(tmp->data->gender) == 0){	//homme
					tmp->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
					cptx++;
					if(liaison != NULL){
						liaison->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
						cptx++;
					}
				}
				else{	//femme
					if(liaison == NULL){	
						tmp->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
							cptx++;
					}
				}
			}
			else{	//pas la premiere gen
				printdebug("");
				if(!has_parent && liaison == NULL){
					//tmp->tile_info->x = lastx + cptx*(tmp->tile_info->poly->width + 300);
					tmp->tile_info->x = -8888;
				}
				printdebug("");
				if(!has_parent && liaison != NULL && !liaison_has_parent){
					//tmp->tile_info->x = lastx + cptx*(tmp->tile_info->poly->width + 300);
					//liaison->tile_info->x = tmp->tile_info->x + 600;
					tmp->tile_info->x = -8888;
					liaison->tile_info->x = -8888;
				}
				printdebug("");
				if(!has_parent && liaison_has_parent){
					int posmom = getPosxMom(liaison);
					int posdad = getPosxDad(liaison);
					int x = (posdad + posmom)/2;
					if(sexe == 0){
						tmp->tile_info->x = x -300;
						liaison->tile_info->x = x +300;
					}
					else{
						tmp->tile_info->x = x +300;
						liaison->tile_info->x = x -300;
					}
				}
				printdebug("");
				if(has_parent && liaison == NULL){
					int posmom = getPosxMom(tmp);
					int posdad = getPosxDad(tmp);
					int x = (posdad + posmom)/2;
					tmp->tile_info->x = x;
				}
				printdebug("");
				if(has_parent && liaison != NULL && !liaison_has_parent){
					int posmom = getPosxMom(tmp);
					int posdad = getPosxDad(tmp);
					int x = (posdad + posmom)/2;
					tmp->tile_info->x = x;
					liaison->tile_info->x = x+600;
				}
				printdebug("");
				if(tmp->dad != NULL && liaison_has_parent){
					int posmom = getPosxMom(tmp);
					int posdad = getPosxDad(tmp);
					int posmom2 = getPosxMom(liaison);
					int posdad2 = getPosxDad(liaison);
					int x1 = (posdad2 + posmom2)/2;
					int x2 = (posdad + posmom)/2;
					int x = (x1+x2)/2;
					if(x1 > x2){
						//println("%s avant %s",tmp->data->name[0],liaison->data->name[0]);
						tmp->tile_info->x = x -300;
						if(liaison->tile_info->isShown)
							liaison->tile_info->x = x +300;
					}
					else{
						//println("%s apres %s",tmp->data->name[0],liaison->data->name[0]);
						tmp->tile_info->x = x +600;
						if(liaison->tile_info->isShown)
							liaison->tile_info->x = x -600;
					}
				}
				printdebug("");
			}
			last_gen = tmp->data->numGen;
		}
	}
	cptx = 0;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->tile_info->x == -8888){
			lastx = getLastXGen(ptrTete,tmp->data->numGen);
			Maillon* liaison = getLiaisonMaillon(tmp);
			bool sexe = *(tmp->data->gender);
			bool has_parent = (tmp->dad != NULL || tmp->mom!= NULL);
			bool liaison_has_parent = (liaison != NULL && (liaison->mom != NULL || liaison->dad != NULL));
			if(!has_parent && liaison == NULL){
				tmp->tile_info->x = lastx + 600;
			}
			printdebug("");
			if(!has_parent && liaison != NULL && !liaison_has_parent){
				tmp->tile_info->x = lastx + 600;
				liaison->tile_info->x = lastx + 1200;
			}
		}
	}
}*/
//*/

void actualisePositionTiles (Maillon* ptrTete){
	int cptx = 0;
	int last_gen = 0;
	int lastx = 0;
	int max = goToLC(ptrTete,countLC(ptrTete)-1)->data->numGen;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		Maillon* liaison = getLiaisonMaillon(tmp);
		bool sexe = *(tmp->data->gender);
		bool has_parent = (tmp->dad != NULL || tmp->mom!= NULL);
		bool liaison_has_parent = (liaison != NULL && (liaison->mom != NULL || liaison->dad != NULL));
		if(tmp->data->numGen != last_gen){
			cptx = 0;
		}
		if(tmp->data->numGen == max){	//premiere gen
			if(*(tmp->data->gender) == 0){	//homme
				tmp->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
				cptx++;
				if(liaison != NULL){
					liaison->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
					cptx++;
				}
			}
			else{	//femme
				if(liaison == NULL){	
					tmp->tile_info->x = cptx*(tmp->tile_info->poly->width + 300);
						cptx++;
				}
			}
		}
		else{	//pas la premiere gen
			printdebug("");
			if(!has_parent && liaison == NULL){
				//tmp->tile_info->x = lastx + cptx*(tmp->tile_info->poly->width + 300);
				tmp->tile_info->x = -8888;
			}
			printdebug("");
			if(!has_parent && liaison != NULL && !liaison_has_parent){
				//tmp->tile_info->x = lastx + cptx*(tmp->tile_info->poly->width + 300);
				//liaison->tile_info->x = tmp->tile_info->x + 600;
				tmp->tile_info->x = -8888;
				liaison->tile_info->x = -8888;
			}
			printdebug("");
			if(!has_parent && liaison_has_parent){
				int posmom = getPosxMom(liaison);
				int posdad = getPosxDad(liaison);
				int x = (posdad + posmom)/2;
				if(sexe == 0){
					tmp->tile_info->x = x -300;
					liaison->tile_info->x = x +300;
				}
				else{
					tmp->tile_info->x = x +300;
					liaison->tile_info->x = x -300;
				}
			}
			printdebug("");
			if(has_parent && liaison == NULL){
				int posmom = getPosxMom(tmp);
				int posdad = getPosxDad(tmp);
				int x = (posdad + posmom)/2;
				tmp->tile_info->x = x;
			}
			printdebug("");
			if(has_parent && liaison != NULL && !liaison_has_parent){
				int posmom = getPosxMom(tmp);
				int posdad = getPosxDad(tmp);
				int x = (posdad + posmom)/2;
				tmp->tile_info->x = x;
				liaison->tile_info->x = x+600;
			}
			printdebug("");
			if(tmp->dad != NULL && liaison_has_parent){
				int posmom = getPosxMom(tmp);
				int posdad = getPosxDad(tmp);
				int posmom2 = getPosxMom(liaison);
				int posdad2 = getPosxDad(liaison);
				int x1 = (posdad2 + posmom2)/2;
				int x2 = (posdad + posmom)/2;
				int x = (x1+x2)/2;
				if(x1 > x2){
					//println("%s avant %s",tmp->data->name[0],liaison->data->name[0]);
					tmp->tile_info->x = x -300;
					liaison->tile_info->x = x +300;
				}
				else{
					//println("%s apres %s",tmp->data->name[0],liaison->data->name[0]);
					tmp->tile_info->x = x +300;
					liaison->tile_info->x = x -300;
				}
			}
			printdebug("");
		}
		last_gen = tmp->data->numGen;
	}
	cptx = 0;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(tmp->tile_info->x == -8888){
			lastx = getLastXGen(ptrTete,tmp->data->numGen);
			Maillon* liaison = getLiaisonMaillon(tmp);
			bool has_parent = (tmp->dad != NULL || tmp->mom!= NULL);
			bool liaison_has_parent = (liaison != NULL && (liaison->mom != NULL || liaison->dad != NULL));
			if(!has_parent && liaison == NULL){
				tmp->tile_info->x = lastx + 600;
			}
			printdebug("");
			if(!has_parent && liaison != NULL && !liaison_has_parent){
				tmp->tile_info->x = lastx + 600;
				liaison->tile_info->x = lastx + 1200;
			}
		}
	}
}

/**
 * @brief
 * Change le curseur linux sur celui approprié selon le hover
 */
void setAppropriateCursor (){
	//TODO adapter cette fonc pour la map et les textfields, boutons, et main menu
	bool hover = false;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		IndividuTile *IT = tmp->tile_info; 
		if(tmp->tile_info->isShown && AS >= IT->x*zoom+cam_x && AS <= IT->x*zoom+cam_x + IT->poly->width*zoom && OS >= IT->y*zoom+cam_y && OS <= IT->y*zoom+cam_y + IT->poly->height*zoom){
			hover = true;
			break; 		//Si c est ca je me casse !
		}
	}
	if(grab){
		glutSetCursor(GLUT_CURSOR_CROSSHAIR);
	}
	else{
		if(hover)
			glutSetCursor(GLUT_CURSOR_INFO);
		else
			glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
	}
}

/**
 * @brief 
 * redefinit la taille de toutes les images des individus de la LC
 * |param ptrtete : tete de la LC
 * |param size  : nouvelle taille des images
 */
void resizePhotoLC (Maillon* ptrtete,int size){
	for(Maillon* ptr = ptrtete; ptr != NULL;ptr = ptr->next){
		if(ptr->data->photo != NULL){
			float zoom = size*1. / ptr->data->photo->width;
			ptr->data->photo->zoom = zoom;
			ptr->data->photo->static_width = zoom*ptr->data->photo->width;
			ptr->data->photo->static_height = zoom*ptr->data->photo->height;
		}
		//ptr->data->photo = resizeDonneesImageRGB(&(ptr->data->photo),size);
	}
}


/**
 * @brief
 * action pour revenir au menu principal
 */
void actionOfBack (){
	if((state == tree_view || state == network_view || state == fractals_view) && RechGFX.isShown){
		if(RechGFX.showResult){
			RechGFX.showResult = false;
			RechGFX.result = NULL;
			zoom = 1;
		}
		else{
			RechGFX.isShown = false;
			zoom = 1;
		}
	}
	else{
		state = main_menu;
		toshow = NULL;
		cam_x = cam_y = 0;
		zoom = 1;
		show_setting = false;
		glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
		grab = false;
	}
}

/**
 * @brief
 * action pour ouvrir le menu setting à l'aide de movers
 */
void actionOfMenuSettings (){
	mov_bt1 = new_Mover(makeLine(menu_button[0].x,menu_button[0].y,-menu_button[0].width,menu_button[0].y),false,0.7,true,false);
	mov_bt2 = new_Mover(makeLine(menu_button[1].x,menu_button[1].y,-menu_button[1].width,menu_button[1].y),false,0.7,true,false);
	mov_bt3 = new_Mover(makeLine(menu_button[2].x,menu_button[2].y,-menu_button[2].width,menu_button[2].y),false,0.7,true,false);
	mov_bt4 = new_Mover(makeLine(menu_button[3].x,menu_button[3].y,-menu_button[3].width,menu_button[3].y),false,0.7,true,false);
	mov_bt5 = new_Mover(makeLine(menu_button[4].x,menu_button[4].y,-menu_button[4].width,menu_button[4].y),false,0.7,true,false);
	mov_bt6 = new_Mover(makeLine(menu_button[5].x,menu_button[5].y,-menu_button[5].width,menu_button[5].y),false,0.7,true,false);
	Menu_Settings.mov_poly_setting=new_Mover(makeLine(LF,Menu_Settings.pos_settings.y,(LF/2)-Menu_Settings.polysetting.width/2,Menu_Settings.pos_settings.y),false,0.7,true,false);
	Menu_Settings.mov_poly_fichier=new_Mover(makeLine(LF,Menu_Settings.pos_settings.y,(LF/2)-Menu_Settings.polysetting.width/2,Menu_Settings.pos_settings.y),false,0.7,true,false);
	assignToMover(&mov_bt1,&menu_button[0].x,&menu_button[0].y);
	assignToMover(&mov_bt2,&menu_button[1].x,&menu_button[1].y);
	assignToMover(&mov_bt3,&menu_button[2].x,&menu_button[2].y);
	assignToMover(&mov_bt4,&menu_button[3].x,&menu_button[3].y);
	assignToMover(&mov_bt5,&menu_button[4].x,&menu_button[4].y);
	assignToMover(&mov_bt6,&menu_button[5].x,&menu_button[5].y);
	assignToMover(&Menu_Settings.mov_poly_setting,&(Menu_Settings.pos_settings.x),&(Menu_Settings.pos_settings.y));
	assignToMover(&Menu_Settings.mov_poly_fichier,&(Menu_Settings.pos_settings.x),&(Menu_Settings.pos_settings.y));
	activateMover(&mov_bt1);
	activateMover(&mov_bt2);
	activateMover(&mov_bt3);	
	activateMover(&mov_bt4);
	activateMover(&mov_bt5);
	activateMover(&mov_bt6);
	activateMover(&Menu_Settings.mov_poly_setting);
	activateMover(&Menu_Settings.mov_poly_fichier);
	show_setting=true;
}

/**
 * @brief
 * action pour fermer le menu setting à l'aide de movers
 */
void actionOfCloseSettings(){
	mov_bt1=new_Mover(makeLine(-menu_button[0].width,menu_button[0].y,LF/2 - 300,menu_button[0].y),false,0.7,true,false);
	mov_bt2=new_Mover(makeLine(-menu_button[1].width,menu_button[1].y,LF/2 - menu_button[1].width/2,menu_button[1].y),false,0.7,true,false);
	mov_bt3=new_Mover(makeLine(-menu_button[2].width,menu_button[2].y,LF/2 - menu_button[2].width/2,menu_button[2].y),false,0.7,true,false);
	mov_bt4=new_Mover(makeLine(-menu_button[3].width,menu_button[3].y,LF/2 - menu_button[3].width/2,menu_button[3].y),false,0.7,true,false);
	mov_bt5=new_Mover(makeLine(-menu_button[4].width,menu_button[4].y,LF/2 + 100,menu_button[4].y),false,0.7,true,false);
	mov_bt6=new_Mover(makeLine(-menu_button[5].width,menu_button[5].y,LF/2 - 100,menu_button[5].y),false,0.7,true,false);
	Menu_Settings.mov_poly_setting=new_Mover(makeLine((LF/2)-Menu_Settings.polysetting.width/2,Menu_Settings.pos_settings.y,LF,Menu_Settings.pos_settings.y),false,0.7,true,false);
	Menu_Settings.mov_poly_fichier=new_Mover(makeLine((LF/2)-Menu_Settings.polysetting.width/2,Menu_Settings.pos_settings.y,LF,Menu_Settings.pos_settings.y),false,0.7,true,false);
	assignToMover(&mov_bt1,&menu_button[0].x,&menu_button[0].y);
	assignToMover(&mov_bt2,&menu_button[1].x,&menu_button[1].y);
	assignToMover(&mov_bt3,&menu_button[2].x,&menu_button[2].y);
	assignToMover(&mov_bt4,&menu_button[3].x,&menu_button[3].y);
	assignToMover(&mov_bt5,&menu_button[4].x,&menu_button[4].y);
	assignToMover(&mov_bt6,&menu_button[5].x,&menu_button[5].y);
	assignToMover(&Menu_Settings.mov_poly_setting,&(Menu_Settings.pos_settings.x),&(Menu_Settings.pos_settings.y));
	assignToMover(&Menu_Settings.mov_poly_fichier,&(Menu_Settings.pos_settings.x),&(Menu_Settings.pos_settings.y));
	activateMover(&mov_bt1);
	activateMover(&mov_bt2);
	activateMover(&mov_bt3);
	activateMover(&mov_bt4);
	activateMover(&mov_bt5);
	activateMover(&mov_bt6);
	activateMover(&Menu_Settings.mov_poly_setting);
	activateMover(&Menu_Settings.mov_poly_fichier);

	actionOfBack();
}

/**
 * @brief
 * action pour afficher l'explorateur de fichier pour enregistrer un fichier Individu
 */
void actionOfAjoutFichierIndv(){
	Menu_Settings.EX.isShown=true;
	Menu_Settings.ajout_fichier=0;
}

/**
 * @brief
 * action pour afficher l'explorateur de fichier pour enregistrer un fichier Mariage
 */
void actionOfAjoutFichierMar(){
	Menu_Settings.EX.isShown=true;
	Menu_Settings.ajout_fichier=1;
}

/**
 * @brief
 * action pour afficher l'explorateur de fichier pour enregistrer un ficher pour la Carte
 */ 
void actionOfAjoutCarte(){
	Menu_Settings.EX.isShown=true;
	Menu_Settings.ajout_fichier=2;
}
/**
 * @brief
 * Permet de gérer le fichier choisis en le plaçant dans le dossier CSV ou Map di c'est une carte
 * en effectuant toutes les vérifications
 */
void actionOfExploFichier (){
	if(Menu_Settings.ajout_fichier==0){
		println("Ajout Indv");
		if(!strcmp(fileGetExtension(Menu_Settings.EX.selected_file.nom),"csv")){
			if(!(fichierExistant(Menu_Settings.EX.selected_file.nom,Menu_Settings.fichierIndv))){
				char* fullPath = str_format("%s/%s",Menu_Settings.EX.currentDir,Menu_Settings.EX.selected_file.nom);
				char command[200];
				strcpy(command,"cp ");
				strcat(command,fullPath);
				strcat(command," ../CSV/");
				strcat(command,Menu_Settings.EX.selected_file.nom);
				system(command);

				if(numerofichierind==9){
					numerofichierind=0;
				}
				strcpy(Menu_Settings.fichierIndv[numerofichierind],Menu_Settings.EX.selected_file.nom);
				numerofichierind++;

					
				println("%s \n%s \n%s",Menu_Settings.EX.selected_file.nom,Menu_Settings.EX.selected_file.fullInfo,Menu_Settings.EX.selected_file.taille);;
				println("Full file location : '%s'",fullPath);
				println("c'est un fichier de type '%s'",fileGetExtension(Menu_Settings.EX.selected_file.nom));
			}
			else{
				println("Fichier Deja Ajouté !");
			}
		}
		else{
			println("Ce n'est pas un fichier CSV");
		}
	}
	else if(Menu_Settings.ajout_fichier==1){
		println("Ajout Mari");
		if(!strcmp(fileGetExtension(Menu_Settings.EX.selected_file.nom),"csv")){
			if(!(fichierExistant(Menu_Settings.EX.selected_file.nom,Menu_Settings.fichierMar))){
				char* fullPath = str_format("%s/%s",Menu_Settings.EX.currentDir,Menu_Settings.EX.selected_file.nom);
				char command[200];
				strcpy(command,"cp ");
				strcat(command,fullPath);
				strcat(command," ../CSV/");
				strcat(command,Menu_Settings.EX.selected_file.nom);
				system(command);

				if(numerofichiermari==9){
					numerofichiermari=0;
				}
				strcpy(Menu_Settings.fichierMar[numerofichiermari],Menu_Settings.EX.selected_file.nom);
				numerofichiermari++;

				println("%s \n%s \n%s",Menu_Settings.EX.selected_file.nom,Menu_Settings.EX.selected_file.fullInfo,Menu_Settings.EX.selected_file.taille);
				println("Full file location : '%s'",fullPath);
				println("c'est un fichier de type '%s'",fileGetExtension(Menu_Settings.EX.selected_file.nom));
			}
			else{
				println("Fichier Deja Ajouté !");
			}
		}
		else{
			println("Ce n'est pas un fichier CSV");
		}
	}
	else if(Menu_Settings.ajout_fichier==2){
		println("Ajout Carte");
		if(!strcmp(fileGetExtension(Menu_Settings.EX.selected_file.nom),"bmp")){
			char* fullPath = str_format("%s/%s",Menu_Settings.EX.currentDir,Menu_Settings.EX.selected_file.nom);
			char command[100];
			strcpy(command,"cp ");
			strcat(command,fullPath);
			strcat(command," ../Ressources/Map/");
			strcat(command,Menu_Settings.EX.selected_file.nom);
			system(command);
			println("%s \n%s \n%s",Menu_Settings.EX.selected_file.nom,Menu_Settings.EX.selected_file.fullInfo,Menu_Settings.EX.selected_file.taille);
			println("Full file location : '%s'",fullPath);
			println("c'est un fichier de type '%s'",fileGetExtension(Menu_Settings.EX.selected_file.nom));
		}
		else{
		println("Ce n'est pas un fichier BMP");
		}
	
	}
}
/**
 * @brief
 * Permet de savoir si le fichier a été deja ajouter auparavant
 * @param : char * nomfichier => nom du fichier
 * @param : char ** Tfichier => tableau des chaines de fichier déja enregistré
 * @return : true si il existe déja sinon false
 */
bool fichierExistant(char * nomfichier,char **Tfichier){

	for(int i=0;i<9;i++){
		if(!strcmp(nomfichier,Tfichier[i])){
			return true;
		}
	}
	return false;
}
/**
 * @brief
 * Permet de recharger le fichier choisis par l'utilisateur.
 */
void actionOfSelectFichier(){
	
	for(int i =0;i<9;i++){
		if(pointIsInRectangle(AS,OS,Menu_Settings.button_fichierI[i].x,Menu_Settings.button_fichierI[i].y,Menu_Settings.button_fichierI[i].width,Menu_Settings.button_fichierI[i].height)){
			println("appuyé sur boutonI %d",i);
			Menu_Settings.button_fichierI[i].couleur=(RGB){200,200,200};
			Menu_Settings.button_fichierI[Menu_Settings.ligneselectI].couleur=(RGB){69,69,69};
			databaseQuery(str_format("./exec UPDATE chosen = 1 IN ../Ressources/Bdd/indivcsv.osql WHERE name = %s",Menu_Settings.fichierIndv[i]));
			databaseQuery(str_format("./exec UPDATE chosen = 0 IN ../Ressources/Bdd/indivcsv.osql WHERE name = %s",Menu_Settings.fichierIndv[Menu_Settings.ligneselectI]));
			Menu_Settings.ligneselectI=i;

		}
		if(pointIsInRectangle(AS,OS,Menu_Settings.button_fichierM[i].x,Menu_Settings.button_fichierM[i].y,Menu_Settings.button_fichierM[i].width,Menu_Settings.button_fichierM[i].height)){
			println("appuyé sur boutonM %d",i);
			Menu_Settings.button_fichierM[i].couleur=(RGB){200,200,200};
			Menu_Settings.button_fichierM[Menu_Settings.ligneselectM].couleur=(RGB){69,69,69};
			databaseQuery(str_format("./exec UPDATE chosen = 1 IN ../Ressources/Bdd/mariagecsv.osql WHERE name = %s",Menu_Settings.fichierMar[i]));
			databaseQuery(str_format("./exec UPDATE chosen = 0 IN ../Ressources/Bdd/mariagecsv.osql WHERE name = %s",Menu_Settings.fichierMar[Menu_Settings.ligneselectM]));
			Menu_Settings.ligneselectM=i;	
		}
	}

}

/**
 * @brief
 * action pour afficher le picker couleur pour le bouton Individu
 */
void actionOfCouleurIndividu(){
	Menu_Settings.indexcolorbtn = 2;
	Menu_Settings.show_picker = true;
}

/**
 * @brief
 * action pour afficher le picker couleur pour le bouton Parent
 */
void actionOfCouleurParent(){
	Menu_Settings.indexcolorbtn = 3;
	Menu_Settings.show_picker = true;
}

/**
 * @brief
 * action pour afficher le picker couleur pour le bouton Mariage
 */
void actionOfCouleurMariage(){
	Menu_Settings.indexcolorbtn = 4;
	Menu_Settings.show_picker = true;
}

/**
 * @brief
 * action pour afficher le picker couleur pour le bouton Fraterie
 */
void actionOfCouleurFraterie(){
	Menu_Settings.indexcolorbtn = 5;
	Menu_Settings.show_picker = true;
}

/**
 * @brief
 * Action pour le changement de couleur des boutons lors de la validation du picker
 */
void actionOfPicker(){
	Menu_Settings.button_settings[Menu_Settings.indexcolorbtn].couleur = Menu_Settings.picker.selection;
	char colname[50];
	switch(Menu_Settings.indexcolorbtn){
		case 2:
			strcpy(colname,"couleurIndividu");
		break;
		case 3:
			strcpy(colname,"couleurLienParent");
		break;
		case 4:
			strcpy(colname,"couleurLienMariage");
		break;
		case 5:
			strcpy(colname,"couleurLienFraterie");
		break;
	}
	char* command = str_format("./exec UPDATE value = %d IN ../Ressources/Bdd/settings.osql WHERE param = %s_r",Menu_Settings.picker.selection.r,colname);
	databaseQuery(command);
	command = str_format("./exec UPDATE value = %d IN ../Ressources/Bdd/settings.osql WHERE param = %s_g",Menu_Settings.picker.selection.g,colname);
	databaseQuery(command);
	command = str_format("./exec UPDATE value = %d IN ../Ressources/Bdd/settings.osql WHERE param = %s_b",Menu_Settings.picker.selection.b,colname);
	databaseQuery(command);
	Menu_Settings.show_picker = false;
}

/**
 * @brief
 * action effectuer lors du changement du theme dans le menu Settings(Sauvegarde dans BDD)
 */
void actionOfTheme(){
	if(betterstrcmp(Menu_Settings.menu_mode[0].selected_option,"light mode")){
		databaseQuery("./exec UPDATE value = light IN ../Ressources/Bdd/settings.osql WHERE param = theme");
		
	}	
	else{
		databaseQuery("./exec UPDATE value = dark IN ../Ressources/Bdd/settings.osql WHERE param = theme");
	}
}

/** 
 * @brief
 * action effectuer lors du changement du choix sur l'affichage de NULL Island dans le Menu Setting(sauvegarde dans BDD)
*/
void actionOfIslandNull(){
	if(betterstrcmp(Menu_Settings.menu_mode[1].selected_option,"oui")){
		databaseQuery("./exec UPDATE value = 1 IN ../Ressources/Bdd/settings.osql WHERE param = afficherNullIsland");
		
	}	
	else{
		databaseQuery("./exec UPDATE value = 0 IN ../Ressources/Bdd/settings.osql WHERE param = afficherNullIsland");
	}
}

/**
 * @brief
 * update le menu setting
 */
void updateSetting(){
	Menu_Settings.EX.callback = actionOfExploFichier;
	gereExploFichier(&Menu_Settings.EX);
	if(Menu_Settings.show_picker){
		Menu_Settings.picker.update(&(Menu_Settings.picker));
	}
	executeActionBouton(Menu_Settings.button_settings[0]);
	executeActionBouton(Menu_Settings.button_settings[1]);
	executeActionBouton(Menu_Settings.button_settings[2]);
	executeActionBouton(Menu_Settings.button_settings[3]);
	executeActionBouton(Menu_Settings.button_settings[4]);
	executeActionBouton(Menu_Settings.button_settings[5]);
	executeActionBouton(Menu_Settings.button_settings[6]);
	executeActionBouton(Menu_Settings.button_settings[7]);
	for(int i=0;i<9;i++){
		if(strcmp(Menu_Settings.fichierIndv[i]," ") && i!=Menu_Settings.ligneselectI){
		executeActionBouton(Menu_Settings.button_fichierI[i]);
		}
		if(strcmp(Menu_Settings.fichierMar[i]," ") && i!=Menu_Settings.ligneselectM){
		executeActionBouton(Menu_Settings.button_fichierM[i]);
		}
	}
	updateMover(&Menu_Settings.mov_poly_setting);
	updateMover(&Menu_Settings.mov_poly_fichier);

	Menu_Settings.menu_mode[0].update(&Menu_Settings.menu_mode[0]);
	Menu_Settings.menu_mode[1].update(&Menu_Settings.menu_mode[1]);
	
	if(betterstrcmp(Menu_Settings.menu_mode[0].selected_option,"light mode")){
		Menu_Settings.light_mode=true;	
	}
	else{
		Menu_Settings.light_mode=false;
		
	}
	if(betterstrcmp(Menu_Settings.menu_mode[1].selected_option,"oui")){
		Menu_Settings.show_NullIsland=true;
	}
	else{
		Menu_Settings.show_NullIsland=false;
	}

	ifkeyup('e'){
				Menu_Settings.EX.isShown = !Menu_Settings.EX.isShown;
			}
}

/**
 * @brief
 * initialise le menu setting
 * @param 
 * S : la structure Settings à afficher
 */
void initialiseMenuSetting(){
	//init couleur bouton setting
	Menu_Settings.picker = new_ColorPicker(500,500);
	Menu_Settings.show_picker = false;
	Menu_Settings.picker.setCallback(&(Menu_Settings.picker),actionOfPicker);
	Table T = databaseQuery("./exec SELECT ALL FROM ../Ressources/Bdd/settings.osql");
	afficheTable(T);

	Menu_Settings.couleurbouton[0]=(RGB){atoi(T.T[2][1]),atoi(T.T[3][1]),atoi(T.T[4][1])};
	Menu_Settings.couleurbouton[1]=(RGB){atoi(T.T[5][1]),atoi(T.T[6][1]),atoi(T.T[7][1])};
	Menu_Settings.couleurbouton[2]=(RGB){atoi(T.T[8][1]),atoi(T.T[9][1]),atoi(T.T[10][1])};
	Menu_Settings.couleurbouton[3]=(RGB){atoi(T.T[11][1]),atoi(T.T[12][1]),atoi(T.T[13][1])};
	libereTable(T);
	//init bouton setting
	Menu_Settings.button_settings[0] = initBoutonFromImg("../Ressources/fermer.bmp",(RGB){255,255,255});
	Menu_Settings.button_settings[0].addEventListener(&Menu_Settings.button_settings[0],"click",actionOfCloseSettings);
	Menu_Settings.button_settings[1]=initBouton(216,20,"map8K.bmp",(RGB){255,255,255});
	Menu_Settings.button_settings[1].addEventListener(&Menu_Settings.button_settings[1],"click",actionOfAjoutCarte);//Doit appeler un explofichier pour la carte
	roundBorders(&Menu_Settings.button_settings[1],0.18);
	Menu_Settings.button_settings[2]=initBouton(216,20,"",Menu_Settings.couleurbouton[0]);
	Menu_Settings.button_settings[2].addEventListener(&Menu_Settings.button_settings[2],"click",actionOfCouleurIndividu);
	roundBorders(&Menu_Settings.button_settings[2],0.18);
	Menu_Settings.button_settings[3]=initBouton(216,20,"",Menu_Settings.couleurbouton[1]);
	Menu_Settings.button_settings[3].addEventListener(&Menu_Settings.button_settings[3],"click",actionOfCouleurParent);
	roundBorders(&Menu_Settings.button_settings[3],0.18);
	Menu_Settings.button_settings[4]=initBouton(216,20,"",Menu_Settings.couleurbouton[2]);
	Menu_Settings.button_settings[4].addEventListener(&Menu_Settings.button_settings[4],"click",actionOfCouleurMariage);
	roundBorders(&Menu_Settings.button_settings[4],0.18);
	Menu_Settings.button_settings[5]=initBouton(216,20,"",Menu_Settings.couleurbouton[3]);
	Menu_Settings.button_settings[5].addEventListener(&Menu_Settings.button_settings[5],"click",actionOfCouleurFraterie);
	roundBorders(&Menu_Settings.button_settings[5],0.18);
	Menu_Settings.button_settings[6]=initBouton(408,20,"+",(RGB){186,186,186});
	Menu_Settings.button_settings[6].addEventListener(&Menu_Settings.button_settings[6],"click",actionOfAjoutFichierIndv);
	Menu_Settings.button_settings[7]=initBouton(408,20,"+",(RGB){186,186,186});
	Menu_Settings.button_settings[7].addEventListener(&Menu_Settings.button_settings[7],"click",actionOfAjoutFichierMar);
	//init bouton fichier Individu / Mariage
	for(int i=0;i<9;i++){
	Menu_Settings.button_fichierI[i]=initBouton(408,14,"",(RGB){69,69,69});
	Menu_Settings.button_fichierI[i].addEventListener(&Menu_Settings.button_fichierI[i],"click",actionOfSelectFichier);
	Menu_Settings.button_fichierM[i]=initBouton(408,14,"",(RGB){69,69,69});
	Menu_Settings.button_fichierM[i].addEventListener(&Menu_Settings.button_fichierM[i],"click",actionOfSelectFichier);
	}
	//Initialisation Polygone/Dropmenu Settings
	Menu_Settings.pos_settings = (Point) {-99999,0};

	Menu_Settings.polysetting=initPolygone(1000,600,0.2,500);
	Menu_Settings.menu_mode[0]=new_DropMenu(216,24);
	Menu_Settings.menu_mode[0].roundBorders(&Menu_Settings.menu_mode[0],0.18);
	Menu_Settings.menu_mode[0].event=actionOfTheme;
	Table M =  databaseQuery("./exec SELECT ALL FROM ../Ressources/Bdd/settings.osql WHERE param = theme");
	if(betterstrcmp(M.T[0][1],"light")){
		Menu_Settings.menu_mode[0].selected_option="light mode";
		Menu_Settings.menu_mode[0].add(&Menu_Settings.menu_mode[0],"light mode");
		Menu_Settings.menu_mode[0].add(&Menu_Settings.menu_mode[0],"dark mode");
	}
	else{
		Menu_Settings.menu_mode[0].selected_option="dark mode";
		Menu_Settings.menu_mode[0].add(&Menu_Settings.menu_mode[0],"dark mode");
		Menu_Settings.menu_mode[0].add(&Menu_Settings.menu_mode[0],"light mode");
	}
	libereTable(M);
	Table I = databaseQuery("./exec SELECT ALL FROM ../Ressources/Bdd/settings.osql WHERE param = afficherNullIsland");
	Menu_Settings.menu_mode[1]=new_DropMenu(216,24);
	Menu_Settings.menu_mode[1].roundBorders(&Menu_Settings.menu_mode[1],0.18);
	Menu_Settings.menu_mode[1].event=actionOfIslandNull;
	if(betterstrcmp(I.T[0][1],"1")){
		Menu_Settings.menu_mode[1].selected_option="oui";
		Menu_Settings.menu_mode[1].add(&Menu_Settings.menu_mode[1],"oui");
		Menu_Settings.menu_mode[1].add(&Menu_Settings.menu_mode[1],"non");
	}
	else{
		Menu_Settings.menu_mode[1].selected_option="non";
		Menu_Settings.menu_mode[1].add(&Menu_Settings.menu_mode[1],"non");
		Menu_Settings.menu_mode[1].add(&Menu_Settings.menu_mode[1],"oui");
	}
	Menu_Settings.polyfichier=initPolygone(408,185,0,0);
	libereTable(I);
	char current_dir[100];
	getcwd(current_dir, sizeof(char)*100);
	Menu_Settings.EX = initExploFichier(current_dir);
	printdebug(current_dir);
	
	Menu_Settings.fichierIndv=malloc(sizeof(char*)*9);
	Menu_Settings.fichierMar=malloc(sizeof(char*)*9);

	for(int i=0;i<9;i++){
		if(i+1<=Menu_Settings.Id.nbligne){
			Menu_Settings.fichierIndv[i]=malloc(sizeof(char)*strlen(Menu_Settings.Id.T[i][0]));
			strcpy(Menu_Settings.fichierIndv[i],Menu_Settings.Id.T[i][0]);
			numerofichierind++;	
		}
		else{
		Menu_Settings.fichierIndv[i]=malloc(sizeof(char));
		strcpy(Menu_Settings.fichierIndv[i]," ");
		}
	}
	
	
	for(int i=0;i<9;i++){
		if(i+1<=Menu_Settings.Mar.nbligne){
			Menu_Settings.fichierMar[i]=malloc(sizeof(char)*strlen(Menu_Settings.Mar.T[i][0]));
			strcpy(Menu_Settings.fichierMar[i],Menu_Settings.Mar.T[i][0]);
			numerofichiermari++;
		}
		else{
		Menu_Settings.fichierMar[i]=malloc(sizeof(char));
		strcpy(Menu_Settings.fichierMar[i]," ");
		}
	}
		
	libereTable(Menu_Settings.Id);
	libereTable(Menu_Settings.Mar);
		
	Menu_Settings.button_fichierI[Menu_Settings.ligneselectI].couleur=(RGB){200,200,200};
	Menu_Settings.button_fichierM[Menu_Settings.ligneselectM].couleur=(RGB){200,200,200};
	//Menu_Settings.Id=databaseQuery("./exec SELECT chosen FROM ../Ressources/Bdd/indivcsv.osql ");
	//Menu_Settings.Mar=databaseQuery("./exec SELECT chosen FROM ../Ressources/Bdd/mariagecsv.osql");
	

}

/**
 * @brief
 * affiche le menu setting
 * @param 
 * S : la structure Settings à afiicher
 */
void afficheMenuSetting(){
				int iw = 1000;//image width
				int ih=700;//image height
				int pw=Menu_Settings.polysetting.width;
				int ph=Menu_Settings.polysetting.height;
				int x =Menu_Settings.pos_settings.x;
				int y=Menu_Settings.pos_settings.y;
				couleurCourante(0,0,0);
				dessinePolygone(Menu_Settings.polysetting,x,y);
				couleurCourante(255,255,255);
				epaisseurDeTrait(3);
				fitTextToRectangle("Parametres :",x+330,y+250,pw-690,ph);
				Menu_Settings.menu_mode[0].show(&Menu_Settings.menu_mode[0],x+215*pw/iw,y+490*ph/ih);
				afficheBouton(Menu_Settings.pos_settings.x + Menu_Settings.polysetting.width - Menu_Settings.button_settings[0].skin->largeurImage -30,Menu_Settings.pos_settings.y + Menu_Settings.polysetting.height - Menu_Settings.button_settings[0].skin->hauteurImage - 30,&Menu_Settings.button_settings[0]);
				afficheBouton(x+215*pw/iw,y-20+452*ph/ih,&Menu_Settings.button_settings[1]);
				afficheBouton(x+215*pw/iw,y-15+332*ph/ih,&Menu_Settings.button_settings[2]);
				afficheBouton(x+215*pw/iw,y-25+308*ph/ih,&Menu_Settings.button_settings[3]);
				afficheBouton(x+215*pw/iw,y-35+281*ph/ih,&Menu_Settings.button_settings[4]);
				afficheBouton(x+215*pw/iw,y-45+257*ph/ih,&Menu_Settings.button_settings[5]);
				Menu_Settings.menu_mode[1].show(&Menu_Settings.menu_mode[1],x+215*pw/iw,y-65+167*ph/ih);
				couleurCourante(255,255,255);
				epaisseurDeTrait(2);
				printGfx(x+116*pw/iw,y+492*ph/ih,16,"Theme :");
				printGfx(x+129*pw/iw,y-20+454*ph/ih,16,"Carte :");
				printGfx(x+39*pw/iw,y+370*ph/ih,16,"Parametres de l'Arbre :");
				printGfx(x+45*pw/iw,y-10+334*ph/ih,16,"Couleur Individu :");
				printGfx(x+15*pw/iw,y-20+310*ph/ih,16,"Couleur Lien Parent :");
				printGfx(x+3*pw/iw,y-30+283*ph/ih,16,"Couleur Lien Mariage :");
				printGfx(x+5*pw/iw,y-40+259*ph/ih,16,"Couleur Lien Fraterie :");
				printGfx(x+30*pw/iw,y-50+201*ph/ih,16,"Parametre de la Carte :");
				printGfx(x+25*pw/iw,y-60+169*ph/ih,16,"Afficher Null Island :");

				printGfx(x+702*pw/iw,y+560*ph/ih,16,"Individus");
				couleurCourante(69,69,69);
				dessinePolygone(Menu_Settings.polyfichier,x+532*Menu_Settings.polysetting.width/iw,y+335*Menu_Settings.polysetting.height/ih);
				afficheBouton(x+532*pw/iw,y+335*ph/ih,&Menu_Settings.button_settings[6]);
				couleurCourante(255,255,255);
				printGfx(x+702*pw/iw,y+297*ph/ih,16,"Mariages");
				couleurCourante(69,69,69);
				dessinePolygone(Menu_Settings.polyfichier,x+532*Menu_Settings.polysetting.width/iw,y+74*Menu_Settings.polysetting.height/ih);
				afficheBouton(x+532*pw/iw,y+74*ph/ih,&Menu_Settings.button_settings[7]);
				couleurCourante(0,0,0);
				epaisseurDeTrait(2);
				for(int i=0;i<9;i++){
					if(strcmp(Menu_Settings.fichierIndv[i]," ")){
						afficheBouton(x+532*pw/iw,(y+529-i*20)*ph/ih,&Menu_Settings.button_fichierI[i]);
					}
					if(strcmp(Menu_Settings.fichierMar[i]," ")){
						afficheBouton(x+532*pw/iw,(y+267-i*20)*ph/ih,&Menu_Settings.button_fichierM[i]);
					}	
					printGfx(x+550*pw/iw,(y+531-i*20)*ph/ih,16,Menu_Settings.fichierIndv[i]);
					printGfx(x+550*pw/iw,(y+269-i*20)*ph/ih,16,Menu_Settings.fichierMar[i]);
				
				}
				if(Menu_Settings.show_picker){
					setColor4i(255,255,255,120);
					rectangle(0,0,LF,HF);
					Menu_Settings.picker.show(&(Menu_Settings.picker),LF/2 -Menu_Settings.picker.width/2 ,HF/2 -Menu_Settings.picker.height/2);
				}
				afficheExploFichier(Menu_Settings.EX);
				
}

/**
 * @brief
 * Action effectuée lors de la fermeture du menu édition
 */
void actionOfCloseEditors(){
	edit.choisis = NULL;
	edit.show_edit=false;
	edit.enfants.deleteAll(&edit.enfants);
	edit.fraterie.deleteAll(&edit.fraterie);
	for(int i=0;i<10;i++){
	edit.Indice[i].deleteAll(&edit.Indice[i]);
	edit.afficheIndice[i]=0;
	}
	free(edit.lien_pere.texte);
	edit.lien_pere.texte=NULL;
	free(edit.lien_mere.texte);
	edit.lien_mere.texte=NULL;
	free(edit.lien_conjoint.texte);
	edit.lien_conjoint.texte=NULL;
	free(tmpc);
	tmpc=NULL;
	handleSouris=true;

}

/**
 * @brief
 * Action éffectuée lors du clique sur les boutons IA 
 */
void actionOfIA(){
	int index= -1;
	int i=0;
	char ** ind;
	int *indint;
	bool * indgenre;
	Maillon** indmaillon;
	Mariage** indmar;
	while(i<11){
		if(pointIsInRectangle(AS,OS,edit.IA[i].x,edit.IA[i].y,edit.IA[i].width,edit.IA[i].height)){
			printf("bonton %d press\n",i);	
			index=i;
		}
		i++;
	}
	for(int i=0;i<10;i++){
	edit.Indice[i].deleteAll(&edit.Indice[i]);
	edit.afficheIndice[i]=0;
	}

	switch(index){

		case 0:
		
		search_menu.selec = edit.choisis->data;
		search_menu.search = searchGoogleImageIndividu(search_menu.selec);
		search_menu.searchMenu = true;
				
		
	
	

		break;

		case 1:
		edit.afficheIndice[index-1]=1;
		indgenre=propGenre(edit.choisis);

		if(*indgenre){
			edit.Indice[index-1].add(&edit.Indice[index-1],"Femme");
		}
		else{
			edit.Indice[index-1].add(&edit.Indice[index-1],"Homme");
		}
	
		free(indgenre);

		break;

		case 2:
		edit.afficheIndice[index-1]=1;
		ind=propPrenom(edit.choisis);
		
		for(int i=0;i<3;i++){
			if(ind[i]!=NULL){
				edit.Indice[index-1].add(&edit.Indice[index-1],ind[i]);
			}
		}

		break;

		case 3:
		edit.afficheIndice[index-1]=1;
		ind=propNom(edit.choisis);
		for(int i=0;i<3;i++){
			if(ind[i]!=NULL){
				edit.Indice[index-1].add(&edit.Indice[index-1],ind[i]);
			}
		}

		break;

		case 4:
		edit.afficheIndice[index-1]=1;
		indint=propDateNaissance(edit.choisis);
		if(indint!=NULL){
			for(int i=0;i<5;i++){
			edit.Indice[index-1].add(&edit.Indice[index-1],str_format("%d",indint[i]));
			}
		}

		break;

		case 5:
		/* 
		edit.afficheIndice[index-1]=1;
		ind=propLieuNaissance(edit.choisis);
		for(int i=0;ind[i]!=NULL;i++){	
				edit.Indice[index-1].add(&edit.Indice[index-1],ind[i]);
		}
		*/
	

		break;

		case 6:
		edit.afficheIndice[index-1]=1;
		indint=propDateMort(edit.choisis);
		if(indint!=NULL){
			for(int i=0;i<4;i++){
			edit.Indice[index-1].add(&edit.Indice[index-1],str_format("%d",indint[i]));
			}
		}


		break;

		case 7:
		edit.afficheIndice[index-1]=1;
		//TODO mettre les bons Poids
		int poids[8]={0,3,4,3,4,6,2};
		ind=propLieuMort(edit.choisis,poids);
		for(int i=0;ind[i]!=NULL;i++){
			edit.Indice[index-1].add(&edit.Indice[index-1],ind[i]);
		}

		break;

		case 8:
		edit.afficheIndice[index-1]=1;
		indmaillon=propDad(edit.choisis,ptrTete);
		for(int i=0;indmaillon[i]!=NULL;i++){
			edit.Indice[index-1].add(&edit.Indice[index-1],str_format("%s %s",indmaillon[i]->data->name[0],indmaillon[i]->data->surname));
		}

		break;

		case 9:
		edit.afficheIndice[index-1]=1;
		indmaillon=propMom(edit.choisis,ptrTete);
		for(int i=0;indmaillon[i]!=NULL;i++){
			edit.Indice[index-1].add(&edit.Indice[index-1],str_format("%s %s",indmaillon[i]->data->name[0],indmaillon[i]->data->surname));
		}

		break;

		case 10:
		edit.afficheIndice[index-1]=1;
		indmar=propMariage(edit.choisis,ptrTete);
		for(int i=0;indmar[i]!=NULL;i++){
			if(*edit.choisis->data->gender){
			edit.Indice[index-1].add(&edit.Indice[index-1],str_format("%s %s",indmar[i]->maries[0]->data->name[0],indmar[i]->maries[0]->data->surname));
			}
			else{
				edit.Indice[index-1].add(&edit.Indice[index-1],str_format("%s %s",indmar[i]->maries[1]->data->name[0],indmar[i]->maries[1]->data->surname));
			}
			free(indmar[i]);
				
		}
		free(indmar);

		

		break;
	}
	

}

void actionOfChoixIndividu(){
	println("ouvre le menu de choix");
}
/**
 * @brief
 * Permet d'ajouter les Infos d'un Individu dans le Menu Edition
 */
void AjouteInfo(Maillon * M){
	strcpy(edit.generation.texte,str_format("%d",M->data->numGen));
	strcpy(edit.prenom.texte,M->data->surname);
	strcpy(edit.nom.texte,M->data->name[0]);
	println("ok");
	if(M->data->birth_data->lieu.nom!=NULL){
		strcpy(edit.lieu_naissance.texte,M->data->birth_data->lieu.nom);
	}
	else{
		strcpy(edit.lieu_naissance.texte,"");
	}
	println("ok2");
	if(!dateIsNULL(M->data->birth_data->date)){
		strcpy(edit.date_naissance.texte,dateToString(M->data->birth_data->date));
	}
	else{
		strcpy(edit.date_naissance.texte,"");
	}

	if(M->data->death_data->lieu.nom!=NULL){
		strcpy(edit.lieu_mort.texte,M->data->death_data->lieu.nom);
	}
	else{
		strcpy(edit.lieu_mort.texte,"");
	}

	if(!dateIsNULL(M->data->death_data->date)){
		strcpy(edit.date_mort.texte,dateToString(M->data->death_data->date));
	}
	else{
		strcpy(edit.date_mort.texte,"");
	}
	println("ok3");
	if(*M->data->gender){
		choixG=1;
	}
	else{
		choixG=0;
	}
	edit.lien_pere.texte=malloc(sizeof(char)*50);
	strcpy(edit.lien_pere.texte,"");
	if(M->dad!=NULL){
		strcpy(edit.lien_pere.texte,M->dad->data->name[0]);
		strcat(edit.lien_pere.texte," ");
		strcat(edit.lien_pere.texte,M->dad->data->surname);
	}
	edit.lien_mere.texte=malloc(sizeof(char)*50);
	strcpy(edit.lien_mere.texte,"");
	if(M->mom!=NULL){
		strcpy(edit.lien_mere.texte,M->mom->data->name[0]);
		strcat(edit.lien_mere.texte," ");
		strcat(edit.lien_mere.texte,M->mom->data->surname);
	}
	println("ok4");
	edit.lien_conjoint.texte=malloc(sizeof(char)*50);
	strcpy(edit.lien_conjoint.texte,"");
	 if(M->wedding!=NULL){
		int i=0;
		if(compareIndividu(M->wedding->data->maries[0]->data,M->data)==1){
			i=1;
		}
		else{
			i=0;
		}
		strcpy(edit.lien_conjoint.texte,M->wedding->data->maries[i]->data->name[0]);
		strcat(edit.lien_conjoint.texte," ");
		strcat(edit.lien_conjoint.texte,M->wedding->data->maries[i]->data->surname);
	}
	

}

/**
 * @brief
 * action lors du changement du genre dans le Menu Edition
 */
void actionOfChoixGenre(){
	 if(pointIsInRectangle(AS,OS,edit.H.x,edit.H.y,edit.H.width,edit.H.height)){
		choixG=0;
	}
	else if(pointIsInRectangle(AS,OS,edit.F.x,edit.F.y,edit.F.width,edit.F.height)){
		choixG=1;
	}
}

/**
 * @brief
 * action pour ajouter un enfant à un Individu
 */
void actionOfAjoutEnfants(){
	
	char **newenfants =str_split(edit.ajout_enfants.texte,' ');
	printf("%s\n",newenfants[0]);
	printf("%s\n",newenfants[1]);
	Critere C=creeCritere(edit.choisis->data->numGen-1,newenfants[0],newenfants[1],NULL,NULL,NULL,NULL,NULL,NULL);
	Maillon **M;
	M=superRechercheMaillon(C,ptrTete);

	if(M[0]!=NULL){
		printf("ok\n");
		M[0]->dad=edit.choisis;
		updateLiensParente(ptrTete);
		updateLiensFraterie(ptrTete);
		printMaillon(edit.choisis);
		printMaillon(M[0]);
	}
	

}

void actionOfAjoutFraterie(){
	
}

void actionOfSupprimeEnfants(){

}

void actionOfSupprimeFraterie(){
	
}

/**
 * @brief
 * action pour sauvagarder les infos modifiés dans le Menu Edition
 */
void actionOfSauvegardeInfo(){

	
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(compareIndividu(tmp->data,edit.choisis->data)){
			free(tmp->data->name[0]);
			free(tmp->data->surname);
			free(tmp->data->birth_data->lieu.nom);
			free(tmp->data->death_data->lieu.nom);

			tmp->data->name[0]=malloc(sizeof(char)*strlen(edit.nom.texte));
			tmp->data->surname=malloc(sizeof(char)*strlen(edit.prenom.texte));
			tmp->data->birth_data->lieu.nom=malloc(sizeof(char)*strlen(edit.lieu_naissance.texte));
			tmp->data->death_data->lieu.nom=malloc(sizeof(char)*strlen(edit.lieu_mort.texte));
			strcpy(tmp->data->name[0],edit.nom.texte);
			strcpy(tmp->data->surname,edit.prenom.texte);
			strcpy(tmp->data->birth_data->lieu.nom,edit.lieu_naissance.texte);
			strcpy(tmp->data->death_data->lieu.nom,edit.lieu_mort.texte);
			tmp->data->numGen=atoi(edit.generation.texte);
			if(strcmp(edit.date_naissance.texte,"")){
			tmp->data->birth_data->date=new_Date(edit.date_naissance.texte);
			}
			else{
			tmp->data->birth_data->date=new_Date(NULL);
			}
			if(strcmp(edit.date_mort.texte,"")){
			tmp->data->death_data->date=new_Date(edit.date_mort.texte);
			}
			else{
				tmp->data->death_data->date=new_Date(NULL);
			}
			if(strcmp(edit.lieu_naissance.texte,"")){
				strcpy(tmp->data->birth_data->lieu.nom,edit.lieu_naissance.texte);
				Point_2f loc = getCoordinatesFromPlaceName(edit.lieu_naissance.texte);
				println("loc %f %f",loc.x,loc.y);
				tmp->data->birth_data->lieu.X =(double) loc.x;
				tmp->data->birth_data->lieu.Y =(double) loc.y;
			}
			else{
				tmp->data->birth_data->lieu.nom=NULL;
			}
			if(strcmp(edit.lieu_mort.texte,"")){
				strcpy(tmp->data->death_data->lieu.nom,edit.lieu_mort.texte);
				Point_2f loc = getCoordinatesFromPlaceName(edit.lieu_naissance.texte);
				println("loc %f %f",loc.x,loc.y);
				tmp->data->death_data->lieu.X =(double) loc.x;
				tmp->data->death_data->lieu.Y =(double) loc.y;
			}
			else{
				tmp->data->death_data->lieu.nom=NULL;
			}
			if(choixG==1){
				*tmp->data->gender=true;
			}
			else{
				*tmp->data->gender=false;
			}
		
		}
	}
	actionOfCloseEditors();
}

/**
 * @brief
 * Permet d'initialiser le menu édition
 */
void initMenuEdition(){
	edit.choisis=NULL;
	//init bouton fermer
	edit.close = initBouton(30,30,"",(RGB){0,0,0});
	edit.close.addEventListener(&edit.close,"click",actionOfCloseEditors);
	//init Champ principaux 
	edit.generation=initTextField(0,0,59,60,(RGB){160,160,160},"");
	edit.prenom=initTextField(0,0,370,25,(RGB){160,160,160},"");
	edit.nom=initTextField(0,0,370,25,(RGB){160,160,160},"");
	edit.date_naissance=initTextField(0,0,370,25,(RGB){160,160,160},"");
	edit.lieu_naissance=initTextField(0,0,370,25,(RGB){160,160,160},"");
	edit.date_mort=initTextField(0,0,370,25,(RGB){160,160,160},"");
	edit.lieu_mort=initTextField(0,0,370,25,(RGB){160,160,160},"");
	edit.lien_mere=initBouton(400,25,"",(RGB){160,160,160});
	edit.lien_mere.addEventListener(&edit.lien_mere,"click",actionOfChoixIndividu);
	edit.lien_pere=initBouton(400,25,"",(RGB){160,160,160});
	edit.lien_pere.addEventListener(&edit.lien_pere,"click",actionOfChoixIndividu);
	edit.lien_conjoint=initBouton(400,25,"",(RGB){160,160,160});
	edit.lien_conjoint.addEventListener(&edit.lien_conjoint,"click",actionOfChoixIndividu);
	edit.ajout_enfants=initTextField(0,0,370,25,(RGB){160,160,160},"");
	edit.ajout_freres=initTextField(0,0,370,25,(RGB){160,160,160},"");
	edit.enfants=new_DropMenu(310,28);
	edit.enfants.color=(RGB){160,160,160};
	edit.enfants.roundBorders(&edit.enfants,0.18);
	edit.fraterie=new_DropMenu(310,28);
	edit.fraterie.color=(RGB){160,160,160};
	edit.fraterie.roundBorders(&edit.fraterie,0.18);
	
	edit.H=initBouton(54,54,"",(RGB){0,0,0});
	edit.H.addEventListener(&edit.H,"click",actionOfChoixGenre);
	edit.F=initBouton(54,54,"",(RGB){0,0,0});
	edit.F.addEventListener(&edit.F,"click",actionOfChoixGenre);

	for(int i=0;i<11;i++){
		edit.IA[i]=initBouton(28,28,"",(RGB){160,160,160});
		edit.IA[i].addEventListener(&edit.IA[i],"click",actionOfIA);
	}

	edit.ajoutE=initBouton(28,28,"+",(RGB){160,160,160});
	edit.ajoutE.addEventListener(&edit.ajoutE,"click",actionOfAjoutEnfants);
	edit.ajoutF=initBouton(28,28,"+",(RGB){160,160,160});
	edit.ajoutF.addEventListener(&edit.ajoutF,"click",actionOfAjoutFraterie);
	edit.supprimeE=initBouton(28,28,"x",(RGB){160,160,160});
	edit.supprimeE.addEventListener(&edit.supprimeE,"click",actionOfSupprimeEnfants);
	edit.supprimeF=initBouton(28,28,"x",(RGB){160,160,160});
	edit.supprimeF.addEventListener(&edit.supprimeF,"click",actionOfSupprimeFraterie);

	edit.sauv=initBouton(159,42,"Sauvegarder",(RGB){160,160,160});
	edit.sauv.addEventListener(&edit.sauv,"click",actionOfSauvegardeInfo);
	roundBorders(&edit.sauv,0.18);


	//Indice IA
	for(int i=0;i<10;i++){
	edit.Indice[i]=new_DropMenu(100,20);
	edit.Indice[i].color=(RGB){160,160,160};
	edit.Indice[i].roundBorders(&edit.Indice[i],0.18);
	}
}

/**
 * @brief
 * Permet d'update le Menu Edition
 */
void updateMenuEdition(){

	widthrec=1076*LF/1200;
	heightrec=693*HF/800;
	if(edit.show_edit){
		
		textFieldClickListener(&edit.generation);
		edit.generation.width=59*widthrec/1200;
		edit.generation.height=60*heightrec/1200;
		textFieldClickListener(&edit.prenom);
		edit.prenom.width=370*widthrec/1200;
		textFieldClickListener(&edit.nom);
		edit.nom.width=370*widthrec/1200;
		textFieldClickListener(&edit.date_naissance);
		edit.date_naissance.width=370*widthrec/1200;
		textFieldClickListener(&edit.lieu_naissance);
		edit.lieu_naissance.width=370*widthrec/1200;
		textFieldClickListener(&edit.date_mort);
		edit.date_mort.width=370*widthrec/1200;
		textFieldClickListener(&edit.lieu_mort);
		edit.lieu_mort.width=370*widthrec/1200;
		executeActionBouton(edit.lien_mere);
		edit.lien_mere.width=400*widthrec/1200;
		executeActionBouton(edit.lien_pere);
		edit.lien_pere.width=400*widthrec/1200;
		executeActionBouton(edit.lien_conjoint);
		edit.lien_conjoint.width=400*widthrec/1200;
		textFieldClickListener(&edit.ajout_enfants);
		edit.ajout_enfants.width=300*widthrec/1200;
		textFieldClickListener(&edit.ajout_freres);
		edit.ajout_freres.width=300*widthrec/1200;
		executeActionBouton(edit.H);
		executeActionBouton(edit.F);
		for(int i=0;i<11;i++){
			executeActionBouton(edit.IA[i]);
		}
		executeActionBouton(edit.ajoutE);
		executeActionBouton(edit.ajoutF);
		executeActionBouton(edit.supprimeE);
		executeActionBouton(edit.supprimeF);
		executeActionBouton(edit.close);
		edit.enfants.update(&edit.enfants);
		edit.fraterie.update(&edit.fraterie);
		executeActionBouton(edit.sauv);

		for(int i=0;i<10;i++){
		edit.Indice[i].update(&edit.Indice[i]);
		}

		if(edit.Indice[0].selected_option!=NULL){
			if(!strcmp(edit.Indice[0].selected_option,"Femme")){
				choixG=1;
			}
			else{
				choixG=0;
			}

		}
		if(edit.Indice[1].selected_option!=NULL){
			strcpy(edit.prenom.texte,edit.Indice[1].selected_option);
		}
		if(edit.Indice[2].selected_option!=NULL){
			strcpy(edit.nom.texte,edit.Indice[2].selected_option);
		}
		if(edit.Indice[3].selected_option!=NULL){
			strcpy(edit.date_naissance.texte,str_format("%s/%s/%s","01","01",edit.Indice[3].selected_option));
		}
		if(edit.Indice[4].selected_option!=NULL){
			strcpy(edit.lieu_naissance.texte,edit.Indice[4].selected_option);
		}
		if(edit.Indice[5].selected_option!=NULL){
			strcpy(edit.date_mort.texte,str_format("%s/%s/%s","01","01",edit.Indice[5].selected_option));
		}
		if(edit.Indice[6].selected_option!=NULL){
			strcpy(edit.lieu_mort.texte,edit.Indice[6].selected_option);
		}
		if(edit.Indice[7].selected_option!=NULL){
			strcpy(edit.lien_pere.texte,edit.Indice[7].selected_option);
		}
		if(edit.Indice[8].selected_option!=NULL){
			strcpy(edit.lien_mere.texte,edit.Indice[8].selected_option);
		}
		if(edit.Indice[9].selected_option!=NULL){
			strcpy(edit.lien_conjoint.texte,edit.Indice[9].selected_option);
		}
		
		
		handleSouris=false;
	}
}

/**
 * @brief
 * Permet d'afficher le Menu Edition
 */
void afficheMenuEdition(){
	if(edit.show_edit){
		float iw=1200;
		float ih=800;
		float x1rec=68*LF/iw;
		float y1rec=48*HF/ih;
		float x2rec=1130*LF/iw;
		float y2rec=740*HF/ih;
		int x=68*LF/iw;
		int y=48*HF/ih;
		couleurCourante(0,0,0);
		rectangle(x1rec,y1rec,x2rec,y2rec);

		//Affiche les boutons fermer et les genres
		afficheBouton(x +1120*widthrec/iw ,y + 750 *heightrec/ih,&edit.close);
		ecrisImageTexture(new_Image("../Ressources/fermer.bmp",true),x +1120*widthrec/iw ,y + 750 *heightrec/ih);
		afficheBouton(x+334*widthrec/iw,y+616*heightrec/ih,&edit.F);
		ecrisImageTexture(new_Image("../Ressources/genre_f.png",true),x+334*widthrec/iw,y+616*heightrec/ih);
		afficheBouton(x+391*widthrec/iw,y+616*heightrec/ih,&edit.H);
		ecrisImageTexture(new_Image("../Ressources/genre_m.png",true),x+391*widthrec/iw,y+616*heightrec/ih);
		
		if(choixG==1){
			couleurCourante(160,160,160);
			rectangle(x+327*widthrec/iw,y+604*heightrec/ih,x+386*widthrec/iw,y+610*heightrec/ih);
		}
		else {
			couleurCourante(160,160,160);
			rectangle(x+386*widthrec/iw,y+604*heightrec/ih,x+444*widthrec/iw,y+610*heightrec/ih);
		}
		//fitImageToRectangle(edit.choisis->data->photo,130*widthrec/1200,138*heightrec/800);
		
		//Affiche les differents titre et Textfield
		ecrisImageTextureCustom(edit.choisis->data->photo,x+95*widthrec/iw,y+536*heightrec/ih,139*widthrec/iw,139*widthrec/iw);
		afficheTextField(&edit.generation,x+418*widthrec/iw,y+533*heightrec/ih);
		afficheTextField(&edit.prenom,x+99*widthrec/iw,y+465*heightrec/ih);
		afficheTextField(&edit.nom,x+99*widthrec/iw,y+401*heightrec/ih);
		afficheTextField(&edit.date_naissance,x+99*widthrec/iw,y+266*heightrec/ih);
		afficheTextField(&edit.lieu_naissance,x+99*widthrec/iw,y+199*heightrec/ih);
		afficheTextField(&edit.date_mort,x+99*widthrec/iw,y+94*heightrec/ih);
		afficheTextField(&edit.lieu_mort,x+99*widthrec/iw,y+31*heightrec/ih);
		afficheBouton(x+622*widthrec/iw,y+562*heightrec/ih,&edit.lien_pere);
		afficheBouton(x+622*widthrec/iw,y+496*heightrec/ih,&edit.lien_mere);
		afficheBouton(x+622*widthrec/iw,y+437*heightrec/ih,&edit.lien_conjoint);
		
		afficheTextField(&edit.ajout_enfants,x+622*widthrec/iw,y+223*heightrec/ih);
		couleurCourante(255,255,255);
		epaisseurDeTrait(3);
		printGfx(x+746*widthrec/iw,(y+250+14)*heightrec/ih,16,"Ajout :");
		edit.enfants.show(&edit.enfants,x+622*widthrec/iw,y+283*heightrec/ih);
		afficheTextField(&edit.ajout_freres,x+622*widthrec/iw,y+55*heightrec/ih);
		couleurCourante(255,255,255);
		epaisseurDeTrait(3);
		printGfx(x+746*widthrec/iw,(y+80+14)*heightrec/ih,16,"Ajout :");
		edit.fraterie.show(&edit.fraterie,x+622*widthrec/iw,y+113*heightrec/ih);

		couleurCourante(255,255,255);
		epaisseurDeTrait(3);
		printGfx((x+297)*widthrec/iw,(y+551)*heightrec/ih,16,"Generation :");
		printGfx(x+205*widthrec/iw,(y+489+14)*heightrec/ih,16,"Prenom :");
		printGfx(x+215*widthrec/iw,(y+427+14)*heightrec/ih,16,"Nom :");
		printGfx(x+151*widthrec/iw,(y+335+14)*heightrec/ih,24,"Naissance");
		printGfx(x+205*widthrec/iw,(y+289+14)*heightrec/ih,16,"Date :");
		printGfx(x+205*widthrec/iw,(y+224+14)*heightrec/ih,16,"Lieu :");
		printGfx(x+193*widthrec/iw,(y+158+14)*heightrec/ih,24,"Mort");
		printGfx(x+205*widthrec/iw,(y+121+14)*heightrec/ih,16,"Date :");
		printGfx(x+205*widthrec/iw,(y+53+14)*heightrec/ih,16,"Lieu :");
		printGfx(x+741*widthrec/iw,(y+626+14)*heightrec/ih,24,"Lien");
		printGfx(x+761*widthrec/iw,(y+589+14)*heightrec/ih,16,"Pere :");
		printGfx(x+761*widthrec/iw,(y+527+14)*heightrec/ih,16,"Mere :");
		printGfx(x+742*widthrec/iw,(y+467+14)*heightrec/ih,16,"Conjoint :");
		printGfx(x+726*widthrec/iw,(y+317+14)*heightrec/ih,24,"Enfants");
		printGfx(x+726*widthrec/iw,(y+146+14)*heightrec/ih,24,"Fraterie");

		//Affiche les boutons Indices IA
	 	afficheBouton(x+235*widthrec/iw,y+585*heightrec/ih,&edit.IA[0]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+235*widthrec/iw,y+585*heightrec/ih);
		afficheBouton(x+465*widthrec/iw,y+622*heightrec/ih,&edit.IA[1]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+465*widthrec/iw,y+622*heightrec/ih);
		afficheBouton(x+475*widthrec/iw,y+465*heightrec/ih,&edit.IA[2]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+475*widthrec/iw,y+465*heightrec/ih);
		afficheBouton(x+475*widthrec/iw,y+401*heightrec/ih,&edit.IA[3]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+475*widthrec/iw,y+401*heightrec/ih);
		afficheBouton(x+475*widthrec/iw,y+266*heightrec/ih,&edit.IA[4]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+475*widthrec/iw,y+266*heightrec/ih);
		afficheBouton(x+475*widthrec/iw,y+199*heightrec/ih,&edit.IA[5]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+475*widthrec/iw,y+199*heightrec/ih);
		afficheBouton(x+475*widthrec/iw,y+94*heightrec/ih,&edit.IA[6]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+475*widthrec/iw,y+94*heightrec/ih);
		afficheBouton(x+475*widthrec/iw,y+31*heightrec/ih,&edit.IA[7]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+475*widthrec/iw,y+31*heightrec/ih);
		afficheBouton(x+1030*widthrec/iw,y+562*heightrec/ih,&edit.IA[8]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+1030*widthrec/iw,y+562*heightrec/ih);
		afficheBouton(x+1030*widthrec/iw,y+496*heightrec/ih,&edit.IA[9]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+1030*widthrec/iw,y+496*heightrec/ih);
		afficheBouton(x+1030*widthrec/iw,y+437*heightrec/ih,&edit.IA[10]);
		ecrisImageTexture(new_Image("../Ressources/Ampoule.bmp",true),x+1030*widthrec/iw,y+437*heightrec/ih);

		//Affiche les boutons +
		afficheBouton(x+971*widthrec/iw,y+225*heightrec/ih,&edit.ajoutE);
		afficheBouton(x+971*widthrec/iw,y+56*heightrec/ih,&edit.ajoutF);
		//Affiche les boutons X
		afficheBouton(x+971*widthrec/iw,y+283*heightrec/ih,&edit.supprimeE);
		afficheBouton(x+971*widthrec/iw,y+113*heightrec/ih,&edit.supprimeF);
		//Affiche le bouton sauvegarder 
		afficheBouton(x+1000*widthrec/iw,y+2*heightrec/ih,&edit.sauv);
		
		//Afficher les indices IA
		if(edit.afficheIndice[0]){
			edit.Indice[0].show(&edit.Indice[0],x+530*widthrec/iw,y+622*heightrec/ih);
			}
			
		if(edit.afficheIndice[1]){
			edit.Indice[1].show(&edit.Indice[1],x+510*widthrec/iw,(y+470)*heightrec/ih);
			}

		if(edit.afficheIndice[2]){
			edit.Indice[2].show(&edit.Indice[2],x+510*widthrec/iw,(y+405)*heightrec/ih);
			}

		if(edit.afficheIndice[3]){
			edit.Indice[3].show(&edit.Indice[3],x+510*widthrec/iw,(y+270)*heightrec/ih);
			}

		if(edit.afficheIndice[4]){
			edit.Indice[4].show(&edit.Indice[4],x+510*widthrec/iw,(y+204)*heightrec/ih);
			}

		if(edit.afficheIndice[5]){
			edit.Indice[5].show(&edit.Indice[5],x+510*widthrec/iw,(y+99)*heightrec/ih);
			}

		if(edit.afficheIndice[6]){
			edit.Indice[6].show(&edit.Indice[6],x+510*widthrec/iw,(y+35)*heightrec/ih);
			}

		if(edit.afficheIndice[7]){
			edit.Indice[7].show(&edit.Indice[7],x+1070*widthrec/iw,(y+570)*heightrec/ih);
			}

		if(edit.afficheIndice[8]){
			edit.Indice[8].show(&edit.Indice[8],x+1070*widthrec/iw,(y+505)*heightrec/ih);
			}

		if(edit.afficheIndice[9]){
			edit.Indice[9].show(&edit.Indice[9],x+1070*widthrec/iw,(y+445)*heightrec/ih);
		}

	}	
}


//Fonction RECHERCHE MAILLON GFX
void rechercheMaillonsGFX(){
	char *dateN;
	char *dateM;
	char *Nom;
	char *Prenom;
	char *lieuN;
	char *lieuM;
	bool *gender;
	int numGen;
	if(str_same(RechGFX.numgen.texte,""))
		numGen = -1;
	else
		numGen = atoi(str_format("%s",RechGFX.numgen.texte));
	//printf("numgen = %d\n",numGen);
	dateN = str_format("%s",RechGFX.date_naissance.texte);
	//if(dateN !=NULL){printf("date naissance = %s\n",dateN);}
	dateM = str_format("%s",RechGFX.date_mort.texte);
	//if(dateM !=NULL){printf("date mort = %s\n",dateM);}

	lieuN = initChaine(RechGFX.lieu_naissance.texte);
	//if(lieuN !=NULL){printf("Lieu naissance = %s\n",lieuN);}
	lieuM = initChaine(RechGFX.lieu_mort.texte);
	//if(lieuM !=NULL){printf("Lieu mort = %s\n",lieuM);}
	Nom = initChaine(RechGFX.nom.texte);
	//if(Nom !=NULL){printf("Nom = %s\n",Nom);}
	Prenom = initChaine(RechGFX.prenom.texte);
	//if(Prenom != NULL){printf("Prenom = %s\n",Prenom);}
	dateN = initChaine(RechGFX.date_naissance.texte);
	//if(dateN !=NULL){printf("dateN = %s\n",dateN);}
	dateM = initChaine(RechGFX.date_mort.texte);
	//if(lieuM !=NULL){printf("dateM = %s\n",dateM);}
	gender = malloc(sizeof(bool));
	if(!strcmp(RechGFX.genre.selected_option,"Homme"))
		gender[0] = 0;
	if(!strcmp(RechGFX.genre.selected_option,"Femme"))
		gender[0] = 1;
	if(!strcmp(RechGFX.genre.selected_option,"Inconnu"))
		gender = NULL;
	

	Birth *B = NULL;
	if(lieuN != NULL && dateN != NULL){
		B = malloc(sizeof(Birth));
		B->lieu.nom = str_format(lieuN);
		B->date = new_Date(dateN);
		//printf("date convert B %d/%d/%d\n",B->date.day,B->date.month,B->date.year);
		if((B->date.day > 31 && B->date.day < 1) || (B->date.month < 1 && B->date.month > 12) || (B->date.year < -1000 || B->date.year > 9000)){
			B = NULL;
		}
		//printf("is b NULL -> %d\n",B == NULL);
	}

	Death *D = NULL;
	if(lieuM != NULL && dateM != NULL){
		D = malloc(sizeof(Death));
		D->lieu.nom = str_format(lieuM);
		D->date = new_Date(dateM);
		//printf("date convert D %d/%d/%d\n",D->date.day,D->date.month,D->date.year);
		if((D->date.day > 31 || D->date.day < 1 )&& (D->date.month < 1 || D->date.month > 12) && (D->date.year < -1000 || D->date.year > 9000)){
			D = NULL;
		}
		//printf("is d NULL -> %d\n",D == NULL);
	}

	//printf("genre %d\n",gender[0]);
	//printf("numgen = %d\n",numGen);
	Critere C = creeCritere(numGen,Nom,Prenom,B,D,gender,NULL,NULL,"=");
	RechGFX.result = superRechercheMaillon(C,ptrTete);
	//printf("j ai mon maillon\n");

	foreach(Maillon **t,RechGFX.result){
		Maillon *tmp = *t;
		printMaillon(tmp);
		/*TODO
			mettre la fonction de l'affichage graphique d'un maillon
		 */
	}

	free(dateN);
	dateN = NULL;
	free(dateM);
	dateM = NULL;
	free(Nom);
	Nom = NULL;
	free(Prenom);
	Prenom = NULL;
	betterFree(&gender);
}

/**
 * @brief bouge la camera sur l'individu donné
 * 
 * @param m 
 */
void moveTo (Maillon* m){
	cam_x = -m->tile_info->x*zoom + (LF/2 - m->tile_info->poly->width/2*zoom);
	cam_y = -m->tile_info->y*zoom + (HF/2 - m->tile_info->poly->height/2*zoom);
}

/**
 * @brief action du bouton Go sur le menu recherche
 * 
 */
void actionOfGo(){
	rechercheMaillonsGFX();
	if(RechGFX.result != NULL && RechGFX.result[0] != NULL){
		if(state == tree_view){
			moveTo(RechGFX.result[0]);
		}
		if(state == network_view){
			start = RechGFX.result[0];
			resetNetworkTree(start,count.value);
			moveTo(start);
		}
		if(state == fractals_view){
			start = RechGFX.result[0];
		}
		RechGFX.isShown = false;
	}
}

/**
 * @brief action du bouton recherche du menu recherche
 * 
 */
void actionOfRecherche(){
	rechercheMaillonsGFX();
	if(RechGFX.result != NULL){
		RechGFX.showResult = true;
		RechGFX.yscroll = 0;
	}
}

/**
 * @brief  version customisée de strcpy avec allocation memoire
 * 
 * @param c 
 * @return char* 
 */
char* initChaine(char *c){
	char *c2;
	if(strcmp("",c)){
		c2 = str_format("%s",c);
	}else{
		c2 = NULL;
	}
	return c2;
}

/**
 * @brief Affichage du menu recherche d individus
 * 
 */
void renderRechercheGFX(void){
	if(RechGFX.isShown){
		setColor4i(0,0,0,120);
		rectangle(0,0,LF,HF);
		couleurCourante(0,0,0);
		rectangle(0,0,400,HF);
		afficheTextField(&(RechGFX.nom),50,				7*HF/11);
		afficheTextField(&(RechGFX.prenom),50,			8*HF/11);
		afficheTextField(&(RechGFX.date_naissance),50,	5*HF/11);
		afficheTextField(&(RechGFX.date_mort),50,		4*HF/11);
		afficheTextField(&(RechGFX.lieu_naissance),50,	3*HF/11);
		afficheTextField(&(RechGFX.lieu_mort),50,	 	2*HF/11);
		afficheTextField(&(RechGFX.numgen),50,		   	9*HF/11);

		afficheBouton(50,HF/10,&(RechGFX.recherche));
		afficheBouton(50+140+20,HF/10,&(RechGFX.btn_go));

		epaisseurDeTrait(3);
		couleurCourante(255,255,255);
		textField* txt = &(RechGFX.nom);
		fitTextToRectangle("Nom :",txt->x,txt->y+txt->height,txt->width,txt->height/2);
		txt = &(RechGFX.date_naissance);
		fitTextToRectangle("Date Naissance :",txt->x,txt->y+txt->height,txt->width,txt->height/2);
		txt = &(RechGFX.prenom);
		fitTextToRectangle("Prenom :",txt->x,txt->y+txt->height,txt->width,txt->height/2);
		txt = &(RechGFX.date_mort);
		fitTextToRectangle("Date Mort :",txt->x,txt->y+txt->height,txt->width,txt->height/2);
		txt = &(RechGFX.lieu_naissance);
		fitTextToRectangle("Lieu Naissance :",txt->x,txt->y+txt->height,txt->width,txt->height/2);
		txt = &(RechGFX.lieu_mort);
		fitTextToRectangle("Lieu Mort :",txt->x,txt->y+txt->height,txt->width,txt->height/2);
		txt = &(RechGFX.numgen);
		fitTextToRectangle("Gen :",txt->x,txt->y+txt->height,txt->width,txt->height/2);
		
		DropMenu* dm = &(RechGFX.genre);
		fitTextToRectangle("Genre :",dm->x,dm->y+dm->height,dm->width,dm->height/2);
		RechGFX.genre.show(&(RechGFX.genre),50,6*HF/11);

		if(RechGFX.showResult){
			if(RechGFX.result != NULL){
				if(Menu_Settings.light_mode){
					couleurCourante(255,255,255);
				}
				else{
					couleurCourante(40,40,40);
				}
				rectangle(400,0,LF,HF);
				for(int i=0;RechGFX.result[i] != NULL;i++){
					Maillon* tmp = RechGFX.result[i];
					int oldZoom = zoom;
					int oldX = tmp->tile_info->x;
					int oldY = tmp->tile_info->y;
					int oldCamx = cam_x;
					int oldCamy = cam_y;
					zoom = 1;
					cam_x = 0;
					cam_y = 0;

					tmp->tile_info->x = 400 + (LF-400)/2 - tmp->tile_info->poly->width/2;
					tmp->tile_info->y = HF-(tmp->tile_info->poly->height + 200)*(i+1) + RechGFX.yscroll;

					afficheIndividuTile(tmp);

					zoom = oldZoom;
					cam_x = oldCamx;
					cam_y = oldCamy;
					tmp->tile_info->x = oldX;
					tmp->tile_info->y = oldY;
				}
			}
		}
	}
}

/**
 * @brief Actualisation du menu recherche d'individus
 * 
 */
void updateRechercheGFX(void){
	if(RechGFX.isShown){
		textFieldClickListener(&(RechGFX.nom));
		textFieldClickListener(&(RechGFX.prenom));
		textFieldClickListener(&(RechGFX.date_naissance));
		textFieldClickListener(&(RechGFX.date_mort));
		textFieldClickListener(&(RechGFX.lieu_naissance));
		textFieldClickListener(&(RechGFX.lieu_mort));
		textFieldClickListener(&(RechGFX.numgen));
		executeActionBouton(RechGFX.recherche);
		executeActionBouton(RechGFX.btn_go);
		RechGFX.genre.update(&(RechGFX.genre));

		if(RechGFX.showResult && RechGFX.result != NULL && RechGFX.result[0] != NULL){
			if(handleSouris && mouseState == MOUSE_SCROLL_UP){
				if(RechGFX.yscroll > 0){
					RechGFX.yscroll -= 75;
				}
			}
			if(handleSouris && mouseState == MOUSE_SCROLL_DOWN){
				if(RechGFX.yscroll < (RechGFX.result[0]->tile_info->poly->height + 200)*(count(RechGFX.result))){
					RechGFX.yscroll += 75;
				}
			}
			if(handleSouris && mouseState == MOUSE_LEFT_UP){
				bool selected = false;
				for(int i=0;RechGFX.result[i] != NULL;i++){
					Maillon* tmp = RechGFX.result[i];
					int x = 400 + (LF-400)/2 - tmp->tile_info->poly->width/2;
					int y = HF-(tmp->tile_info->poly->height + 200)*(i+1) + RechGFX.yscroll;
					if(pointIsInRectangle(AS,OS,x,y,tmp->tile_info->poly->width,tmp->tile_info->poly->height)){
						toshow = tmp;
						selected = true;
						break;
					}
				}
				if(!selected){
					toshow = NULL;
				}
			}
		}
		handleSouris = false;
	}
}

/**
 * @brief Creation d une nouvelle structure de menu recherche d invidus
 * 
 * @return RechercheGFX 
 */
RechercheGFX new_RechercheGFX(void){
	RechercheGFX RechGFX;
	RechGFX.genre = 			new_DropMenu(300,33);
	RechGFX.genre.add(&(RechGFX.genre),"Inconnu");
	RechGFX.genre.add(&(RechGFX.genre),"Homme");
	RechGFX.genre.add(&(RechGFX.genre),"Femme");
	RechGFX.genre.color = (RGB){110,110,110};
	RechGFX.genre.roundBorders(&(RechGFX.genre),0.3);
	RechGFX.genre.selected_option = str_format("Inconnu");
	RechGFX.nom = 				initTextField(0,0,300,33,(RGB){110,110,110},"");
	RechGFX.prenom = 			initTextField(0,0,300,33,(RGB){110,110,110},"");
	RechGFX.date_naissance = 	initTextField(0,0,300,33,(RGB){110,110,110},"");
	RechGFX.date_mort = 		initTextField(0,0,300,33,(RGB){110,110,110},"");
	RechGFX.lieu_naissance = 	initTextField(0,0,300,33,(RGB){110,110,110},"");
	RechGFX.lieu_mort = 		initTextField(0,0,300,33,(RGB){110,110,110},"");
	RechGFX.numgen = 			initTextField(0,0,300,33,(RGB){110,110,110},"");
	RechGFX.recherche = 		initBouton(140,33,"Rechercher",(RGB){110,110,110});
	RechGFX.btn_go = 			initBouton(140,33,"Go",(RGB){110,110,110});
	textFieldRoundBorders(&(RechGFX.nom),0.7);
	textFieldRoundBorders(&(RechGFX.prenom),0.7);
	textFieldRoundBorders(&(RechGFX.date_naissance),0.7);
	textFieldRoundBorders(&(RechGFX.date_mort),0.7);
	textFieldRoundBorders(&(RechGFX.lieu_naissance),0.7);
	textFieldRoundBorders(&(RechGFX.lieu_mort),0.7);
	textFieldRoundBorders(&(RechGFX.numgen),0.7);
	roundBorders(&(RechGFX.recherche),0.7);
	roundBorders(&(RechGFX.btn_go),0.7);
	RechGFX.recherche.addEventListener(&(RechGFX.recherche),"click",actionOfRecherche);
	RechGFX.btn_go.addEventListener(&(RechGFX.btn_go),"click",actionOfGo);
	RechGFX.show = renderRechercheGFX;
	RechGFX.update = updateRechercheGFX;
	RechGFX.isShown = false;
	RechGFX.showResult = false;
	RechGFX.yscroll = 0;
	return RechGFX;
}

/**
 * @brief Trace une fleche entre deux points
 * 
 * @param x1 coordonée x de depart
 * @param y1 coordonée y de depart
 * @param x2 coordonée x d arrivée
 * @param y2 coordonée y d'arrivée
 * @param arrow_width largeur de la poite de fleche
 * @param arrow_length longueur de la pointe de fleche
 */
void traceFleche (int x1,int y1,int x2,int y2,int arrow_width,int arrow_length){
	int AB = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) ); 
	int xC = x2+arrow_length*(x1-x2)/AB;
	int yC = y2+arrow_length*(y1-y2)/AB; 
	int xD = xC+arrow_width*(-(y2-y1))/AB;
	int yD = yC+arrow_width*((x2-x1))/AB; 
	int xE = xC-arrow_width*(-(y2-y1))/AB;
	int yE = yC-arrow_width*((x2-x1))/AB; 
	ligne(cam_x+x1*zoom,cam_y+y1*zoom,cam_x+x2*zoom,cam_y+y2*zoom);
	ligne(cam_x+xD*zoom,cam_y+yD*zoom,cam_x+x2*zoom,cam_y+y2*zoom);
	ligne(cam_x+xE*zoom,cam_y+yE*zoom,cam_x+x2*zoom,cam_y+y2*zoom);
}

/**
 * @brief Trace une double fleche entre deux points
 * 
 * @param x1 coordonée x de depart
 * @param y1 coordonée y de depart
 * @param x2 coordonée x d arrivée
 * @param y2 coordonée y d'arrivée
 * @param arrow_width largeur de la poite de fleche
 * @param arrow_length longueur de la pointe de fleche
 */
void doubleFleche (int x1,int y1,int x2,int y2,int arrow_width,int arrow_length){
	traceFleche(x1,y1,x2,y2,arrow_width,arrow_length);
	traceFleche(x2,y2,x1,y1,arrow_width,arrow_length);
}

/**
 * @brief Fonction d'affichage d'un maillons pour les fractales
 * 
 * @param m maillon a afficher
 * @param x coordonnée x
 * @param y coordonée y
 * @param size taille de la tile a afficher en px
 * @param recursfact facteur de recursivité pour la couleur d affichage
 */
void afficheMaillonFractale (Maillon* m,float x,float y,float size,int recursfact){
	float xrel = (x-size/2)*zoom + cam_x;
	float yrel = (y-size/2)*zoom + cam_y;
	if(xrel > -size*2*zoom && xrel < LF && yrel > -size*2*zoom && yrel < HF){
		couleurCourante(100,100,100);
		m->tile_info->x = xrel;
		m->tile_info->y = yrel;
		m->tile_info->poly->width = size*zoom;
		m->tile_info->poly->height = size*zoom;
		cercle(cam_x + (x)*zoom,cam_y + (y)*zoom,sqrt(2)*size/2*zoom,40);
		ecrisImageTextureCustom(m->data->photo,cam_x + (x-size/2)*zoom,cam_y + (y-size/2)*zoom,size*zoom,size*zoom);
		couleurCourante(0,0,0);
		float sizet = size*20/120*zoom;
		if(sizet > 6){
			afficheChaine(m->data->name[0],sizet,cam_x + (x)*zoom-tailleChaine(m->data->name[0],sizet)/2,cam_y + (y-110/pow(10,recursfact))*zoom);
			afficheChaine(m->data->surname,sizet,cam_x + (x)*zoom-tailleChaine(m->data->surname,sizet)/2,cam_y + (y-140/(pow(10,recursfact)))*zoom);
		}
	}
}


//sous fonction de traceCercleFract()
RGB getColorAtRank(int val,int max){
	return (RGB) {0,255*val/max,255 - 255*val/max};
}

//sous fonction de traceCercleFract()
RGB getLinkColor (Maillon* m,Maillon* m2){
	if(m->dad == m2){
		return Menu_Settings.button_settings[3].couleur;
	}
	if(m->mom == m2){
		return Menu_Settings.button_settings[3].couleur;
	}
	if(m->wedding != NULL && m->wedding->data->maries[!*(m->data->gender)] == m2){
		return Menu_Settings.button_settings[4].couleur;
	}
	if(m->sibling != NULL){
		for(int i=0;m->sibling[i] != NULL;i++){
			if(m->sibling[i] == m2){
				return Menu_Settings.button_settings[5].couleur;
			}
		}
	}
	if(m->child != NULL){
		for(int i=0;m->child[i] != NULL;i++){
			if(m->child[i] == m2){
				return Menu_Settings.button_settings[3].couleur;
			}
		}
	}
	return (RGB) {0,0,0};
}

/**
 * @brief Fonction d'affichage des maillons factorielle
 * 
 * @param xc x du centre du cercle
 * @param yc y du centre du cercle
 * @param r rayon du cercle en px
 * @param repet facteur de recursivité
 * @param m maillon a afficher
 */
void traceCercleFract (float xc,float yc,float r,int repet,Maillon* m){
	if(repet > 0 && r*zoom>10){ //condition d'arret si le cercle est trop petit ou limite d'appels recursifs

		//on cherche les coordonées du cercle selon la camera et le zoom
		float xrel = (xc-r)*zoom + cam_x;
		float yrel = (yc-r)*zoom + cam_y;
		
		// si le cercle est visible on l'affiche
		if(xrel > -r*2*zoom && xrel < LF && yrel > -r*2*zoom && yrel < HF){
			couleurCouranteRGB(getColorAtRank(repet,MAX_ZOOM_10POW));
			cercle(cam_x+xc*zoom,cam_y+yc*zoom,r*zoom,50);
			afficheMaillonFractale(m,xc,yc,r*1./3,MAX_ZOOM_10POW-repet);
		}

		//obtention de tous les liens de l individu
		Maillon** lienliste = getLinkList(m);
		int nbpt = count(lienliste) -1;
		for(int i=0;i<nbpt;i++){	//on va diviser le cercle en (nombre de liens) parts egales
			float x = cos(i*2*PI/nbpt)*r;
			float y = sin(i*2*PI/nbpt)*r;

			//pour chaque part du cercle on lance un appel recursif pour recreer un cercle a pour chaque part du cercle courant
			if(xrel > -r*2*zoom && xrel < LF && yrel > -r*2*zoom && yrel < HF){
				float x1 = cos(i*2*PI/nbpt)*r/3;
				float y1 = sin(i*2*PI/nbpt)*r/3;
				epaisseurDeTrait(2);
				//on relie les cercle en suivant la couleur du lien
				couleurCouranteRGB(getLinkColor(m,lienliste[i]));
				ligne(cam_x+(xc+x1)*zoom,cam_y+(yc+y1)*zoom,cam_x+(xc+x)*zoom,cam_y+(yc+y)*zoom);
			}
			traceCercleFract((xc+x),(yc+y),r/10,repet-1,lienliste[i]);
		}
		//liberation mémoire
		free(lienliste);
		lienliste = NULL;
	}
}

/**
 * @brief fonction de gestion de click dans la vue fractale
 * 
 */
void LeftClickUpFractals (){
	bool selected = false;
		if(!moved){
			for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
				if(pointIsInCircle(AS,OS,tmp->tile_info->x,tmp->tile_info->y,tmp->tile_info->poly->width)){
					if(toshow != tmp){
						toshow = tmp;
						selected = true;
						break; 		//Si c est ca je me casse !
					}
				}
			}
		}
		if(!selected && !moved){
			toshow = NULL;
		}
	moved = false;
}

/**
 * @brief Affichage d'un maillon pour la vue reseau
 * optimization si la tile est trop petite
 * @param m maillon a afficher
 * @param size taille de la tile a afficher
 */
void afficheMaillonReseau (Maillon* m,int size){
	float xrel = (m->tile_info->x-size/2)*zoom + cam_x;
	float yrel = (m->tile_info->y-size/2)*zoom + cam_y;
		if(xrel > -size*2*zoom && xrel < LF && yrel > -size*2*zoom && yrel < HF){
		couleurCourante(100,100,100);
		cercle(cam_x + (m->tile_info->x)*zoom,cam_y + (m->tile_info->y)*zoom,sqrt(2)*size/2*zoom,40);
		ecrisImageTextureCustom(m->data->photo,cam_x + (m->tile_info->x-size/2)*zoom,cam_y + (m->tile_info->y-size/2)*zoom,size*zoom,size*zoom);
		couleurCourante(0,0,0);
		int sizet = 20*zoom;
		if(sizet > 6){
			afficheChaine(m->data->name[0],sizet,cam_x + (m->tile_info->x)*zoom-tailleChaine(m->data->name[0],sizet)/2,cam_y + (m->tile_info->y-90)*zoom);
			afficheChaine(m->data->surname,sizet,cam_x + (m->tile_info->x)*zoom-tailleChaine(m->data->surname,sizet)/2,cam_y + (m->tile_info->y-120)*zoom);
		}
	}
}

/**
 * @brief fonction de tracage d un petit cercle
 * 
 * @param x coordonée x
 * @param y coordonée y
 * @param r rayon du cercle
 */
void traceCercle (int x,int y,int r){
	int diam = 2*r;
	epaisseurDeTrait(diam);
	point(x,y);
}

/**
 * @brief fonction de calcul de l'ordonée y en fonction de x sur un demi cercle inferieur
 * 
 * @param x abscisse du point
 * @param r rayon du cercle
 * @return int 
 */
int cercleInferieur (int x,int r){
	return (int)-sqrt(r*r - x*x);
}

/**
 * @brief fonction de calcul de l'ordonée y en fonction de x sur un demi cercle superieur
 * 
 * @param x abscisse du point
 * @param r rayon du cercle
 * @return int 
 */
int cercleSuperieur (int x,int r){
	return (int)-sqrt(r*r - x*x);
}

/**
 * @brief fonction generant un point aléatoire sur un cercle
 * 
 * @param r rayon du cercle
 * @return Point 
 */
Point generePointAleatoireCercle (int r){
	Point pt;
	pt.x =(int) ( (1.*valeurAleatoire()*2 -1)*r );
	if(valeurAleatoire() >= 0.5){
		pt.y = (int)sqrt(r*r - pt.x*pt.x);
	}
	else{
		pt.y = (int)-sqrt(r*r - pt.x*pt.x);
	}
	return pt;
}

/**
 * @brief doit on afficher un maillon ?
 * 
 * @param m 
 * @return true 
 * @return false 
 */
bool showMaillon (Maillon* m){
	return !(m->tile_info->x == -999 && m->tile_info->y == -999);
}

/**
 * @brief y'a t il assez de place autour des maillons pour en placer un nouveau sur la vue reseau ?
 * 
 * @param pt point x,y
 * @param ptrTete tete de liste
 * @param rayon rayon
 * @return true oui il y a de la place
 * @return false non pas de place
 */
bool placeLibre (Point pt,Maillon* ptrTete,int rayon){
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(pointIsInCircle(pt.x,pt.y,tmp->tile_info->x,tmp->tile_info->y,rayon)){
			return false;
		}
	}
	return true;
}

/**
 * @brief genere un  point libre avec de l espace pour placer un maillon en graphique
 * 
 * @param x coordonée x
 * @param y coordonée y
 * @param rayon rayon
 * @return Point 
 */
Point generePointLibre (int x,int y, int rayon){
	int timeout = 1000;
	int cpt = 0;
	bool libre = false;
	Point pt;
	while(libre == false && cpt < timeout){
		pt = generePointAleatoireCercle(rayon);
		pt.x += x;
		pt.y += y;
		if(placeLibre(pt,ptrTete,rayon-cpt)){
			libre = true;
		}
		cpt++;
	}
	return pt;
}

/**
 * @brief fonction recursive de génération de points pour la vue en reseau
 * 
 * @param start maillon central du reseau
 * @param n rang de distance du maillon central
 * @param max rand de distance maximal du maillon central
 * @param x coordonée x
 * @param y coordonée y
 */
void networkTree (Maillon* start,int n,int max,int x,int y){
	if(n < max){	//sortie de la recursivité si on a atteint le rang max
		if(!showMaillon(start)){
			start->tile_info->x = x;
			start->tile_info->y = y;
		}
		else{	//on place le maillon graphiquement
			x = start->tile_info->x;
			y = start->tile_info->y;
		}
		if(start->dad != NULL){ // on genere l emplacement du pere
			Point pt = generePointLibre(x,y,800);
			networkTree(start->dad,n+1,max,pt.x,pt.y);
		}
		if(start->mom != NULL){	// on genere l emplacement de la mere
			Point pt = generePointLibre(x,y,800);
			networkTree(start->mom,n+1,max,pt.x,pt.y);
		}
		if(start->wedding != NULL){	// on genere l emplacement du conjoint
			Point pt = generePointLibre(x,y,800);
			networkTree(start->wedding->data->maries[!(*(start->data->gender))],n+1,max,pt.x,pt.y);
		}
		if(start->sibling != NULL){	// on genere l emplacement des freres/soeurs
			for(int i=0;start->sibling[i]!= NULL;i++){
				Point pt = generePointLibre(x,y,800);
				networkTree(start->sibling[i],n+1,max,pt.x,pt.y);
			}
		}
		if(start->child != NULL){	// on genere l emplacement des enfants
			for(int i=0;start->child[i]!= NULL;i++){
				Point pt = generePointLibre(x,y,800);
				networkTree(start->child[i],n+1,max,pt.x,pt.y);
			}
		}
	}
}

/**
 * @brief demarage de la creation d un resaue d individus
 * 
 * @param ptrTete tete de liste
 * @param start maillon central
 * @param n rang de distance maximale  au maillon centrale
 */
void startNetworkTree (Maillon* ptrTete,Maillon* start,int n){
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		tmp->tile_info->x = -999;
		tmp->tile_info->y = -999;
	}
	networkTree(start,0,n+1,0,0);
}

/**
 * @brief un maillon fait partie d un tableau ?
 * 
 * @param m maillon
 * @param list tableau de maillon
 * @param show_all_lien 
 * @return true le maillon fait partie de la liste
 * @return false le maillon ne fait pas partie de la liste
 */
bool isInList (Maillon* m,Maillon** list,bool show_all_lien){
	if(show_all_lien) return false;
	for(int i=0;list[i] != NULL;i++){
		if(m == list[i]){
			return true;
		}
	}
	return false;
}

/**
 * @brief ajouter un maillon a un tableau
 * 
 * @param m 
 * @param list 
 */
void addToList (Maillon* m,Maillon ***list){
	int size = count(*list)-1;
	*list = realloc(*list,sizeof(Maillon)*(size+2));
	(*list)[size] = m;
	(*list)[size+1] = NULL;
}

/**
 * @brief reinitialise le maillon centre d un reseau en gardant la position des maillons commun a l ancien centre
 * 
 * @param start nouveau centre
 * @param n rang maximal de distance
 */
void resetNetworkTree (Maillon* start,int n){
	Maillon** listMaillon = malloc(sizeof(Maillon*));
	Point* listPt = malloc(sizeof(Point));
	listMaillon[0] = NULL;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(showMaillon(tmp)){
			int size = count(listMaillon)-1;
			listMaillon = realloc(listMaillon,sizeof(Maillon)*(size+2));
			listMaillon[size] = tmp;
			listMaillon[size+1] = NULL;
			listPt = realloc(listPt,sizeof(Point)*(size+1));
			listPt[size] = (Point) {tmp->tile_info->x,tmp->tile_info->y};
		}
	}

	startNetworkTree(ptrTete,start,n);

	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(showMaillon(tmp)){
			tmp->tile_info->x = -999;
			tmp->tile_info->y = -999;
			for(int i=0;listMaillon[i]!=NULL;i++){
				if(tmp == listMaillon[i]){
					tmp->tile_info->x = listPt[i].x;
					tmp->tile_info->y = listPt[i].y;
					break;
				}
			}
		}
	}

	free(listMaillon);
	free(listPt);
	networkTree(start,0,n+1,0,0);
}

/**
 * @brief affichage des liens entre individus dans la vue reseau
 * 
 * @param ptrTete tete de liste
 * @param show_all_lien representation complexe avec tous les liens ?
 */
void afficheLienNetworkTree (Maillon* ptrTete,bool show_all_lien){
	Maillon** dejalie = malloc(sizeof(Maillon*));
	dejalie[0] = NULL;
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(showMaillon(tmp)){;
			if(tmp->mom != NULL && showMaillon(tmp->mom) && !isInList(tmp->mom,dejalie,show_all_lien)){
				Maillon* m1 = tmp;
				Maillon* m2 = tmp->mom;
				addToList(m2,&dejalie);
				int x1 = m1->tile_info->x,
					x2 = m2->tile_info->x,
					y1 = m1->tile_info->y,
					y2 = m2->tile_info->y;
				int AB = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) ); 
				int x4 = x1 + sqrt(2)*120/2* (x2-x1)*1./AB;
				int y4 = y1 + sqrt(2)*120/2* (y2-y1)*1./AB;
				int x3 = x2 + sqrt(2)*120/2* (x1-x2)*1./AB;
				int y3 = y2 + sqrt(2)*120/2* (y1-y2)*1./AB;
				couleurCouranteRGB(getLinkColor(m1,m2));
				traceFleche(x4,y4,x3,y3,20,40);
			}
			if(tmp->dad != NULL && showMaillon(tmp->dad) && !isInList(tmp->dad,dejalie,show_all_lien)){
				Maillon* m1 = tmp;
				Maillon* m2 = tmp->dad;
				addToList(m2,&dejalie);
				int x1 = m1->tile_info->x,
					x2 = m2->tile_info->x,
					y1 = m1->tile_info->y,
					y2 = m2->tile_info->y;
				int AB = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) ); 
				int x4 = x1 + sqrt(2)*120/2* (x2-x1)*1./AB;
				int y4 = y1 + sqrt(2)*120/2* (y2-y1)*1./AB;
				int x3 = x2 + sqrt(2)*120/2* (x1-x2)*1./AB;
				int y3 = y2 + sqrt(2)*120/2* (y1-y2)*1./AB;
				couleurCouranteRGB(getLinkColor(m1,m2));
				traceFleche(x4,y4,x3,y3,20,40);
			}
			if(tmp->wedding != NULL && showMaillon(tmp->wedding->data->maries[!(*(tmp->data->gender))]) && !isInList(tmp->wedding->data->maries[!(*(tmp->data->gender))],dejalie,show_all_lien)){
				Maillon* m1 = tmp;
				Maillon* m2 = tmp->wedding->data->maries[!(*(tmp->data->gender))];
				addToList(m2,&dejalie);
				int x1 = (m1->tile_info->x),
					x2 = (m2->tile_info->x),
					y1 = (m1->tile_info->y),
					y2 = (m2->tile_info->y);
				int AB = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) ); 
				if(AB != 0){
					int x4 = x1 + sqrt(2)*120/2* (x2-x1)*1./AB;
					int y4 = y1 + sqrt(2)*120/2* (y2-y1)*1./AB;
					int x3 = x2 + sqrt(2)*120/2* (x1-x2)*1./AB;
					int y3 = y2 + sqrt(2)*120/2* (y1-y2)*1./AB;
					couleurCouranteRGB(getLinkColor(m1,m2));
					doubleFleche(x4,y4,x3,y3,20,40);
				}
			}
			if(tmp->sibling != NULL){
				for(int i = 0;tmp->sibling[i] != NULL;i++){
					if(showMaillon(tmp->sibling[i]) && !isInList(tmp->sibling[i],dejalie,show_all_lien)){
						Maillon* m1 = tmp;
						Maillon* m2 = tmp->sibling[i];
						addToList(m2,&dejalie);
						int x1 = (m1->tile_info->x),
							x2 = (m2->tile_info->x),
							y1 = (m1->tile_info->y),
							y2 = (m2->tile_info->y);
						int AB = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) ); 
						if(AB != 0){
							int x4 = x1 + sqrt(2)*120/2* (x2-x1)*1./AB;
							int y4 = y1 + sqrt(2)*120/2* (y2-y1)*1./AB;
							int x3 = x2 + sqrt(2)*120/2* (x1-x2)*1./AB;
							int y3 = y2 + sqrt(2)*120/2* (y1-y2)*1./AB;
							couleurCouranteRGB(getLinkColor(m1,m2));
							doubleFleche(x4,y4,x3,y3,20,40);
						}
					}
				}
			}
		}
	}
	free(dejalie);
}

/**
 * SOUS FONCTION DE afficheNetworkTree()
 */
void afficheIndividusNetworkTree (Maillon* ptrTete,Maillon* start){
	for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
		if(showMaillon(tmp)){
			if(tmp == start){
				couleurCourante(255,0,0);
				int width = 80;
				rectangle(cam_x + (tmp->tile_info->x -width)*zoom,cam_y + (tmp->tile_info->y -width)*zoom,cam_x + (tmp->tile_info->x +width)*zoom,cam_y + (tmp->tile_info->y +width)*zoom);
			}
			afficheMaillonReseau(tmp,120);
		}
	}
}

/**
 * @brief affichage des maillons reseau selon leur position prédefinie
 * 
 * @param ptrTete tete de liste
 * @param start maillon central
 * @param show_all_lien afficher tous les liens ? (affichage complexe)
 */
void afficheNetworkTree (Maillon* ptrTete,Maillon* start,bool show_all_lien){
	afficheLienNetworkTree(ptrTete,show_all_lien);
	afficheIndividusNetworkTree(ptrTete,start);
}