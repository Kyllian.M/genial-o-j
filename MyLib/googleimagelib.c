#ifdef _WIN32
	#include <windows.h>
#endif
#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "../GfxLib/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "../GfxLib/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "../GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <unistd.h>
#include <string.h>
#include "consoletools.h"
#include "../GfxLib/commonIO.h"
#include "KyllianToolsLib.h"
#include "KToolsLibForGfxLib.h"
#include "sdlglutils.h"
#include "geometry.h"
#include "googleimagelib.h"

GoogleSearchIMG* new_GoogleSearchIMG (char* termes,int nb_resultats,char* save_location){
	char current_dir[100];
	getcwd(current_dir, sizeof(char)*100);
	char* location = str_format("%s/%s",current_dir,save_location);
	if(location[strlen(location)-1] == '/'){
		location[strlen(location)-1] = '\0';
	}

	GoogleSearchIMG* ret = malloc(sizeof(GoogleSearchIMG));
	ret->termes = termes;
	ret->nb_resultats = nb_resultats;
	ret->save_location = location;
	ret->finished = false;
	ret->logs = NULL;
	ret->file_location = NULL;
	ret->result = malloc(sizeof(Image*)*nb_resultats);
	for(int i = 0;i< nb_resultats;i++){
		ret->result[i] = NULL;
	}

	char* command;
	command = str_format("rm -rf \"%s/*\"",save_location);
	system(command);
	command = str_format("googleimagesdownload --keywords \"%s\" --limit %d -o \"%s\" > \"%s/logs.txt\"",termes,nb_resultats,location,location);
	ret->google_process = popen(command,"r");
	ret->null_process = popen("","r");		//Pour activer le multithreading il faut ouvrir 3 process JSP pourquoi
	return ret;
}

void updateGoogleSearchIMG (GoogleSearchIMG* google){
	if(google != NULL && !google->finished){
		if(google->logs == NULL){
			char* logs_location = str_format("%s/logs.txt",google->save_location);
			static FILE* logs = NULL;
			logs = fopen(logs_location,"rt");
			if(logs == NULL || strlen(fileGetText(logs_location)) < 10){
				fclose(logs);
			}
			else{
				pclose(google->google_process);
				pclose(google->null_process);
				fclose(logs);
				google->logs = fileGetText(logs_location);
				println("%s",google->logs);
				char* command = str_format("ls \"%s/%s\" > \"%s/filelist.txt\"",google->save_location,google->termes,google->save_location);
				system(command);
			}
		}
		else{
			if(google->file_location == NULL){
				char* flist_location = str_format("%s/filelist.txt",google->save_location);
				static FILE* flist = NULL;
				flist = fopen(flist_location,"rt");
				if(flist == NULL || strlen(fileGetText(flist_location)) < 10){
					fclose(flist);
				}
				else{
					fclose(flist);
					char* fcontents = fileGetText(flist_location);
					println("%s",fcontents);
					google->file_location = str_split(fcontents,'\n');
					println("%s",google->file_location[0]);
				}
			}
			else{
				int imagetoload;
				for(imagetoload=0;imagetoload < google->nb_resultats && google->result[imagetoload] != NULL;imagetoload++);
				println("image %d",imagetoload);
				char* img_location = str_format("%s/%s/%s",google->save_location,google->termes,google->file_location[imagetoload]);
				google->result[imagetoload] = new_Image(img_location,true);
				if(imagetoload == google->nb_resultats-1){
					google->finished = true;
					for(int i = 0;i<google->nb_resultats;i++){
						system(str_format("rm -rf \"%s/%s/\"",google->save_location,google->termes,google->file_location[i]));
					}
				}
			}
		}
	}
}

void saveGoogleImageAs (GoogleSearchIMG* google,int numImage,char* path){
	char* img_location = str_format("%s/%s/%s",google->save_location,google->termes,google->file_location[numImage]);
	char* command = str_format("mv \"%s\" \"%s\"",img_location,path);
	println("%s",command);
	system(command);
}
