import sys
from geopy.geocoders import Nominatim

in_str = "" + sys.argv[1] + ", " + sys.argv[2]

geolocator = Nominatim(user_agent="geolocator")
location = geolocator.reverse(in_str)

file = open("../Ressources/geolocator.txt","w")
string = ""
if location.address is None :
    string = "NOWHERE"
else :
    string = location.address
file.write(string.encode('utf8'))
file.close()