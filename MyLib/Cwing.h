// TYPEDEFS

typedef Point** Trajectoire;

// Structure bouton clickable
typedef struct bouton bouton;
struct bouton{
	int width,height;
	RGB couleur;
	char *texte;
	int tailleTexte;
	DonneesImageRGB* skin;
	int x,y;
	bool show;
	bool hover;
	Polygone* poly;

	/**
	 * @brief ajout d'un evenement a un bouton
	 * 
	 * @param listener type d'evenement activant le bouton ex: "click", "key_a" ...
	 * @param event fonction callback du bouton
	 */
	void (*addEventListener) (bouton *B,char* listener,void (*event) (void));
	void (*event) (void);
	char listener[50];
};

//structure champs de texte
typedef struct textField{
	int width,height;
	RGB couleur;
	char *texte;
	int tailleTexte;
	DonneesImageRGB* skin;
	int x,y;
	void (*event) (void);
	Polygone* poly;
	
	bool editing;
	bool showCursor;
	bool isTitle;
} textField;

//structure menu clic droit
typedef struct ClickMenu ClickMenu;
struct ClickMenu{
	bouton* option;
	int* separateurs;
	int nboption;
	int nbseparateurs;
	bool is_shown;

	void (*show) (ClickMenu* self,int x,int y);
	void (*update) (ClickMenu* self);
};

//structure menu déroulant
typedef struct DropMenu DropMenu;
struct DropMenu {
	int x,y;
	int width,height;
	char** option;
	char* selected_option;
	int index_selected_option;
	bool isOpened;
	Polygone* poly;
	RGB color;
	RGB text_color;
	RGB hover_color;
	RGB hover_text_color;
	int index_option_hovered;

	//Methodes

	//ajout d'une option au dropMenu
	void (*add) (DropMenu* self,char* option);
	//retourne l'index de l'option donnée
	int (*indexOf) (DropMenu* self,char* option);
	//supprime une option du dropmenu
	void (*delete) (DropMenu* self,char* option);
	//affiche le dropmenu
	void (*show) (DropMenu* self,int x,int y);
	//arrondit les bordures du dropmenu
	void (*roundBorders) (DropMenu* self,float factor);
	//actualise le dropmenu
	void (*update) (DropMenu* self);
	//supprime toutes les options du dropmenu
	void (*deleteAll) (DropMenu* self);
	void (*event) ();
};

//structure permettant de faire varier des coordonées selon une trajectoire
typedef struct Mover {
	bool activated;
	Trajectoire trajectoire;
	int current_pt;
	bool repeat;
	double t0;
	float tps;
	float elapsed_time;
	bool reverse;
	bool reverse_loop;
	bool loop;
	
	int *x;
	int *y;
}Mover;

//structure slider
typedef struct Slider Slider;
struct Slider{
	int width;
	int x,y;
	double min,max;
	double value;
	RGB color;
	bool float_mode;
	bool selected;
	void (*callback) (void);

	//definit si le slider accepte les flottants
	void (*setFloatMode) (Slider* self);
	//definit si le slider n accepte que les entiers
	void (*setIntMode) (Slider* self);
	//affichage
	void (*show) (Slider* self,int x,int y,int width);
	//définit le callback a la selection d une option
	void (*setCallback) (Slider* self,void (*callback) (void));
	//actualisation
	void (*update) (Slider* self);
};

//structure selecteur de couleur RGB
typedef struct ColorPicker ColorPicker;
struct ColorPicker{
	bool selected;
	RGB selection;
	DonneesImageRGB* gradient;
	RGB** indexer;
	int width;
	int height;
	int x,y;
	int xselec,yselec;

	Slider r_slider;
	Slider g_slider;
	Slider b_slider;

	bouton validate;

	//actualisation
	void (*update) (ColorPicker* self);
	//affichage
	void (*show) (ColorPicker* self,int x,int y);
	//definit la fonction a appeler lorsque une couleur est selectionée
	void (*setCallback) (ColorPicker* self,void (*function) (void));
};

typedef struct Counter Counter;
struct Counter{
	int x,y;
	int width,height;
	int value;
	int valmax;
	int valmin;
	RGB color;

	void (*eventListener) (void);
};

// FONCTIONS

/**
 * @brief création d un nouveau colorPicker
 * 
 * @param width largeur
 * @param height hauteur
 * @return ColorPicker 
 */
ColorPicker new_ColorPicker (int width,int height);

/**
 * @brief création d un nouveau dropMenu
 * 
 * @param width largeur
 * @param height hauteur
 * @return ColorPicker 
 */
DropMenu new_DropMenu (int width,int height);

/**
 * @brief création d un nouveau slider
 * 
 * @param min valeur minimale
 * @param max valeur maximale
 * @param default_value valeur par défaut
 */
Slider new_Slider (double min,double max,double default_value);

/**
 * @brief creation d'un nouveau Mover
 * 
 * @param trajectoire trajectoire a suivre
 * @param repeat repeter letrajet a l infini ?
 * @param tps temps imparti pour faire le trajet
 * @param reverse_loop repeter le trajet en inversé
 * @param loop repeter le trajet a l infini
 * @return Mover 
 */
Mover new_Mover (Point** trajectoire,bool repeat,float tps,bool reverse_loop,bool loop);

/**
 * @brief démare le mover 
 * 
 * @param M 
 */
void activateMover (Mover* M);

/**
 * @brief assigne des coordonée x et y au mover
 * 
 * @param M 
 * @param x 
 * @param y 
 */
void assignToMover (Mover* M,int* x,int* y);

//actualise le mover
void updateMover (Mover* M);

/**
 * @brief creation d un nouveau clickmenu
 * 
 * @param option tableau d'options
 * @param separateurs position des separateurs
 * @param nboption nombre d options
 * @param nbseparateurs nombre de separateurs
 * @return ClickMenu 
 */
ClickMenu new_ClickMenu (bouton* option,int* separateurs,int nboption,int nbseparateurs);

/**
 * @brief création d'un nouveau textfield 
 * 
 * @param xx coordonée x du textfield
 * @param yy coordonée y
 * @param width largeur 
 * @param height hauteur
 * @param couleur couleur du textfield
 * @param chaine chaine par defaut du textfield
 * @return textField 
 */
textField initTextField(int xx,int yy,int width,int height,RGB couleur,char* chaine);
//affichage du textfield
void afficheTextField(textField *B,int x,int y);
//actualisation du textfield
void textFieldClickListener (textField *B);
void textFieldRoundBorders (textField *T,float coef);

/**
 * @brief crée un nouveau bouton
 * 
 * @param width largeur
 * @param height hauteur
 * @param texte texte du bouton
 * @param couleur couleur du bouton
 * @return bouton 
 */
bouton initBouton(int width,int height,char* texte,RGB couleur);
/**
 * @brief crée un nouveau bouton a partir d'une image 
 * 
 * @param chemin chemin d acces de l'image
 * @param fondVert couleur a supprimer
 * @return bouton 
 */
bouton initBoutonFromImg(char* chemin,RGB fondVert);
//affichage du bouton
void afficheBouton(int x,int y,bouton *B);
/* arrondit les bordures du bouton
 * @param coef coeficient de rondeur (entre 0 et 1)
*/
void roundBorders (bouton* B,float coef);
//actualise le bouton
void executeActionBouton (bouton B);

/**
 * @brief affiche du texte en ajustant sa taille de facon a ce qu'l rentre dans un rectangle
 * 
 * @param text texte a afficher
 * @param x x du rectangle
 * @param y y du rectangle
 * @param width largeur du rectangle
 * @param height hauteur du rectangle
 */
void fitTextToRectangle (char* text,int x,int y,int width,int height);

RGB rainbowRGB (int x,int y,int width,int height);

void afficheCounter (Counter* c,int x,int y,int width,int height);

void updateCounter (Counter* c);

Counter new_Counter (int valmin,int val,int valmax);