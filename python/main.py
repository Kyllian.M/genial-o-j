import sys
from geopy.geocoders import Nominatim

in_str = "" + sys.argv[1] + ", " + sys.argv[2]
print(in_str)

geolocator = Nominatim(user_agent="geolocator")
location = geolocator.reverse(in_str)
print(location.address)