#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "consoletools.h"
#include "KyllianToolsLib.h"

#define DBSIZE 16000

/**
 * @brief retourne les cellules d'un fichier CSV
 * 
 * @param nomFichier nom du fichier a charger
 * @return char*** tableau 2D de chaines representant le CSV
 */
char*** getCellsCSV (char* nomFichier){
	DEBUGSTART;
	char* fcontents = fileGetText(nomFichier);
	char*** textByWord = NULL;
	if(fcontents != NULL){
		printdebug("\n%s",fcontents);
		if(fcontents[strlen(fcontents)-1] == '\n'){
			fcontents[strlen(fcontents)-1] = '\0';
		}
		printdebug("\n%s",fcontents);
		char** textByLine = str_split(str_replaceAll(fcontents),'\n');
		textByWord = calloc((count((void**)textByLine)+1),sizeof(char**));
		for(int i=0;textByLine[i];i++){
			if(textByLine[i][strlen(textByLine[i])-3] == ';'){
				printdebug(">>>>>>>>\n\n\n");
				int a = strlen(textByLine[i]);
				textByLine[i] = realloc(textByLine[i],sizeof(char)*(a+1));
				textByLine[i][a-2] = ' ';
				textByLine[i][a-1] = '\0';
			}
			else{
				printdebug(">>>>> %d \n\n",textByLine[i][strlen(textByLine[i])-2] == ';');
			}
			printdebug("i -> %d",i);
			textByWord[i] = str_split(textByLine[i],';');
			free(textByLine[i]);
			//textByLine[i] = NULL;
		}
		
		//textByLine = NULL;
		textByWord[count((void**)textByLine)] = false;
		free(textByLine);
	}
	else{
		printf("ERROR : FILE '%s' NOT FOUND \n (t'es un PD)\n",nomFichier);
	}
	//free(fcontents);
	//fcontents = NULL;
	DEBUGEND;
	return textByWord;
}

/**
 * @brief affichage des cellules d'un fichier CSV
 * 
 * @param textByWord cellules du fichier CSV
 */
void printCellsCSV (char*** textByWord){
	DEBUGSTART;
	for(int i=0;textByWord[i];i++){
		for(int i2=0;textByWord[i][i2];i2++){
			if(textByWord[i][i2][0] == ' '){
				printf("???");
			}
			else{
				printf("%s",textByWord[i][i2]);
			}
			printf("\t");
		}
		printf("\n");
	}
	DEBUGEND;
}

/**
 * @brief charge un fichier CSV
 * 
 * @param file fihier CSV
 * @param CSVcategories nom des colonnes du fichier CSV
 * @param CSVtextdatabyword cellules du fichier qui seront remplies
 */
void chargeCSV (char* file,char*** CSVcategories,char**** CSVtextdatabyword){
	char*** CSVrawtextbyword = getCellsCSV(file);
	printdebug("here");
	printdebug("%s",CSVrawtextbyword[0][0]);
	*CSVcategories = malloc(sizeof(char*)*count((void**)CSVrawtextbyword[0]));
	*CSVtextdatabyword = malloc(sizeof(char**)*(count((void**)CSVrawtextbyword)-1));

	for(int i = 1;CSVrawtextbyword[i-1];i++){
		(*CSVtextdatabyword)[i-1] = CSVrawtextbyword[i];
	}
	printdebug("here");
	foreach(char** mot, CSVrawtextbyword[0]){
		(*CSVcategories)[foreach_cpt] = *mot;
	}
	
	//free(CSVcategories);
	//CSVrawtextbyword = NULL;
}