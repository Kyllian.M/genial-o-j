#define HOMME 0
#define FEMME 1
#define NAME 5
#define SURNAME 6
#define D_BIRTH 2
#define D_DEATH 3
#define GENDER 4
#define NUM_GEN 7

//tableur utilisé pour afficher le CSV
typedef struct Tableur Tableur;
struct Tableur {
	textField** cell;
	int nb_ligne;
	int nb_colonne;

	void (*update)(Tableur* self);
	void (*show)(Tableur* self,int x,int y);
};

//lieu avec emplacement geographique
typedef struct Lieu{
    char* nom;
    double X;
    double Y;
}Lieu;

//structure date
typedef struct Date{
    int day;
    int month;
    int year;
}Date;

//structure contenant la date et le lieu de naissance
typedef struct Birth{
    Date date;
    Lieu lieu;
}Birth;

//structure contenat date et lieu de mort
typedef struct Death{
    Date date;
    Lieu lieu;
}Death;

//informations sur un individu
typedef struct Individu{
	int numGen;
    char *surname;
    char **name;
    Birth *birth_data;
    Death* death_data;
    bool *gender;
    Image *photo;
}Individu;

//Informations sur un mariage
typedef struct Mariage{
    Date date;
    Lieu lieu;
    struct Maillon* maries[2];
}Mariage;

//maillon de liste chainnee mariage
typedef struct MaillonMariage{
    Mariage* data;
    struct MaillonMariage* next;
}MaillonMariage;

//varaibles graphiques pour afffichage d un maillon
typedef struct IndividuTile {
	int x,y;
    bool isShown;
    Polygone* poly;
    Polygone* shadow;
}IndividuTile;

//maillon individu
typedef struct Maillon{
    Individu *data;
    struct Maillon *dad;
    struct Maillon *mom;
    struct Maillon **sibling; 
    MaillonMariage *wedding;
    struct Maillon **child;
    struct Maillon *next;
    IndividuTile *tile_info;
}Maillon;

//critere de recherche d individu
typedef struct Critere{
    int numGen;
	char *surname;
    char *name;
    Birth* birth_data;
    Death* death_data;
    bool *gender;
    Maillon *dad;
    Maillon *mom;
    char *comparaison;
}Critere;

// Anciens prototypes des structures actuelles :

typedef struct Temporaire{
    int* numGen;
	char *surname;
    char *name;
    Birth* birth_data;
    Death* death_data;
    bool *gender;
    struct Maillon *dad;
    struct Maillon *mom;
    struct Maillon **sibling; 
    Mariage** wedding;
    struct Maillon **child;
}Temporaire;

typedef struct rech {
    Maillon **tab;
    int cpt;
}rech;


typedef struct mScore{
    Maillon *m;
    int i;
}mScore;



Maillon** getLinkList (Maillon* m);

Tableur new_Tableur (char* filename);

int countLC (Maillon* ptrtete);

Maillon* goToLC (Maillon* ptrtete,int a);

#define foreachLC(val,lc) \
	for(Maillon* val = lc; val != NULL;val = val->next)

void InsereIndividuFin(Maillon * M ,Maillon ** ptrTete);


/**
 * @brief Fonction utilitaire qui vérifie si deux dates sont comprises dans un intervalle de temps.
 * 
 * @param d1 
 * @param d2 
 * @param period 
 * @return true 
 * @return false 
 */
bool isInPeriod(Date d1, Date d2,Date period);

/**
 * @brief Fonction utilitaire qui retourne le l’index d’un maillon dans une liste chainée.
 * 
 * @param wanted 
 * @param ptrTete 
 * @return int 
 */
int getIndexOf(Maillon* wanted, Maillon* ptrTete);

void InverseListe(Maillon **ptrTete);

Maillon * cherchePrecedent(Maillon * M,Maillon *ptrTete);

MaillonMariage * cherchePrecedentMariage(MaillonMariage * M,MaillonMariage *ptrTete);

void InsereNouveauMaillon(Maillon * M,Maillon ** ptrTete);

void SupprimeMaillon(Maillon* ptrSupp,Maillon **ptrTete,MaillonMariage **ptrTeteMariagesCSV);

//Renvoie la date au format : 01/01/2000
char* dateToString (Date D);

//Renvoie la date au format : 1er Janvier 2000
char* dateToFancyString (Date D);

/**
 * @brief Fonction utilitaire qui trie un tableau de pointeur sur mariages du plus récent au plus vieux.
 * 
 * @param listeDataMariage 
 */
void triListeDataMariage(Mariage*** listeDataMariage);

bool compareIndividu(Individu *I1, Individu *I2);

short compareMariage(Mariage *M1, Mariage* M2);

Maillon * RechercheIndividu(Individu * I,Maillon ** ptrTete);

void KillMaillon(Individu * I,Maillon **ptrTete,MaillonMariage** ptrTeteMariagesCSV);

void freeMaillon(Maillon **suppr);

void freeIndividu(Individu * I);


/**
 * @brief Fonction qui permet de sauvegarder la liste chainée sous forme de fichier spécifié par l’argument numéro un.
 * 
 * @param path 
 * @param ptrTete 
 */
void sauverCsvIndividu(char* path,Maillon* ptrTete);

Birth* new_Birth(char* date_n,char* lieu_n,char* lieu_X_n,char* lieu_Y_n);

Death* new_Death(char* date_n,char* lieu_n,char* lieu_X_n,char* lieu_Y_n);

Lieu new_Lieu(char * lieu_n);

Date new_Date(char* date_n);

Individu* new_Individu (int nbgen,char* nom,char* prenom,char genre,char* date_n,char* lieu_n,char* date_d,char *lieu_d,Image* photo,char* lieu_X_n,char* lieu_Y_n,char* lieu_X_d,char* lieu_Y_d);

Maillon* new_Maillon (Individu* I);


/**
 * @brief Fonction utilitaire qui fait des addtions/soustractions sur des dates.

 * 
 * @param d1 
 * @param d2 
 * @param operator 
 * @return Date 
 */
Date operationDate(Date d1, Date d2, char operator);

Mariage * new_Mariage(char *ndate,char*nlieu,Maillon * M1,Maillon * M2);

bool dateIsNULL (Date D);

/**
 * @brief Fonction utilitaire qui envoie un résumé des informations d’un maillon sur la sortie standard du terminal.
 * 
 * @param ptr 
 */
void printMaillon(Maillon* ptr);

/**
 * @brief Fonction utilitaire qui appelle la fonction printMaillon pour chaque élement de la liste chainée pour avoir un résumé des informations de la liste chainée.
 * 
 * @param ptrtete 
 */
void printLC(Maillon* ptrtete);

/**
 * @brief Fonction utilitaire qui vérifie si deux Maillon possèdent deux personnes qui font partie de la même fratrie.
 * 
 * @param m1 
 * @param m2 
 * @return true 
 * @return false 
 */
bool isSibling(Maillon* m1, Maillon* m2);

void deNullify (char** str);

Maillon* initLC (char*** CSVtextdatabyword);

int stringCompare(char* s1, char* s2);

bool betterstrcmp(char* val1,char* val2);


/**
 * @brief : compare deux lieu, renvoie true si les deux lieux sont égaux et false sinon
 * 
 * @param1 : structure Lieu à comparer 
 * @param2 : structure Lieu à comparer
 * 
 * @return : boolean
 * 
 */
bool compareLieu(Lieu l1,Lieu l2);

/**
 * @brief : compare deux date en fonction du @param3 et renvoie true si @param1 est @param3 que @param2 et false sinon
 * 
 * @param1 : structure Date à comparer
 * @param2 : structure Date à comparer
 * @param3 : caractère '<', '>' ou '=' (pour comparaison de date)
 * 
 * @return : boolean
 */
bool compareDate(Date d1,Date d2,char *comparaison);


/**
 * @brief : compare deux pointeurs sur une structure contenant les informations sur la naissance de l'individu (ne prend pas en compte le cas ou les deux @param sont NULL) et renvoie true si les deux sont égaux et renvoie false sinon
 * 
 * @param1 : pointeurs sur stucture Birth à comparer
 * @param2 : pointeurs sur structure Birth à comparer
 * @param3 : caractère '<', '>' ou '=' (pour comparaison de date)
 * 
 * @return : boolean
 */
bool compareBirth(Birth *b1,Birth *b2,char *comparaison);


/**
 * @brief : compare deux pointeurs sur une structure contenant les informations sur la mort de l'individu (ne prend pas en compte le cas ou les deux @param sont NULL) et renvoie true si les deux sont égaux et renvoie false sinon
 * 
 * @param1 : pointeurs sur stucture Death à comparer
 * @param2 : pointeurs sur structure Death à comparer
 * @param3 : caractère '<', '>' ou '=' (pour comparaison de date)
 * 
 * @return : boolean
 */
bool compareDeath(Death *d1,Death *d2,char *comparaison);


/**
 * @brief : compare deux pointeurs sur une structure contenant les informations sur la naissance de l'individu (prend en compte le cas ou les deux @param sont NULL) et renvoie true si les deux sont égaux ou si @param2 est NULL et renvoie false sinon
 * 
 * @param1 : pointeurs sur stucture Birth à comparer
 * @param2 : pointeurs sur structure Birth à comparer
 * @param3 : caractère '<', '>' ou '=' (pour comparaison de date)
 * 
 * @return : boolean
 */
bool bCompareBirth(Birth *b1,Birth *b2,char *comparaison);


/**
 * @brief : compare deux pointeurs sur une structure contenant les informations sur la mort de l'individu (prend en compte le cas ou les deux @param sont NULL) et renvoie true si les deux sont égaux ou si @param2 est NULL et renvoie false sinon
 * 
 * @param1 : pointeurs sur stucture Death à comparer
 * @param2 : pointeurs sur structure Death à comparer
 * @param3 : caractère '<', '>' ou '=' (pour comparaison de date)
 * 
 * @return : boolean
 */
bool bCompareDeath(Death *d1,Death *d2,char *comparaison);


/**
 * @brief : compare deux pointeurs sur boolean(prend en compte le cas ou les deux @param sont NULL) et renvoie true si les deux sont égaux ou si @param2 est NULL et renvoie false sinon
 * 
 * @param1 : pointeur sur boolean
 * @param2 : pointeur sur boolean
 * 
 * @return : boolean
 */
bool compareGender(bool *g1,bool *g2);

void insertIndividuFinBis( Maillon* ptr, Maillon** addPtrTete);

/**
 * @brief Fonction utilitaire qui vérifie si un maillon est présent dans une liste chainée.
 * 
 * @param ptrTete 
 * @param ptr 
 * @return true 
 * @return false 
 */
bool contains(Maillon* ptrTete,Maillon* ptr);

/**
 * @brief Fonction utilitaire qui supprime les doublons présents dans un tableau de pointeurs sur Maillon.
 * 
 * @param Array 
 * @return Maillon** 
 */
Maillon** supprDoublons(Maillon** Array);

/**
 * @brief  Fonction utilitaire qui fusionne deux tableaux de strings.
 * 
 * @param T1 
 * @param T2 
 * @return char** 
 */
char** fusionTab(char** T1, char** T2);

/**
 * @brief Fonction mettant à jour les liens des fratries de la liste chainée.
 * 
 * @param ptrTete 
 */
void updateLiensFraterie(Maillon* ptrTete);

/**
 * @brief Fonction utilitaire qui fusionne deux tableaux de Maillon.
 * 
 * @param T1 
 * @param T2 
 * @return Maillon** 
 */
Maillon** fusionTabMaillon(Maillon** T1, Maillon** T2);


/**
 * @brief : renvois une struct contenant un tableau de pointeur sur maillon et un entier qui contient la taille du tableau, le contenu du tableau sont les maillons correspondant au critère passé en paramètre 
 * 
 * @param1 : permet de definir quelle champs de la structure individu va être testé (NAME, SURNAME, D_BIRTH, D_DEATH, GENDER, NUM_GEN)
 * @param2 : pointeur contenant le critère d'un champ de la structure (Attention si le mauvais champ et donner par rapport a @param1 le programme cessera de fonctionner)
 * @param3 : pointeur de tête de la liste chainée de maillon
 * @param4 : caractère '<', '>' ou '=' (pour comparaison de date)
 * 
 * @return : tableau contenant les resultats de la recherche
 */
rech rechercheMaillon(int attribut_type,void *attribut,Maillon *ptrtete,char *comparaison);


/**
 * @brief : Initialise une structure critère en fonction des paramètres, donner NULL pour que le paramètre ne soit pas pris en compte
 * 
 * @param1 : entier contenant le numéro de génération de l'individu recherché
 * @param2 : chaine de caractères contenant le nom de famille de l'individu recherché
 * @param3 : chaine de caractères contenant le prénom de l'individu recherché
 * @param4 : structure contenant les informations sur la naissance de l'individu recherché
 * @param5 : structure contenant les informations sur la mort de l'individu recherché
 * @param6 : boolean servant a définir le genre de l'individu recherché
 * @param7 : pointeur vers le maillon du père de l'individu recherché
 * @param8 : pointeur vers le maillon de la mère de l'individu recherché
 * @param9 : caractère permettant de chercher les individus ayant une date de naissance ou de mort "<", ">" ou "=" à @param4 ou @param5
 * 
 * @return : structure critère
 */
Critere creeCritere(int numGen, char *surname, char *name, Birth *birth_data, Death *death_data, bool *gender,Maillon *dad, Maillon *mom,char *comparaison);


/**
 * @brief : renvoi un tableau contenant les maillons qui correspondent aux critères donnés en paramètre
 * 
 * @param1 : structure contenant les Critères de la recherche
 * @param2 : pointeur de tête de la liste chaînée de Maillon
 * 
 * @return : tableau de pointeur sur maillon
 */
Maillon** superRechercheMaillon(Critere C, Maillon *ptrtete);

/**
 * @brief Fonction initiant les listes chainées des mariages pour chaque individu et retournant une liste chainée globale liant chaque mariage.
 * 
 * @param CSVtextdatabyword 
 * @param ptrTete 
 * @return MaillonMariage* 
 */
MaillonMariage* initMariageLC(char*** CSVtextdatabyword, Maillon* ptrTete);

/**
 * @brief Fonction initiant les liens parentés entre chaque individu. Liens parentés comprenant enfants et parents.
 * 
 * @param ptrTete 
 * @param tableauCsvIndividus 
 */
void initLiensParente(Maillon* ptrTete, char*** tableauCsvIndividus);

/**
 * @brief Fonction qui permet de sauvegarder la liste chainée des mariages sous forme de fichier spécifié par l’argument numéro un.
 * 
 * @param path 
 * @param listeDataMariage 
 */
void sauverCsvMariage(char* path,MaillonMariage* listeDataMariage);

/**
 * @brief Fonction qui charge le fichier CSV des mariages dans un tableau 2D de strings.
 * 
 * @param path_name 
 * @param tableauCsvMariages 
 */
void chargeCsvMariages(char* path_name, char**** tableauCsvMariages);

/**
 * @brief Fonction utilitaire qui insert un MaillonMariage dans la liste chainée des mariages globale et de l’individu et son éventuel(le) conjoint(e).
 * 
 * @param ptrTeteMariage 
 * @param Personne 
 * @param nouveauMariage 
 */
void insertMaillonMariage(MaillonMariage* ptrTeteMariage, Maillon* Personne, Mariage* nouveauMariage);

/**
 * @brief Fonction utilitaire qui ajoute un mariage.
 * 
 * @param mari 
 * @param femme 
 * @param lieu 
 * @param date 
 * @param ptrTeteMariages 
 */
void ajoutMariage(Maillon* mari, Maillon* femme, char* lieu, char* date, MaillonMariage* ptrTeteMariages);

/**
 * @brief Fonction utilitaire qui compte le nombre de maillons présent dans une liste chainée de MaillonMariage.
 * 
 * @param ptrTete 
 * @return int 
 */
int countLcMariage(MaillonMariage* ptrTete);

/**
 * @brief Fonction utilitaire qui permet de modifier toutes les informations d’un mariage et met à jour les mariages en conséquent.
 * 
 * @param mode 
 * @param data 
 * @param newHusband 
 * @param newWife 
 * @param newDay 
 * @param newMonth 
 * @param newYear 
 * @param newName 
 */
void modifDataMariage(int mode, Mariage* data, Maillon* newHusband, Maillon* newWife, int newDay, int newMonth, int newYear, char* newName);

/**
 * @brief Libère tous les MaillonMariage* pointant sur un Mariage* puis le libère.
 * 
 * @param mariage 
 * @param ptrTeteMariages 
 */
void supprimeMariage(MaillonMariage* mariage, MaillonMariage* ptrTeteMariages);

/**
 * @brief  Fonction initiant les liens fraternels entre chaque individu.
 * 
 * @param ptrTete 
 */
void initLiensFraterie(Maillon* ptrTete);

/**
 * @brief Fonction utilitaire qui ajoute un frère à une famille
 * 
 * @param personne 
 * @param nouveauPere 
 * @param nouvelleMere 
 * @param ptrTete 
 */
void ajoutFrere(Maillon* personne,Maillon* nouveauPere,Maillon* nouvelleMere,Maillon* ptrTete);

/**
 * @brief Fonction mettant à jour les liens parentés de la liste chainée.
 * 
 * @param ptrTete 
 */
void updateLiensParente(Maillon* ptrTete);

/**
 * @brief Fonction utilitaire qui supprime un frère d’une fratrie.
 * 
 * @param mode 
 * @param personne 
 * @param ptrTete 
 */
void supprimeFrere(int mode, Maillon* personne,Maillon* ptrTete);

/**
 * @brief : compare deux structure maillon et renvoie true si les deux struct sont égales et renvois false sinon
 * 
 * @param1 : pointeur sur maillon
 * @param2 : pointeur sur maillon
 * 
 * @return : boolean
 */
bool compareDad(Maillon *dad1,Maillon *dad2);

/**
 * @brief : compare deux structure maillon et renvoie true si les deux struct sont égales et renvois false sinon
 * 
 * @param1 : pointeur sur maillon
 * @param2 : pointeur sur maillon
 * 
 * @return : boolean
 */
bool compareMom(Maillon *mom1,Maillon *mom2);

/**
 * @brief : compare deux chaine de charactère renvoie true si les deux chaines sont égales (cas NULL pris en compte) ou si uniquement le deuxième paramètre est NULL et false sinon
 * 
 * @param1 : chaine de charactère
 * @param2 : chaine de charactère
 * 
 * @return : boolean
 */
bool bstrcmp(char* c1,char* c2);


void modifCoordLieu(Lieu *l);