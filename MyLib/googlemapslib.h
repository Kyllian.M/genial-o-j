char* getPlaceNameFromCoordinates (float longitude,float latitude);
Point_2f getCoordinatesFromPlaceName (char* endroit);
double getGeoDistance (double x1,double y1,double x2,double y2);