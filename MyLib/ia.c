#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../GfxLib/BmpLib.h"
#include <string.h>
#include "KyllianToolsLib.h"
#include "KToolsLibForGfxLib.h"
#include "Timer.h"
#include "images.h"
#include "consoletools.h"
#include "geometry.h"
#include "Cwing.h"
#include "sdlglutils.h"
#include "CSVlib.h"
#include "listechainee.h"
#include "googlemapslib.h"
#include "ia.h"

char** propPrenom(Maillon *prs){
    char **prenom = malloc(sizeof(char*)*3);
    if(*prs->data->gender == HOMME){
        if(prs->dad != NULL){
            if(prs->dad->dad !=NULL){
                prenom[0] = malloc(sizeof(char)*(strlen(prs->dad->dad->data->name[0])));
                strcpy(prenom[0],prs->dad->dad->data->name[0]);
                prenom[1] = malloc(sizeof(char)*(strlen(prs->dad->data->name[0])));
                strcpy(prenom[1],prs->dad->data->name[0]);
                prenom[2] = NULL;
            }else{
                prenom[0] = malloc(sizeof(char)*(strlen(prs->dad->data->name[0])));
                strcpy(prenom[0],prs->dad->data->name[0]);
                prenom[1] = NULL;
                prenom[2] = NULL;
            }
        }else{
            prenom[0] = NULL;
            prenom[1] = NULL;
            prenom[2] = NULL;
        }


    }else if(prs->data->gender != NULL && *(prs->data->gender) == FEMME){
        if(prs->mom != NULL){
            if(prs->mom->mom != NULL){
                printf("ok\n");
                prenom[0] = malloc(sizeof(char)*(strlen(prs->mom->mom->data->name[0])));
                strcpy(prenom[0],prs->mom->mom->data->name[0]);
                prenom[1] = malloc(sizeof(char)*(strlen(prs->mom->data->name[0])));
                strcpy(prenom[1],prs->mom->data->name[0]);
                prenom[2] = NULL;
            }else{
                  println("ok");
                prenom[0] = malloc(sizeof(char)*(strlen(prs->mom->data->name[0])));
                 println("ok");
                strcpy(prenom[0],prs->mom->data->name[0]);
                 println("ok");
                prenom[1] = NULL;
                prenom[2] = NULL;
            }
        }else{
            prenom[0] = NULL;
            prenom[1] = NULL;
            prenom[2] = NULL;
        }
    }

    return prenom;
}

char** propNom(Maillon *prs){
    char **nom = malloc(sizeof(char*)*3);
    
    if(prs->dad != NULL){
        nom[0] = malloc(sizeof(char)*(strlen(prs->dad->data->surname)));
        strcpy(nom[0],prs->dad->data->surname);
        if(prs->mom->data->surname != NULL){
            nom[1] = malloc(sizeof(char)*(strlen(prs->mom->data->surname)));
            strcpy(nom[1],prs->mom->data->surname);
            nom[2] = NULL;
        }else{
            nom[1] = NULL;
            nom[2] = NULL;
        }
    }else{
        if(prs->mom != NULL){
            nom[0] = malloc(sizeof(char)*(strlen(prs->mom->data->surname)));
            strcpy(nom[0],prs->mom->data->surname);
            nom[1] = NULL;
            nom[2] = NULL;
        }else{
            nom[0] = NULL;
            nom[1] = NULL;
            nom[2] = NULL;
        }
    }
    //TODO Ajouter le cas des mariages
    return nom;
}

bool* propGenre(Maillon *prs){
    bool *genre = malloc(sizeof(bool));

    if(prs->child != NULL && prs->child[0]->dad != NULL && prs->child[0]->mom != NULL ){
         println("ok1");
        if(compareIndividu(prs->child[0]->dad->data,prs->data)){
            *genre = HOMME;
        }
        if(compareIndividu(prs->child[0]->mom->data,prs->data)){
            *genre = FEMME;
        }
    }else if(prs->wedding != NULL){
         println("ok2");
        if(compareIndividu(prs->wedding->data->maries[0]->data,prs->data)){
            *genre = HOMME;
        }else{
            *genre = FEMME;
        }
    }else{
        println("ok");
       // free(genre);
        *genre = NULL;
    }
    return genre;
}

char** propLieuNaissance(Maillon *prs){
    int score1=0;
    int score2=0;
    Lieu l1;
    l1.nom = NULL;

    printf("1111111\n");
    if(prs->dad != NULL && prs->dad->wedding != NULL && prs->mom != NULL && prs->mom->wedding != NULL){
                
        for(MaillonMariage *mp = prs->dad->wedding;mp != NULL; mp = mp->next){
            for(MaillonMariage *mm = prs->mom->wedding; mm != NULL; mm = mm->next){
                if(mm->data == mp->data){
                    l1 = new_Lieu(mm->data->lieu.nom);
                    score1 = 3;
                }
            }
        }
        printf("222222222\n");
        if(prs->sibling != NULL){
            for(int i = 0; prs->sibling[i];i++){
                if(prs->sibling[i]->data->birth_data != NULL){
                    if(betterstrcmp(l1.nom,prs->sibling[i]->data->birth_data->lieu.nom)){
                        score1 += 2;
                    }
                }
            }
        }

    printf("3333333333\n");
        if(prs->wedding != NULL && prs->wedding->data != NULL && prs->wedding->data->lieu.nom != NULL){
            if(betterstrcmp(l1.nom,prs->wedding->data->lieu.nom)){
                score1 += 1;
            }
        }
    }

    Lieu l2;
    l2.nom = NULL;
    printf("4444444444\n");
    if(prs->wedding != NULL){

        l2 = new_Lieu(prs->wedding->data->lieu.nom);
        score2 = 1;


        if(prs->sibling != NULL){
            for(int i = 0; prs->sibling[i];i++){
                if(prs->sibling[i]->data->birth_data != NULL && prs->sibling[i]->data->birth_data->lieu.nom != NULL){
                    if(betterstrcmp(l2.nom,prs->sibling[i]->data->birth_data->lieu.nom)){
                        score2 += 2;
                    }
                }
            }
        }
    printf("5555555\n");
        if(prs->dad != NULL && prs->dad->wedding != NULL && prs->mom != NULL && prs->mom->wedding != NULL){
                
            for(MaillonMariage *mp = prs->dad->wedding;mp != NULL; mp = mp->next){
                for(MaillonMariage *mm = prs->mom->wedding; mm != NULL; mm = mm->next){
                    if(mm->data == mp->data && betterstrcmp(l2.nom,mm->data->lieu.nom)){
                        score2 += 3;
                    }
                }
            }

        }

    }

    printf("66666666\n");
    Lieu *tmp;
    int *scoreSi = NULL;
    printf("77777777\n");
    if(prs->sibling != NULL){
     
        tmp = malloc(sizeof(Lieu));
        scoreSi = malloc(sizeof(int));
        int i2=0;
        for(int i = 0; prs->sibling[i];i++){
           
            for(int j=0;j<=i2;j++){
                int cpt = 0;
                
                if(prs->sibling != NULL && prs->sibling[i]->data->birth_data != NULL && prs->sibling[i]->data->birth_data->lieu.nom != NULL){
                    if(betterstrcmp(tmp[j].nom,prs->sibling[i]->data->birth_data->lieu.nom)){
                        cpt ++;
                    }

                    if(cpt == 0){
                        tmp[i2] = new_Lieu(prs->sibling[i]->data->birth_data->lieu.nom);
                        scoreSi[i2] = 3; 
                        i2++;
                        scoreSi = realloc(scoreSi,sizeof(int)*(i2+1));
                        tmp = realloc(tmp,sizeof(Lieu)*(i2+1));
                    }
                }
            }
            
        }
        tmp[i2].nom = NULL;
        scoreSi[i2] = 0;
        printf("8888888\n");
        if(prs->dad != NULL && prs->dad->wedding != NULL && prs->mom != NULL && prs->mom->wedding != NULL){
                
            for(MaillonMariage *mp = prs->dad->wedding;mp != NULL; mp = mp->next){
                for(MaillonMariage *mm = prs->mom->wedding; mm != NULL; mm = mm->next){
                    if(mm->data == mp->data){
                        for(int i=0;tmp[i].nom;i++){
                            if(betterstrcmp(tmp[i].nom,mm->data->lieu.nom)){
                                scoreSi[i] += 2;
                            }
                        }
                    }
                }
            }
        }
        printf("99999999\n");
        if(prs->wedding != NULL){
            for(int i=0;tmp[i].nom;i++){
                if(betterstrcmp(tmp[i].nom,prs->wedding->data->lieu.nom)){
                    scoreSi[i] += 1;
                }
            }
            
        }
    }
    
    int scoreMax = score1;
    int repere = -1;
    if(score2 >= scoreMax){
        scoreMax = score2;
        repere = -2;
    }
    println("ok");
    printf("%d\n",scoreSi==NULL);
    println("ok");
    if(scoreSi != NULL){
        for(int i=0;scoreSi[i];i++){
            if(scoreSi[i]>scoreMax){
                scoreMax = scoreSi[i];
                repere = i;
            }
        }
    }
    char **Lfinal = malloc(sizeof(char)*4);

    

    switch (repere)
    {
    case -1:
        Lfinal[0] = malloc(sizeof(char)*strlen(l1.nom));
        strcpy(Lfinal[0],l1.nom);
        scoreMax = score2;
        repere = -2;

        for(int i=0;scoreSi[i];i++){
            if(scoreSi[i]>scoreMax){
                scoreMax = scoreSi[i];
                repere = i;
            }
        }

        if(repere == -2){
            Lfinal[1] = malloc(sizeof(char)*strlen(l2.nom));
            strcpy(Lfinal[1],l2.nom);
            for(int i=0;scoreSi[i];i++){
                if(scoreSi[i]>scoreMax){
                    scoreMax = scoreSi[i];
                    repere = i;
                }
            }
            Lfinal[2] = malloc(sizeof(char)*strlen(tmp[repere].nom));
            strcpy(Lfinal[2],tmp[repere].nom);
        }else{
            Lfinal[1] = malloc(sizeof(char)*strlen(tmp[repere].nom));
            strcpy(Lfinal[1],tmp[repere].nom);
            scoreSi[repere] = 0;
            scoreMax = score2;
            repere = -2;

            for(int i=0;scoreSi[i];i++){
                if(scoreSi[i]>scoreMax){
                    scoreMax = scoreSi[i];
                    repere = i;
                }
            }

            if(repere == -2){
                Lfinal[1] = malloc(sizeof(char)*strlen(l2.nom));
                strcpy(Lfinal[1],l2.nom);
            }else{
                Lfinal[1] = malloc(sizeof(char)*strlen(tmp[repere].nom));
                strcpy(Lfinal[1],tmp[repere].nom);
            }

        }

        free(scoreSi);
        return Lfinal;
        break;
    
    case -2:

        Lfinal[0] = malloc(sizeof(char)*strlen(l2.nom));
        strcpy(Lfinal[0],l2.nom);
        scoreMax = score1;
        repere = -1;

        for(int i=0;scoreSi[i];i++){
            if(scoreSi[i]>scoreMax){
                scoreMax = scoreSi[i];
                repere = i;
            }
        }

        if(repere == -1){
            Lfinal[1] = malloc(sizeof(char)*strlen(l1.nom));
            strcpy(Lfinal[1],l1.nom);
            for(int i=0;scoreSi[i];i++){
                if(scoreSi[i]>scoreMax){
                    scoreMax = scoreSi[i];
                    repere = i;
                }
            }
            Lfinal[2] = malloc(sizeof(char)*strlen(tmp[repere].nom));
            strcpy(Lfinal[2],tmp[repere].nom);
        }else{
            Lfinal[1] = malloc(sizeof(char)*strlen(tmp[repere].nom));
            strcpy(Lfinal[1],tmp[repere].nom);
            scoreSi[repere] = 0;
            scoreMax = score1;
            repere = -1;

            for(int i=0;scoreSi[i];i++){
                if(scoreSi[i]>scoreMax){
                    scoreMax = scoreSi[i];
                    repere = i;
                }
            }

            if(repere == -1){
                Lfinal[1] = malloc(sizeof(char)*strlen(l1.nom));
                strcpy(Lfinal[1],l1.nom);
            }else{
                Lfinal[1] = malloc(sizeof(char)*strlen(tmp[repere].nom));
                strcpy(Lfinal[1],tmp[repere].nom);
            }

        }

        free(scoreSi);
        return Lfinal;
        break;

    default:

        Lfinal[0] = malloc(sizeof(char)*strlen(tmp[repere].nom));
        strcpy(Lfinal[0],tmp[repere].nom);

        scoreMax = score1;
        repere = -1;
        if(score2 >= scoreMax){
            scoreMax = score2;
            repere = -2;
        }
        
        for(int i=0;scoreSi[i];i++){
            if(scoreSi[i]>scoreMax){
                scoreMax = scoreSi[i];
                repere = i;
            }
        }

        switch(repere){
            case -1:
                score1 = 0;
                Lfinal[1] = malloc(sizeof(char)*strlen(l1.nom));
                strcpy(Lfinal[1],l1.nom);
                break;

            case -2:
                score2 = 0;
                Lfinal[1] = malloc(sizeof(char)*strlen(l2.nom));
                strcpy(Lfinal[1],l2.nom);
                break;

            default:
                scoreSi[repere] = 0;
                Lfinal[1] = malloc(sizeof(char)*strlen(tmp[repere].nom));
                strcpy(Lfinal[1],tmp[repere].nom);
                break;
        }

        scoreMax = score1;
        repere = -1;
        if(score2 >= scoreMax){
            scoreMax = score2;
            repere = -2;
        }
        
        for(int i=0;scoreSi[i];i++){
            if(scoreSi[i]>scoreMax){
                scoreMax = scoreSi[i];
                repere = i;
            }
        }

        switch(repere){
            case -1:
                score1 = 0;
                Lfinal[2] = malloc(sizeof(char)*strlen(l1.nom));
                strcpy(Lfinal[2],l1.nom);
                break;

            case -2:
                score2 = 0;
                Lfinal[2] = malloc(sizeof(char)*strlen(l2.nom));
                strcpy(Lfinal[2],l2.nom);
                break;

            default:
                scoreSi[repere] = 0;
                Lfinal[2] = malloc(sizeof(char)*strlen(tmp[repere].nom));
                strcpy(Lfinal[2],tmp[repere].nom);
                break;
        }

        free(scoreSi);
        return Lfinal;
        break;
    }
}


int* propDateMort(Maillon *prs){
    int *mort = NULL;
    int esperance;
    Birth *b = NULL;
    b = prs->data->birth_data;
    if(b == NULL){
        if(prs->sibling[0] != NULL){
             b=prs->sibling[0]->data->birth_data;
         }
         else{
              b->date.year=9000;
         }
    }
    if(b!=NULL){
        if(b->date.year < 1800){
            esperance = 30;
            mort = malloc(sizeof(int)*4);
            mort[0] = (b->date.year) + esperance - 10;
            mort[1] = (b->date.year) + esperance;
            mort[2] = (b->date.year) + esperance + 10;
            mort[3] = 9000;
        }else if(b->date.year >= 1800 && b->date.year < 1850){
            esperance = 40;
            mort = malloc(sizeof(int)*4);
            mort[0] = (b->date.year) + esperance - 10;
            mort[1] = (b->date.year) + esperance;
            mort[2] = (b->date.year) + esperance + 10;
            mort[3] = 9000;
        }else if(b->date.year >= 1850 && b->date.year < 1900){
            esperance = 50;
            mort = malloc(sizeof(int)*4);
            mort[0] = (b->date.year) + esperance - 10;
            mort[1] = (b->date.year) + esperance;
            mort[2] = (b->date.year) + esperance + 10;
            mort[3] = 9000;
        }else if(b->date.year >= 1900 && b->date.year < 1960){
            esperance = 50;
            mort = malloc(sizeof(int)*4);
            mort[0] = (b->date.year) + esperance - 10;
            mort[1] = (b->date.year) + esperance;
            mort[2] = (b->date.year) + esperance + 10;
            mort[3] = 9000;
        }else if(b->date.year >= 1960 && b->date.year < 2000){
            esperance = 80;
            mort = malloc(sizeof(int)*4);
            mort[0] = (b->date.year) + esperance - 10;
            mort[1] = (b->date.year) + esperance;
            mort[2] = (b->date.year) + esperance + 10;
            mort[3] = 9000;
        }else if(b->date.year >= 2000){
            esperance = 85;
            mort = malloc(sizeof(int)*4);
            mort[0] = (b->date.year) + esperance - 10;
            mort[1] = (b->date.year) + esperance;
            mort[2] = (b->date.year) + esperance + 10;
            mort[3] = 9000;
        }
    }

    return mort;
}


Maillon** propDad(Maillon *prs,Maillon *ptrTete){
    mScore *Dad = NULL;
    int cpt = 0;
    if(prs->data->birth_data){
        Dad = malloc(sizeof(mScore));
        Dad[0].m = NULL;
        Dad[0].i = 0;
        for(Maillon *current=ptrTete;current;current=current->next){
            Dad[cpt].m = malloc(sizeof(Maillon));
            if(current->data->birth_data != NULL && current->data->gender != NULL && current->data->gender[0] == HOMME){
                if(prs->data->birth_data->date.year < (current->data->birth_data->date.year) + 50 && prs->data->birth_data->date.year > (current->data->birth_data->date.year) + 20){
                    Dad[cpt].m = current;
                    Dad[cpt].i += 1;
                    cpt++;
                    Dad = realloc(Dad,sizeof(mScore)*(cpt+1));
                    if(prs->data->birth_data->lieu.nom != NULL && current->data->birth_data->lieu.nom != NULL){
                        
                        modifCoordLieu(&prs->data->birth_data->lieu);
                        modifCoordLieu(&current->data->birth_data->lieu);
                        if(prs->data->birth_data->lieu.X != 9000 && current->data->birth_data->lieu.X != 9000){
                            double dist = getGeoDistance(prs->data->birth_data->lieu.X,prs->data->birth_data->lieu.Y,current->data->birth_data->lieu.X,current->data->birth_data->lieu.Y);
                            if(dist <= 35 && dist > 30){
                                Dad[cpt-1].i += 1;                            
                            }else if(dist <= 30 && dist > 20){
                                Dad[cpt-1].i += 2;  
                            }else if(dist <= 20 && dist > 10){
                                Dad[cpt-1].i += 3;  
                            }else if(dist <= 10 && dist > 5){
                                Dad[cpt-1].i += 4;  
                            }else if(dist <= 5){
                                Dad[cpt-1].i += 5;  
                            }
                        }
                    }
                    if(prs->mom && current->wedding != NULL){
                        if(current->wedding->data->maries[1] == prs->mom){
                            Dad[cpt-1].i += 20;
                        }
                    }
                }
            }
        }
        Dad[cpt].m= NULL;
    }

    if(Dad == NULL){
        return NULL;
    }else{
        
        Maillon **d = malloc(sizeof(Maillon*)*4);

        for(int j=0;j<3;j++){
            int scoreMax = 0;
            int repere = -1;
            d[j] = NULL;
            for(int i=0;Dad[i].m;i++){
                if(Dad[i].i > scoreMax){
                    scoreMax = Dad[i].i;
                    repere = i;
                }
            }
            if(repere !=-1){
            d[j] = Dad[repere].m;
            Dad[repere].i = 0;
            }
        }
        d[4] = NULL;
        return d;
    }
}


Maillon** propMom(Maillon *prs,Maillon *ptrTete){
    mScore *Mom = NULL;
    int cpt = 0;
    if(prs->data->birth_data){
        Mom = malloc(sizeof(mScore));
        Mom[0].m = NULL;
        Mom[0].i = 0;
        for(Maillon *current=ptrTete;current;current=current->next){
            Mom[cpt].m = malloc(sizeof(Maillon));
            if(current->data->birth_data != NULL && current->data->gender != NULL && current->data->gender[0] == FEMME){
                if(prs->data->birth_data->date.year < (current->data->birth_data->date.year) + 50 && prs->data->birth_data->date.year > (current->data->birth_data->date.year) + 20){
                    Mom[cpt].m = current;
                    Mom[cpt].i += 1;
                    cpt++;
                    Mom = realloc(Mom,sizeof(mScore)*(cpt+1));
                    if(prs->data->birth_data->lieu.nom != NULL && current->data->birth_data->lieu.nom != NULL){
                        modifCoordLieu(&prs->data->birth_data->lieu);
                        modifCoordLieu(&current->data->birth_data->lieu);
                        if(prs->data->birth_data->lieu.X != 9000 && current->data->birth_data->lieu.X != 9000){
                            double dist = getGeoDistance(prs->data->birth_data->lieu.X,prs->data->birth_data->lieu.Y,current->data->birth_data->lieu.X,current->data->birth_data->lieu.Y);
                            if(dist <= 35 && dist > 30){
                                Mom[cpt-1].i += 1;                            
                            }else if(dist <= 30 && dist > 20){
                                Mom[cpt-1].i += 2;  
                            }else if(dist <= 20 && dist > 10){
                                Mom[cpt-1].i += 3;  
                            }else if(dist <= 10 && dist > 5){
                                Mom[cpt-1].i += 4;  
                            }else if(dist <= 5){
                                Mom[cpt-1].i += 5;  
                            }
                        }
                    }
                    if(prs->dad && current->wedding != NULL){
                        if(current->wedding->data->maries[0] == prs->dad){
                            Mom[cpt-1].i += 20;
                        }
                    }
                }
            }
        }
        Mom[cpt].m= NULL;
    }

    if(Mom == NULL){
        return NULL;
    }else{
        
        Maillon **d = malloc(sizeof(Maillon*)*4);

        for(int j=0;j<3;j++){
            int scoreMax = 0;
            int repere = -1;
            d[j] = NULL;
            for(int i=0;Mom[i].m;i++){
                if(Mom[i].i > scoreMax){
                    scoreMax = Mom[i].i;
                    repere = i;
                }
            }
            if(repere !=-1){
            d[j] = Mom[repere].m;
            Mom[repere].i = 0;
            }
        }
        d[4] = NULL;
        return d;
    }

}


int* propDateNaissance(Maillon *prs){

    //Si le pointeur en argument n'est pas NULL
    if(prs){

        //Declaration variables
        int* date = malloc(sizeof(int)*5);
    
        int dateMax;
        int dateMin;

        //Initialisation variables

        dateMax = 0;
        dateMin = 0;
        
        //Si le pointeur possède une Fraterie
        if(prs->sibling){
            int moyenneAnneeFraterie = 0;
            int diviseur = 0;

            for(int i = 0; prs->sibling[i]; i++){

                //Si on connait la date de naissance du frere
                if(prs->sibling[i]->data->birth_data->date.year != 9000){
                    moyenneAnneeFraterie += prs->sibling[i]->data->birth_data->date.year;
                    diviseur++;
                }
            }

            if(diviseur != 0){
                moyenneAnneeFraterie /= diviseur;

                dateMax = moyenneAnneeFraterie + 7;
                dateMin = moyenneAnneeFraterie - 7;
            }
        }

        //Si le pointeur possède une Mere
        if(prs->mom){
            
            //Si le pointeur possède un Pere
            if(prs->dad){

                //Si on connait la date de naissance du pere
                if(prs->dad->data->birth_data->date.year != 9000){
                    
                    //Si on connait la date de naissance de la mere
                    if(prs->mom->data->birth_data->date.year != 9000){

                        //Si le père est plus jeune que la mère
                        if((prs->dad->data->birth_data->date.year > prs->mom->data->birth_data->date.year) || (prs->dad->data->birth_data->date.year == prs->mom->data->birth_data->date.year && prs->dad->data->birth_data->date.month > prs->mom->data->birth_data->date.month) || (prs->dad->data->birth_data->date.year == prs->mom->data->birth_data->date.year && prs->dad->data->birth_data->date.month > prs->mom->data->birth_data->date.month && prs->dad->data->birth_data->date.day > prs->mom->data->birth_data->date.day)){
                            
                            dad:

                            //Si il y a une dateMin
                            if(dateMin != 0){

                                //Si la nouvelle information réduis l'écart donc augmente la précision
                                if((dateMax - dateMin > dateMax - prs->dad->data->birth_data->date.year+20) || (dateMax - dateMin > 45-20)){
                                    dateMin = prs->dad->data->birth_data->date.year+20;
                                }
                            }
                            else{
                                dateMin = prs->dad->data->birth_data->date.year + 20;
                            }
                            
                            //Si il y a une date Max
                            if(dateMax != 0){
                                //Si la nouvelle information réduis l'écart donc augmente la précision
                                if((prs->dad->data->birth_data->date.year + 45 - dateMin < dateMax - dateMin) || (dateMax - dateMin > 45-20)){
                                    dateMax = prs->dad->data->birth_data->date.year + 45;
                                }
                            }
                            else{
                                dateMax = prs->dad->data->birth_data->date.year + 45;
                            }
                        }
                        else{

                            mom:

                            //Si il y a une dateMin
                            if(dateMin != 0){

                                //Si la nouvelle information réduis l'écart donc augmente la précision
                                if((dateMax - dateMin > dateMax - prs->mom->data->birth_data->date.year+20) || (dateMax - dateMin > 45-20)){
                                    dateMin = prs->mom->data->birth_data->date.year + 20;
                                }
                            }
                            else{
                                dateMin = prs->mom->data->birth_data->date.year + 20;
                            }
                            
                            //Si il y a une date Max
                            if(dateMax != 0){
                                //Si la nouvelle information réduis l'écart donc augmente la précision
                                if((prs->mom->data->birth_data->date.year + 45 - dateMin < dateMax - dateMin) || (dateMax - dateMin > 45-20)){
                                    dateMax = prs->mom->data->birth_data->date.year + 45;
                                }
                            }
                            else{
                                dateMax = prs->mom->data->birth_data->date.year + 45;
                            }
                        }

                    }
                    else{
                        goto dad;
                    }
                }
                else if(prs->mom->data->birth_data->date.year != 9000){
                    goto mom;
                }
            }
            else{
                if(prs->mom->data->birth_data->date.year != 9000){
                    goto mom;
                }
            }
        }
        else if(prs->dad){
            if(prs->dad->data->birth_data->date.year != 9000){
                goto dad;
            }
        }

        if(prs->child){

            int yearOfOldestChild = 0;
            for(int i = 0; prs->child[i]; i++){
                if(prs->child[i]->data->birth_data->date.year != 9000){
                    if(prs->child[i]->data->birth_data->date.year > yearOfOldestChild){
                        yearOfOldestChild = prs->child[i]->data->birth_data->date.year;
                    }
                }
            }

            if(yearOfOldestChild){
                //Si il y a une dateMin
                if(dateMin != 0){

                    //Si la nouvelle information réduis l'écart donc augmente la précision
                    if((dateMax - dateMin > dateMax - yearOfOldestChild-45) || (dateMax - dateMin > 45-20)){
                        dateMin = yearOfOldestChild - 45;
                    }
                }
                else{
                    dateMin = yearOfOldestChild - 45;
                }
                
                //Si il y a une date Max
                if(dateMax != 0){
                    //Si la nouvelle information réduis l'écart donc augmente la précision
                    if((yearOfOldestChild - 20 - dateMin < dateMax - dateMin) || (dateMax - dateMin > 45-20)){
                        dateMax = yearOfOldestChild - 10;
                    }
                }
                else{
                    dateMax = yearOfOldestChild - 20;
                }
            }
        }

        if(prs->wedding){
            MaillonMariage* current = prs->wedding;

            while(current->next)
                current = current->next;
            
            if(*(prs->data->gender) == HOMME && current->data->maries[0] && current->data->maries[0]->data->birth_data->date.year != 9000){
                //Si il y a une dateMin
                if(dateMin != 0){

                    //Si la nouvelle information réduis l'écart donc augmente la précision
                    if((dateMax - dateMin > dateMax - current->data->maries[0]->data->birth_data->date.year-10) || (dateMax - dateMin > 10+10)){
                        dateMin = current->data->maries[0]->data->birth_data->date.year -10;
                    }
                }
                else{
                    dateMin = current->data->maries[0]->data->birth_data->date.year -10;
                }
                
                //Si il y a une date Max
                if(dateMax != 0){

                    //Si la nouvelle information réduis l'écart donc augmente la précision
                    if((current->data->maries[1]->data->birth_data->date.year + 10 - dateMin < dateMax - dateMin) || (dateMax - dateMin > 10+10)){
                        dateMax = current->data->maries[0]->data->birth_data->date.year + 10;
                    }
                }
                else{
                    dateMax = current->data->maries[0]->data->birth_data->date.year + 10;
                }
            }
            else if(*(prs->data->gender) == FEMME && current->data->maries[1] && current->data->maries[1]->data->birth_data->date.year != 9000){
                //Si il y a une dateMin
                if(dateMin != 0){

                    //Si la nouvelle information réduis l'écart donc augmente la précision
                    if((dateMax - dateMin > dateMax - current->data->maries[1]->data->birth_data->date.year-10) || (dateMax - dateMin > 10+10)){
                        dateMin = current->data->maries[1]->data->birth_data->date.year -10;
                    }
                }
                else{
                    dateMin = current->data->maries[1]->data->birth_data->date.year -10;
                }
                
                //Si il y a une date Max
                if(dateMax != 0){

                    //Si la nouvelle information réduis l'écart donc augmente la précision
                    if((current->data->maries[1]->data->birth_data->date.year + 10 - dateMin < dateMax - dateMin) || (dateMax - dateMin > 10+10)){
                        dateMax = current->data->maries[1]->data->birth_data->date.year + 10;
                    }
                }
                else{
                    dateMax = current->data->maries[1]->data->birth_data->date.year + 10;
                }
            }
        }

        date[1] = (dateMax+dateMin)/2;
        date[0] = (date[1] + dateMin)/2;
        date[2] = (date[1] + dateMax)/2;
        date[3] = dateMin;
        date[4] = dateMax;

        return date;

    }
    else{
        println("ERREUR: argument null dans propDateNaissance (ia.c");
        return NULL;
    }

}

char** propLieuMort(Maillon *prs,int poids[8]){
    int* scores;
    char** lieux;
    int size;

    size = 1;
    scores = malloc(sizeof(int));
    lieux = malloc(sizeof(char*));
    
    //Initialise la première case mémoire à NULL

    lieux[0] = NULL;
    
    //Initialise le premier score à -1

    scores[0] = -1;
    
    //Si l'argument n'est pas à NULL
    if(prs){
        
        //Si le pere à existé et qu'on connait l'endroit de sa mort 
        if(prs->dad && prs->dad->data->death_data->lieu.nom){

            //Remplace la case mémoire NULL par lieu de la mort du père
            lieux[size-1] = prs->dad->data->death_data->lieu.nom;

            //Ajoute des points
            scores[size-1] = poids[0];

            //Augmente le tableau d'une case et l'initialise à NULL
            size++;
            lieux = realloc(lieux,size*sizeof(char*));
            lieux[size-1] = NULL;

            scores = realloc(scores,size*sizeof(int));
            scores[size-1] = -1;
        }

        //Si la mere à existée et qu'on connait l'endroit de sa mort
        if(prs->mom && prs->mom->data->death_data->lieu.nom){
            
            bool skip = false;
            skipMom:
            if(!skip){
                //Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
                for(int i = 0; lieux[i]; i++){
                    if(!strcmp(lieux[i],prs->mom->data->death_data->lieu.nom)){
                        //Si oui ajoute des points à ce lieu
                        scores[i] += poids[1];
                        skip = true;
                        goto skipMom;
                    }
                }
                //SI n'a pas été skip

                //Remplace la case mémoire NULL par lieu de la mort du mere
                lieux[size-1] = prs->mom->data->death_data->lieu.nom;

                //Ajoute des points
                scores[size-1] = poids[1];
                //Augmente le tableau d'une case et l'initialise à NULL
                size++;
                lieux = realloc(lieux,size*sizeof(char*));
                lieux[size-1] = NULL;

                scores = realloc(scores,size*sizeof(int));
                scores[size-1] = -1;
            }
            
        }
        /*
        println("ENDROIT MORT MERE");
        for(int i = 0; lieux[i]; i++){
            printf("%d ",scores[i]);
        }
        for(int i = 0; lieux[i]; i++){
            printf("%s ",lieux[i]);
        }
        printf("\n\n");
        */
        //Si fraterie existe
        if(prs->sibling){
            //Parcours la fraterie
            for(int j = 0; prs->sibling[j]; j++){
                //Si on connait le lieu de la mort du frère qu'on parcours
                if(prs->sibling[j]->data->death_data->lieu.nom){
                    bool skip = false;
                    skipSibling:
                    if(!skip){

                        //Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
                        for(int i = 0; lieux[i]; i++){

                            if(prs->sibling[j]->data->death_data->lieu.nom){

                                if(!strcmp(lieux[i],prs->sibling[j]->data->death_data->lieu.nom)){
                                    
                                    //Si déjà existant ajoute des points à ce lieu
                                    scores[i] += poids[2];
                                    skip = true;
                                    goto skipSibling;
                                }
                            }
                        }
                        
                        //SI n'a pas été skip

                        //Remplace la case mémoire NULL par lieu de la mort du mere
                        lieux[size-1] = prs->sibling[j]->data->death_data->lieu.nom;

                        //Ajoute des points
                        scores[size-1] = poids[2];

                        //Augmente le tableau d'une case et l'initialise à NULL
                        size++;
                        
                        lieux = realloc(lieux,size*sizeof(char*));
                        
                        lieux[size-1] = NULL;
                        
                        scores = realloc(scores,size*sizeof(int));
                        
                        scores[size-1] = -1;
                        
                    }
                }
            }
        }
        /*
        println("ENDROIT FRATERIE");
        for(int i = 0; lieux[i]; i++){
            printf("%d ",scores[i]);
        }
        for(int i = 0; lieux[i]; i++){
            printf("%s ",lieux[i]);
        }
        printf("\n\n");
        */
        //Si à été marié
        if(prs->wedding){

            //Va au dernier mariage
            MaillonMariage* current = prs->wedding;
            while(current->next){current = current->next;}

            //Si le lieu de son dernier mariage est renseigné
            if(current->data->lieu.nom){
                bool skip = false;
                skipWeddingPlace:
                
                if(!skip){
                    //Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
                    for(int i = 0; lieux[i]; i++){
                        if(!strcmp(lieux[i],current->data->lieu.nom)){
                            //Si déjà existant ajoute des points à ce lieu
                            scores[i] += poids[3];
                            skip = true;
                            goto skipWeddingPlace;
                        }
                    }

                    //SI n'a pas été skip

                    //Remplace la case mémoire NULL par lieu de la mort du mere
                    lieux[size-1] = current->data->lieu.nom;

                    //Ajoute des points
                    scores[size-1] = poids[3];

                    //Augmente le tableau d'une case et l'initialise à NULL
                    size++;
                    lieux = realloc(lieux,size*sizeof(char*));
                    lieux[size-1] = NULL;

                    scores = realloc(scores,size*sizeof(int));
                    scores[size-1] = -1;
                }
                
            }
            /*
            println("ENDROIT MARIAGE");
            for(int i = 0; lieux[i]; i++){
                printf("%d ",scores[i]);
            }
            for(int i = 0; lieux[i]; i++){
                printf("%s ",lieux[i]);
            }
            printf("\n\n");
            */
            //Si est un homme
            if(*(prs->data->gender) == HOMME){
                
                bool skip = false;
                skipFemme:
                
                if(!skip){
                    //Si sa femme est renseignée et que l'on connait le lieu de sa mort
                    if(current->data->maries[1] && current->data->maries[1]->data->death_data->lieu.nom){
                        //Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
                        for(int i = 0; lieux[i]; i++){
                            if(!strcmp(lieux[i],current->data->maries[1]->data->death_data->lieu.nom)){
                                //Si déjà existant ajoute des points à ce lieu
                                scores[i] += poids[4];
                                skip = true;
                                goto skipFemme;
                            }
                        }

                        //SI n'a pas été skip

                        //Remplace la case mémoire NULL par lieu de la mort du mere
                        lieux[size-1] = current->data->maries[1]->data->death_data->lieu.nom;

                        //Ajoute des points
                        scores[size-1] = poids[4];

                        //Augmente le tableau d'une case et l'initialise à NULL
                        size++;
                        lieux = realloc(lieux,size*sizeof(char*));
                        lieux[size-1] = NULL;

                        scores = realloc(scores,size*sizeof(int));
                        scores[size-1] = -1;
                    }
                }
            }
            else{
                //Si son mari est renseignée et que l'on connait le lieu de sa mort
                if(current->data->maries[0] && current->data->maries[0]->data->death_data->lieu.nom){
                    
                    bool skip = false;
                    skipMari:
                    
                    if(!skip){
                        //Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
                        for(int i = 0; lieux[i]; i++){
                            if(!strcmp(lieux[i],current->data->maries[0]->data->death_data->lieu.nom)){
                                //Si déjà existant ajoute des points à ce lieu
                                scores[i] += poids[4];
                                skip = true;
                                goto skipMari;
                            }
                        }

                        //SI n'a pas été skip

                        //Remplace la case mémoire NULL par lieu de la mort du mere
                        lieux[size-1] = current->data->maries[0]->data->death_data->lieu.nom;

                        //Ajoute des points
                        scores[size-1] = poids[4];

                        //Augmente le tableau d'une case et l'initialise à NULL
                        size++;
                        lieux = realloc(lieux,size*sizeof(char*));
                        lieux[size-1] = NULL;

                        scores = realloc(scores,size*sizeof(int));
                        scores[size-1] = -1;
                    }
                }
            }
            /*
            println("ENDROIT MORT MARI/FEMME");
            for(int i = 0; lieux[i]; i++){
                printf("%d ",scores[i]);
            }
            for(int i = 0; lieux[i]; i++){
                printf("%s ",lieux[i]);
            }
            printf("\n\n");
            */
        }

        //Si enfants existe
        if(prs->child){

            //Parcours les enfants
            for(int j = 0; prs->child[j]; j++){
                
                //Si on connait le lieu de naissance de l'enfant qu'on parcours
                if(prs->child[j]->data->birth_data->lieu.nom){
                    
                    if(prs->child[j]->child){
                        
                        //Parcours les petits enfants
                        for(int k = 0; prs->child[j]->child[k]; k++){
                            
                            //Si on connait le lieu de naissance de l'enfant qu'on parcours
                            if(prs->child[j]->child[k]->data->birth_data->lieu.nom){
                                
                                bool skipGrandChild = false;
                                skipGrandChild:
                                
                                if(!skipGrandChild){
                                    //Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
                                    for(int i = 0; lieux[i]; i++){
                                        if(!strcmp(lieux[i],prs->child[j]->child[k]->data->birth_data->lieu.nom)){
                                            //Si déjà existant ajoute des points à ce lieu
                                            scores[i] += poids[5];
                                            skipGrandChild = true;
                                            goto skipGrandChild;
                                        }
                                    }

                                    //SI n'a pas été skip

                                    //Remplace la case mémoire NULL par lieu de la mort du mere
                                    lieux[size-1] = prs->child[j]->child[k]->data->birth_data->lieu.nom;

                                    //Ajoute des points
                                    scores[size-1] = poids[5];

                                    //Augmente le tableau d'une case et l'initialise à NULL
                                    size++;
                                    lieux = realloc(lieux,size*sizeof(char*));
                                    lieux[size-1] = NULL;

                                    scores = realloc(scores,size*sizeof(int));
                                    scores[size-1] = -1;
                                }
                            }
                        }
                    }
                    
                    bool skip = false;
                    skipChild:
                    
                    if(!skip){
                        //Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
                        for(int i = 0; lieux[i]; i++){
                            if(!strcmp(lieux[i],prs->child[j]->data->birth_data->lieu.nom)){
                                //Si déjà existant ajoute des points à ce lieu
                                scores[i] += poids[6];
                                skip = true;
                                goto skipChild;
                            }
                        }

                        //SI n'a pas été skip

                        //Remplace la case mémoire NULL par lieu de la mort du mere
                        lieux[size-1] = prs->child[j]->data->birth_data->lieu.nom;

                        //Ajoute des points
                        scores[size-1] = poids[6];

                        //Augmente le tableau d'une case et l'initialise à NULL
                        size++;
                        lieux = realloc(lieux,size*sizeof(char*));
                        lieux[size-1] = NULL;

                        scores = realloc(scores,size*sizeof(int));
                        scores[size-1] = -1;
                    }
                }
            }
        }

        //Si on connait le lieu de naissance de l'individu
        if(prs->data->birth_data->lieu.nom){
            
            bool skip = false;
            skipBirthPlace:
            
            if(!skip){
                //Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
                for(int i = 0; lieux[i]; i++){
                    if(!strcmp(lieux[i],prs->data->birth_data->lieu.nom)){
                        //Si déjà existant ajoute des points à ce lieu
                        scores[i] += poids[7];
                        skip = true;
                        goto skipBirthPlace;
                    }
                }

                //SI n'a pas été skip

                //Remplace la case mémoire NULL par lieu de la mort du mere
                lieux[size-1] = prs->data->birth_data->lieu.nom;

                //Ajoute des points
                scores[size-1] = poids[7];

                //Augmente le tableau d'une case et l'initialise à NULL
                size++;
                lieux = realloc(lieux,size*sizeof(char*));
                lieux[size-1] = NULL;

                scores = realloc(scores,size*sizeof(int));
                scores[size-1] = -1;
            }
        }
        /*
        println("LIEU NAISSANCE INDIVIDU");
        for(int i = 0; lieux[i]; i++){
            printf("%d ",scores[i]);
        }
        for(int i = 0; lieux[i]; i++){
            printf("%s ",lieux[i]);
        }
        printf("\n\n");*/

        if(lieux[0]){

            for(int i = 0; lieux[i+1];i++){
                if(scores[i+1] > scores[i]){
                    int tmpScore;
                    char* tmpLieu;

                    tmpScore = scores[i];
                    scores[i] = scores[i+1];
                    scores[i+1] = tmpScore;

                    tmpLieu = lieux[i];
                    lieux[i] = lieux[i+1];
                    lieux[i+1] = tmpLieu;

                }
            }
            for(int i = 0; lieux[i]; i++){
                printf("%d ",scores[i]);
            }
            printf("\n");
            for(int i = 0; lieux[i]; i++){
                printf("%s ",lieux[i]);
            }
            printf("\n\n\n\n");
            char** results = malloc(sizeof(char*)*4);
            
            if(lieux[0]){
                results[0] = lieux[0];
            }
            else{
                results[0] = NULL;
                results[1] = NULL;
                results[2] = NULL;
                results[3] = NULL;
                free(scores);
                free(lieux);
                return results;
            }

            if(lieux[1]){
                results[1] = lieux[1];
            }
            else{
                results[1] = NULL;
                results[2] = NULL;
                results[3] = NULL;
                free(scores);
                free(lieux);
                return results;
            }

            if(lieux[2]){
                results[2] = lieux[2];
            }
            else{
                results[2] = NULL;
            }

            
            results[3] = NULL;

            free(scores);
            free(lieux);
            return results;

        }
        else{
            return lieux;
        }

    }
    else{
        println("ERREUR: argument null dans propLieuMort (ia.c");
        return NULL;
    }
}

Mariage** propMariage(Maillon *prs,Maillon* ptrTete){
    DEBUGSTART;
    /* CRITERE'S VARIABLES */

        //Death Place Critere
        double deathPlaceMaxRadius = 20;
        
        int deathPlaceDistScores[3];
        deathPlaceDistScores[0] = 0;//5
        deathPlaceDistScores[1] = 0;//3
        deathPlaceDistScores[2] = 0;//1

        //Born Place Critere
        double bornPlaceMaxRadius = 20;

        int bornPlaceDistScores[3];
        bornPlaceDistScores[0] = 0;//5
        bornPlaceDistScores[1] = 0;//3
        bornPlaceDistScores[2] = 0;//1

        //Dad Criteres
        int dadsSameBornPlaceScore = 1;
        int dadAndMomSameBornPlaceScore = 1;

        //Mom Criteres
        int momsSameBornPlaceScore = 1;
        int momAndDadSameBornPlaceScore = 1;

        //Common Parent's Criters
        Date mariedPeriod;
        mariedPeriod.day = 0;
        mariedPeriod.month = 0;
        mariedPeriod.year = 1;


        int mariedInPeriodeScore = 1;

        //Siblings Criteres
        int siblingsMariedEachOtherScore = 4;

        //Birth date Criteres
        Date birthPeriod;
        birthPeriod.day = 0;
        birthPeriod.month = 0;
        birthPeriod.year = 1;

        int bornInPeriodScore = 2;

        //Child Criteres
        Date childsBirthPeriod;
        childsBirthPeriod.day = 0;
        childsBirthPeriod.month = 0;
        childsBirthPeriod.year = 1;

        int childBornInPeriodScore = 2;

        //Wedding Criteres
        int sameWeddingDateScore = 5;
        int sameWeddingPlaceScore = 5;


    /* END OF CRITERE'S VARIABLES */

    /* BASIC VARIABLES DECLARATION */

        //Declare our array of pointer to ia_Tuple with his size
        ia_Tuple** arrayOfPtrTuples = malloc(sizeof(ia_Tuple**));
        int size = 1;

        //Declare our return array
        Mariage** result = malloc(sizeof(Maillon)*4);
        result[3] = NULL;

        //init the first element to null
        arrayOfPtrTuples[0] = NULL;

    /* END BASIC VARIABLES DECLARATION */
    
    //look over linked list
    for(Maillon* current = ptrTete; current; current = current->next){
        
        //If they have opposite sex and same generation
        if(!isSibling(current,prs)&&((*(current->data->gender) == HOMME && *(prs->data->gender) == FEMME && current->data->numGen == prs->data->numGen) || (*(current->data->gender) == FEMME && *(prs->data->gender) == HOMME && current->data->numGen == prs->data->numGen))){
            printf("Pour %s %s test %s %s\n\n\n",prs->data->surname,prs->data->name[0],current->data->surname,current->data->name[0]);
            /* 
                DEATH PLACE CRITERE
            
                ARE THEY DEAD CLOSE TO EACH OTHER
            */

            //check if the prs has a death place
            if(prs->data->death_data && prs->data->death_data->lieu.nom){
                
                //check if the current has a death place
                if(current->data->death_data && current->data->death_data->lieu.nom){

                    //check if the distance is under a certain amount of kilometers
                    if(current->data->death_data->lieu.X != 0 && current->data->death_data->lieu.Y != 0 && prs->data->death_data->lieu.X != 0 && prs->data->death_data->lieu.Y != 0 && getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) <= deathPlaceMaxRadius){

                        //init var to false
                        bool isAlreadyInArray = false;

                        //Anchor goto
                        alreadyInArray1:

                        //if the current isn't in the array
                        if(!isAlreadyInArray){

                            //Check if the person is in the array
                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                if(arrayOfPtrTuples[i]->prs == current){
                                    //If yes, add a certain amount of points
                                    if(getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) < 5){
                                        arrayOfPtrTuples[i]->score += deathPlaceDistScores[0];
                                        printf("death distance add %d points for (%s %s)\n",deathPlaceDistScores[0],arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);
                                    }
                                    else if(getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) >= 5 && getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) < 10){
                                        arrayOfPtrTuples[i]->score += deathPlaceDistScores[1];
                                        printf("death distance add %d points for (%s %s)\n",deathPlaceDistScores[1],arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);
                                    }
                                    else{
                                        arrayOfPtrTuples[i]->score += deathPlaceDistScores[2];
                                        printf("death distance add %d points for (%s %s)\n",deathPlaceDistScores[2],arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);                                    }

                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                    isAlreadyInArray = true;
                                    goto alreadyInArray1;
                                }
                            }
                            //If isn't in the array

                            //Replace the NULL memory case by a new ia_Tuple struct
                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                            arrayOfPtrTuples[size-1]->prs = current;
                            arrayOfPtrTuples[size-1]->prop = NULL;

                            //Set his score
                            if(getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) < 5){
                                arrayOfPtrTuples[size-1]->score = deathPlaceDistScores[0];
                                printf("death distance add %d points for (%s %s)\n",deathPlaceDistScores[0],arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                            }
                            else if(getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) >= 5 && getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) < 10){
                                arrayOfPtrTuples[size-1]->score = deathPlaceDistScores[1];
                                printf("death distance add %d points for (%s %s)\n",deathPlaceDistScores[1],arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                            }
                            else{
                                arrayOfPtrTuples[size-1]->score = deathPlaceDistScores[2];
                                printf("death distance add %d points for (%s %s)\n",deathPlaceDistScores[2],arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                            }

                            //Increase the array size by one and set it to NULL
                            size++;
                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                            arrayOfPtrTuples[size-1] = NULL;
                        }
                    }

                }
            }

            /* 
                BIRTH PLACE CRITERE
            
                ARE THEY BORN CLOSE TO EACH OTHER
            */

            //check if the prs has a born place
            if(prs->data->birth_data && prs->data->birth_data->lieu.nom){
                
                //check if the current has a birth place
                if(current->data->birth_data && current->data->birth_data->lieu.nom){

                    //check if the distance is under a certain amount of kilometers
                    if(current->data->birth_data->lieu.X != 0 && current->data->birth_data->lieu.Y != 0 && prs->data->birth_data->lieu.X != 0 && prs->data->birth_data->lieu.Y != 0 && getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) <= bornPlaceMaxRadius){

                        //init var to false
                        bool isAlreadyInArray = false;

                        //Anchor goto
                        alreadyInArray2:

                        //if the current isn't in the array
                        if(!isAlreadyInArray){

                            //Check if the person is in the array
                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                if(arrayOfPtrTuples[i]->prs == current){

                                    //If yes, add a certain amount of points
                                    if(getGeoDistance(current->data->birth_data->lieu.X,current->data->birth_data->lieu.Y,prs->data->birth_data->lieu.X,prs->data->birth_data->lieu.Y) < 5){
                                        arrayOfPtrTuples[i]->score += bornPlaceDistScores[0];
                                        printf("birth distance add %d points for (%s %s)\n",bornPlaceDistScores[0],arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                    }
                                    else if(getGeoDistance(current->data->birth_data->lieu.X,current->data->birth_data->lieu.Y,prs->data->birth_data->lieu.X,prs->data->birth_data->lieu.Y) >= 5 && getGeoDistance(current->data->death_data->lieu.X,current->data->death_data->lieu.Y,prs->data->death_data->lieu.X,prs->data->death_data->lieu.Y) < 10){
                                        arrayOfPtrTuples[i]->score += bornPlaceDistScores[1];
                                        printf("birth distance add %d points for (%s %s)\n",bornPlaceDistScores[1],arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);
                                    }
                                    else{
                                        arrayOfPtrTuples[i]->score += bornPlaceDistScores[2];
                                        printf("birth distance add %d points for (%s %s)\n",bornPlaceDistScores[2],arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);
                                    }

                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                    isAlreadyInArray = true;
                                    goto alreadyInArray2;
                                }
                            }
                            //If isn't in the array

                            //Replace the NULL memory case by a new ia_Tuple struct
                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                            arrayOfPtrTuples[size-1]->prs = current;
                            arrayOfPtrTuples[size-1]->prop = NULL;

                            //Set his score
                            if(getGeoDistance(current->data->birth_data->lieu.X,current->data->birth_data->lieu.Y,prs->data->birth_data->lieu.X,prs->data->birth_data->lieu.Y) < 5){
                                arrayOfPtrTuples[size-1]->score = bornPlaceDistScores[0];
                                printf("birth distance add %d points for (%s %s)\n",bornPlaceDistScores[0],arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                            }
                            else if(getGeoDistance(current->data->birth_data->lieu.X,current->data->birth_data->lieu.Y,prs->data->birth_data->lieu.X,prs->data->birth_data->lieu.Y) >= 5 && getGeoDistance(current->data->birth_data->lieu.X,current->data->birth_data->lieu.Y,prs->data->birth_data->lieu.X,prs->data->birth_data->lieu.Y) < 10){
                                arrayOfPtrTuples[size-1]->score = bornPlaceDistScores[1];
                                printf("birth distance add %d points for (%s %s)\n",bornPlaceDistScores[1],arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                            }
                            else{
                                arrayOfPtrTuples[size-1]->score = bornPlaceDistScores[2];
                                printf("birth distance add %d points for (%s %s)\n",bornPlaceDistScores[2],arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);

                            }

                            //Increase the array size by one and set it to NULL
                            size++;
                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                            arrayOfPtrTuples[size-1] = NULL;
                        }
                    }

                }
            }
            
            /* 
                PARENT'S CRITERES
            
                DOES HE KNOW THE CURRENT'S PARENTS:
                    -SAME BIRTH PLACE
                    -MARRIED AT THE SAME PLACE IN A SHORT PERIOD OF TIME
            */

            //check if the prs has at least one parent
            if(prs->dad || prs->mom){

                /* FIRST DAD'S CRITERE */

                    //check if the prs has a dad
                    if(prs->dad){
                        
                        //check if the current has a dad
                        if(current->dad || current->mom){
                            
                            /* FIRST DAD'S CRITERE */
                                //if we know both dad's birth places
                                if(prs->dad->data->birth_data && prs->dad->data->birth_data->lieu.nom && current->dad && current->dad->data->birth_data && current->dad->data->birth_data->lieu.nom){

                                    //if they have the same birth place
                                    if(!strcmp(prs->dad->data->birth_data->lieu.nom,current->dad->data->birth_data->lieu.nom)){

                                        //init var to false
                                        bool isAlreadyInArray = false;

                                        //Anchor goto
                                        alreadyInArray3:

                                        //if the current isn't in the array
                                        if(!isAlreadyInArray){

                                            //Check if the person is in the array
                                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                if(arrayOfPtrTuples[i]->prs == current){

                                                    arrayOfPtrTuples[i]->score += dadsSameBornPlaceScore;
                                                    printf("same dad birth place add %d points for (%s %s)\n",dadsSameBornPlaceScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);
                                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                                    isAlreadyInArray = true;
                                                    goto alreadyInArray3;
                                                }
                                            }
                                            //If isn't in the array

                                            //Replace the NULL memory case by a new ia_Tuple struct
                                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                            arrayOfPtrTuples[size-1]->prs = current;

                                            //Set his score
                                            arrayOfPtrTuples[size-1]->score = dadsSameBornPlaceScore;
                                            printf("same dad birth place add %d points for (%s %s)\n",dadsSameBornPlaceScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                                            //Increase the array size by one and set it to NULL
                                            size++;
                                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                            arrayOfPtrTuples[size-1] = NULL;
                                        }
                                    }

                                }

                                //if we know prs's dad and current's mom birth places
                                if(prs->dad->data->birth_data && prs->dad->data->birth_data->lieu.nom && current->mom && current->mom->data->birth_data && current->mom->data->birth_data->lieu.nom){

                                    //if they have the same birth place
                                    if(!strcmp(prs->dad->data->birth_data->lieu.nom,current->mom->data->birth_data->lieu.nom)){

                                        //init var to false
                                        bool isAlreadyInArray = false;

                                        //Anchor goto
                                        alreadyInArray4:

                                        //if the current isn't in the array
                                        if(!isAlreadyInArray){

                                            //Check if the person is in the array
                                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                if(arrayOfPtrTuples[i]->prs == current){

                                                    arrayOfPtrTuples[i]->score += dadAndMomSameBornPlaceScore;
                                                    printf("same dad with mom birth date add %d points for (%s %s)\n",dadAndMomSameBornPlaceScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                                    isAlreadyInArray = true;
                                                    goto alreadyInArray4;
                                                }
                                            }
                                            //If isn't in the array

                                            //Replace the NULL memory case by a new ia_Tuple struct
                                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                            arrayOfPtrTuples[size-1]->prs = current;

                                            //Set his score
                                            arrayOfPtrTuples[size-1]->score = dadAndMomSameBornPlaceScore;
                                            printf("same dad with mom birth date add %d points for (%s %s)\n",dadAndMomSameBornPlaceScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                                            //Increase the array size by one and set it to NULL
                                            size++;
                                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                            arrayOfPtrTuples[size-1] = NULL;
                                        }
                                    }

                                }
                        }
                    }

                    //check if the prs has a mom
                    if(prs->mom){
                        
                        //check if the current has at least one parent
                        if(current->mom || current->dad){
                            
                            /* FIRST MOM'S CRITERE */
                                //if we know prs's mom and current's dad birth places
                                if(prs->mom->data->birth_data && prs->mom->data->birth_data->lieu.nom && current->dad && current->dad->data->birth_data && current->dad->data->birth_data->lieu.nom){

                                    //if they have the same birth place
                                    if(!strcmp(prs->mom->data->birth_data->lieu.nom,current->dad->data->birth_data->lieu.nom)){

                                        //init var to false
                                        bool isAlreadyInArray = false;

                                        //Anchor goto
                                        alreadyInArray5:

                                        //if the current isn't in the array
                                        if(!isAlreadyInArray){

                                            //Check if the person is in the array
                                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                if(arrayOfPtrTuples[i]->prs == current){

                                                    arrayOfPtrTuples[i]->score += momAndDadSameBornPlaceScore;
                                                    printf("same mom with dad birth place add %d points for (%s %s)\n",momAndDadSameBornPlaceScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                                    isAlreadyInArray = true;
                                                    goto alreadyInArray5;
                                                }
                                            }
                                            //If isn't in the array

                                            //Replace the NULL memory case by a new ia_Tuple struct
                                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                            arrayOfPtrTuples[size-1]->prs = current;

                                            //Set his score
                                            arrayOfPtrTuples[size-1]->score = momAndDadSameBornPlaceScore;
                                            printf("same mom with dad birth place add %d points for (%s %s)\n",momAndDadSameBornPlaceScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                                            //Increase the array size by one and set it to NULL
                                            size++;
                                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                            arrayOfPtrTuples[size-1] = NULL;
                                        }
                                    }

                                }

                                //if we know prs's mom and current's mom birth places
                                if(prs->mom->data->birth_data && prs->mom->data->birth_data->lieu.nom && current->mom && current->mom->data->birth_data && current->mom->data->birth_data->lieu.nom){

                                    //if they have the same birth place
                                    if(!strcmp(prs->mom->data->birth_data->lieu.nom,current->mom->data->birth_data->lieu.nom)){

                                        //init var to false
                                        bool isAlreadyInArray = false;

                                        //Anchor goto
                                        alreadyInArray6:
                                        //if the current isn't in the array
                                        if(!isAlreadyInArray){
                                            //Check if the person is in the array
                                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                if(arrayOfPtrTuples[i]->prs == current){
                                                    arrayOfPtrTuples[i]->score += momsSameBornPlaceScore;
                                                    printf("same mom with mom birth place add %d points for (%s %s)\n",momsSameBornPlaceScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                                    isAlreadyInArray = true;
                                                    goto alreadyInArray6;
                                                }
                                            }
                                            //If isn't in the array

                                            //Replace the NULL memory case by a new ia_Tuple struct
                                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                            arrayOfPtrTuples[size-1]->prs = current;

                                            //Set his score
                                            arrayOfPtrTuples[size-1]->score = momsSameBornPlaceScore;
                                            printf("same mom with mom birth place add %d points for (%s %s)\n",momsSameBornPlaceScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                                            
                                            //Increase the array size by one and set it to NULL
                                            size++;
                                            
                                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                            arrayOfPtrTuples[size-1] = NULL;
                                            
                                        }
                                        
                                    }

                                }
                        }
                    }
                /* SECOND PARENTS'S CRITERE */
                    //check for all prs's dad weddings
                    if(prs->dad){
                        //check if current has at least 1 parent
                        if(current->dad || current->mom){
                            //check if current has a dad
                            if(current->dad){
                                for(MaillonMariage* prsDadsWedding = prs->dad->wedding; prsDadsWedding; prsDadsWedding = prsDadsWedding->next){
                                    
                                    //check for all current's dad weddings
                                    for(MaillonMariage* currentDadsWedding = current->dad->wedding; currentDadsWedding; currentDadsWedding = currentDadsWedding->next){
                                        
                                        //if we know both wedding place
                                        if(prsDadsWedding->data->lieu.nom && currentDadsWedding->data->lieu.nom){

                                            //if it's the same place
                                            if(!strcmp(prsDadsWedding->data->lieu.nom,currentDadsWedding->data->lieu.nom)){
                                                
                                                //if we know their wedding date
                                                if(prsDadsWedding->data->date.day != 1 && prsDadsWedding->data->date.month != 1 && prsDadsWedding->data->date.year != 9000 && currentDadsWedding->data->date.day != 1 && currentDadsWedding->data->date.month != 1 && currentDadsWedding->data->date.year != 9000){

                                                    //if it's in the period
                                                    if(isInPeriod(prsDadsWedding->data->date,currentDadsWedding->data->date,mariedPeriod)){
                                                        //init var to false
                                                        bool isAlreadyInArray = false;

                                                        //Anchor goto
                                                        alreadyInArray7:

                                                        //if the current isn't in the array
                                                        if(!isAlreadyInArray){

                                                            //Check if the person is in the array
                                                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                                if(arrayOfPtrTuples[i]->prs == current){

                                                                    arrayOfPtrTuples[i]->score += mariedInPeriodeScore;
                                                                    printf("mariedInPeriod add %d points for (%s %s)\n",mariedInPeriodeScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                                                    isAlreadyInArray = true;
                                                                    goto alreadyInArray7;
                                                                }
                                                            }
                                                            //If isn't in the array

                                                            //Replace the NULL memory case by a new ia_Tuple struct
                                                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                                            arrayOfPtrTuples[size-1]->prs = current;
                                                            arrayOfPtrTuples[size-1]->prop = NULL;

                                                            //Set his score
                                                            arrayOfPtrTuples[size-1]->score = mariedInPeriodeScore;
                                                            printf("mariedInPeriod add %d points for (%s %s)\n",mariedInPeriodeScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                                                            //Increase the array size by one and set it to NULL
                                                            size++;
                                                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                                            arrayOfPtrTuples[size-1] = NULL;
                                                        }
                                                    }

                                                }
                                                    

                                            }

                                        }
                                    }
                                    
                                }
                            }
                            else{
                                for(MaillonMariage* prsDadsWedding = prs->dad->wedding; prsDadsWedding; prsDadsWedding = prsDadsWedding->next){
                                    
                                    //check for all current's mom weddings
                                    for(MaillonMariage* currentMomsWedding = current->mom->wedding; currentMomsWedding; currentMomsWedding = currentMomsWedding->next){
                                        
                                        //if we know both wedding place
                                        if(prsDadsWedding->data->lieu.nom && currentMomsWedding->data->lieu.nom){

                                            //if it's the same place
                                            if(!strcmp(prsDadsWedding->data->lieu.nom,currentMomsWedding->data->lieu.nom)){
                                                
                                                //if we know their wedding date
                                                if(prsDadsWedding->data->date.day != 1 && prsDadsWedding->data->date.month != 1 && prsDadsWedding->data->date.year != 9000 && currentMomsWedding->data->date.day != 1 && currentMomsWedding->data->date.month != 1 && currentMomsWedding->data->date.year != 9000){

                                                    //if it's in the period
                                                    if(isInPeriod(prsDadsWedding->data->date,currentMomsWedding->data->date,mariedPeriod)){
                                                        //init var to false
                                                        bool isAlreadyInArray = false;

                                                        //Anchor goto
                                                        alreadyInArray8:

                                                        //if the current isn't in the array
                                                        if(!isAlreadyInArray){

                                                            //Check if the person is in the array
                                                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                                if(arrayOfPtrTuples[i]->prs == current){

                                                                    arrayOfPtrTuples[i]->score += mariedInPeriodeScore;
                                                                    printf("mariedInPeriod add %d points for (%s %s)\n",mariedInPeriodeScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                                                    isAlreadyInArray = true;
                                                                    goto alreadyInArray8;
                                                                }
                                                            }
                                                            //If isn't in the array

                                                            //Replace the NULL memory case by a new ia_Tuple struct
                                                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                                            arrayOfPtrTuples[size-1]->prs = current;
                                                            arrayOfPtrTuples[size-1]->prop = NULL;

                                                            //Set his score
                                                            arrayOfPtrTuples[size-1]->score = mariedInPeriodeScore;
                                                            printf("mariedInPeriod add %d points for (%s %s)\n",mariedInPeriodeScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);

                                                            //Increase the array size by one and set it to NULL
                                                            size++;
                                                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                                            arrayOfPtrTuples[size-1] = NULL;
                                                        }
                                                    }

                                                }
                                                    

                                            }

                                        }
                                    }
                                    
                                }
                            }
                        }

                    }
                    else{
                        
                        //check if current has at least 1 parent
                        if(current->dad || current->mom){
                            //check if current has a dad
                            if(current->dad){
                                
                                for(MaillonMariage* prsMomsWedding = prs->mom->wedding; prsMomsWedding; prsMomsWedding = prsMomsWedding->next){
                                    
                                    //check for all current's dad weddings
                                    for(MaillonMariage* currentDadsWedding = current->dad->wedding; currentDadsWedding; currentDadsWedding = currentDadsWedding->next){
                                        
                                        //if we know both wedding place
                                        if(prsMomsWedding->data->lieu.nom && currentDadsWedding->data->lieu.nom){

                                            //if it's the same place
                                            if(!strcmp(prsMomsWedding->data->lieu.nom,currentDadsWedding->data->lieu.nom)){
                                                
                                                //if we know their wedding date
                                                if(prsMomsWedding->data->date.day != 1 && prsMomsWedding->data->date.month != 1 && prsMomsWedding->data->date.year != 9000 && currentDadsWedding->data->date.day != 1 && currentDadsWedding->data->date.month != 1 && currentDadsWedding->data->date.year != 9000){

                                                    //if it's in the period
                                                    if(isInPeriod(prsMomsWedding->data->date,currentDadsWedding->data->date,mariedPeriod)){
                                                        //init var to false
                                                        bool isAlreadyInArray = false;

                                                        //Anchor goto
                                                        alreadyInArray9:

                                                        //if the current isn't in the array
                                                        if(!isAlreadyInArray){

                                                            //Check if the person is in the array
                                                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                                if(arrayOfPtrTuples[i]->prs == current){

                                                                    arrayOfPtrTuples[i]->score += mariedInPeriodeScore;
                                                                    printf("mariedInPeriod add %d points for (%s %s)\n",mariedInPeriodeScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                                                    isAlreadyInArray = true;
                                                                    goto alreadyInArray9;
                                                                }
                                                            }
                                                            //If isn't in the array

                                                            //Replace the NULL memory case by a new ia_Tuple struct
                                                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                                            arrayOfPtrTuples[size-1]->prs = current;
                                                            arrayOfPtrTuples[size-1]->prop = NULL;

                                                            //Set his score
                                                            arrayOfPtrTuples[size-1]->score = mariedInPeriodeScore;
                                                            printf("mariedInPeriod add %d points for (%s %s)\n",mariedInPeriodeScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);

                                                            //Increase the array size by one and set it to NULL
                                                            size++;
                                                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                                            arrayOfPtrTuples[size-1] = NULL;
                                                        }
                                                    }

                                                }
                                                    

                                            }

                                        }
                                    }
                                    
                                }
                            }
                            else{
                                if(prs->dad){
                                    for(MaillonMariage* prsMomsWedding = prs->dad->wedding; prsMomsWedding; prsMomsWedding = prsMomsWedding->next){
                                        
                                        //check for all current's mom weddings
                                        for(MaillonMariage* currentMomsWedding = current->mom->wedding; currentMomsWedding; currentMomsWedding = currentMomsWedding->next){
                                            
                                            //if we know both wedding place
                                            if(prsMomsWedding->data->lieu.nom && currentMomsWedding->data->lieu.nom){

                                                //if it's the same place
                                                if(!strcmp(prsMomsWedding->data->lieu.nom,currentMomsWedding->data->lieu.nom)){
                                                    
                                                    //if we know their wedding date
                                                    if(prsMomsWedding->data->date.day != 1 && prsMomsWedding->data->date.month != 1 && prsMomsWedding->data->date.year != 9000 && currentMomsWedding->data->date.day != 1 && currentMomsWedding->data->date.month != 1 && currentMomsWedding->data->date.year != 9000){

                                                        //if it's in the period
                                                        if(isInPeriod(prsMomsWedding->data->date,currentMomsWedding->data->date,mariedPeriod)){
                                                            //init var to false
                                                            bool isAlreadyInArray = false;

                                                            //Anchor goto
                                                            alreadyInArray10:

                                                            //if the current isn't in the array
                                                            if(!isAlreadyInArray){

                                                                //Check if the person is in the array
                                                                for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                                    if(arrayOfPtrTuples[i]->prs == current){

                                                                        arrayOfPtrTuples[i]->score += mariedInPeriodeScore;
                                                                        printf("mariedInPeriod add %d points for (%s %s)\n",mariedInPeriodeScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                                        //And set isAlreadyInArray to true in order to skip the next if instructions
                                                                        isAlreadyInArray = true;
                                                                        goto alreadyInArray10;
                                                                    }
                                                                }
                                                                //If isn't in the array

                                                                //Replace the NULL memory case by a new ia_Tuple struct
                                                                arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                                                arrayOfPtrTuples[size-1]->prs = current;

                                                                //Set his score
                                                                arrayOfPtrTuples[size-1]->score = mariedInPeriodeScore;
                                                                printf("mariedInPeriod add %d points for (%s %s)\n",mariedInPeriodeScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);

                                                                //Increase the array size by one and set it to NULL
                                                                size++;
                                                                arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                                                arrayOfPtrTuples[size-1] = NULL;
                                                            }
                                                        }

                                                    }
                                                        

                                                }

                                            }
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
            }

            /*
                SIBLING'S CRITERES

                PRS'S SIBLINGS HAVE MARIED CURRENT'S SIBLINGS ?
            */

            //if prs and current have siblings
            if(prs->sibling && current->sibling){

                //for all prs's siblings
                for(int i = 0; prs->sibling[i]; i++){

                    //if prs->sibling[i] has a wedding
                    if(prs->sibling[i]->wedding){

                        //for all current's siblings
                        for(int j = 0; current->sibling[j];j++){

                            //if current->sibling[j] has a wedding
                            if(current->sibling[j]->wedding){

                                //look over all prs's sibling weddings
                                for(MaillonMariage* prsSiblingWedding = prs->sibling[i]->wedding; prsSiblingWedding; prsSiblingWedding = prsSiblingWedding->next){

                                    //look over all current's sibling weddings
                                    for(MaillonMariage* currentSiblingWedding = current->sibling[j]->wedding; currentSiblingWedding; currentSiblingWedding = currentSiblingWedding->next){

                                        if(prsSiblingWedding->data == currentSiblingWedding->data){
                                            //init var to false
                                            bool isAlreadyInArray = false;

                                            //Anchor goto
                                            alreadyInArray11:

                                            //if the current isn't in the array
                                            if(!isAlreadyInArray){

                                                //Check if the person is in the array
                                                for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                    if(arrayOfPtrTuples[i]->prs == current){

                                                        arrayOfPtrTuples[i]->score += siblingsMariedEachOtherScore;
                                                        printf("siblings add %d points for (%s %s)\n",siblingsMariedEachOtherScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);
                                                        //And set isAlreadyInArray to true in order to skip the next if instructions
                                                        isAlreadyInArray = true;
                                                        goto alreadyInArray11;
                                                    }
                                                }
                                                //If isn't in the array

                                                //Replace the NULL memory case by a new ia_Tuple struct
                                                arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                                arrayOfPtrTuples[size-1]->prs = current;

                                                //Set his score
                                                arrayOfPtrTuples[size-1]->score = siblingsMariedEachOtherScore;
                                                printf("siblings add %d points for (%s %s)\n",siblingsMariedEachOtherScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                                                //Increase the array size by one and set it to NULL
                                                size++;
                                                arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                                arrayOfPtrTuples[size-1] = NULL;
                                            }
                                        }

                                    }
                                }

                            }

                        }

                    }

                }
            }

            /*
                BIRTH DATE'S CRITERES

                THEIR BIRTH DATES ARE IN A SHORT PERIOD ?
            */

            //if we know their birth date
            if(prs->data->birth_data && prs->data->birth_data->date.day != 1 && prs->data->birth_data->date.month != 1 && prs->data->birth_data->date.year != 9000 && current->data->birth_data && current->data->birth_data->date.day != 1 && current->data->birth_data->date.month != 1 && current->data->birth_data->date.year != 9000){

                //if it's in the period
                if(isInPeriod(prs->data->birth_data->date,current->data->birth_data->date,birthPeriod)){
                    //init var to false
                    bool isAlreadyInArray = false;

                    //Anchor goto
                    alreadyInArray12:

                    //if the current isn't in the array
                    if(!isAlreadyInArray){

                        //Check if the person is in the array
                        for(int i = 0; arrayOfPtrTuples[i]; i++){
                            if(arrayOfPtrTuples[i]->prs == current){

                                arrayOfPtrTuples[i]->score += bornInPeriodScore;
                                printf("born same time add %d points for (%s %s)\n",bornInPeriodScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                //And set isAlreadyInArray to true in order to skip the next if instructions
                                isAlreadyInArray = true;
                                goto alreadyInArray12;
                            }
                        }
                        //If isn't in the array

                        //Replace the NULL memory case by a new ia_Tuple struct
                        arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                        arrayOfPtrTuples[size-1]->prs = current;

                        //Set his score
                        arrayOfPtrTuples[size-1]->score = bornInPeriodScore;
                        arrayOfPtrTuples[size-1]->prop = NULL;
                        printf("born same time add %d points for (%s %s)\n",bornInPeriodScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                        //Increase the array size by one and set it to NULL
                        size++;
                        arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                        arrayOfPtrTuples[size-1] = NULL;
                    }
                }

            }

            /*
                CHILD'S CRITERES:

                    - THEIR CHILDS ARE BORN IN THE SAME CITY AND BORN IN THE SAME SHORT PERIOD

            */

            //if they have childs
            if(prs->child && current->child){

                //look over all prs's childs
                for(int i = 0; prs->child[i];i++){
                    //init var to false
                    bool isAlreadyInArray = false;

                    //Anchor goto
                    alreadyInArray13:
                    //if the current isn't in the array
                    if(!isAlreadyInArray){
                        //look over all current's childs
                        for(int j = 0; current->child[j];j++){
                            //if we know their child's birth place
                            if(prs->child[i]->data->birth_data && prs->child[i]->data->birth_data->lieu.nom && current->child[j]->data->birth_data && current->child[j]->data->birth_data->lieu.nom){
                                //if they have the same birth place
                                if(!strcmp(prs->child[i]->data->birth_data->lieu.nom,current->child[j]->data->birth_data->lieu.nom)){
                                    //if we know their child's birth date
                                    if(prs->child[i]->data->birth_data && prs->child[i]->data->birth_data->date.day != 1 && prs->child[i]->data->birth_data->date.month != 1 && prs->child[i]->data->birth_data->date.year != 9000 && current->child[j]->data->birth_data && current->child[j]->data->birth_data->date.day != 1 && current->child[j]->data->birth_data->date.month != 1 && current->child[j]->data->birth_data->date.year != 9000){                    
                                        //if their birth date are close in term of time
                                        if(isInPeriod(prs->data->birth_data->date,current->data->birth_data->date,childsBirthPeriod)){
                                                
                                            //Check if the person is in the array
                                            for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                if(arrayOfPtrTuples[i]->prs == current){

                                                    arrayOfPtrTuples[i]->score += childBornInPeriodScore;
                                                    printf("child(%s %s) born same time add %d points for (%s %s)\n",current->child[j]->data->surname,current->child[j]->data->name[0],childBornInPeriodScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                    //And set isAlreadyInArray to true in order to skip the next if instructions
                                                    isAlreadyInArray = true;
                                                    goto alreadyInArray13;
                                                }
                                            }
                                            //If isn't in the array

                                            //Replace the NULL memory case by a new ia_Tuple struct
                                            arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                            arrayOfPtrTuples[size-1]->prs = current;
                                            arrayOfPtrTuples[size-1]->prop = NULL;

                                            //Set his score
                                            arrayOfPtrTuples[size-1]->score = childBornInPeriodScore;
                                            printf("child(%s %s) born same time add %d points for (%s %s)\n",current->child[j]->data->surname,current->child[j]->data->name[0],childBornInPeriodScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                                            //Increase the array size by one and set it to NULL
                                            size++;
                                            arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                            arrayOfPtrTuples[size-1] = NULL;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }

            }

            /* 
                WEDDING'S CRITERES
            
                IF EXIST WEDDING WITH UNKNOW PARTNER:
                    -IF WE KNOW WEDDING PLACES:
                        MUST BE SAME PLACES

                    -IF WE KNOW DATES:
                        MUST BE SAME DATES
            */

            //check if prs has at least a wedding
            if(prs->wedding){
                //for all prs Weddings
                for(MaillonMariage* prsWedding = prs->wedding; prsWedding; prsWedding = prsWedding->next){
                    //If the wedding has an unkown partner
                    if(prsWedding->data->maries[0] == NULL || prsWedding->data->maries[1] == NULL){
                        //if current has a least a wedding
                        if(current->wedding){
                            // for all current weddings
                            for(MaillonMariage* currentWedding = current->wedding; currentWedding; currentWedding = currentWedding->next){
                                //if the wedding has an unknow partner
                                if(currentWedding->data->maries[0] == NULL || currentWedding->data->maries[1] == NULL){
                                    //if we know both wedding place
                                    if(prsWedding->data->lieu.nom && currentWedding->data->lieu.nom){

                                        //if it's the same place
                                        if(!strcmp(prsWedding->data->lieu.nom,currentWedding->data->lieu.nom)){
                                            //init var to false
                                            bool isAlreadyInArray = false;

                                            //Anchor goto
                                            alreadyInArray14:

                                            //if the current isn't in the array
                                            if(!isAlreadyInArray){

                                                //Check if the person is in the array
                                                for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                    if(arrayOfPtrTuples[i]->prs == current && arrayOfPtrTuples[i]->prop == currentWedding->data){

                                                        arrayOfPtrTuples[i]->score += sameWeddingPlaceScore;
                                                        printf("same wedding place add %d points for (%s %s)\n",sameWeddingPlaceScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                        //And set isAlreadyInArray to true in order to skip the next if instructions
                                                        isAlreadyInArray = true;
                                                        goto alreadyInArray14;
                                                    }
                                                }
                                                //If isn't in the array

                                                //Replace the NULL memory case by a new ia_Tuple struct
                                                arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                                arrayOfPtrTuples[size-1]->prs = current;

                                                //Set his score
                                                arrayOfPtrTuples[size-1]->score = sameWeddingPlaceScore;
                                                arrayOfPtrTuples[size-1]->prop = currentWedding->data;
                                                printf("same wedding place add %d points for (%s %s)\n",sameWeddingPlaceScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);

                                                //Increase the array size by one and set it to NULL
                                                size++;
                                                arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                                arrayOfPtrTuples[size-1] = NULL;
                                            }
                                        }
                                    }

                                    //if we know their wedding date
                                    if(prsWedding->data->date.day != 1 && prsWedding->data->date.month != 1 && prsWedding->data->date.year != 9000 && currentWedding->data->date.day != 1 && currentWedding->data->date.month != 1 && currentWedding->data->date.year != 9000){

                                        //if it's the same date
                                        if(compareDate(prsWedding->data->date,currentWedding->data->date,"=")){
                                            //init var to false
                                            bool isAlreadyInArray = false;

                                            //Anchor goto
                                            alreadyInArray15:

                                            //if the current isn't in the array
                                            if(!isAlreadyInArray){

                                                //Check if the person is in the array
                                                for(int i = 0; arrayOfPtrTuples[i]; i++){
                                                    if(arrayOfPtrTuples[i]->prs == current && arrayOfPtrTuples[i]->prop == currentWedding->data){

                                                        arrayOfPtrTuples[i]->score += sameWeddingDateScore;
                                                        printf("same wedding date add %d points for (%s %s)\n",sameWeddingDateScore,arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0]);

                                                        //And set isAlreadyInArray to true in order to skip the next if instructions
                                                        isAlreadyInArray = true;
                                                        goto alreadyInArray15;
                                                    }
                                                }
                                                //If isn't in the array

                                                //Replace the NULL memory case by a new ia_Tuple struct
                                                arrayOfPtrTuples[size-1] = malloc(sizeof(ia_Tuple));
                                                arrayOfPtrTuples[size-1]->prs = current;

                                                //Set his score
                                                arrayOfPtrTuples[size-1]->score = sameWeddingDateScore;
                                                arrayOfPtrTuples[size-1]->prop = currentWedding->data;
                                                printf("same wedding date add %d points for (%s %s)\n",sameWeddingDateScore,arrayOfPtrTuples[size-1]->prs->data->surname,arrayOfPtrTuples[size-1]->prs->data->name[0]);
                                                //Increase the array size by one and set it to NULL
                                                size++;
                                                arrayOfPtrTuples = realloc(arrayOfPtrTuples,size*sizeof(ia_Tuple*));
                                                arrayOfPtrTuples[size-1] = NULL;
                                            }
                                        }
                                        
                                    }
                                }
                            
                            }
                        }
                    }
                }
            }

        }
    }
    for(int i = 0; arrayOfPtrTuples[i];i++){
            printf("(%s %s %d)\n",arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0],arrayOfPtrTuples[i]->score);
        }
    if(count(arrayOfPtrTuples)-1 > 2){

        for(int i = 0; arrayOfPtrTuples[i];i++){
            for(int j = 0; arrayOfPtrTuples[j];j++){
                if(arrayOfPtrTuples[i]->score > arrayOfPtrTuples[j]->score){
                    ia_Tuple* tmp;

                    tmp = arrayOfPtrTuples[i];
                    arrayOfPtrTuples[i] = arrayOfPtrTuples[j];
                    arrayOfPtrTuples[j] = tmp;
                }
            }
        }

        for(int i = 0; arrayOfPtrTuples[i];i++){
            printf("(%s %s %d)\n",arrayOfPtrTuples[i]->prs->data->surname,arrayOfPtrTuples[i]->prs->data->name[0],arrayOfPtrTuples[i]->score);
        }
        

        if(arrayOfPtrTuples[0]->prop){
            result[0] = arrayOfPtrTuples[0]->prop;
        }
        else{
            Mariage* newMariage = malloc(sizeof(Mariage));
            if(*(arrayOfPtrTuples[0]->prs->data->gender) == HOMME)
                newMariage->maries[0] = arrayOfPtrTuples[0]->prs;
            else
                newMariage->maries[1] = arrayOfPtrTuples[0]->prs;
            result[0] = newMariage;
        }

        if(arrayOfPtrTuples[1]->prop){
            result[1] = arrayOfPtrTuples[1]->prop;
        }
        else{
            Mariage* newMariage = malloc(sizeof(Mariage));
            if(*(arrayOfPtrTuples[1]->prs->data->gender) == HOMME)
                newMariage->maries[0] = arrayOfPtrTuples[1]->prs;
            else
                newMariage->maries[1] = arrayOfPtrTuples[1]->prs;
            result[1] = newMariage;
        }

        if(arrayOfPtrTuples[2]->prop){
            result[2] = arrayOfPtrTuples[2]->prop;
        }
        else{
            Mariage* newMariage = malloc(sizeof(Mariage));
            if(*(arrayOfPtrTuples[2]->prs->data->gender) == HOMME)
                newMariage->maries[0] = arrayOfPtrTuples[2]->prs;
            else
                newMariage->maries[1] = arrayOfPtrTuples[2]->prs;
            result[2] = newMariage;
        }

        result[3] = NULL;

        

    }
    else{
        if(count(arrayOfPtrTuples)-1 == 1){
            if(arrayOfPtrTuples[0]->prop){
            result[0] = arrayOfPtrTuples[0]->prop;
            }
            else{
                Mariage* newMariage = malloc(sizeof(Mariage));
                if(*(arrayOfPtrTuples[0]->prs->data->gender) == HOMME)
                    newMariage->maries[0] = arrayOfPtrTuples[0]->prs;
                else
                    newMariage->maries[1] = arrayOfPtrTuples[0]->prs;
                result[0] = newMariage;
            }
            result[1] = NULL;
            result[2] = NULL;
            result[3] = NULL;
        }
        else if(count(arrayOfPtrTuples)-1 == 2){
            if(arrayOfPtrTuples[0]->score < arrayOfPtrTuples[1]->score){
                if(arrayOfPtrTuples[1]->prop){
                    result[0] = arrayOfPtrTuples[1]->prop;
                }
                else{
                    Mariage* newMariage = malloc(sizeof(Mariage));
                    if(*(arrayOfPtrTuples[1]->prs->data->gender) == HOMME)
                        newMariage->maries[0] = arrayOfPtrTuples[1]->prs;
                    else
                        newMariage->maries[1] = arrayOfPtrTuples[1]->prs;
                    result[0] = newMariage;
                }
                if(arrayOfPtrTuples[0]->prop){
                    result[1] = arrayOfPtrTuples[0]->prop;
                }
                else{
                    Mariage* newMariage = malloc(sizeof(Mariage));
                    if(*(arrayOfPtrTuples[0]->prs->data->gender) == HOMME)
                        newMariage->maries[0] = arrayOfPtrTuples[0]->prs;
                    else
                        newMariage->maries[1] = arrayOfPtrTuples[0]->prs;
                    result[1] = newMariage;
                }
                result[2] = NULL;
                result[3] = NULL;
            }
            else{
                if(arrayOfPtrTuples[0]->prop){
                result[0] = arrayOfPtrTuples[0]->prop;
                }
                else{
                    Mariage* newMariage = malloc(sizeof(Mariage));
                    if(*(arrayOfPtrTuples[0]->prs->data->gender) == HOMME)
                        newMariage->maries[0] = arrayOfPtrTuples[0]->prs;
                    else
                        newMariage->maries[1] = arrayOfPtrTuples[0]->prs;
                    result[0] = newMariage;
                }
                if(arrayOfPtrTuples[1]->prop){
                    result[1] = arrayOfPtrTuples[1]->prop;
                }
                else{
                    Mariage* newMariage = malloc(sizeof(Mariage));
                    if(*(arrayOfPtrTuples[1]->prs->data->gender) == HOMME)
                        newMariage->maries[0] = arrayOfPtrTuples[1]->prs;
                    else
                        newMariage->maries[1] = arrayOfPtrTuples[1]->prs;
                    result[1] = newMariage;
                }
                result[2] = NULL;
                result[3] = NULL;
            }
        }
        else{
            result[0] = NULL;
            result[1] = NULL;
            result[2] = NULL;
            result[3] = NULL;
        }
    }
    DEBUGEND;
    return result;
}