#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "stdbool.h"
#include <unistd.h>
#include <string.h>
#include "consoletools.h"
#include "KyllianToolsLib.h"
#include "geometry.h"
#include "googlemapslib.h"

char* getPlaceNameFromCoordinates (float longitude,float latitude){
	return runProcess(str_format("python ../python/CoordinatesToName.py %f %f",latitude,longitude),"../Ressources/geolocator.txt");
}

Point_2f getCoordinatesFromPlaceName (char* endroit){
	char* fcontents = NULL;
	int cpt = 0;	
	do{
		cpt++;
		fcontents = runProcess(str_format("python ../python/NameToCoordiantes.py %s",endroit),"../Ressources/geolocator.txt");
	}while(fcontents == NULL && cpt < 10);
	if(fcontents == NULL){
		Point_2f pt = (Point_2f) {9000,9000};
		return pt;
	}else
	{
		char** txtByLine = str_split(fcontents,'\n');
		char** coordinates_char = str_split(txtByLine[0],',');
		Point_2f pt = (Point_2f) {atof(coordinates_char[0]),atof(coordinates_char[1])};
		return pt;
	}
	
	
}

double getGeoDistance (double x1,double y1,double x2,double y2){
	return atof(runProcess(str_format("python ../python/geoDistance.py %f %f %f %f",x1,y1,x2,y2),"../Ressources/geolocator.txt"));
}

