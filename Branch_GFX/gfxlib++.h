#ifdef _WIN32
	#include <windows.h>
#endif
#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../GfxLib/GfxLib.h"
#include "../GfxLib/BmpLib.h"
#include "../GfxLib/ESLib.h"
#include <string.h>
#include "../GfxLib/commonIO.h"
#include "../MyLib/KyllianToolsLib.h"
#include "../MyLib/KToolsLibForGfxLib.h"
#include "../MyLib/geometry.h"
#include "../MyLib/Cwing.h"

//premiere boucle ?
bool first = true;
//temps au lancement du programme
double runtime_t0;
//temps d'execution en secondes
double runtime;
//etat actif/inactif des 255 touches ASCII du clavier
unsigned char keyboardState[255];
/*
etat de la souris
MOUSE_NOTHING
MOUSE_LEFT_UP
MOUSE_LEFT_DOWN 
MOUSE_RIGHT_UP
MOUSE_RIGHT_DOWN
MOUSE_SCROLL_UP
MOUSE_SCROLL_DOWN
*/
int mouseState = 0;
//position en x de la souris sur la fenetre
int AS = 0;
//position en y de la souris sur la fenetre
int OS = 0;
//la souris est elle utilisée ?
bool handleSouris = false;
//nom de la fenetre principale
char processname[100];
//largeur de la fenetre
int processwidth;
//hauteur de la fenetre
int processheight;
