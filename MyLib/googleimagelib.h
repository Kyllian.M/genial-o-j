typedef struct GoogleSearchIMG{
	char* termes;
	int nb_resultats;
	char* save_location;

	bool finished;
	char* logs;
	char** file_location;
	Image** result;

	FILE* google_process;
	FILE* null_process;
}GoogleSearchIMG;

GoogleSearchIMG* new_GoogleSearchIMG (char* termes,int nb_resultats,char* save_location);
void updateGoogleSearchIMG (GoogleSearchIMG* google);
void saveGoogleImageAs (GoogleSearchIMG* google,int numImage,char* path);
