#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../GfxLib/BmpLib.h"
#include <string.h>
#include "KyllianToolsLib.h"
#include "KToolsLibForGfxLib.h"
#include "Timer.h"
#include "images.h"
#include "consoletools.h"
#include "geometry.h"
#include "Cwing.h"
#include "sdlglutils.h"
#include "CSVlib.h"
#include "listechainee.h"
#include "googlemapslib.h"

/**
 * @brief Fonction de rafraichissement du tableur
 * 
 * @param t tableur a mettre a jour
 */
void updateTableur (Tableur* t){
	for(int i=0;i<t->nb_ligne;i++){
		for(int i2=0;i2<t->nb_colonne;i2++){
			textFieldClickListener(&(t->cell[i][i2]));
		}
	}
}

/**
 * @brief affiche un tableur
 * 
 * @param t tableau a afficher
 * @param x coordonée x d affichage
 * @param y coordonée y d'affichage
 */
void afficheTableur (Tableur* t,int x,int y){
	for(int i=0;i<t->nb_ligne;i++){
		for(int i2=0;i2<t->nb_colonne;i2++){
			afficheTextField(&(t->cell[i][i2]),x + i2*t->cell[i][i2].width, y -(i*t->cell[i][i2].height));
		}
	}
}

/**
 * @brief Creation d'un nouveau tableur a partir d'un fichier CSV
 * 
 * @param filename fichier CSV a charger
 * @return Tableur 
 */
Tableur new_Tableur (char* filename){
	Tableur ret;
	char** CSVcategories;
	char*** CSVtextdatabyword;
	chargeCSV(filename,&CSVcategories,&CSVtextdatabyword);
	ret.nb_ligne = count(CSVtextdatabyword);
	ret.nb_colonne = count(CSVtextdatabyword[0])-1;
	ret.cell = malloc(sizeof(textField*)*ret.nb_ligne);
	ret.cell[0] = malloc(sizeof(textField)*ret.nb_colonne);
	for(int i=0;i<ret.nb_colonne;i++){
		ret.cell[0][i] = initTextField(0,0,150,30,(RGB){255,255,255},CSVcategories[i]); 
	}
	for(int i=1;i<ret.nb_ligne;i++){
		ret.cell[i] = malloc(sizeof(textField)*ret.nb_colonne);
		for(int i2=0;i2<ret.nb_colonne;i2++){
			ret.cell[i][i2] = initTextField(0,0,150,30,(RGB){190,190,190},CSVtextdatabyword[i-1][i2]); 
		}
	}
	ret.update = updateTableur;
	ret.show = afficheTableur;
	return ret;
}

/**
 * @brief Compte le nombre de maillons d'une liste chainée
 * 
 * @param ptrtete tete de liste
 * @return int nombre de maillons
 */
int countLC (Maillon* ptrtete){
	Maillon* tmp = ptrtete;
	int cpt = 0;
	for(;tmp != NULL;tmp = tmp->next){
		cpt++;
	}
	return cpt;
}

/**
 * @brief accede au n-ieme maillon d'une liste chainee
 * 
 * @param ptrtete tete de liste
 * @param a index d'acces
 * @return Maillon* 
 */
Maillon* goToLC (Maillon* ptrtete,int a){
	Maillon* tmp = ptrtete;
	for(int i = 0;i<a && tmp != NULL;tmp = tmp->next){
		i++;
	}
	return tmp;
}

void fusionLC(Maillon** addPtrLc1,Maillon** addPtrLc2);

/**
 * Fonction InsereIndividuFin
 * 
 * Cette fonction permet d'inserer un nouveau Maillon dans une LC sans trie
 * @param M : Maillon à inserer
 * @param ptrTete : Pointeur de Tete de la LC
 * @return /
 * */
void InsereIndividuFin(Maillon * M ,Maillon ** ptrTete){
	DEBUGSTART;
	M->next=*ptrTete;
	*ptrTete=M;
	DEBUGEND;
}

/**
 * Fonction InsereIndividuFin
 * 
 * Cette fonction permet d'inserer un nouveau Maillon dans une LC sans trie
 * @param M : Maillon à inserer
 * @param ptrTete : Pointeur de Tete de la LC
 * @return /
 * */
void insertIndividuFinBis( Maillon* ptr, Maillon** addPtrTete){
	Maillon* current = *addPtrTete;
	while(current->next){
		current=current->next;
	}
	current->next = ptr;
}


/**
 * Fonction InverseListe
 * Cette Fonction permet d'inverser une LC
 * @param ptrTete : Pointeur de Tete de La LC à inverser
 * @return /
 * */
void InverseListe(Maillon **ptrTete){
	DEBUGSTART;
	Maillon *ptr,*ptrTeteInverse;
	ptrTeteInverse=NULL;
	while(*ptrTete!=NULL){
		ptr=*ptrTete;  			
		*ptrTete=(*ptrTete)->next; 	//Suppresion en en-tête 
		ptr->next=ptrTeteInverse;	//
		ptrTeteInverse=ptr;			//Insertion en en-tête
	}
	*ptrTete=ptrTeteInverse;
	DEBUGEND;
}

/**
 * Fonction CherchePrecedent
 * 
 * Cette Fonction permet de rechercher le maillon precedent de celui passer 
 * en paramètre dans une LC donnée et renvoit ce maillon ( renvoit NULL si c'est en début de chaine ou inexistant)
 *
 * @param M : Maillon dont on cherche le précédent
 * @param ptrTete : Pointeur de Tete de la LC
 * @return Precedent : Renvoit le Precedent 
 * */
Maillon * cherchePrecedent(Maillon * M,Maillon *ptrTete){
	DEBUGSTART;
	Maillon *ptr=ptrTete;
	Maillon * precedent=NULL;
	if(compareIndividu(M->data,ptrTete->data)==1){
		precedent=NULL;
		return precedent;
	}
	while (ptr->next!=NULL){
		if(compareIndividu(M->data,ptr->next->data)==1){
			precedent = ptr;
			return precedent;
		}
		ptr=ptr->next;	
	}
	if(precedent==NULL){
		precedent=false;
	}
	DEBUGEND;
	return precedent;
}

/**
 * Fonction CherchePrecedentMariage
 * 
 * Cette Fonction permet de rechercher le maillon precedent de celui passer 
 * en paramètre dans une LC donnée et renvoit ce maillon ( renvoit NULL si c'est en début de chaine ou inexistant)
 *
 * @param M : Maillon dont on cherche le précédent
 * @param ptrTete : Pointeur de Tete de la LC
 * @return Precedent : Renvoit le Precedent 
 * */
MaillonMariage * cherchePrecedentMariage(MaillonMariage * M,MaillonMariage *ptrTete){
	DEBUGSTART;
	MaillonMariage *ptr=ptrTete;
	MaillonMariage * precedent=NULL;
	printf("%p\n",M->data);
	if(compareMariage(M->data,ptrTete->data)==1){
		printf("if1\n");
		precedent=NULL;
		return precedent;
	}
	printf("if1out\n");
	while (ptr->next!=NULL){
		if(compareMariage(M->data,ptr->next->data)==1){
			precedent = ptr;
			return precedent;
		}
		ptr=ptr->next;	
	}
	if(precedent==NULL){
		precedent=false;
	}
	DEBUGEND;
	return precedent;
}

/**
 * Fonction SupprimeMaillon
 * 
 * Cette fonction prend un paramètre le maillon à supprimer et le pointeur de Tete 
 * de la liste chainée.Elle permet de supprimer le maillon concerné de la liste chainée
 * 
 * @param ptrSupp : Maillon à supprimer
 * @param ptrTete : Poiteur de Tete de la LC
 * @return /
 * */
void SupprimeMaillon(Maillon* ptrSupp,Maillon **ptrTete,MaillonMariage **ptrTeteMariagesCSV){
	DEBUGSTART;
	Maillon * precedent;
	printdebug("1");
	precedent = cherchePrecedent(ptrSupp,*ptrTete);
	printdebug("précédent: %p",precedent);
	if(precedent==NULL){ //Liste vide ou insertion en tête
		printdebug("if precedent == NULL");
		*ptrTete=ptrSupp->next;
		ptrSupp->next=NULL;
	}
	else{
		printdebug("%s %s", precedent->data->name[0],precedent->data->surname);
		precedent->next=ptrSupp->next;
		ptrSupp->next=NULL;
		
		if(ptrSupp->wedding){
			while(ptrSupp->wedding){
				supprimeMariage(ptrSupp->wedding,*ptrTeteMariagesCSV);
			}
			
		}
		if(ptrSupp->sibling){
			for(int i =0; ptrSupp->sibling[i]; i++){
				if(ptrSupp == ptrSupp->sibling[i]->mom){
					ptrSupp->sibling[i]->mom = NULL;
				}
				else{
					ptrSupp->sibling[i]->dad = NULL;
				}
			}
			free(ptrSupp->sibling);
			ptrSupp->sibling = NULL;
		}
		if(ptrSupp->child){
			for(int i =0; ptrSupp->child[i]; i++){
				if(ptrSupp == ptrSupp->child[i]->mom){
					ptrSupp->child[i]->mom = NULL;
				}
				else{
					ptrSupp->child[i]->dad = NULL;
				}
			}
			free(ptrSupp->child);
			ptrSupp->child = NULL;
		}
		//free(ptrSupp);

	}
	DEBUGEND;
}

/**
 * @brief comparaison de string avec gestion de cas nul inclusive
 * 
 * @param c1 chaine a comparer
 * @param c2 cahine a comparer
 * @return true les chaines sont identiques ou NULL
 * @return false les chaines sont differentes
 */
bool betterstrcmp(char* c1,char* c2){
	if(c1 == NULL && c2 == NULL){
		return true;
	}else if(c2 == NULL){
		return true;
	}else if(c1 == NULL){
		return false;
	}else{
		return !strcmp(c1,c2);
	}
	
}

/**
 * @brief comparaison de string avec gestion de cas nul inclusive
 * 
 * @param c1 chaine a comparer
 * @param c2 cahine a comparer
 * @return true les chaines sont identiques ou NULL
 * @return false les chaines sont differentes
 */
int stringCompare(char* s1, char* s2){
	DEBUGSTART;
	printdebug("%s:%d/%s:%d",s1,strlen(s1),s2,strlen(s2));
    if(strlen(s1) == strlen(s2)-1){
		printdebug("if1");
        int compteur = 0;
        for(int i = 0; i < strlen(s1); i++){
            if(s1[i] == s2[i]){
				printdebug("i=%d",i);
                compteur++;
            }
        }
        if(compteur == strlen(s1)){
			printdebug("if2");
            return 1;
        }
        else{
			printdebug("else1");
            return 0;
        }
    }
    else{
	printdebug("else2");
        return 0;
    }
    DEBUGEND;
}

//Fonction fille de compareIndividu
bool compareLieu2(Lieu l1,Lieu l2){
	bool c1 = false,c2 = false,c3 = false;
	if(l1.X == 0 || (l1.X == l2.X)){
		c1 = true;
	}
	if(l1.Y == 0 || (l1.Y == l2.Y)){
		c2 = true;
	}
	c3 = betterstrcmp(l1.nom,l2.nom);
	return c1 && c2 && c3;
}

/**
 * fonction CompareIndividu
 * 
 * Cette fonction prend 2 individus en paramètre et les compare entre eux.
 * Elle renvoit 0 si ils sont differents et 1 si ils sont identiques.
 * @param I1 
 * @param I2 : 2 individus à comparer
 * @return 0 si différents / 1 si identiques
 * */
bool compareIndividu(Individu *I1, Individu *I2){
	DEBUGSTART;
	//int etat=0;
	int longueur = count(I1->name) > count(I2->name) ? count(I2->name):count(I1->name);
	printdebug("longueur %d",longueur);
	printdebug("surname %s/%s",I1->surname,I2->surname);
	printdebug("%s %s",dateToString(I1->birth_data->date),dateToString(I2->birth_data->date));
	if(betterstrcmp(I1->surname,I2->surname)){
		if(compareDate(I1->birth_data->date,I2->birth_data->date,"=")){
			if(compareDate(I1->death_data->date,I2->death_data->date,"=")){
				if(compareLieu2(I1->death_data->lieu,I2->death_data->lieu) && compareLieu2(I1->birth_data->lieu,I2->birth_data->lieu)){
					if(betterstrcmp(I1->name[0],I2->name[0])){
						return true;
					}
				}
			}
		}
	}
	if(betterstrcmp(I1->surname,I2->surname) && betterstrcmp(I1->name[0],I2->name[0]) && I2->numGen == I1->numGen){
		return true;
	}
	DEBUGEND;
	return false;
}

/**
 * @brief comparaison des mariages
 * 
 * @param M1 mariage a comparer
 * @param M2 mariage a comparer
 * @return short 
 */
short compareMariage(Mariage *M1, Mariage* M2){
	DEBUGSTART;
	if(compareIndividu(M1->maries[0]->data,M2->maries[0]->data)==1 || compareIndividu(M1->maries[1]->data,M2->maries[1]->data)==1){
		if(compareDate(M1->date,M2->date,"=") && compareLieu(M1->lieu,M2->lieu)){
			return 1;
		}
	}
	return 0;
	DEBUGEND;
}

/**
 * @brief affichage console d'un individu
 * 
 * @param indiv individu a afficher
 */
void printIndividu(Individu* indiv){
	Maillon a;
	a.data = indiv;
	Maillon* ptr = &a;
	//int numero = 0;
	if(ptr==NULL){
		printerr("ERROR : TENTATIVE D AFFICHAGE D UN MAILLON NULL\n (sur ce coup, le codeur est un pd)");
	}
	else{
		//Affichage d'un maillon
		printyellow("Data (gen%d):\n",ptr->data->numGen);
		printlightblue("\t Nom: %s %s\n",TXT_COLOR_END,ptr->data->surname);
		printlightblue("\t Prenom(s):  ");
		printf(TXT_COLOR_END);
		for(int i = 0; ptr->data->name[i]; i++)
			printf("%s ",ptr->data->name[i]);
		printf("\n");
		printgreen("\t Naissance: \n");
		if( dateIsNULL(ptr->data->birth_data->date) ){
			printlightblue("\t\t Date: %s %s \n",TXT_COLOR_END,"NULL");
		}
		else{
			printlightblue("\t\t Date: %s %s \n",TXT_COLOR_END,dateToFancyString(ptr->data->birth_data->date));
		}
		if(ptr->data->birth_data->lieu.nom == NULL){
			printlightblue("\t\t Lieu: %s %s\n",TXT_COLOR_END,"NULL");
		}
		else{
			printlightblue("\t\t Lieu: %s %s (X=%f,Y=%f)\n",TXT_COLOR_END,ptr->data->birth_data->lieu.nom,ptr->data->birth_data->lieu.X,ptr->data->birth_data->lieu.Y);
		}
		printgreen("\t Mort: \n");
		if( dateIsNULL(ptr->data->death_data->date) ){
			printlightblue("\t\t Date: %s %s \n",TXT_COLOR_END,"NULL");
		}
		else{
			printlightblue("\t\t Date: %s %s\n",TXT_COLOR_END,dateToFancyString(ptr->data->death_data->date));
		}
		if(ptr->data->death_data->lieu.nom == NULL){
			printlightblue("\t\t Lieu: %s %s\n",TXT_COLOR_END,"NULL");
		}
		else{
			printlightblue("\t\t Lieu: %s %s (X=%f,Y=%f)\n",TXT_COLOR_END,ptr->data->death_data->lieu.nom,ptr->data->death_data->lieu.X,ptr->data->death_data->lieu.Y);
		}
		printgreen("\t Genre: %s%s \n",TXT_COLOR_END,(ptr->data->gender[0] == FEMME)?"Femme":"Homme");
	}
}

/**
 * Fonction RechercheIndividu
 * 
 * La fonction recherche le maillon correspondant à l'individu 
 * et le renvoit.
 * 
 * @param I : L'individu à rechercher
 * @param ptrTete : Pointeur sur le debut de la Liste chainée
 * @return : Maillon * contenant l'individu recherché
 * */
Maillon * RechercheIndividu(Individu * I,Maillon ** ptrTete){
	DEBUGSTART;
	Maillon *ptr=*ptrTete;
	while (ptr!=NULL){
		printdebug("here");
		printdebug("%s %p / %s %p", I->name[0],I,ptr->data->name[0],ptr->data);
		if(compareIndividu(I,ptr->data)==1){
			printdebug("trouvé");
			return ptr;
		}
		ptr= ptr->next;
	}
	//printdebug("pas trouvé lui __________________");
	//printIndividu(I);
	//printdebug("_________________________________");
	return ptr=NULL;
}

/**
 * Fonction RechercheMariage
 * 
 * La fonction recherche le maillonMariage correspondant au mariage
 * et le renvoit.
 * 
 * @param I : Le Mariage à rechercher
 * @param ptrTete : Pointeur sur le debut de la Liste chainée
 * @return : MaillonMariage * contenant le mariage recherché
 * */
MaillonMariage * RechercheMariage(Mariage * M,MaillonMariage ** ptrTete){
	DEBUGSTART;
	MaillonMariage *ptr=*ptrTete;
	while (ptr!=NULL){
		if(compareMariage(M,ptr->data)==1){
			return ptr;
		}
		ptr= ptr->next;
	}
	return ptr=NULL;
	DEBUGEND;
}

void KillMaillon(Individu * I,Maillon **ptrTete,MaillonMariage** ptrTeteMariagesCSV){
	Maillon * supprime;
	supprime=RechercheIndividu(I,ptrTete);
	if(supprime!=NULL){
		printdebug("%s",supprime->data->name[0]);
		SupprimeMaillon(supprime,ptrTete,ptrTeteMariagesCSV);
		freeMaillon(&supprime);
		freeIndividu(I);
	}
	else{
		printf("Tes un PD ce maillon n'existe pas\n");	
	}
}

/**
 * @brief libere les donnees d'un maillon
 * 
 * @param suppr 
 */
void freeMaillon(Maillon **suppr){
	free(*suppr);
	*suppr=NULL;
	printdebug("Maillon free");
}


/**
 * @brief libere les données d'un individu
 * 
 * @param I 
 */
void freeIndividu(Individu * I){
	for(int i=0;i<1;i++){ // boucle selon le nombre de prenom
		free(I->name[i]);
		I->name[i]=NULL;
		free(I->name);
		I->name=NULL;
	}
	free(I->surname);
	I->surname=NULL;
	free(I);
	I=NULL;
	printdebug("Individu free");
}

/**
 * @brief Creation d'une structure date
 * 
 * @param date_n chaine de caractere au format 00/00/0000
 * @return Date 
 */
Date new_Date(char* date_n){
	DEBUGSTART;
	Date D;
	if(date_n == NULL){
		D.day = 1;
		D.month = 1;
		D.year = 9000;
	}
	else{
		printdebug("%s",date_n);
		char str[] = "01/01/0101";
		strcpy(str,date_n);
		
		D.day = atoi(strtok(str, "/"));
		D.month = atoi(strtok(NULL,"/"));
		D.year = atoi(strtok(NULL,"/"));
		printdebug("%d %d %d",D.day,D.month,D.year);
	}
	
	DEBUGEND;
	return D;
}

/**
 * @brief Creation d'une structure Naissance
 * 
 * @param date_n date de naissance au format 00/00/00
 * @param lieu_n lieu de naissance
 * @param lieu_X_n longitude 
 * @param lieu_Y_n latitude
 * @return Birth
 */
Birth* new_Birth(char* date_n,char* lieu_n,char* lieu_X_n,char* lieu_Y_n){
	printdebug("args : %s|%s",date_n,lieu_n);
	Birth* B = malloc(sizeof(Birth));
	Date D;
	Lieu L;
	if(date_n == NULL){
		D.day = 1;
		D.month = 1;
		D.year = 9000;
	}
	else{
		

		D = new_Date(date_n);
	}
	B->date = D;
	if(lieu_n == NULL){
		L.nom = NULL;
	}
	else{
		L.nom = strdup(lieu_n);
		printdebug(L.nom);
	}
	if(lieu_X_n != NULL){
		L.X = atof(lieu_X_n);
	}
	else{
		L.X = 0;
	}
	if(lieu_Y_n != NULL){
		L.Y = atof(lieu_Y_n);
	}
	else{
		L.Y = 0;
	}
	B->lieu = L;
	return B;
}

/**
 * @brief Creation d'une structure Mort
 * 
 * @param date_n date de naissance au format 00/00/00
 * @param lieu_n lieu de naissance
 * @param lieu_X_n longitude 
 * @param lieu_Y_n latitude
 * @return Birth
 */
Death* new_Death(char* date_n,char* lieu_n,char* lieu_X_n,char *lieu_Y_n){
	printdebug("%s,%s",date_n,lieu_n);
	Death* B = malloc(sizeof(Death));
	Date D;
	Lieu L;
	if(date_n == NULL){
		D.day = 1;
		D.month = 1;
		D.year = 9000;
	}
	else{
		D = new_Date(date_n);
	}
	B->date = D;
	if(lieu_n == NULL){
		L.nom = NULL;
	}
	else{
		L.nom = strdup(lieu_n);
	}
	if(lieu_X_n != NULL){
		L.X = atof(lieu_X_n);
	}
	else{
		L.X = 0;
	}
	if(lieu_Y_n != NULL){
		L.Y = atof(lieu_Y_n);
	}
	else{
		L.Y = 0;
	}
	B->lieu = L;
	printdebug(">> %d/%d/%d %s",B->date.day,B->date.month,B->date.year,B->lieu.nom);
	return B;
}

/**
 * @brief creation d'une structure lieu
 * 
 * @param lieu_n nom du lieu
 * @return Lieu 
 */
Lieu new_Lieu(char * lieu_n){
	Lieu L;
	if(lieu_n == NULL){
		L.nom = NULL;
		L.X = 0;
		L.Y = 0;
	}
	else{
		L.nom = strdup(lieu_n);
		L.X = 0;
		L.Y = 0;
	}
	return L;
}

/**
 * @brief Creation d'une structure individu
 */
Individu* new_Individu (int nbgen,char* nom,char* prenom,char genre,char* date_n,char* lieu_n,char* date_d,char *lieu_d,Image* photo,char* lieu_X_n,char* lieu_Y_n,char* lieu_X_d,char* lieu_Y_d){
	Individu* I = malloc(sizeof(Individu));
	I->numGen=nbgen;
	I->surname = malloc(sizeof(char)*strlen(nom));
	strcpy(I->surname,nom);
	I->name = malloc(sizeof(char*)*2);
	I->name[0] = malloc(sizeof(char)*strlen(prenom));
	strcpy(I->name[0],prenom);
	I->name[1] = false;
	I->birth_data = new_Birth(date_n,lieu_n,lieu_X_n,lieu_Y_n);
	I->death_data = new_Death(date_d,lieu_d,lieu_X_d,lieu_Y_d);
	I->gender = malloc(sizeof(bool));
	I->gender[0] = genre=='M'? HOMME : FEMME;
	I->photo = photo;
	return I;
}

/**
 * @brief Creatuion d'une structure Maillon
 * 
 * @param I Individu
 * @return Maillon* 
 */
Maillon* new_Maillon (Individu* I){
	Maillon * M = malloc(sizeof(Maillon));
	M->data = I;
	M->dad = NULL;
	M->mom = NULL;
	M->sibling = NULL;
	M->wedding = NULL;
	M->child = NULL;
	M->next = NULL;
	M->tile_info = NULL;
	return M;
}

/**
 * @brief Creation d'une structure mariage
 * 
 * @param ndate date du mariage
 * @param nlieu lieu du mariage
 * @param M1 mari
 * @param M2 femme
 * @return Mariage* 
 */
Mariage * new_Mariage(char *ndate,char*nlieu,Maillon * M1,Maillon * M2){
	DEBUGSTART;
	Mariage *Ma= malloc(sizeof(Mariage));
	Ma->date=new_Date(ndate);
	Ma->lieu=new_Lieu(nlieu);
	Ma->maries[0]=M1;
	Ma->maries[1]=M2;
	return Ma;
	DEBUGEND;
}

//Renvoie la date au format : 01/01/2000
char* dateToString (Date D){
	char* ret = malloc(sizeof(char)*11);
	if(D.day < 10){
		if(D.month < 10){
			sprintf(ret,"0%d/0%d/%d",D.day,D.month,D.year);
		}
		else{
			sprintf(ret,"0%d/%d/%d",D.day,D.month,D.year);
		}
	}
	else{
		if(D.month < 10){
			sprintf(ret,"%d/0%d/%d",D.day,D.month,D.year);
		}
		else{
			sprintf(ret,"%d/%d/%d",D.day,D.month,D.year);
		}
	}
	return ret;
}

//Renvoie la date au format : 1er Janvier 2000
char* dateToFancyString (Date D){
	char j[5], m[30], a[5];
	if(D.day == 1){
		sprintf(j,"1er");
	}
	else{
		sprintf(j,"%d",D.day);
	}
	switch(D.month){
		case 1:
			sprintf(m,"Janvier");
		break;
		case 2:
			sprintf(m,"Fevrier");
		break;
		case 3:
			sprintf(m,"Mars");
		break;
		case 4:
			sprintf(m,"Avril");
		break;
		case 5:
			sprintf(m,"Mai");
		break;
		case 6:
			sprintf(m,"Juin");
		break;
		case 7:
			sprintf(m,"Juillet");
		break;
		case 8:
			sprintf(m,"Aout");
		break;
		case 9:
			sprintf(m,"Septembre");
		break;
		case 10:
			sprintf(m,"Octobre");
		break;
		case 11:
			sprintf(m,"Novembre");
		break;
		case 12:
			sprintf(m,"Decembre");
		break;
		default:
			sprintf(m,"T'as Foiré qqe part");
		break;
	}
	sprintf(a,"%d",D.year);
	int size = 1 + strlen(j) + 1 + strlen(m) + 1 +strlen(a);
	char* ret = malloc(sizeof(char)*size);
	sprintf(ret,"%s %s %s",j,m,a);
	return ret;
}

/**
 * @brief gestion du cas null pour la date
 * 
 * @param D 
 * @return true date définie
 * @return false date non définie
 */
bool dateIsNULL (Date D){
	return D.year == 9000;
}

/**
 * @brief Affichage console d'un maillon
 * 
 * @param ptr maillon a afficher
 */
void printMaillon(Maillon* ptr){
	int numero = 0;
	if(ptr==NULL){
		printerr("ERROR : TENTATIVE D AFFICHAGE D UN MAILLON NULL\n");
	}
	else{
		//Affichage d'un maillon
		printyellow("Data:\n");
		printlightblue("\t Nom: %s %s\n",TXT_COLOR_END,ptr->data->surname);
		printlightblue("\t Prenom(s):  ");
		printf(TXT_COLOR_END);
		for(int i = 0; ptr->data->name[i]; i++)
			printf("%s ",ptr->data->name[i]);
		printf("\n");
		printgreen("\t Naissance: \n");
		if( dateIsNULL(ptr->data->birth_data->date) ){
			printlightblue("\t\t Date: %s %s \n",TXT_COLOR_END,"NULL");
		}
		else{
			printlightblue("\t\t Date: %s %s \n",TXT_COLOR_END,dateToFancyString(ptr->data->birth_data->date));
		}
		if(ptr->data->birth_data->lieu.nom == NULL){
			printlightblue("\t\t Lieu: %s %s\n",TXT_COLOR_END,"NULL");
		}
		else{
			printlightblue("\t\t Lieu: %s %s (X=%f,Y=%f)\n",TXT_COLOR_END,ptr->data->birth_data->lieu.nom,ptr->data->birth_data->lieu.X,ptr->data->birth_data->lieu.Y);
		}
		printgreen("\t Mort: \n");
		if( dateIsNULL(ptr->data->death_data->date) ){
			printlightblue("\t\t Date: %s %s \n",TXT_COLOR_END,"NULL");
		}
		else{
			printlightblue("\t\t Date: %s %s\n",TXT_COLOR_END,dateToFancyString(ptr->data->death_data->date));
		}
		if(ptr->data->death_data->lieu.nom == NULL){
			printlightblue("\t\t Lieu: %s %s\n",TXT_COLOR_END,"NULL");
		}
		else{
			printlightblue("\t\t Lieu: %s %s (X=%f,Y=%f)\n",TXT_COLOR_END,ptr->data->death_data->lieu.nom,ptr->data->death_data->lieu.X,ptr->data->death_data->lieu.Y);
		}
		printgreen("\t Genre: %s%s \n",TXT_COLOR_END,(ptr->data->gender[0] == FEMME)?"Femme":"Homme");
		//Verification IMAGE RGB à faire
		printgreen("Pere:%s %p ",TXT_COLOR_END,ptr->dad);
		if(ptr->dad)
			printf("(%s %s Birth:%s)\n",ptr->dad->data->surname,ptr->dad->data->name[0],(dateIsNULL(ptr->dad->data->birth_data->date)?"NULL":dateToString(ptr->dad->data->birth_data->date)));
		else
			println("");
		printgreen("Mere:%s %p ",TXT_COLOR_END,ptr->mom);
		if(ptr->mom)
			printf("(%s %s Birth:%s)\n",ptr->mom->data->surname,ptr->mom->data->name[0],(dateIsNULL(ptr->mom->data->birth_data->date)?"NULL":dateToString(ptr->mom->data->birth_data->date)));
		else
			println("");
		
		printgreen("PointeurTete sur liste chaine Fraterie:%s %p \n",TXT_COLOR_END,ptr->sibling);
		if(ptr->sibling){
			for(int i = 0; ptr->sibling[i];i++){
				printf("%s %s | ",ptr->sibling[i]->data->name[0],ptr->sibling[i]->data->surname);
			}
			printf("\n");
		}
		printgreen("PointeurTete sur liste chaine Mariages:%s %p \n",TXT_COLOR_END,ptr->wedding);
		if(ptr->wedding){
			MaillonMariage* current = ptr->wedding;
			while(current){
				printf(TXT_COLOR_LIGHT_BLUE"\tDatas Mariage %d(%p)<-(%p) :\n"TXT_COLOR_END,numero,current->data,current);
				printf(TXT_COLOR_YELLOW"\t\t Date:"TXT_COLOR_END" %s\n",(current->data->date.day == 1 && current->data->date.month == 1 && current->data->date.year == 9000)?"Inconnue":dateToString(current->data->date));
				printf(TXT_COLOR_YELLOW"\t\t Lieu:"TXT_COLOR_END" %s\n",current->data->lieu.nom);
				printf("\t\t\t(X=%lf Y=%lf)\n", current->data->lieu.X,current->data->lieu.Y);
				printf(TXT_COLOR_YELLOW"\t\t Mari:"TXT_COLOR_END" %s %s (%p)\n",(current->data->maries[0])?current->data->maries[0]->data->name[0]:"",(current->data->maries[0])?current->data->maries[0]->data->surname:"none",current->data->maries[0]);
				printf(TXT_COLOR_YELLOW"\t\t Femme:"TXT_COLOR_END" %s %s (%p)\n"TXT_COLOR_END,(current->data->maries[1])?current->data->maries[1]->data->name[0]:"",(current->data->maries[1])?current->data->maries[1]->data->surname:"none",current->data->maries[1]);
				current = current->next;
				numero++;
			}
		}
		printgreen("Pointeur sur tableau Enfants:%s %p \n",TXT_COLOR_END,ptr->child);
		if(ptr->child){
			for(int i =0; ptr->child[i]; i++){
				printf("\t%s %s (%p)\n",ptr->child[i]->data->name[0],ptr->child[i]->data->surname,ptr->child[i]);
			}
		}
	}
}

/**
 * @brief Affichage d'une liste chainee entiere dans la console
 * 
 * @param ptrtete 
 */
void printLC(Maillon* ptrtete){
	int number=0;
	Maillon* current = ptrtete;
	while(current != NULL){
		printf(TXT_COLOR_RED);
		printf("Individu n° %d :\n",number);
		printMaillon(current);
		current = current->next;
		number++;
		printf("____________________________________________\n");
	}
}

/**
 * @brief si une chaine passée est vide, ca la change en NULL
 * 
 * @param str 
 */
void deNullify (char** str){
	if(*str!= NULL && (!strcmp(" ",*str) || !strcmp("",*str))){
		*str = NULL;
	}
}

/**
 * @brief Trouve l image associée a l'individu en local si elle est disponible
 * 
 * @param nom nom de l individu
 * @param prenom prenom de l individu
 * @param nbgen numero de génération de l'individu
 * @return Image* 
 */
Image* trouvePhotoIndividu (char* nom,char* prenom,int nbgen){
	char* nomfichier = malloc(sizeof(char)*  (strlen(nom) + strlen(prenom) + 4+19+4) );
	sprintf(nomfichier,"../individus_image/%s_%s_%d.bmp",nom,prenom,nbgen);
	printdebug("%s",nomfichier);
	Image* photo = new_Image(nomfichier,true);
	if(photo == NULL){
		sprintf(nomfichier,"../individus_image/%s_%s_%d.jpg",nom,prenom,nbgen);
		printdebug("%s",nomfichier);
		photo = new_Image(nomfichier,false);
		if(photo == NULL){
			sprintf(nomfichier,"../individus_image/%s_%s_%d.png",nom,prenom,nbgen);
			printdebug("%s",nomfichier);
			photo = new_Image(nomfichier,false);
			if(photo == NULL){
				photo = new_Image("../individus_image/NULL.bmp",true);
			}
		}
	}
	free(nomfichier);
	nomfichier = NULL;
	return photo;
}

/**
 * @brief Fonction d'initialisation d'un individu
 * 
 * @param CSVtextdatabyword Cellules du tableur CSV
 * @return Maillon* ptrTete 
 */
Maillon* initLC (char*** CSVtextdatabyword){
	Maillon* ptrTete = NULL;
	foreach(char*** ptrline , CSVtextdatabyword){ //pour chaque ligne on cree un nouvel individu
		printdebug("nouvelle boucle");
		char** line = *ptrline;

		// Recuperation des données sous forme de char*
		int nbgen = 	line[0][0] - '0';
		char* nom = 	line[1];
		char* prenom = 	line[2];
		char genre = 	line[3][0];
		char* date_n = 	line[4];
		char* lieu_n = 	line[5];
		char* date_d = 	line[6];
		char* lieu_d = 	line[7];
		char* lieu_X_n = line[12];
		char* lieu_Y_n = line[13];
		char* lieu_X_d = line[14];
		char* lieu_Y_d = line[15];

		Image* photo = trouvePhotoIndividu(nom,prenom,nbgen);

		//gestion du cas vide ou NULL
		deNullify(&nom);
		deNullify(&prenom);
		deNullify(&date_n);
		deNullify(&date_d);
		deNullify(&lieu_d);
		deNullify(&lieu_n);
		deNullify(&lieu_X_n);
		deNullify(&lieu_X_d);
		deNullify(&lieu_Y_d);
		deNullify(&lieu_Y_n);

		//création du maillon
		Individu *I = new_Individu(nbgen,nom,prenom,genre,date_n,lieu_n,date_d,lieu_d,photo,lieu_X_n,lieu_Y_n,lieu_X_d,lieu_Y_d);
		Maillon* maille = new_Maillon(I);

		//insertion de l'individu a la fin de la liste chainée (le fichier CSV est trié)
		InsereIndividuFin(maille,&ptrTete);
	}
	InverseListe(&ptrTete);

	//si le pere ou la mere de l individu n'est pas dans le CSV, on va l'ajouter
	for(int i=0;i<2;i++){
		char genre;
		int num_rep;
		if(i==0){
			genre = 'M';
			num_rep = 8;
		}
		else{
			genre = 'F';
			num_rep = 10;
		}
		foreach(char*** ptrLine, CSVtextdatabyword){	

			//recuperation des données
			char** line = *ptrLine;
			int generation = line[0][0] - '0'+1;

			char* nom = line[num_rep];
			char* prenom = line[num_rep+1];

			deNullify(&nom);
			deNullify(&prenom);

			if(prenom != NULL && nom != NULL){
				//verification de si oui ou non on a la photo de l'individu
				char* nomfichier = malloc(sizeof(char)*  (strlen(nom) + strlen(prenom) + 4+19+4) );
				sprintf(nomfichier,"../individus_image/%s_%s_%d.bmp",nom,prenom,generation);
				printdebug("%s",nomfichier);
				Image* photo = new_Image(nomfichier,true);
				int nbgen = generation;
				if(photo == NULL){
					sprintf(nomfichier,"../individus_image/%s_%s_%d.jpg",nom,prenom,nbgen);
					printdebug("%s",nomfichier);
					photo = new_Image(nomfichier,false);
					if(photo == NULL){
						sprintf(nomfichier,"../individus_image/%s_%s_%d.png",nom,prenom,nbgen);
						printdebug("%s",nomfichier);
						photo = new_Image(nomfichier,false);
						if(photo == NULL){
							photo = new_Image("../individus_image/NULL.bmp",true);
						}
					}
				}
				free(nomfichier);
				nomfichier = NULL;

				//creation de l'individu parent
				Individu* parent = new_Individu(generation,nom,prenom,genre,NULL,NULL,NULL,NULL,photo,NULL,NULL,NULL,NULL);
				if(RechercheIndividu(parent,&ptrTete) == NULL){	//si l'individu n'est pas dans le CSV
					//on l'insére dans la liste chainee
					Maillon* newM = new_Maillon(parent);
					newM->next = NULL;
					InsereNouveauMaillon(newM,&ptrTete);
				}
			}
		}
	}
	DEBUGEND;
	return ptrTete;
}

/**
 * @brief fonction reseau/fractales, retourne un tableau des maillons ayant un lien de parenté/fraternité/mariage avec le maillon
 * 
 * @param m maillon recherche
 * @return Maillon** 
 */
Maillon** getLinkList (Maillon* m){
	Maillon** ret = malloc(sizeof(Maillon*));
	int size = 0;
	ret[0] = NULL;
	if(m->dad != NULL){
		size++;
		ret = realloc(ret,sizeof(Maillon*)*(size+1));
		ret[size-1] = m->dad;
		ret[size] = NULL;
	}
	if(m->mom != NULL){
		size++;
		ret = realloc(ret,sizeof(Maillon*)*(size+1));
		ret[size-1] = m->mom;
		ret[size] = NULL;
	}
	if(m->wedding != NULL){
		size++;
		ret = realloc(ret,sizeof(Maillon*)*(size+1));
		ret[size-1] = m->wedding->data->maries[!(*(m->data->gender))];
		ret[size] = NULL;
	}
	if(m->sibling != NULL){
		for(int i=0;m->sibling[i] != NULL;i++){
			size++;
			ret = realloc(ret,sizeof(Maillon*)*(size+1));
			ret[size-1] = m->sibling[i];
			ret[size] = NULL;
		}
	}
	if(m->child != NULL){
		for(int i=0;m->child[i] != NULL;i++){
			size++;
			ret = realloc(ret,sizeof(Maillon*)*(size+1));
			ret[size-1] = m->child[i];
			ret[size] = NULL;
		}
	}
	return ret;
}


bool compareLieu(Lieu l1,Lieu l2){
	DEBUGSTART;
	int cpt = 0;
	
	if(betterstrcmp(l1.nom,l2.nom)){
		cpt++;
	}

	if(l2.X !=0){
		if(l1.X == l2.X){
			cpt++;
		}
	}else{
		cpt++;
	}

	if(l2.Y != 0){
		if(l1.Y == l2.Y){
			cpt++;
		}
	}else{
		cpt++;
	}
	if(cpt == 3){
		return true;
	}else{
		return false;
	}
	DEBUGEND;
}

bool compareDate(Date d1,Date d2,char *comparaison){
	int cpt = 0;
	int cpt2 = 0;
	switch(*comparaison){
		case '<' :
			if(d1.year < d2.year){
				cpt++;
			}else if(d1.year == d2.year){
				if(d1.month < d2.month){
					cpt++;
				}else if (d1.month == d2.month){
					if(d1.day < d2.day){
						cpt++;
					}
				}
			}
			break;

		case '>' :
			if(d1.year > d2.year){
				cpt++;
			}else if(d1.year == d2.year){
				if(d1.month > d2.month){
					cpt++;
				}else if (d1.month == d2.month){
					if(d1.day > d2.day){
						cpt++;
					}
				}
			}
			break;

		case '=' :
			if(d1.day == d2.day){
				cpt2++;
			}
			if(d1.month == d2.month){
				cpt2++;
			}
			if(d1.year == d2.year){
				cpt2++;
			}
			break;

		default :
			break;
	}
	if(cpt == 1|| cpt2 == 3){
		return true;
	}else{
		return false;
	}

}
bool compareBirth(Birth *b1,Birth *b2,char *comparaison){
	if(compareDate(b1->date,b2->date,comparaison)){
		if(compareLieu(b1->lieu,b2->lieu)){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

bool compareDeath(Death *d1,Death *d2,char *comparaison){
	if(compareDate(d1->date,d2->date,comparaison)){
		if(compareLieu(d1->lieu,d2->lieu)){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}
//Créer la structure critère
Critere creeCritere(int numGen, char *surname, char *name, Birth *birth_data, Death *death_data, bool *gender,Maillon *dad, Maillon *mom,char *comparaison){
	return (Critere){numGen,surname,name,birth_data,death_data,gender,dad,mom,comparaison};
}


bool bstrcmp(char* c1,char* c2){
	
	if(c1 == NULL && c2 == NULL){
		return true;
	}else if(c2 == NULL){
		return true;
	}else if(c1 == NULL){
		return false;
	}else{
		return !strcmp(c1,c2);
	}
	
}


bool bCompareBirth(Birth *b1,Birth *b2,char *comparaison){

	if(b1 == NULL && b2 == NULL){
		return true;
	}else if(b2 == NULL){
		return true;
	}else if(b1 == NULL){
		return false;
	}else{
		return compareBirth(b1,b2,comparaison);
	}
}

bool bCompareDeath(Death *d1,Death *d2,char *comparaison){

	if(d1 == NULL && d2 == NULL){
		return true;
	}else if(d2 == NULL){
		return true;
	}else if(d1 == NULL){
		return false;
	}else{
		return compareDeath(d1,d2,comparaison);
	}
}

bool compareGender(bool *g1,bool *g2){

	if(g1 == NULL && g2 == NULL){
		return true;
	}else if(g2 == NULL){
		return true;
	}else if(g1 == NULL){
		return false;
	}else{
		if(*g1 == *g2){
			return true;
		}else{
			return false;
		}
	}
}

bool compareDad(Maillon *dad1,Maillon *dad2){

	if(dad1 == NULL && dad2 == NULL){
		return true;
	}else if(dad2 == NULL){
		return true;
	}else if(dad1 == NULL){
		return false;
	}else{
		if(dad1->dad == dad2){
			return true;
		}else{
			return false;
		}
	}
}

bool compareMom(Maillon *mom1,Maillon *mom2){
	
	if(mom1 == NULL && mom2 == NULL){
		return true;
	}else if(mom2 == NULL){
		return true;
	}else if(mom1 == NULL){
		return false;
	}else{
		if(mom1->mom == mom2){
			return true;
		}else{
			return false;
		}
	}
}

rech rechercheMaillon(int attribut_type,void *attribut,Maillon *ptrtete,char *comparaison){
    //printf("dans rechercheMaillon\n");
	Maillon *inter = ptrtete;
	
	rech r;
	r.tab = malloc(sizeof(Maillon*));
	r.cpt = 0;

    switch (attribut_type)
    {
		case NUM_GEN:
			while(inter != NULL){
				int *tmp = (int*) attribut;
				if(*tmp == inter->data->numGen){
					r.tab[(r.cpt)] = inter;
					r.cpt = r.cpt + 1;
					r.tab = realloc(r.tab,sizeof(Maillon*)*((r.cpt)+1));
				}
				inter = inter->next;
			}
			r.tab[r.cpt] = false;
			return r;
		break;

        case NAME:
            while(inter != NULL){
				char* tmp = (char*) attribut;
                if(!strcmp(tmp,inter->data->name[0])){
					r.tab[(r.cpt)] = inter;
					r.cpt = r.cpt + 1;
					r.tab = realloc(r.tab,sizeof(Maillon*)*((r.cpt)+1));
                }
                inter = inter->next;
            }
			r.tab[r.cpt] = false;
			return r;
            break;
        
        case SURNAME:
            while(inter != NULL){
				char* tmp = (char*) attribut;
                if(!strcmp(tmp,inter->data->surname)){
					r.tab[(r.cpt)] = inter;
					(r.cpt)++;
					r.tab = realloc(r.tab,(sizeof(Maillon*))*((r.cpt)+1));
                }
                inter = inter->next;
            }
			r.tab[r.cpt] = false;
			return r;
            break;

        case D_BIRTH:
            while(inter != NULL){
				Birth * tmp = (Birth*)attribut;
                if(compareBirth(tmp,inter->data->birth_data,comparaison)){
					r.tab[(r.cpt)] = inter;
					(r.cpt)++;
					r.tab = realloc(r.tab,sizeof(Maillon*)*((r.cpt)+1));
                }
                inter = inter->next;
            }
			r.tab[r.cpt] = false;
			return r;
            break;

        case D_DEATH:
            while(inter != NULL){
				Death *tmp = (Death*)attribut;
                if(compareDeath(tmp,inter->data->death_data,comparaison)){
					r.tab[(r.cpt)] = inter;
					(r.cpt)++;
					r.tab = realloc(r.tab,sizeof(Maillon*)*((r.cpt)+1));
                }
                inter = inter->next;
            }
			r.tab[r.cpt] = false;
			return r;
            break;

        case GENDER:
            while(inter != NULL){
				bool * tmp = (bool*)attribut;
                if(*tmp == inter->data->gender[0]){
					r.tab[(r.cpt)] = inter;
					(r.cpt)++;
					r.tab = realloc(r.tab,sizeof(Maillon*)*((r.cpt)+1));
                }
                inter = inter->next;
            }
			r.tab[r.cpt] = false;
			return r;
            break;
    
        default:
            printf("Le codeur est une oie\n");
            break;
    }
	printf("oui oui une oie\n");
	rech r2;
	r2.tab = NULL;
	r2.cpt= 0;
	return r2;
}


void petitRechercheMaillon(rech *r,Maillon ***result, int *cpt,Critere C){

	foreach(Maillon **tmp,r->tab){
		if(((*tmp)->data->numGen == C.numGen || C.numGen == -1) && bstrcmp((*tmp)->data->name[0],C.name) && bstrcmp((*tmp)->data->surname,C.surname)){
			if((bCompareBirth((*tmp)->data->birth_data,C.birth_data,C.comparaison)) && bCompareDeath((*tmp)->data->death_data,C.death_data,C.comparaison) && compareGender((*tmp)->data->gender,C.gender)){
				if(compareDad((*tmp),C.dad) && compareMom((*tmp),C.mom)){
					bool estpresent = false;
					for(int moncpt = 0; moncpt<(*cpt) ; moncpt ++){
						if(compareIndividu(((*result)[moncpt])->data,(*tmp)->data)){
							estpresent = true;
   						}
					}
					if(!estpresent){
						(*result)[(*cpt)] = (*tmp);
						(*cpt)++;
						(*result) = realloc(*result,sizeof(Maillon*)*((*cpt)+1));
					}
					estpresent = false;
												
					(*result) = realloc(*result,sizeof(Maillon*)*((*cpt)+1));
				}				
			}
		} 
	}
}

//Fonction principale qui renvoi un tableau des maillons correspondants aux critères donnés
Maillon** superRechercheMaillon(Critere C, Maillon *ptrtete){
	Maillon **m = malloc(sizeof(Maillon*));

	rech r1 = {NULL,0};
	rech r2 = {NULL,0};
	rech r3 = {NULL,0};
	rech r4 = {NULL,0};
	rech r5 = {NULL,0};
	rech r6 = {NULL,0};

	if(C.numGen != -1){
		r1 = rechercheMaillon(NUM_GEN,&(C.numGen),ptrtete,NULL);
	}

	if(C.surname != NULL){
		r2 = rechercheMaillon(SURNAME,C.surname,ptrtete,NULL);
	}
	if(C.name != NULL){
		r3 = rechercheMaillon(NAME,C.name,ptrtete,NULL);
	}
	if(C.birth_data != NULL){
		r4 = rechercheMaillon(D_BIRTH,C.birth_data,ptrtete,C.comparaison);
	}
	if(C.death_data != NULL){
		r5 = rechercheMaillon(D_DEATH,C.death_data,ptrtete,C.comparaison);
	}
	if (C.gender){
		r6 = rechercheMaillon(GENDER,C.gender,ptrtete,NULL);
	}
	

	int *cpt = malloc(sizeof(int));
	*cpt = 0;

	if(r1.tab != NULL){
		petitRechercheMaillon(&r1,&m,cpt,C);
	}
	
	if(r2.tab != NULL){
		
		petitRechercheMaillon(&r2,&m,cpt,C);
	}

	if(r3.tab != NULL){
		petitRechercheMaillon(&r3,&m,cpt,C);
	}

	if(r4.tab != NULL){
		petitRechercheMaillon(&r4,&m,cpt,C);
	}

	if(r5.tab != NULL){
		petitRechercheMaillon(&r5,&m,cpt,C);
	}
	if(r6.tab != NULL){
		petitRechercheMaillon(&r6,&m,cpt,C);
	}
	//printred("cpt = %d\n",*cpt);
	m[*cpt] = false;
	
	return m;
}

bool contains(Maillon* ptrTete,Maillon* ptr){
	bool contains = false;
	for(Maillon* current = ptrTete; current;current=current->next){
		if(current == ptr){
			contains = true;
		}
	}
	return contains;
}

Maillon** supprDoublons(Maillon** Array){

	Maillon** result = malloc(sizeof(Maillon*));
	result[0] = NULL;
	
	int size = 1;

	for(int i = 0; Array[i]; i++){
		
		int tmp = 0;
		for(int j = 0; result[j]; j++){
			if(result[j] != Array[i]){
				tmp++;
			}
		}
		if(tmp == count(result)-1){
			result[size-1] = Array[i];
			size++;
			result = realloc(result, size * sizeof(Maillon*));
			
			result[size-1] = NULL;
		}
	}
	free(Array);
	return result;
}

/*
 * Fonction InsereNouveauMaillon
 * 
 * Cette fonction permet d'inserer un nouveau maillon dans une LC déja existante
 * @param M : Maillon à inserer
 * @param ptrTete: Pointeur de Tete de la LC
 * @return \ 
 * */
void InsereNouveauMaillon(Maillon * M,Maillon ** ptrTete){
	Maillon *ptr =*ptrTete;
	Maillon *precedent;
	int etat=0;
	while(ptr!= NULL && etat!=1){
		if(M->data->numGen > ptr->data->numGen){
			ptr=ptr->next;
		}
		else{
			if(strcmp(M->data->surname,ptr->data->surname)>0 && M->data->numGen==ptr->data->numGen){
				ptr=ptr->next;
			}
			else{
				if(strcmp(M->data->name[0],ptr->data->name[0])>0 && strcmp(M->data->surname,ptr->data->surname)==0){
					ptr=ptr->next;
				}
				else{
					precedent=cherchePrecedent(ptr,*ptrTete);
					if(precedent!=NULL){
						M->next=precedent->next;
						precedent->next=M;
						etat=1;
					}
					else{
						M->next=*ptrTete;
						*ptrTete=M;
						etat=1;
					}
				}
			}
		}
	}
	//Insere à la fin si le chiffre de génération est plus grand que celui de tous ceux de la liste
	if(etat==0){
		ptr=*ptrTete;
		while(ptr->next!=NULL){
			ptr=ptr->next;
		}
		ptr->next=M;
	}	
}
void sauverCsvIndividu(char* path,Maillon* ptrTete){
	Maillon* current;
	current = ptrTete;

	FILE* fp;
	fp = fopen (path,"w");
	if(fp){ //Si fp est différent de NULL
			fprintf (fp, "REGARDE_MAMAN_SANS_LES_MAINS;nom;prenom;Genre;date_N;lieu_N;date_D;lieu_D;nom_P;prenom_P;nom_M;prenom_M;lieu_N_X;lieu_N_Y;lieu_M_X;lieu_M_Y\n");
		while(current){
				
			fprintf (fp, "%d;%s;%s",
			current->data->numGen,
			(current->data->surname != NULL)?current->data->surname:"",
			(current->data->name[0] != NULL)?current->data->name[0]:"");
			
			if(current->data->gender[0] ==  HOMME)
				fprintf(fp,";M;");
			else
				fprintf(fp,";F;");

			fprintf(fp,"%s;%s;%s;%s;%s;%s",
			((current->data->birth_data->date.day==1)&&(current->data->birth_data->date.month==1)&&(current->data->birth_data->date.year==9000))?"":dateToString(current->data->birth_data->date),
			(current->data->birth_data->lieu.nom != NULL)?current->data->birth_data->lieu.nom:"",
			((current->data->death_data->date.day==1)&&(current->data->death_data->date.month==1)&&(current->data->death_data->date.year==9000))?"":dateToString(current->data->death_data->date),
			(current->data->death_data->lieu.nom != NULL)?current->data->death_data->lieu.nom:"",
			(current->dad != NULL)?current->dad->data->surname:"",
			(current->dad != NULL)?current->dad->data->name[0]:"");

			fprintf(fp,";%s;%s",
			(current->mom != NULL)?current->mom->data->surname:"",
			(current->mom != NULL)?current->mom->data->name[0]:"");

			fprintf(fp,";%lf;%lf;%f;%f",
			current->data->birth_data->lieu.X,
			current->data->birth_data->lieu.Y,
			current->data->death_data->lieu.X,
			current->data->death_data->lieu.Y);

			if(current->next)
				fprintf(fp,"\n");
			current = current->next;
			
		}
		/* close the file*/  
		fclose (fp);
	}
	else{
		printf("\nErreur de sauvegarde de la liste Individus (Erreur d'ouverture du fichier)\n");
	}
}
int getIndexOf(Maillon* wanted, Maillon* ptrTete){

	int numberWanted = 0;

	Maillon* current = ptrTete;
	
	//Tant que le maillon null n'est pas ateint
	while(current){
		//Si le maillon recherché est égal au maillon actuel
		if(wanted == current)
		{
			return numberWanted;
		}
		else
		{
			numberWanted++;
		}
		//Passe au maillon suivant
		current = current->next;
	}
	//Renvoie code d'erreur (maillon pas trouvé)
	return -1;
}

void chargeCsvMariages(char* path_name, char**** tableauCsvMariages){

	FILE* fp;
	bool first_line;
	char* wordToAdd;
	char sub[100], ret[2];
	int number_categories, number_line, word, line, c;
	
	fp = fopen(path_name, "r");

	first_line = true;
	number_categories = 1;
	number_line = 1;
	
	
	
	if(fp){
		

		while ((c = fgetc(fp)) != EOF){
			if(c == 59 && first_line)
				number_categories++;
			
			if(c == 10){
				number_line++;
				first_line = false;
			}
		}

		(*tableauCsvMariages) = malloc(sizeof(char**)*number_line);
		for(int i = 0; i<number_line-1; i++){
			(*tableauCsvMariages)[i] = malloc(sizeof(char*)*(number_categories+1));
		}
		(*tableauCsvMariages)[number_line-1] = NULL;

		fseek(fp, 0L, SEEK_SET);

		first_line = true;
		strcpy(sub,"");
		word = 0;
		line = 0;
		ret[1] = '\0';
		while ((c = fgetc(fp)) != EOF){

			if(first_line == false){
				ret[0] = c;
				if(c != 59 && c != 10 && c!=13){
					strcat(sub,ret);
				}
				if(c == 59){
					wordToAdd = malloc(sizeof(char)*strlen(sub));
					wordToAdd[0] = '\0';
					strcpy(wordToAdd, sub);
					(*tableauCsvMariages)[line][word] = wordToAdd;
					word++;
					strcpy(sub,"");
				}
				if(c == 10){
					wordToAdd = malloc(sizeof(char)*strlen(sub));
					wordToAdd[strlen(sub)] = '\0';
					strcpy(wordToAdd, sub);
					(*tableauCsvMariages)[line][word] = wordToAdd;
					(*tableauCsvMariages)[line][word+1] = NULL;
					strcpy(sub,"");
					line++;
					word = 0;
				}
			}
			
			if(c == 10 && first_line){
				first_line = false;
			}
		}
		wordToAdd = malloc(sizeof(char)*strlen(sub));
		wordToAdd[strlen(sub)] = '\0';
		strcpy(wordToAdd, sub);
		(*tableauCsvMariages)[line][word] = wordToAdd;
		(*tableauCsvMariages)[line][word+1] = NULL;
		for(int i = 0; (*tableauCsvMariages)[i];i++)
			(*tableauCsvMariages)[i][12] = NULL;
		fclose(fp);

	}
	else{
		//erreur d'ouverture
	}


}
void triListeDataMariage(Mariage*** listeDataMariage){
	Mariage* sub = NULL;
	for(int i = 0; (*listeDataMariage)[i]; i++){
		for(int j = 0; (*listeDataMariage)[j];j++){
			if((((*listeDataMariage)[i]->date.year > (*listeDataMariage)[j]->date.year) || ((*listeDataMariage)[i]->date.year == (*listeDataMariage)[j]->date.year && (*listeDataMariage)[i]->date.month > (*listeDataMariage)[j]->date.month) || ((*listeDataMariage)[i]->date.year == (*listeDataMariage)[j]->date.year && (*listeDataMariage)[i]->date.month == (*listeDataMariage)[j]->date.month && (*listeDataMariage)[i]->date.day > (*listeDataMariage)[j]->date.day)) && i < j){
				sub = (*listeDataMariage)[i];
				(*listeDataMariage)[i] = (*listeDataMariage)[j];
				(*listeDataMariage)[j] = sub;
				sub = NULL;
			}
		}
	}
}

MaillonMariage* initMariageLC(char*** CSVtextdatabywordMariage, Maillon* ptrTete){
	DEBUGSTART;
	MaillonMariage* ptrTeteMariage = malloc(sizeof(MaillonMariage));
	MaillonMariage* current = NULL;
	Maillon* current2 = ptrTete;
	bool once = true;
	
	Mariage** listeDataMariage = malloc(sizeof(Mariage*)*count((void**)(CSVtextdatabywordMariage)));
	listeDataMariage[count((void**)(CSVtextdatabywordMariage))-1]= NULL;
	int compteur = 0;

	foreach(char*** ptrLine, CSVtextdatabywordMariage){
		char** line = *ptrLine;
		int generationHomme = line[0][0] - '0';
		char* nomHomme = line[1];
		char* prenomHomme = line[2];
		char* dateNaissanceHomme = line[3];
		char* lieuNaissanceHomme = line[4];
		int generationFemme = line[5][0] - '0';
		char* nomFemme = line[6];
		char* prenomFemme = line[7];
		char* dateNaissanceFemme = line[8];
		char* lieuNaissanceFemme = line[9];
		int compteurValidation1 = 0;
		int compteurValidation2 = 0;
		deNullify(&nomHomme);
		deNullify(&prenomHomme);
		deNullify(&nomFemme);
		deNullify(&prenomFemme);
		deNullify(&dateNaissanceHomme);
		deNullify(&dateNaissanceFemme);
		deNullify(&lieuNaissanceFemme);
		deNullify(&lieuNaissanceHomme);
		while(current2)
		{
			if(!strcmp(current2->data->name[0],prenomHomme) && !strcmp(current2->data->surname,nomHomme) && current2->data->numGen == generationHomme){
				compteurValidation1++;
				
			}
			if(!strcmp(current2->data->name[0],prenomFemme) && !strcmp(current2->data->surname,nomFemme) && current2->data->numGen == generationFemme){
				compteurValidation2++;
			}

			current2 = current2->next;
		}
		current2 = ptrTete;
		if(compteurValidation1 != 1 && strcmp(nomHomme, " ") && strcmp(prenomHomme, " ")){
			//CREE HOMME avec NOM PRENOM DATE_NAISSANCE LIEU_NAISSANCE
			char* nomfichier = malloc(sizeof(char)*  (strlen(nomHomme) + strlen(prenomHomme) + 4+19+4) );
				sprintf(nomfichier,"../individus_image/%s_%s_%d.bmp",nomHomme,prenomHomme,generationHomme);
				printdebug("%s",nomfichier);
				Image* photo = new_Image(nomfichier,true);
				if(photo == NULL){
					sprintf(nomfichier,"../individus_image/%s_%s_%d.jpg",nomHomme,prenomHomme,generationHomme);
					printdebug("%s",nomfichier);
					photo = new_Image(nomfichier,false);
					if(photo == NULL){
						sprintf(nomfichier,"../individus_image/%s_%s_%d.png",nomHomme,prenomHomme,generationHomme);
						printdebug("%s",nomfichier);
						photo = new_Image(nomfichier,false);
						if(photo == NULL){
							photo = new_Image("../individus_image/NULL.bmp",true);
						}
					}
				}
				free(nomfichier);
				nomfichier = NULL;
			Individu* newI = new_Individu(generationHomme,nomHomme,prenomHomme,'M',dateNaissanceHomme,lieuNaissanceHomme,NULL,NULL,photo,NULL,NULL,NULL,NULL);
			Maillon* newM = new_Maillon(newI);
			newM->next = NULL;
			InsereNouveauMaillon(newM,&ptrTete);
		}
		if(compteurValidation2 != 1 && strcmp(nomFemme," ") && strcmp(prenomFemme," ")){
			//CREE FEMME avec NOM PRENOM DATE_NAISSANCE LIEU_NAISSANCE
			char* nomfichier = malloc(sizeof(char)*  (strlen(nomFemme) + strlen(prenomFemme) + 4+19+4) );
				sprintf(nomfichier,"../individus_image/%s_%s_%d.bmp",nomFemme,prenomFemme,generationHomme);
				printdebug("%s",nomfichier);
				Image* photo = new_Image(nomfichier,true);
				if(photo == NULL){
					sprintf(nomfichier,"../individus_image/%s_%s_%d.jpg",nomFemme,prenomFemme,generationHomme);
					printdebug("%s",nomfichier);
					photo = new_Image(nomfichier,false);
					if(photo == NULL){
						sprintf(nomfichier,"../individus_image/%s_%s_%d.png",nomFemme,prenomFemme,generationHomme);
						printdebug("%s",nomfichier);
						photo = new_Image(nomfichier,false);
						if(photo == NULL){
							photo = new_Image("../individus_image/NULL.bmp",true);
						}
					}
				}
				free(nomfichier);
				nomfichier = NULL;
			Individu* newI = new_Individu(generationFemme,nomFemme,prenomFemme,'F',dateNaissanceFemme,lieuNaissanceFemme,NULL,NULL,photo,NULL,NULL,NULL,NULL);
			Maillon* newM = new_Maillon(newI);
			newM->next = NULL;
			InsereNouveauMaillon(newM,&ptrTete);
		}
		current2 = ptrTete;
	}
	
	


	foreach(char*** ptrLine, CSVtextdatabywordMariage){
		char** line = *ptrLine;

		int generationHomme = line[0][0] - '0';
		char* nomHomme = line[1];
		char* prenomHomme = line[2];
		int generationFemme = line[5][0] - '0';
		char* nomFemme = line[6];
		char* prenomFemme = line[7];
		char* dateMariage = line[10];
		char* lieuMariage = line[11];
		Mariage* dataMariage = malloc(sizeof(Mariage));

		dataMariage->date.day = (strrchr(dateMariage, '/'))? (((dateMariage[0] - '0')*10) + ((dateMariage[1] - '0'))):1;
		dataMariage->date.month = (strrchr(dateMariage, '/'))? (((dateMariage[3] - '0')*10) + ((dateMariage[4] - '0'))):1;
		dataMariage->date.year = (strrchr(dateMariage, '/'))? (((dateMariage[6] - '0')*1000) + ((dateMariage[7] - '0')*100) + ((dateMariage[8] - '0')*10) + ((dateMariage[9] - '0'))):9000;

		dataMariage->lieu.nom = (strcmp(lieuMariage,""))?lieuMariage:NULL;
		dataMariage->lieu.X = 0;
		dataMariage->lieu.Y = 0;
		dataMariage->maries[0] = NULL;
		dataMariage->maries[1] = NULL;
		
		while(current2){
			if(current2->data->gender[0] == HOMME && !strcmp((current2->data->surname != NULL)?current2->data->surname:"",nomHomme) && !strcmp((current2->data->name[0])?current2->data->name[0]:"",prenomHomme) && current2->data->numGen == generationHomme){
				dataMariage->maries[0] = current2;
			}
			else if(current2->data->gender[0] == FEMME && !strcmp((current2->data->surname != NULL)?current2->data->surname:"",nomFemme) && !strcmp((current2->data->name[0])?current2->data->name[0]:"",prenomFemme) && current2->data->numGen == generationFemme){
				dataMariage->maries[1] = current2;
			}
			current2 = current2->next;
		}
		
		current2 = ptrTete;
		
		listeDataMariage[compteur] = dataMariage;
		compteur++;
	}
	
	triListeDataMariage(&listeDataMariage);

	MaillonMariage* ptrTeteMariage2 = malloc(sizeof(MaillonMariage));
	MaillonMariage* currentMariage2 = ptrTeteMariage2;

	for(int i = 0; listeDataMariage[i]; i++){
		Mariage* celluleData = malloc(sizeof(Mariage));
		celluleData->date.day = listeDataMariage[i]->date.day;
		celluleData->date.month = listeDataMariage[i]->date.month;
		celluleData->date.year = listeDataMariage[i]->date.year;
		celluleData->lieu.nom  = listeDataMariage[i]->lieu.nom;
		celluleData->lieu.X  = listeDataMariage[i]->lieu.X;
		celluleData->lieu.Y  = listeDataMariage[i]->lieu.Y;
		celluleData->maries[0] = listeDataMariage[i]-> maries[0];
		celluleData->maries[1] = listeDataMariage[i]-> maries[1];
		if(i==0){
			ptrTeteMariage2->data = celluleData;
			ptrTeteMariage2->next = NULL;
		}
		else{
			currentMariage2->next = malloc(sizeof(MaillonMariage));
			currentMariage2 = currentMariage2->next;
			currentMariage2->data = celluleData;
			currentMariage2->next = NULL;
		}
	}
	currentMariage2 = ptrTeteMariage2;
	while(currentMariage2){
		currentMariage2 = currentMariage2->next;
	}
	current2 = ptrTete;
	while(current2 != NULL){
		currentMariage2 = ptrTeteMariage2;
		while(currentMariage2){
			// Vérification si 
			if(current2 == currentMariage2->data->maries[0] || current2 == currentMariage2->data->maries[1]){
				if(!once){
					while(current->next != NULL){
						current = current->next;
					}
					current->next = malloc(sizeof(MaillonMariage));
					current->next->data = currentMariage2->data;
					current->next->next = NULL;
				}
				if(once){
					ptrTeteMariage->data = currentMariage2->data;
					ptrTeteMariage->next = NULL;
					current2->wedding = ptrTeteMariage;
					current = ptrTeteMariage;
					once = false;
				}
			}
			currentMariage2 = currentMariage2->next;
		}
		if(!once){
			ptrTeteMariage = malloc(sizeof(MaillonMariage));
		}
		once = true;
		current2 = current2->next;
	}
	for(int i = 0; listeDataMariage[i];i++){
		//free(listeDataMariage[i]);
	}
	//free(listeDataMariage);
	for(int i = 0; CSVtextdatabywordMariage[i];i++){
		//free(CSVtextdatabywordMariage[i]);
	}
	//free(CSVtextdatabywordMariage);
	DEBUGEND;
	return ptrTeteMariage2;
}

void sauverCsvMariage(char* path,MaillonMariage* ptrTeteMariages){
	DEBUGSTART;

	MaillonMariage* current = ptrTeteMariages;
	FILE* fp;
	fp = fopen (path,"w");
	if(fp){ //Si fp est différent de NULL
		fprintf (fp, "REGARDE_MAMAN_SANS_LES_MAINS;nom_M;prenom_M;date_N_M;lieu_N_M;nG;nom_F;prenom_F;date_N_F;lieu_N_F;date_M;lieu_M");
		while(current){
			if(current->data->maries[0])
				fprintf(fp,"\n%d;",current->data->maries[0]->data->numGen);
			fprintf (fp, "%s;%s;%s;%s;",
			(current->data->maries[0])?current->data->maries[0]->data->surname:"",
			(current->data->maries[0])?current->data->maries[0]->data->name[0]:"",
			(current->data->maries[0])?((current->data->maries[0]->data->birth_data->date.day == 1 && current->data->maries[0]->data->birth_data->date.month == 1 && current->data->maries[0]->data->birth_data->date.year == 9000)?"":dateToString(current->data->maries[0]->data->birth_data->date)):"",
			(current->data->maries[0])?((current->data->maries[0]->data->birth_data->lieu.nom)?current->data->maries[0]->data->birth_data->lieu.nom:""):"");
			if(current->data->maries[1]){
				fprintf(fp,"%d;",current->data->maries[1]->data->numGen);
			}
			fprintf (fp, "%s;%s;%s;%s;%s;%s",
			(current->data->maries[1])?current->data->maries[1]->data->surname:"",
			(current->data->maries[1])?current->data->maries[1]->data->name[0]:"",
			(current->data->maries[1])?((current->data->maries[1]->data->birth_data->date.day == 1 &&  current->data->maries[1]->data->birth_data->date.month == 1 && current->data->maries[1]->data->birth_data->date.year == 9000)?"":dateToString(current->data->maries[1]->data->birth_data->date)):"",
			(current->data->maries[1])?((current->data->maries[1]->data->birth_data->lieu.nom)?current->data->maries[1]->data->birth_data->lieu.nom:""):"",
			(current->data->date.day == 1 && current->data->date.month == 1 && current->data->date.year == 9000)?"":dateToString(current->data->date),
			(current->data->lieu.nom)?(current->data->lieu.nom):"");
			current = current->next;
		}
		/* close the file*/  
		fclose (fp);
	}
	else{
		printf("\nErreur de sauvegarde de la liste Mariages (Erreur d'ouverture du fichier)\n");
	}
	DEBUGEND;
}

void insertMaillonMariage(MaillonMariage* ptrTeteMariage, Maillon* Personne, Mariage* nouveauMariage){
	DEBUGSTART;
	
	if(nouveauMariage){
		
		if(ptrTeteMariage){
			MaillonMariage* nouveauMaillon = malloc(sizeof(MaillonMariage));
			nouveauMaillon->data = nouveauMariage;
			MaillonMariage* current = ptrTeteMariage;
			while(current){
				if(current->next){
					if(((nouveauMaillon->data->date.year > current->next->data->date.year) || (nouveauMaillon->data->date.year == current->next->data->date.year && nouveauMaillon->data->date.month > current->next->data->date.month) || (nouveauMaillon->data->date.year == current->next->data->date.year && nouveauMaillon->data->date.month == current->next->data->date.month && nouveauMaillon->data->date.day > current->next->data->date.day))){
						nouveauMaillon->next = current->next;
						current->next = nouveauMaillon;
						break;
					}
				}
				else{
					current->next = nouveauMaillon;
					nouveauMaillon->next = NULL;
				}
				current = current->next;
			}
		}
		if (Personne){
			MaillonMariage* nouveauMaillon = malloc(sizeof(MaillonMariage));
			nouveauMaillon->data = nouveauMariage;
			if(Personne->wedding){
				MaillonMariage* current = Personne->wedding;
				while(current){
					if(current->next){
						if(((nouveauMaillon->data->date.year > current->next->data->date.year) || (nouveauMaillon->data->date.year == current->next->data->date.year && nouveauMaillon->data->date.month > current->next->data->date.month) || (nouveauMaillon->data->date.year == current->next->data->date.year && nouveauMaillon->data->date.month == current->next->data->date.month && nouveauMaillon->data->date.day > current->next->data->date.day))){
							nouveauMaillon->next = current->next;
							current->next = nouveauMaillon;
							break;
						}
					}
					else{
						if(((nouveauMaillon->data->date.year > current->data->date.year) || (nouveauMaillon->data->date.year == current->data->date.year && nouveauMaillon->data->date.month > current->data->date.month) || (nouveauMaillon->data->date.year == current->data->date.year && nouveauMaillon->data->date.month == current->data->date.month && nouveauMaillon->data->date.day > current->data->date.day))){

							current->next = nouveauMaillon;
							nouveauMaillon->next = NULL;
							break;
						}
						else{
							
							nouveauMaillon->next = current;
							Personne->wedding = nouveauMaillon;
							break;
						}
					}
					printf("\n%p %p",current,current->next);
					current = current->next;
				}
			}
			else{
				nouveauMaillon->next = NULL;
				Personne->wedding = nouveauMaillon;
			}
		}
		if(!Personne && !ptrTeteMariage){
			printf(TXT_COLOR_RED"\nATTENTION Erreur d'utilisation: Il faut faut au moins ptrTeteMariage ou un Maillon*\n"TXT_COLOR_END);
		}
	}
	else{
		printf(TXT_COLOR_RED"\nATTENTION Erreur d'utilisation: nouveauMaillon ne pointe sur aucune data\n"TXT_COLOR_END);
	}

	DEBUGEND;
}
void ajoutMariage(Maillon* mari, Maillon* femme, char* lieu, char* date, MaillonMariage* ptrTeteMariages){
	DEBUGSTART;

	if(mari || femme){
		Mariage* nouveauMariage = malloc(sizeof(Mariage));
		if(date){
			nouveauMariage->date.day = (strrchr(date, '/'))? (((date[0] - '0')*10) + ((date[1] - '0'))):1;
			nouveauMariage->date.month = (strrchr(date, '/'))? (((date[3] - '0')*10) + ((date[4] - '0'))):1;
			nouveauMariage->date.year = (strrchr(date, '/'))? (((date[6] - '0')*1000) + ((date[7] - '0')*100) + ((date[8] - '0')*10) + ((date[9] - '0'))):9000;
		}
		nouveauMariage->lieu.nom = lieu;
		nouveauMariage->lieu.X = 0;
		nouveauMariage->lieu.Y = 0;
		nouveauMariage->maries[0] = (mari)?mari:NULL;
		nouveauMariage->maries[1] = (mari)?femme:NULL;
		
		MaillonMariage* nouveauMaillonMariageCsv = malloc(sizeof(MaillonMariage));
		nouveauMaillonMariageCsv->data = nouveauMariage;
		if(mari){
			insertMaillonMariage(ptrTeteMariages, mari,nouveauMariage);
		}
		if(femme){
			insertMaillonMariage(NULL, femme,nouveauMariage);
		}

	}
	else{
		printf(TXT_COLOR_RED"\nATTENTION Erreur d'utilisation: Il faut faut au moins une personne\n"TXT_COLOR_END);
	}

	DEBUGEND;
}
void supprimeMariage(MaillonMariage* mariage, MaillonMariage* ptrTeteMariages){
	DEBUGSTART;
	
	if(mariage->data){
		MaillonMariage* sub;
		Mariage* data = mariage->data;
		MaillonMariage* current = ptrTeteMariages;

		if(countLcMariage(ptrTeteMariages) > 1){
			
			

			while(current){
				
				if(current->next){
					
					if(current->next->data == data){
						sub = current->next->next;
						free(current->next);
						current->next = sub;
						break;
					}
				}
				
				if(current->data == data){
					sub = current->next;
					free(current);
					current = NULL;
					ptrTeteMariages = sub;
					break;
				}
				
				
				current = current->next;

			}
		}
		else{
			free(ptrTeteMariages);
			ptrTeteMariages = NULL;
		}

		current = ptrTeteMariages;
		
		
		if(data->maries[0]){
			
			if(countLcMariage(data->maries[0]->wedding) > 1){
				
				current = data->maries[0]->wedding;
				
				while(current){

					if(current->next){
						if(current->next->data == data){
							DEBUGHERE;
							sub = current->next->next;
							free(current->next);
							current->next = sub;
							break;
						}
					}
					if(current->data == data){
						DEBUGHERE;
						sub = current->next;
						free(current);
						current = NULL;
						data->maries[0]->wedding = sub;
						break;
					}

					current = current->next;
				}
				
			
			}
			else{
				free(data->maries[0]->wedding);
				data->maries[0]->wedding = NULL;
			}
		}
		
		
		if(data->maries[1]){
			if(countLcMariage(data->maries[1]->wedding) > 1){
				
				current = data->maries[1]->wedding;

				while(current){

					if(current->next){
						if(current->next->data == data){
							DEBUGHERE;
							sub = current->next->next;
							free(current->next);
							current->next = sub;
							break;
						}
					}
					if(current->data == data){
						DEBUGHERE;
						sub = current->next;
						free(current);
						current = NULL;
						data->maries[1]->wedding = sub;
						break;
					}

					current = current->next;
				}
			}
			else{
				free(data->maries[1]->wedding);
				data->maries[1]->wedding = NULL;
			}
		}

		free(data);
		

	}
	else{
		printf(TXT_COLOR_RED"\nATTENTION Erreur d'utilisation: data Mariage pointe sur rien\n"TXT_COLOR_END);
	}

	DEBUGEND;
}
int countLcMariage(MaillonMariage* ptrTeteMariage){
	MaillonMariage* current = ptrTeteMariage;
	int compteur = 0;
	while(current){
		compteur++;
		current = current->next;
	}
	return compteur;
}
void modifDataMariage(int mode, Mariage* data, Maillon* newHusband, Maillon* newWife, int newDay, int newMonth, int newYear, char* newName){
	DEBUGSTART;
	if(data){
		if(newDay){
			data->date.day = newDay;
		}
		if(newMonth){
			data->date.month = newMonth;
		}
		if(newYear){
			data->date.year = newYear;
		}
		if(newName){
			data->lieu.nom = newName;
			//MODIFIER X  ET  Y EN FONCTION DU NOUVEAU LIEU
		}
		switch (mode)
		{
			case 1:
				if(data->maries[0]){
					MaillonMariage* current = data->maries[0]->wedding;
					MaillonMariage* sub;
					if(countLcMariage(data->maries[0]->wedding) > 1){
						while(current){
							if(current->next){
								if(current->next->data == data){
									sub = current->next->next;
									free(current->next);
									current->next = sub;
									break;
								}
							}
							if(current->data == data){
								sub = current->next;
								free(current);
								data->maries[0]->wedding = sub;
								break;
							}
							
							current = current->next;

						}
					}
					else{
						free(current);
						data->maries[0]->wedding = NULL;
					}
					
				}
				if(newHusband){
					data->maries[0] = newHusband;
					insertMaillonMariage(NULL,newHusband,data);
				}
				else{
					data->maries[0] = NULL;
				}
				break;
		
			case 2:
				if(data->maries[1]){
					MaillonMariage* current = data->maries[1]->wedding;
					MaillonMariage* sub;
					if(countLcMariage(data->maries[1]->wedding) > 1){
						DEBUGHERE;
						while(current){
							if(current->next){
								if(current->next->data == data){
									sub = current->next->next;
									free(current->next);
									current->next = sub;
									break;
								}
							}
							if(current->data == data){
								sub = current->next;
								free(current);
								data->maries[1]->wedding = sub;
								break;
							}
							
							current = current->next;

						}
					}
					else{
						free(current);
						data->maries[1]->wedding = NULL;
					}
					
				}
				if(newWife){
					data->maries[1] = newWife;
					insertMaillonMariage(NULL,newWife,data);
				}
				else{
					data->maries[1] = NULL;
				}
				break;
		}
	}
	else{
		printf(TXT_COLOR_RED"\nATTENTION PIRATAGE Erreur d'utilisation: La data que vous voulez modifier pointe sur rien\n"TXT_COLOR_END);
	}

	DEBUGEND;
}

void temporaire(Temporaire critere, Maillon* ptrTete, Maillon*** resultTab){
	DEBUGSTART;
	Maillon* current = ptrTete;
	int number_expected, number, number_finded;

	number_expected = 0;
	number = 0;
	number_finded = 0;

	if(critere.birth_data){
		if(strcmp(dateToString(critere.birth_data->date),"01/01/9000")){
			number_expected++;
		}
		if(critere.birth_data->lieu.nom){
			number_expected++;
		}
	}
	if(critere.death_data){
		if(strcmp(dateToString(critere.death_data->date),"01/01/9000")){
			number_expected++;
		}
		if(critere.death_data->lieu.nom){
			number_expected++;
		}
	}
	if(critere.gender){
		number_expected++;
	}

	if(critere.name){
		number_expected++;
	}

	if(critere.surname){
		number_expected++;
	}

	if(critere.numGen){
		number_expected++;
	}
	if(critere.dad){
		number_expected++;
	}
	if(critere.mom){
		number_expected++;
	}
	if(critere.sibling){
		for(int i = 0; critere.sibling[i]; i++){
			number_expected++;
		}
	}
	if(critere.wedding){
		for(int i = 0; critere.wedding[i]; i++){
			number_expected++;
		}
	}
	if(critere.child){
		for(int i = 0; critere.child[i]; i++){
			number_expected++;
		}
	}
	while(current){
		number = 0;

		if(critere.birth_data){
			if(compareDate(critere.birth_data->date,current->data->birth_data->date,"=")){
				number++;
			}
			if(critere.birth_data->lieu.nom){
				if(!strcmp(critere.birth_data->lieu.nom,current->data->birth_data->lieu.nom)){
					number++;
				}
			}
		}

		if(critere.death_data){
			if(compareDate(critere.death_data->date,current->data->death_data->date,"=")){
				number++;
			}
			if(critere.death_data->lieu.nom){
				if(!strcmp(critere.death_data->lieu.nom,current->data->death_data->lieu.nom)){
					number++;
				}
			}
		}

		if(critere.gender){
			if(critere.gender[0] == current->data->gender[0]){
				number++;
			}
		}

		if(critere.name){
			if(!strcmp(critere.name,current->data->name[0])){
				number++;
			}
		}

		if(critere.surname){
			if(!strcmp(critere.surname,current->data->surname)){
				number++;
			}
		}

		if(critere.numGen){
			if(*(critere.numGen) == current->data->numGen){
				number++;
			}
		}

		if(critere.dad && current->dad){
			if(critere.dad == current->dad){
				number++;
			}
		}

		if(critere.mom && current->mom){
			if(critere.mom == current->mom){
				number++;
			}
		}
		if(critere.sibling && current->sibling){
			for(int i = 0; critere.sibling[i]; i++){
				for(int j = 0; current->sibling[j]; j++){
					if(critere.sibling[i] == current->sibling[j]){
						number++;
						break;
					}
				}
			}
		}
		if(critere.wedding && current->wedding){
			for(int i = 0; critere.wedding[i]; i++){
				MaillonMariage* currentMariage = current->wedding;
				while(currentMariage){
					if(currentMariage->data == critere.wedding[i]){
						number++;
						break;
					}
					currentMariage = currentMariage->next;
				}
			}
		}
		if(critere.child && current->child){
			for(int i = 0; critere.child[i]; i++){
				for(int j = 0; current->child[j]; j++){
					if(critere.child[i] == current->child[j]){
						number++;
						break;
					}
				}
			}
		}

		if(number == number_expected){
			number_finded++;
		}
		current = current->next;
	}
	if(number_finded == 0){
		if(resultTab){
			memset((*resultTab), 0, sizeof resultTab);
			resultTab = NULL;
		}
		DEBUGEND;
		return;
	}
	(*resultTab) = calloc((number_finded + 1),sizeof(Maillon*));
	(*resultTab)[number_finded] = NULL;

	current = ptrTete;

	int compteur = 0;
	
	while(current){

		number = 0;
		
		if(critere.birth_data){
			if(compareDate(critere.birth_data->date,current->data->birth_data->date,"=")){
				number++;
			}
			if(critere.birth_data->lieu.nom){
				if(!strcmp(critere.birth_data->lieu.nom,current->data->birth_data->lieu.nom)){
					number++;
				}
			}
		}

		if(critere.death_data){
			if(compareDate(critere.death_data->date,current->data->death_data->date,"=")){
				number++;
			}
			if(critere.death_data->lieu.nom){
				if(!strcmp(critere.death_data->lieu.nom,current->data->death_data->lieu.nom)){
					number++;
				}
			}
		}

		if(critere.gender){
			if(critere.gender[0] == current->data->gender[0]){
				number++;
			}
		}

		if(critere.name){
			if(!strcmp(critere.name,current->data->name[0])){
				number++;
			}
		}

		if(critere.surname){
			if(!strcmp(critere.surname,current->data->surname)){
				number++;
			}
		}
		
		if(critere.numGen){
			if(*(critere.numGen) == current->data->numGen){
				number++;
			}
		}

		if(critere.dad && current->dad){
			if(critere.dad == current->dad){
				number++;
			}
		}
		if(critere.mom && current->mom){
			if(critere.mom == current->mom){
				number++;
			}
		}
		if(critere.sibling && current->sibling){
			for(int i = 0; critere.sibling[i]; i++){
				for(int j = 0; current->sibling[j]; j++){
					if(critere.sibling[i] == current->sibling[j]){
						number++;
						break;
					}
				}
			}
		}
		if(critere.wedding && current->wedding){
			for(int i = 0; critere.wedding[i]; i++){
				MaillonMariage* currentMariage = current->wedding;
				while(currentMariage){
					if(currentMariage->data == critere.wedding[i]){
						number++;
						break;
					}
					currentMariage = currentMariage->next;
				}
			}
		}
		if(critere.child && current->child){
			for(int i = 0; critere.child[i]; i++){
				for(int j = 0; current->child[j]; j++){
					if(critere.child[i] == current->child[j]){
						number++;
						break;
					}
				}
			}
		}

		if(number == number_expected){
			(*resultTab)[compteur] = current;
			compteur++;
		}

		
		current = current->next;
	}
	DEBUGEND;
}

void initLiensParente(Maillon* ptrTete, char*** tableauCsvIndividus){
	DEBUGSTART;

	foreach(char*** ptrline, tableauCsvIndividus){
		char** line = *ptrline;

		char* nomE = line[1];
		char* prenomE = line[2];
		int genre = (line[3][0] == 'M')?0:1;

		int numGen = line[0][0] - '0';
		numGen++;
		char* nomP = line[8];
		char* prenomP = line[9];
		char* nomM = line[10];
		char* prenomM = line[11];

		Temporaire critere;
		critere.birth_data = NULL;
		critere.wedding = NULL;
		critere.child = NULL;
		critere.dad = NULL;
		critere.mom = NULL;
		critere.sibling = NULL;
		critere.death_data = NULL;

		critere.gender = malloc(sizeof(bool));
		critere.gender[0] = HOMME;

		critere.name = prenomP;
		critere.surname = nomP;
		critere.numGen = malloc(sizeof(int));
		*(critere.numGen) = numGen;

		Maillon** resultTabP;
		temporaire(critere, ptrTete,&resultTabP);

		critere.gender[0] = genre;

		critere.name = prenomE;
		critere.surname = nomE;
		*(critere.numGen) = numGen -1;
		
		Maillon** resultTabE;
		temporaire(critere, ptrTete,&resultTabE);
		resultTabE[0]->dad = resultTabP[0];

		*(critere.numGen) = numGen;
		critere.gender[0] = FEMME;
		critere.name = prenomM;
		critere.surname = nomM;
		Maillon** resultTabM;
		temporaire(critere, ptrTete,&resultTabM);
		resultTabE[0]->mom = resultTabM[0];

		free(critere.gender);
		critere.gender = NULL;
		free(critere.numGen);
		critere.numGen = NULL;
	}
	Maillon* current = ptrTete;
	while(current){
		Temporaire critere;

		critere.numGen = malloc(sizeof(int));
		critere.gender = NULL;
		critere.birth_data = NULL;
		critere.child = NULL;
		critere.death_data = NULL;
		critere.mom = NULL;
		critere.name = NULL;
		critere.sibling = NULL;
		critere.wedding = NULL;
		critere.surname = NULL;
		*(critere.numGen) = current->data->numGen;
		critere.dad = current->dad;
		Maillon** resultTabP = NULL;
		if(critere.dad)
			temporaire(critere,ptrTete,&resultTabP);
		if(current->dad)
			current->dad->child = resultTabP;
		resultTabP = NULL;

		critere.mom = current->mom;
		Maillon** resultTabM = NULL;
		if(critere.mom)
			temporaire(critere,ptrTete,&resultTabM);
		if(current->mom)
			current->mom->child = resultTabM;
		resultTabM = NULL;
		free(critere.numGen);
		critere.numGen = NULL;


		current = current->next;
	}
	
	DEBUGEND;
}

bool isSibling(Maillon* m1, Maillon* m2){
	if(m1 && m2){
		if(m1->sibling){
			for (int i = 0; m1->sibling[i];i++){
				if(m1->sibling[i] == m2){
					return true;
				}
			}
			return false;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
}


char** fusionTab(char** T1, char** T2){
	char** result = malloc(sizeof(char*));
	int size = 1;
	result[0] = NULL;

	for(int i = 0; T1[i];i++){
		bool skip = false;
		skip1:
		
		if(!skip){
			//Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
			
			for(int j = 0; result[j]; j++){
				if(!strcmp(result[j],T1[i])){
					skip = true;
					goto skip1;
				}
			}

			//SI n'a pas été skip

			//Remplace la case mémoire NULL par lieu de la mort du mere
			result[size-1] = T1[i];

			//Augmente le tableau d'une case et l'initialise à NULL
			size++;
			result = realloc(result,size*sizeof(char*));
			result[size-1] = NULL;
		}
	}

	for(int i = 0; T2[i];i++){
		bool skip = false;
		skip2:
		
		if(!skip){
			//Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
			for(int j = 0; result[j]; j++){
				if(!strcmp(result[j],T2[i])){
					skip = true;
					goto skip2;
				}
			}

			//SI n'a pas été skip

			//Remplace la case mémoire NULL par lieu de la mort du mere
			result[size-1] = T2[i];

			//Augmente le tableau d'une case et l'initialise à NULL
			size++;
			result = realloc(result,size*sizeof(char*));
			result[size-1] = NULL;
		}
	}
	return result;
}

Maillon** fusionTabMaillon(Maillon** T1, Maillon** T2){
	Maillon** result = malloc(sizeof(Maillon*));
	int size = 1;
	result[0] = NULL;

	for(int i = 0; T1[i];i++){
		bool skip = false;
		skip1:
		
		if(!skip){
			//Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
			
			for(int j = 0; result[j]; j++){
				if(result[j] ==T1[i]){
					skip = true;
					goto skip1;
				}
			}

			//SI n'a pas été skip

			//Remplace la case mémoire NULL par lieu de la mort du mere
			result[size-1] = T1[i];

			//Augmente le tableau d'une case et l'initialise à NULL
			size++;
			result = realloc(result,size*sizeof(Maillon*));
			result[size-1] = NULL;
		}
	}

	for(int i = 0; T2[i];i++){
		bool skip = false;
		skip2:
		
		if(!skip){
			//Vérifie que le lieux n'est pas déjà dans le tableau, si oui, goto skip
			for(int j = 0; result[j]; j++){
				if(result[j] == T2[i]){
					skip = true;
					goto skip2;
				}
			}

			//SI n'a pas été skip

			//Remplace la case mémoire NULL par lieu de la mort du mere
			result[size-1] = T2[i];

			//Augmente le tableau d'une case et l'initialise à NULL
			size++;
			result = realloc(result,size*sizeof(Maillon*));
			result[size-1] = NULL;
		}
	}
	return result;
}

void updateLiensFraterie(Maillon* ptrTete){
	Maillon* current = ptrTete;
	while(current){
		Temporaire critere;

		critere.numGen = malloc(sizeof(int));
		critere.gender = NULL;
		critere.birth_data = NULL;
		critere.child = NULL;
		critere.death_data = NULL;
		critere.mom = NULL;
		critere.name = NULL;
		critere.sibling = NULL;
		critere.wedding = NULL;
		critere.surname = NULL;
		*(critere.numGen) = current->data->numGen;
		critere.dad = current->dad;
		Maillon** resultTabP = NULL;
		if(critere.dad)
			temporaire(critere,ptrTete,&resultTabP);
		if(current->dad)
			current->dad->child = resultTabP;
			
		resultTabP = NULL;
		critere.dad = NULL;
		critere.mom = current->mom;
		Maillon** resultTabM = NULL;
		if(critere.mom)
			temporaire(critere,ptrTete,&resultTabM);
		if(current->mom)
			current->mom->child = resultTabM;
		resultTabM = NULL;

		free(critere.numGen);
		critere.numGen = NULL;

		if(current->dad && current->dad->child){
			Maillon** tabFraterie = malloc(sizeof(Maillon*));
			tabFraterie[0] = NULL;
			int cpt = 0;
			for(int i =0; current->dad->child[i];i++){
				if(current->dad->child[i] != current){
					tabFraterie[cpt] = current->dad->child[i];
					cpt++;
					tabFraterie = realloc(tabFraterie,sizeof(Maillon*)*cpt);
					tabFraterie[cpt] = NULL;
				}
			}
			current->sibling = tabFraterie;
		}

		if(current->mom && current->mom->child){
			Maillon** tabFraterie = malloc(sizeof(Maillon*));
			tabFraterie[0] = NULL;
			int cpt = 0;
			for(int i =0; current->mom->child[i];i++){
				if(current->mom->child[i] != current){
					tabFraterie[cpt] = current->mom->child[i];
					cpt++;
					tabFraterie = realloc(tabFraterie,sizeof(Maillon*)* cpt);
					tabFraterie[cpt] = NULL;
				}
			}
			current->sibling = tabFraterie;
		}

		current = current->next;
	}
}

void updateLiensParente(Maillon* ptrTete){
	Maillon* current = ptrTete;
	while(current){
		Temporaire critere;

		critere.numGen = malloc(sizeof(int));
		critere.gender = NULL;
		critere.birth_data = NULL;
		critere.child = NULL;
		critere.death_data = NULL;
		critere.mom = NULL;
		critere.name = NULL;
		critere.sibling = NULL;
		critere.wedding = NULL;
		critere.surname = NULL;
		*(critere.numGen) = current->data->numGen;
		critere.dad = current->dad;
		Maillon** resultTabP = NULL;
		if(critere.dad)
			temporaire(critere,ptrTete,&resultTabP);
		if(current->dad)
			current->dad->child = resultTabP;
			
		resultTabP = NULL;
		critere.dad = NULL;
		critere.mom = current->mom;
		Maillon** resultTabM = NULL;
		if(critere.mom)
			temporaire(critere,ptrTete,&resultTabM);
		if(current->mom)
			current->mom->child = resultTabM;
		resultTabM = NULL;

		free(critere.numGen);
		critere.numGen = NULL;

		if(current->dad && current->dad->child && current->dad->wedding){
			for(MaillonMariage* currentMariage = current->dad->wedding; currentMariage; currentMariage = currentMariage->next){
				if(currentMariage->data->maries[1] && currentMariage->data->maries[1]->child){
					current->dad->child = fusionTabMaillon(current->dad->child,currentMariage->data->maries[1]->child);
				}
			}
		}

		if(current->mom && current->mom->child && current->mom->wedding){
			for(MaillonMariage* currentMariage = current->mom->wedding; currentMariage; currentMariage = currentMariage->next){
				if(currentMariage->data->maries[0] && currentMariage->data->maries[0]->child){
					current->mom->child = fusionTabMaillon(current->mom->child,currentMariage->data->maries[0]->child);
				}
			}
		}

		if(current->dad && current->dad->child && current->dad->wedding){
			for(MaillonMariage* currentMariage = current->dad->wedding; currentMariage; currentMariage = currentMariage->next){
				if(currentMariage->data->maries[1] && currentMariage->data->maries[1]->child){
					current->dad->child = fusionTabMaillon(current->dad->child,currentMariage->data->maries[1]->child);
				}
			}
		}

		if(current->mom && current->mom->child && current->mom->wedding){
			for(MaillonMariage* currentMariage = current->mom->wedding; currentMariage; currentMariage = currentMariage->next){
				if(currentMariage->data->maries[0] && currentMariage->data->maries[0]->child){
					current->mom->child = fusionTabMaillon(current->mom->child,currentMariage->data->maries[0]->child);
				}
			}
		}

		current = current->next;
	}
}


void initLiensFraterie(Maillon* ptrTete){
	DEBUGSTART;

	Maillon* current = ptrTete;
	//bool done = false;
	int cursor;

	while(current){
		
		if(current->dad && !current->mom){
			Critere critere = creeCritere(current->data->numGen,NULL,NULL,NULL,NULL,NULL,current->dad,NULL,"=");
			Maillon** tmpP = superRechercheMaillon(critere,ptrTete);

			Maillon** resultsP = malloc((count(tmpP)-1)*sizeof(Maillon*));

			for(int i = 0; tmpP[i]; i++){
				if(tmpP[i] == current){
					cursor = i;
					break;
				}
			}
			for(int i  = 0; tmpP[i]; i++){
				if(i < cursor){
					resultsP[i] = tmpP[i];
				}
				else if(i > cursor){
					resultsP[i-1] = tmpP[i];
				}
			}
			resultsP[count(tmpP)-2] = NULL;
			free(tmpP);

			if(count(resultsP) != 1){
				current->sibling = resultsP;
			}
		}
		else if(!current->dad && current->mom){
			Critere critere = creeCritere(current->data->numGen,NULL,NULL,NULL,NULL,NULL,NULL,current->mom,"=");
			Maillon** tmpM = superRechercheMaillon(critere,ptrTete);

			Maillon** resultsM = malloc((count(tmpM)-1)*sizeof(Maillon*));

			for(int i = 0; tmpM[i]; i++){
				if(tmpM[i] == current){
					cursor = i;
					break;
				}
			}
			for(int i  = 0; tmpM[i]; i++){
				if(i < cursor){
					resultsM[i] = tmpM[i];
				}
				else if(i > cursor){
					resultsM[i-1] = tmpM[i];
				}
			}
			resultsM[count(tmpM)-2] = NULL;
			free(tmpM);

			if(count(resultsM) != 1){
				current->sibling = resultsM;
			}
		}
		else if(current->dad && current->mom){
			Critere critereM = creeCritere(current->data->numGen,NULL,NULL,NULL,NULL,NULL,NULL,current->mom,"=");
			Maillon** tmpM = superRechercheMaillon(critereM,ptrTete);

			Maillon** resultsM = malloc((count(tmpM)-1)*sizeof(Maillon*));

			for(int i = 0; tmpM[i]; i++){
				if(tmpM[i] == current){
					cursor = i;
					break;
				}
			}
			for(int i  = 0; tmpM[i]; i++){
				if(i < cursor){
					resultsM[i] = tmpM[i];
				}
				else if(i > cursor){
					resultsM[i-1] = tmpM[i];
				}
			}
			resultsM[count(tmpM)-2] = NULL;
			free(tmpM);

			Critere critereP = creeCritere(current->data->numGen,NULL,NULL,NULL,NULL,NULL,current->dad,NULL,"=");
			Maillon** tmpP = superRechercheMaillon(critereP,ptrTete);

			Maillon** resultsP = malloc((count(tmpP)-1)*sizeof(Maillon*));

			for(int i = 0; tmpP[i]; i++){
				if(tmpP[i] == current){
					cursor = i;
					break;
				}
			}
			for(int i  = 0; tmpP[i]; i++){
				if(i < cursor){
					resultsP[i] = tmpP[i];
				}
				else if(i > cursor){
					resultsP[i-1] = tmpP[i];
				}
			}

			resultsP[count(tmpP)-2] = NULL;
			free(tmpP);

			if(count(resultsP) != 1 && count(resultsM) == 1){
				current->sibling = resultsP;
			}
			else if(count(resultsP) == 1 && count(resultsM) != 1){
				current->sibling = resultsM;
			}
			else if(count(resultsP) == 1 && count(resultsM) == 1){
				current->sibling = NULL;
			}
			else if(count(resultsP) != 1 && count(resultsM) != 1){
				int size = 0;
				bool present = false;
				int cpt = 0;
				for(int i = 0; i < count(resultsP); i++){
					for(int j = 0; j < count(resultsM); j++){
						if(resultsP[i] == resultsM[j]){
							size++;
							break;
						}
					}
				}
				size = count(resultsP)+count(resultsM)-size;

				Maillon** results = malloc(size*sizeof(Maillon*));
				for(int i = 0; i < count(resultsP); i++){
					results[i] = resultsP[i];
				}
				cpt = count(resultsP)-1;
				for(int i = 0; resultsM[i]; i++){
					present = false;
					for(int j = 0; resultsP[j]; j++){
						if(resultsM[i] == resultsP[j]){
							present = true;
							break;
						}
					}
					if(present == false){
						results[i+cpt] = resultsM[i];
						cpt++;
					}
				}
				
				results[size-1] = NULL;
				free(resultsP);
				free(resultsM);
				current->sibling = results;
			}
		}

		current = current->next;
	}

	DEBUGEND;
}
void ajoutFrere(Maillon* personne,Maillon* nouveauPere,Maillon* nouvelleMere,Maillon* ptrTete){

	
	if(nouveauPere)
		personne->dad = nouveauPere;
	if(nouvelleMere)
		personne->mom = nouvelleMere;
	
	updateLiensParente(ptrTete);
	initLiensFraterie(ptrTete);
}
Date operationDate(Date d1, Date d2, char operator){
	Date result;
	result.year = 9000;
	result.day = 1;
	result.month = 1;

	switch (operator)
	{
	case '+':
		result.year = d1.year + d2.year;
		result.month = d1.month + d2.month;
		result.day = d1.day + d2.day;

		while(result.day > 31){
			result.month ++;
			result.day -= 12;
		}

		while(result.month > 12){
			result.year ++;
			result.month -= 12;
		}

		return result;
	
	case '-':
		result.year = d1.year - d2.year;
		result.month = d1.month - d2.month;
		result.day = d1.day - d2.day;

		while(result.day < 1){
			result.month--;
			result.day += 31;
		}

		while(result.month < 1){
			result.year --;
			result.month += 12;
		}
	
	}
	return result;
}

bool isInPeriod(Date d1, Date d2,Date period){
	period.year /= 2;
	period.month /= 2;
	period.day /=2;
	if((compareDate(d2,operationDate(d1,period,'-'),">") || compareDate(d1,d2,"=")) && ((compareDate(d1,d2,"=") ||compareDate(d2,operationDate(d1,period,'+'),"<")))){
		return true;
	}
	else{
		return false;
	}
}
void supprimeFrere(int mode, Maillon* personne,Maillon* ptrTete){
	switch(mode){
		case 1:
			personne->dad = NULL;
			break;
		case 2:
			personne->mom = NULL;
			break;
		case 3:
			personne->dad = NULL;
			personne->mom = NULL;
			break;
	}
	updateLiensParente(ptrTete);
	initLiensFraterie(ptrTete);
}

/**
 * @brief Recherche des coordonée d un lieu sur internet grace a geopy
 * 
 * @param l 
 */
void modifCoordLieu(Lieu *l){
	Point_2f p = getCoordinatesFromPlaceName(l->nom);
	l->X = p.x;
	l->Y = p.y;
}



