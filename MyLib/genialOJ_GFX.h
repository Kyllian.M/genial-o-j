#define DARK false
#define LIGHT true
#define COULEUR_INDIVIDU 80,200,255
#define COULEUR_INDIVIDU_DARK 60,60,60
//Puissance de 10 negative a laquelle on peut zoomer au maximum (vue fractale)
#define MAX_ZOOM_10POW 8
#define nameof(x) #x

typedef struct SideMenu{
    bouton bt_pere;
    bouton bt_mere;
    DropMenu dm_enfants;
    DropMenu dm_fraterie;
    bouton bt_conjoint;
    Polygone* poly_bt;
    Maillon* M;
	Image* ico_calendrier;
    Image* ico_place;
    Image* img_genre[2];
	Maillon* last_M;
	Maillon* last_param;
	Mover *move;
	int x;
	int y;
    int width;
    int height;
	bool reload;
}SideMenu;

typedef struct SearchMenu SearchMenu;
struct SearchMenu{
	GoogleSearchIMG* search;
	textField txt;
	textField txt_nb;
	bouton b;
	Individu* selec;
	ImageCropper* crop;
	bouton done;
	bool searchMenu;
	bouton quitsearch;
	bool manualsearch;

    void (*update) (SearchMenu* self);
    void (*show) (SearchMenu* self);
};

typedef struct CSV{
	char* filename;
	char* fullLocation;
}CSV;

typedef struct Settings{
	bool light_mode;
	bool show_NullIsland;
	char* mapLocation;
	bouton button_settings[8];
	bouton button_fichierI[9];
	bouton button_fichierM[9];
	int ligneselectI;
	int ligneselectM;
	RGB couleurbouton[4];
	Polygone polysetting;
	Polygone polyfichier;
	Point pos_settings;
	Mover mov_poly_setting;
	Mover mov_poly_fichier;
	DropMenu menu_mode[2];
	ColorPicker picker;
	bool show_picker;
	int indexcolorbtn;
	exploFichier EX;
	short ajout_fichier;
	char **  fichierIndv;
	char ** fichierMar;
	Table Id;
	Table Mar;

	
}Settings;

typedef struct Edition{
bouton close;
textField generation;
textField prenom;
textField nom;
textField date_naissance;
textField lieu_naissance;
textField date_mort;
textField lieu_mort;
bouton lien_pere;
bouton lien_mere;
bouton lien_conjoint;
DropMenu enfants;
textField ajout_enfants;
DropMenu fraterie;
textField ajout_freres;
bouton IA[11];
bool show_edit;
bouton H;	
bouton F;
bouton ajoutE;
bouton ajoutF;
bouton supprimeE;
bouton supprimeF;
bouton sauv;
DropMenu Indice[10];
bool afficheIndice[10];
Maillon * choisis;
}Edition;

typedef struct bigScreenshotData{
	int done;

	int oldLF;
	int oldHF;
	int oldcam_x;
	int oldcam_y;
	int oldx;
	int oldy;
	float oldzoom;

	int newLF;
	int newHF;
	int newcam_x;
	int newcam_y;

	char nomFichier[100];
}bigScreenshotData;

typedef struct RechercheGFX{
	DropMenu genre;
	textField nom;
	textField prenom;
	textField date_naissance;
	textField date_mort;
	textField numgen;
	textField lieu_naissance;
	textField lieu_mort;
	bouton recherche;
	Maillon **result;
	bool isShown;
	bouton btn_go;  
	bool showResult;
	int yscroll;

	void (*show) (void);
	void (*update) (void);
}RechercheGFX;


extern bigScreenshotData bigScreenshot;

typedef enum State {main_menu,map_view,tree_view,excel_view,fractals_view,network_view} State;
extern State state;
extern bool moved;
extern bool grab;
extern int cam_x;
extern int cam_y;
extern bool showmenu;
extern ClickMenu menu;
extern float zoom;
extern Maillon* ptrTete;
extern int xmenu,ymenu;
extern Maillon* toshow;
extern SearchMenu search_menu;
extern RechercheGFX RechGFX;
extern Maillon* start;
extern Counter count;


extern bouton menu_button[6];
extern Mover mov_bt1;
extern Mover mov_bt2;
extern Mover mov_bt3;
extern Mover mov_bt4;
extern Mover mov_bt5;
extern Mover mov_bt6;
extern Settings Menu_Settings;
extern Edition edit;
extern char * tmpc;
extern bool show_setting;


extern int numerofichierind;
extern int numerofichiermari;

void takeBigScreenshot (int x,int y,int width,int height,char* nomFichier);
void updateBigScreenshot ();
void updateViewBigScreenshot ();

SideMenu new_SideMenu ();
void updateSideMenu (SideMenu* sm,Maillon* M);
void afficheSideMenu (SideMenu* sm);
bool optionHoveredSideMenu (SideMenu* sm);

void initMaillonTiles (Maillon* ptrTete);
void updatePinListener (Maillon* ptrTete,Image* map);
void plan2DGrabbable ();
IndividuTile* new_IndividuTile (int x,int y,Polygone* poly,Polygone* shadow);
void afficheIndividuTile (Maillon* M);
void setAppropriateCursor ();
void resizePhotoLC (Maillon* ptrtete,int size);
ClickMenu new_ClickMenu1 ();
void changeZoom(float toadd,Image* map);
float getLongitude (int x,Image* map);
float getLatitude (int y,Image* map);
//void afficheMenuLateral (Maillon *I);
void afficheIndividuMap (Individu* I,Image* map);
void afficheLiensMariage (Maillon* ptrTete);
void afficheLiensParents (Maillon* ptrTete);
SearchMenu new_SearchMenu ();
void afficheMenuSetting();
void initialiseMenuSetting();
void actionOfMenuSettings ();
void actionOfCloseSettings();
void actionOfBack ();
void actionOfCouleurIndividu();
void actionOfCouleurParent();
void actionOfCouleurMariage();
void actionOfCouleurFraterie();
void actionOfPicker();
void actionOfTheme();
void actionOfIslandNull();
void actionOfExploFichier ();
void actionOfAjoutCarte();
void actionOfAjoutFichierMar();
void actionOfAjoutFichierIndv();
void updateSetting();

void initMenuEdition();
void updateMenuEdition();
void afficheMenuEdition();
void AjouteInfo(Maillon * M);

char* initChaine(char *c);
/**
 * @brief Fonction récursive qui cherche et stocke les personnes ASCENDANTES ayant un lien de sang avec l’individu en argument avec un degré au maximum égal à l’entier n en paramètre.
 * 
 * @param prs 
 * @param n 
 * @param repere 
 * @return Maillon** 
 */
Maillon** initLcArbreAsc(Maillon* prs, int n, int const repere);

/**
 * @brief Fonction récursive qui cherche et stocke les personnes DESCENDANTES ayant un lien de sang avec l’individu en argument avec un degré au maximum égal à l’entier n en paramètre.
 * 
 * @param prs 
 * @param n 
 * @param repere 
 * @return Maillon** 
 */
Maillon** initLcArbreDesc(Maillon* prs, int n, int const repere);
RechercheGFX new_RechercheGFX(void);

bool fichierExistant(char * nomfichier,char **Tfichier);
void actualisePositionTiles (Maillon* ptrTete);
void afficheLiensFraterie (Maillon* ptrTete);
void traceFleche (int x1,int y1,int x2,int y2,int arrow_width,int arrow_length);
void doubleFleche (int x1,int y1,int x2,int y2,int arrow_width,int arrow_length);

void afficheMaillonFractale (Maillon* m,float x,float y,float size,int recursfact);
void traceCercleFract (float xc,float yc,float r,int repet,Maillon* m);
void LeftClickUpFractals ();

void afficheMaillonReseau (Maillon* m,int size);
void traceCercle (int x,int y,int r);
int cercleInferieur (int x,int r);
int cercleSuperieur (int x,int r);
Point generePointAleatoireCercle (int r);
bool showMaillon (Maillon* m);
bool placeLibre (Point pt,Maillon* ptrTete,int rayon);
Point generePointLibre (int x,int y, int rayon);
void networkTree (Maillon* start,int n,int max,int x,int y);
void startNetworkTree (Maillon* ptrTete,Maillon* start,int n);
bool isInList (Maillon* m,Maillon** list,bool show_all_lien);
void addToList (Maillon* m,Maillon ***list);
void resetNetworkTree (Maillon* start,int n);
void afficheLienNetworkTree (Maillon* ptrTete,bool show_all_lien);
void afficheIndividusNetworkTree (Maillon* ptrTete,Maillon* start);
void afficheNetworkTree (Maillon* ptrTete,Maillon* start,bool show_all_lien);