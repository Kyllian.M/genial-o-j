#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "fonction.h"
#include <time.h>
#include "math.h"
#include <assert.h>
#include "MyLib/KyllianToolsLib.h"



int main(int argc,char* argv[]){
	// ------------------------------------------------------- //
	//Transformation des arguments sous la forme {"SELECT","ALL","FROM"...} en "SELECT ALL FROM"
	int size = strlen(argv[0])+1;
	char* command = malloc(sizeof(char)*size);
	strcpy(command,argv[0]);
	for(int i=1;i<argc;i++){
		size += 1 + strlen(argv[i]);
		command = realloc(command,sizeof(char)*size);
		strcat(command," ");
		strcat(command,argv[i]);
	}
	//Affichage de la commande a traiter
	printf(TXT_MAGENTA);
	printf("command executed : %s\n",command);
	printf(TXT_END_COLOR);
	//EXECUTION DE LA COMMANDE 
	if(str_same(argv[1],"SELECT")){
		Table T = databaseQuery(command);
		afficheBDD(T);
		libereTable(T);
	}
	else{
		databaseQuery(command);
	}
	free(command);
}