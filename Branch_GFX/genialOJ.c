#include "gfxlib++.h"
#include "genialOJ.h"
#include "../MyLib/consoletools.h"
#include "../MyLib/KyllianToolsLib.h"
#include "../MyLib/CSVlib.h"
#include "../MyLib/Timer.h"
#include "../MyLib/images.h"
#include "../MyLib/sdlglutils.h"
#include "../MyLib/listechainee.h"
#include "../MyLib/exploFichier.h"
#include "../MyLib/OurSQL.h"
#include "../MyLib/imageCropper.h"
#include "../MyLib/googleimagelib.h"
#include "../MyLib/googlemapslib.h"
#include "../MyLib/genialOJ_GFX.h"
#include <unistd.h>
#include "genialOJ.h"

///////////////////////////////////////////////////////// REMARQUES //////////////////////////////////////////////////////////////////////////
/*
Ce Fichier est le "main" du projet Genial-oj réalisé par :
Kyllian 	MARIE
Christophe 	VOLTO
Valentin 	VOLPELIERRE
Gaian 		DOURVILLE

Les Librairies requises sont :

	C : 
freeglut-3
freeglut-3-dev
libsdl-image1.2-dev
libsdl1.2-dev

	Python :
python-pip
pip install google_images_download
pip install geopy

Voir la doc du programme sur ce lien :
https://docs.google.com/document/d/1roRvzHVFLpzHNo6fwbtaroHGiDKCyPhfJSE2xcE_S-Q/edit#

Voir le diaporama du projet :
https://docs.google.com/presentation/d/11mLI3Hi6PgPoEuXdfwHWpzWs1w-T1zyrIJj_W7Ir6mo/edit?usp=sharing

Toutes les fonctions et globales sont commentées au standard
/ *
* @desc : XXXXXXXX
* @param a : XXXXX
* @return : XXXXXX
* /
Les commentaires d'une fonctions s'afficheront donc lors du hover sur tout IDE qui se respecte

*/
///////////////////////////////////////////////////////// VARIABLES GLOBALES //////////////////////////////////////////////////////////////////////////

//taille par defaut des images Individus
int imgsize = 160;
//Logo du projet
Image* logo;
//bouton pour retourner au menu
bouton button_back;

//affichage ou non des liens de parenté dans l'arbre genealogique
bool show_parent = true;
//tableur de la liste chainee
Tableur tableur;
//menu lateral des individus
SideMenu sm;

//Carte du monde
Image* map;
//bouton sauvegarde
bouton save;

//nom du fichier CSV individu chargé
char nameCSV[100];
//nom du fichier CSV mariage chargé
char nameMari[100];

//chemin absolu du fichier individu chargé
char* fichierindivload;
//mariage selectionné
MaillonMariage* selected_mariage = NULL;
//tete de la liste chainée mariage
MaillonMariage* ptrTeteMariagesCSV;

//Affichage complexe de tous les liens sur la vue reseau ?
bool show_all_lien = false;


///////////////////////////////////////////////////////// FONCTIONS CALLBACK //////////////////////////////////////////////////////////////////////////

//callback du bouton "vue arbre"
void actionOfMenuTree (){
	grab=false;
	state = tree_view;
	initMaillonTiles(ptrTete);
	changeZoom(1 - zoom,map);
}

//callback du bouton "vue carte"
void actionOfMenuMap (){
	grab=false;
	state = map_view;
	if(HF != map->height)
		changeZoom(HF*1./map->height - zoom,map);
}

//callback du bouton "vue tableur"
void actionOfExcelView (){
	grab=false;
	state = excel_view;
	cam_y = 700;
	tableur = new_Tableur("../CSV/Liste_Individus.csv");
}

//callback du bouton "save"
void actionOfBtnSave (){
	sauverCsvIndividu(str_format("../CSV/%s",nameCSV),ptrTete);
	sauverCsvMariage("../CSV/Liste_Mariages.csv",ptrTeteMariagesCSV);
}

//callback du bouton "vue fractale"
void actionOfMenuFractale (){
	grab=false;
	state = fractals_view;
	initMaillonTiles(ptrTete);
	changeZoom(1 - zoom,map);
}

//callback du bouton "vue reseau"
void actionOfMenuReseau (){
	grab=false;
	state = network_view;
	initMaillonTiles(ptrTete);
	changeZoom(1 - zoom,map);
	startNetworkTree(ptrTete,start,1);
}

//callback du counter de lien
void actionOfCounter (){
	resetNetworkTree(start,count.value);
}

///////////////////////////////////////////////////////// FONCTIONS GFXLIB++ //////////////////////////////////////////////////////////////////////////

//fonction de pré-lancement GFXLib++
void prelaunch (){
	strcpy(processname,"GFXLib++");
	processwidth = 1200;
	processheight = 800;

	setDebugMode(false); 		//Affichage des messages de debug ?
	setIgnoreError(false);		//Ignorer les erreurs ?
}

//fonction d'initialisation GFXLib++
void initialize (){
	
	//Initialisation de la BDD Our-sql
	databaseQuery("./exec HELP");
	Menu_Settings.Id=databaseQuery("./exec SELECT ALL FROM ../Ressources/Bdd/indivcsv.osql");
	afficheTable(Menu_Settings.Id);
	strcpy(nameCSV,"");
	for(int i=0;i<Menu_Settings.Id.nbligne;i++){		//Remplissage des champs pour les noms de fichier a load
		if(!strcmp(Menu_Settings.Id.T[i][1],"1")){
			strcat(nameCSV,Menu_Settings.Id.T[i][0]);
			Menu_Settings.ligneselectI=i;
		}	
	}
	Menu_Settings.Mar=databaseQuery("./exec SELECT ALL FROM ../Ressources/Bdd/mariagecsv.osql");	
	strcpy(nameMari,"");
	for(int i=0;i<Menu_Settings.Mar.nbligne;i++){		//Remplissage des champs pour les noms de fichier a load
		if(!strcmp(Menu_Settings.Mar.T[i][1],"1")){
			strcat(nameMari,Menu_Settings.Mar.T[i][0]);
			Menu_Settings.ligneselectM=i;
		}
	}

	//Initialisation des listes chainées
	char** CSVcategories;
	char*** CSVtextdatabyword;
	chargeCSV(str_format("../CSV/%s",nameCSV),&CSVcategories,&CSVtextdatabyword);	//Recuperation des données CSV
	ptrTete = initLC(CSVtextdatabyword);											//Initialisation de la Liste Chainée Individus
	char*** tabMariages;																				
	chargeCsvMariages(str_format("../CSV/%s",nameMari), &tabMariages);				//Recuperation des données CSV
	ptrTeteMariagesCSV =initMariageLC(tabMariages, ptrTete);						//Initialisation de la liste chainee Mariage et edition de liens mariage-individus
	initLiensParente(ptrTete,CSVtextdatabyword);									//Edition des liens de parenté entre les individus
	initLiensFraterie(ptrTete);														//Edition des liens de fraternité entre les individus
	printLC(ptrTete);																//Affichage console de la liste chainee

	//Initialisation des variables pour l'arbre
	initMaillonTiles (ptrTete);					//Initialisation des position des tiles individus
	search_menu = new_SearchMenu();

	//Initialisation des variables pour la Map
	for(Maillon* tmp = ptrTete; tmp != NULL ; tmp = tmp->next){	// CODE POUR VARIER LES LOCALISTAIONS DES INDIVIDUS aléatoirement
		tmp->data->birth_data->lieu.X += valeurAleatoire() - 0.5;
		tmp->data->birth_data->lieu.Y += (valeurAleatoire() - 0.5)*0.3;
	}
	map = new_Image("../Ressources/Map/map8Knew.png",true);

	//Initialisation des variables pour le Menu
	menu = new_ClickMenu1();
	logo = new_Image("../Ressources/logo.bmp",false);
	RGB couleurBoutons = (RGB){127,127,127};
	menu_button[0] = initBouton(200,90,"Vue Arbre",couleurBoutons);
	menu_button[1] = initBouton(600,90,"Vue Carte",couleurBoutons);
	menu_button[2] = initBouton(600,90,"Vue Tableur",couleurBoutons);
	menu_button[3] = initBouton(600,90,"Parametres",couleurBoutons);
	menu_button[4] = initBouton(200,90,"Vue Fractale",couleurBoutons);
	menu_button[5] = initBouton(200,90,"Vue Reseau",couleurBoutons);
	menu_button[0].addEventListener(&(menu_button[0]),"click",actionOfMenuTree);
	menu_button[1].addEventListener(&(menu_button[1]),"click",actionOfMenuMap);
	menu_button[2].addEventListener(&(menu_button[2]),"click",actionOfExcelView);
	menu_button[3].addEventListener(&(menu_button[3]),"click",actionOfMenuSettings);
	menu_button[4].addEventListener(&(menu_button[4]),"click",actionOfMenuFractale);
	menu_button[5].addEventListener(&(menu_button[5]),"click",actionOfMenuReseau);
	roundBorders(&(menu_button[0]),0.18);
	roundBorders(&(menu_button[1]),0.18);
	roundBorders(&(menu_button[2]),0.18);
	roundBorders(&(menu_button[3]),0.18);
	roundBorders(&(menu_button[4]),0.18);
	roundBorders(&(menu_button[5]),0.18);
	button_back = initBoutonFromImg("../Ressources/back.bmp",(RGB){0,255,0});
	button_back.addEventListener(&button_back,"click",actionOfBack);
	initialiseMenuSetting();
	initMenuEdition();

	sm = new_SideMenu();
	save = initBouton(90,20,"Sauvegarder",(RGB){0,50,200});
	save.addEventListener(&save,"click",actionOfBtnSave);
	roundBorders(&save,0.8);

	//Initialisation des variables pour les fractales
	RechGFX = new_RechercheGFX();
	start = ptrTete;

	//Initialisation des variables pour la vue reseau
	startNetworkTree(ptrTete,start,1);
	count = new_Counter(1,1,30);
	count.eventListener = actionOfCounter;

	println("Fini init");
}

//fonction de rafraichissement GFXLib++
void update (){
	switch(state){
		case main_menu :
			if(Menu_Settings.mov_poly_setting.activated == false && show_setting == false){
				Menu_Settings.pos_settings.x = LF;
			}
			if(show_setting==false){
				executeActionBouton(menu_button[0]);
				executeActionBouton(menu_button[1]);
				executeActionBouton(menu_button[2]);
				executeActionBouton(menu_button[3]);
				executeActionBouton(menu_button[4]);
				executeActionBouton(menu_button[5]);
			}
			
			updateMover(&mov_bt1);
			updateMover(&mov_bt2);
			updateMover(&mov_bt3);
			updateMover(&mov_bt4);
			updateMover(&mov_bt5);
			updateMover(&mov_bt6);
			updateSetting();
		break;

		case map_view :
			executeActionBouton(button_back);
			if(!search_menu.searchMenu)
			plan2DGrabbable();
			search_menu.update(&search_menu);
			cam_y = cam_y > 0 ? 0 : cam_y;
			cam_y = cam_y < -map->static_height*1.+HF ?  -map->static_height*1.+HF : cam_y;
			updateSideMenu(&sm,toshow);
		break;

		case tree_view :
			executeActionBouton(button_back);
			search_menu.update(&search_menu);
			updateMenuEdition();
			RechGFX.update();
			executeActionBouton(save);
			ifkeyup('p'){
				show_parent = !show_parent;
			}
			if(!search_menu.searchMenu)
				plan2DGrabbable();
			
			updateSideMenu(&sm,toshow);
			ifkeyup('&'){
				int xmin = ptrTete->tile_info->x,xmax = 0,ymin = ptrTete->tile_info->y,ymax = 0;
				for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
					int x = tmp->tile_info->x;
					int y = tmp->tile_info->y;
					xmin = min(x,xmin);
					xmax = max(x,xmax);
					ymin = min(y,ymin);
					ymax = max(y,ymax);
				}
				println("xmin %d  ymin %d  xmax %d  ymax %d",xmin,-ymin,xmax-xmin,-ymin);
				takeBigScreenshot (xmin,ymax-ymin,xmax-xmin+600,ymax-ymin+600,"test.bmp");
				//takeBigScreenshot (0,3000,6000,4000,"test.bmp");
			}
			ifkeyup('-'){
				RechGFX.isShown = !RechGFX.isShown;
				selected_mariage = NULL;
				toshow = NULL;
			}
			
		break;

		case excel_view :
			executeActionBouton(button_back);
			tableur.update(&tableur);
			plan2DGrabbable();
		break;

		case fractals_view:
			executeActionBouton(button_back);
			RechGFX.update();
			plan2DGrabbable();
			updateSideMenu(&sm,toshow);
			ifkeyup('-'){
				RechGFX.isShown = !RechGFX.isShown;
				selected_mariage = NULL;
				toshow = NULL;
			}
		break;

		case network_view:
			executeActionBouton(button_back);
			RechGFX.update();
			plan2DGrabbable();
			updateCounter(&count);
			ifkeyup('"'){
				show_all_lien = !show_all_lien;
			}
			updateSideMenu(&sm,toshow);
			ifkeyup('-'){
				RechGFX.isShown = !RechGFX.isShown;
				selected_mariage = NULL;
				toshow = NULL;
			}
			ifkeyup('&'){
				int xmin = 99999999,xmax = 0,ymin = 999999,ymax = 0;
				for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
					if(showMaillon(tmp)){
						int x = tmp->tile_info->x;
						int y = tmp->tile_info->y;
						xmin = min(x,xmin);
						xmax = max(x,xmax);
						ymin = min(y,ymin);
						ymax = max(y,ymax);
					}
				}
				println("xmin %d  ymin %d  xmax %d  ymax %d",xmin,ymax-ymin,xmax-xmin+600,ymax-ymin+600);
				takeBigScreenshot (xmax+600,-ymin+600,xmax-xmin+600,ymax-ymin+600,"test.bmp");
				//takeBigScreenshot (0,3000,6000,4000,"test.bmp");
			}
		break;
		
	}
	updateBigScreenshot();
	rafraichisFenetre();
}

void render (){
	switch(state){
		case main_menu :
			effaceFenetre(255,255,255);
			ecrisImageTexture(logo,LF/2 - logo->width/2 , HF - logo->height);
			for(int i=0;i<6;i++){
				int x = menu_button[i].x;
				int y = menu_button[i].y;
				if(Menu_Settings.mov_poly_setting.activated == false && show_setting == false){
					x = LF/2 - menu_button[i].width/2;
					y = (3 - i)*(HF - logo->height)*1./4 ;
					if(i == 4){
						y = (3)*(HF - logo->height)*1./4 ;
						x = LF/2 + 100;
					}
					if(i == 5){
						y = (3)*(HF - logo->height)*1./4 ;
						x = LF/2 - 100;
					}
					if(i == 0){
						x = LF/2 - 300;
					}
				}
				afficheBouton(x,y,&(menu_button[i]));
			}
			if(!(Menu_Settings.mov_poly_setting.activated == false && show_setting == false)){
				afficheMenuSetting();
				
			}
			
		break;

		case map_view :;
			int posx = cam_x;
			int posy = cam_y;
			for(posx = cam_x;posx < LF;posx+=map->static_width){
				ecrisImageTexture(map,posx,posy);
			}
			for(posx = cam_x - map->static_width;posx > -map->static_width;posx-=map->static_width){
				ecrisImageTexture(map,posx,posy);
			}
			
			for(Maillon* tmp = ptrTete; tmp != NULL ; tmp = tmp->next){
				if(Menu_Settings.show_NullIsland==true){
					afficheIndividuMap(tmp->data,map);
				}
				else{
					int lieux=tmp->data->birth_data->lieu.X;
					int lieuy=tmp->data->birth_data->lieu.Y;
					if(!((lieux>=-0.5 && lieuy<=0.5) && (lieuy>=-0.015 && lieuy<=0.015))){
						afficheIndividuMap(tmp->data,map);
					}
				}
			}

			afficheSideMenu(&sm);
			search_menu.show(&search_menu);
			epaisseurDeTrait(2);
			couleurCourante(255,0,255);
			printGfx(0,0,20,"%.2f",zoom);
			afficheBouton(0,HF - button_back.skin->hauteurImage,&button_back );
		break;

		case tree_view :
			if(Menu_Settings.light_mode == true/*!=DARK*/){
				effaceFenetre(255,255,255);
			}
			else{
				effaceFenetre(30,30,30);
			}
			actualisePositionTiles(ptrTete);
			for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
				if(tmp->tile_info->isShown)
					afficheIndividuTile(tmp);
			}
			afficheLiensMariage(ptrTete);
			afficheLiensFraterie(ptrTete);
			if(show_parent){
				afficheLiensParents(ptrTete);
			}
			epaisseurDeTrait(2);
			afficheBouton(LF/2 - save.width/2,10,&save);
			RechGFX.show();
			afficheSideMenu(&sm);
			afficheMenuEdition();
			search_menu.show(&search_menu);
			if(selected_mariage != NULL){
				setColor4i(0,0,0,120);
				rectangle(0,0,LF,HF);
				couleurCourante(60,60,60);
				rectangle(LF/8,4*HF/10,7*LF/8,6*HF/10);
				char* strmariage = malloc(sizeof(char)*300);
				sprintf(strmariage,"%s %s et %s %s se sont maries",
					selected_mariage->data->maries[0]->data->name[0],
					selected_mariage->data->maries[0]->data->surname,
					selected_mariage->data->maries[1]->data->name[0],
					selected_mariage->data->maries[1]->data->surname
				);
				if(selected_mariage->data->lieu.nom != NULL){
					strcat(strmariage," a ");
					strcat(strmariage,selected_mariage->data->lieu.nom);
				}
				if(!dateIsNULL(selected_mariage->data->date)){
					strcat(strmariage," le ");
					strcat(strmariage,dateToFancyString(selected_mariage->data->date));
				}
				couleurCourante(255,255,255);
				fitTextToRectangle(strmariage,LF/8,4*HF/10,6*LF/8,2*HF/10);
				free(strmariage);
			}
			couleurCourante(255,0,255);
			epaisseurDeTrait(2);
			printGfx(0,0,20,"%.2f",zoom);
			afficheBouton(0,HF - button_back.skin->hauteurImage,&button_back );
			

		break;

		case excel_view :
			if(Menu_Settings.light_mode == true/*!=DARK*/){
				effaceFenetre(255,255,255);
			}
			else{
				effaceFenetre(30,30,30);
			}
			tableur.show(&tableur,cam_x,cam_y);
			afficheBouton(0,HF - button_back.skin->hauteurImage,&button_back );
		break;

		case fractals_view:
			if(Menu_Settings.light_mode == true/*!=DARK*/){
				effaceFenetre(255,255,255);
			}
			else{
				effaceFenetre(30,30,30);
			}
			traceCercleFract(500,500,500,MAX_ZOOM_10POW,start);
			RechGFX.show();
			afficheSideMenu(&sm);
			printGfx(0,0,26,"%f",zoom);
			afficheBouton(0,HF - button_back.skin->hauteurImage,&button_back );
		break;

		case network_view:
			if(Menu_Settings.light_mode == true/*!=DARK*/){
				effaceFenetre(255,255,255);
			}
			else{
				effaceFenetre(30,30,30);
			}
			afficheNetworkTree(ptrTete,start,show_all_lien);
			afficheCounter(&count,10,10,40,60);
			RechGFX.show();
			afficheSideMenu(&sm);
			afficheBouton(0,HF - button_back.skin->hauteurImage,&button_back );
		break;
	}
	menu.show(&menu,xmenu,ymenu);
	displayFPS(3);
	updateViewBigScreenshot();
}

void windowResize(){
	switch(state){
		case main_menu :
			Menu_Settings.pos_settings.x = LF/2 - Menu_Settings.polysetting.width/2;
			Menu_Settings.pos_settings.y = HF/2.5 -Menu_Settings.polysetting.height/2;
		break;

		case map_view :
			if(HF != map->height)
				changeZoom(HF*1./map->height - zoom,map);
		break;

		case tree_view :
			setAppropriateCursor();
		break;
		case excel_view : break;
		case fractals_view :break;
		case network_view :break;
	}
	Menu_Settings.EX.isRefreshed = true;
}


void mouseMoved(){
	switch(state){
		case main_menu :break;
		case map_view :break;
		case tree_view :
			setAppropriateCursor();
		break;
		case excel_view:break;
		case fractals_view :break;
		case network_view :break;
	}
}

void rightClickDown (){
	menu.is_shown = false;
	switch(state){
		case main_menu :break;
		case map_view :break;
		case tree_view :break;
		case fractals_view :break;
		case network_view :break;
		case excel_view:break;
	}
}

void rightClickUp (){
	menu.is_shown = true;
	xmenu = AS;
	ymenu = OS;
	switch(state){
		case main_menu :break;
		case map_view :
			println("lon=%f lat=%f\n%s",getLongitude(AS,map),getLatitude(OS,map),getPlaceNameFromCoordinates(getLongitude(AS,map),getLatitude(OS,map)));
		break;
		case tree_view :break;
		case excel_view:break;
		case network_view:;
			bool selected = false;
				for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
					if(pointIsInCircle(AS,OS,cam_x+(tmp->tile_info->x-120/2)*zoom,cam_y+(tmp->tile_info->y-120/2)*zoom,120*sqrt(2)*zoom)){
						toshow = tmp;
						start = tmp;
						resetNetworkTree(start,count.value);
						selected = true;
						break; 		//Si c est ca je me casse !
					}
				}
			if(!selected && !moved){
				toshow = NULL;
			}
			moved = false;
		break;
		case fractals_view :break;
	}
}

void leftClickDown (){
	menu.is_shown = false;
	switch(state){
		case main_menu :break;
		case map_view :break;
		case tree_view :break;
		case excel_view:break;
		case fractals_view :break;
		case network_view :break;
	}
}

void leftClickUp (){
	menu.update(&menu);
	bool selected = false;
	switch(state){
		case main_menu :
			
		break;

		case map_view :
			updatePinListener(ptrTete,map);
			moved = false;
		break;

		case tree_view :
			//if(!optionHoveredSideMenu(&sm)){
				if(selected_mariage != NULL){
					selected_mariage = NULL;
				}
				if(!moved){
					for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
					
						IndividuTile *IT = tmp->tile_info; 
					
						if(AS >= IT->x*zoom+cam_x && AS <= IT->x*zoom+cam_x + IT->poly->width*zoom && OS >= IT->y*zoom+cam_y && OS <= IT->y*zoom+cam_y + IT->poly->height*zoom){
							
							if(toshow != tmp){
								
								if(tmp->tile_info->isShown){
									toshow = tmp;
									selected = true;
									
								}
								break; 		//Si c est ca je me casse !
							}
							
						}
						if(AS >= (IT->x)*zoom+cam_x && AS <= IT->x*zoom+cam_x + IT->poly->width*zoom && OS >= (IT->y-55)*zoom+cam_y && OS <= (IT->y-5)*zoom+cam_y){
							if(tmp->wedding != NULL){
								selected_mariage = tmp->wedding;
							}
						}
					}
				}
				if(!selected && !moved){
					toshow = NULL;
				}
			//}
			moved = false;
			setAppropriateCursor();
		break;
		case excel_view : break;

		case fractals_view : 
			LeftClickUpFractals();
		break;

		case network_view :;
			bool selected = false;
			//if(!optionHoveredSideMenu(&sm)){
				if(!moved){
					for(Maillon* tmp = ptrTete;tmp != NULL;tmp = tmp->next){
						if(pointIsInCircle(AS,OS,cam_x+(tmp->tile_info->x-120/2)*zoom,cam_y+(tmp->tile_info->y-120/2)*zoom,120*sqrt(2)*zoom)){
							if(toshow != tmp){
								toshow = tmp;
								selected = true;
								break; 		//Si c est ca je me casse !
							}
						}
					}
				}
				if(!selected && !moved){
					toshow = NULL;
				}
			//}
			moved = false;
		break;
	}
}

void scrollUp (){
	switch(state){
		case main_menu : break;

		case map_view :
			changeZoom(0.2*zoom,map);
		break;

		case tree_view :
			changeZoom(0.2*zoom,map);
		break;

		case excel_view : break;

		case fractals_view :
			changeZoom(0.2*zoom,map);
		break;

		case network_view :
			changeZoom(0.2*zoom,map);
		break;
	}
}

void scrollDown (){
	switch(state){
		case main_menu : break;

		case map_view :
			if( ! ( (zoom*0.8)*map->height < HF ) )
				changeZoom(-0.2*zoom,map);
			else{
				if(HF != map->height)
					changeZoom(HF*1./map->height - zoom,map);
			}
		break;

		case tree_view :
			changeZoom(-0.2*zoom,map);
		break;

		case excel_view : break;

		case fractals_view :
			changeZoom(-0.2*zoom,map);
		break;

		case network_view :
			changeZoom(-0.2*zoom,map);
		break;
	}
}