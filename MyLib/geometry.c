#ifdef _WIN32
	#include <windows.h>
#endif
#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../GfxLib/GfxLib.h"
#include "../GfxLib/ESLib.h"
#include <string.h>
#include "KyllianToolsLib.h"
#include "consoletools.h"
#include "geometry.h"

/**
 * @brief cree un tableau de point formant un segment entre deux points
 * 
 * @param xa abscisse du point 1
 * @param ya ordonée du point 1
 * @param xb abscisse du point 2
 * @param yb ordonee du point 2
 * @return Point** tableau de pointeurs sur points terminé par un NULL
 * Kyllian MARIE, 2019
 */
Point** makeLine (int xa, int ya, int xb, int yb){
	Point** p = malloc(sizeof(Point*));
	int cpt = 1;
	float a;
	float c;
	int yi;
	int xi;
	if( xb == xa){
		a = 9999999;
	}
	else{
		a = (yb-ya)/(1.*xb-xa);
	}
	c = ya - a*xa;
	if(a>= -1 || a<=1){
		if(xa<=xb){
			for(xi=xa;xi<xb;xi++){
				yi = a*xi + c;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
		else{
			for(xi=xa;xi>xb;xi--){
				yi = a*xi + c;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
	}
	else if((a<-1. || a>1.) && a != 9999999){
		if(ya<=yb){
			for(yi=ya;yi<yb;yi++){
				xi = (yi-c)*1./a;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
		else{
			for(yi=ya;yi>yb;yi--){
				xi = (yi-c)*1./a;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
	}
	else if(a == 9999999){
		if(ya<=yb){
			for(yi=ya;yi<yb;yi++){
				xi = xa;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
		else{
			for(yi=ya;yi>yb;yi--){
				xi = xa;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
	}
	p = realloc(p,sizeof(Point*)*cpt);
	p[cpt-1] = NULL;
	return p;
}

Point** Point_cat(Point** p1,Point** p2){
	int size1 = count(p1)-1,
		size2 = count(p2)-1;
	Point** p = calloc((size1 + size2 + 1),sizeof(Point*));
	for(int i = 0; i < size1; i++){
		p[i] = malloc(sizeof(Point));
		*(p[i]) = (Point) {p1[i]->x,p1[i]->y};
	}
	for(int i = size1; i < size1 + size2; i++){
		p[i] = malloc(sizeof(Point));
		*(p[i]) = (Point) {p2[i-size1]->x,p2[i-size1]->y};
	}
	p[size1 + size2] = NULL;
	return p;
}

bool pointIsInCircle (int x,int y,int xc,int yc,int r){
	return (sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc)) < r);
}

bool pointIsInRectangle (int x,int y,int r_pos_x,int r_pos_y,int r_width,int r_height){
	return (x >= r_pos_x && x <= r_pos_x + r_width && y >= r_pos_y && y <= r_pos_y + r_height);
}

void freePolygone(Polygone p){
	betterFree(&(p.pts));
}

Polygone initPolygone (int width,int height,float coef,float rnbpts){
	if(rnbpts > 150){
		rnbpts = 150;
	}
	if(rnbpts < 17){
		rnbpts = 17;
	}
	if(coef > height*1./width * 2){
		coef = height*1./width*2;
	}
	DEBUGSTART;
	//int x1 = 0;
	//int y1 = 0;
	int x2 = width;
	int y2 = height;

	int x3 = width/2;
	int y3 = height/2;
	int nbpts = rnbpts/4;
	float scale = coef*x3*1./2;

	Polygone P;
	P.height = height;
	P.width = width;
	P.pts = malloc(sizeof(Point)*rnbpts);
	P.nbpts = rnbpts;

	//Definition d'une forme a bords arrondis
	P.pts[0].x = x3;
	P.pts[0].y = y2;
	for(int i= 1; i< nbpts-1;i++){
		float x,y;
		x = (i-1)*1./(nbpts-3);
		y = sqrt(1 - x*x);
		P.pts[i].x = x2-scale + x*scale;
		P.pts[i].y = y2-scale + y*scale;
		//printf("%d %d [%d %f %f]\n",P.pts[i].x,P.pts[i].y,i,x,y);
	}
	P.pts[nbpts-1].x = x2;
	P.pts[nbpts-1].y = y3;

	// Symetrie par l axe X
	int cpt = rnbpts/4 - 1;
	for(int i=rnbpts/4;i<rnbpts/2;i++){
		if(cpt < 0)
			cpt = 0;
		P.pts[i].x = P.pts[cpt].x;
		P.pts[i].y = 2*y3 - P.pts[cpt].y;
		cpt--;
		//printf("%d %d [%d %d]\n",P.pts[i].x,P.pts[i].y,i,cpt);
	}

	//Symetrie par l'axe Y
	int cpt2 = rnbpts/2 - 1;
	for(int i=rnbpts/2;i<rnbpts;i++){
		P.pts[i].x = 2*x3- P.pts[cpt2].x;
		P.pts[i].y = P.pts[cpt2].y;
		cpt2--;
	}

	//Debug Printf
	/*for(int i=0;i<rnbpts;i++){
		printf("Vertex[%d] %d %d\n",i,P.pts[i].x,P.pts[i].y);
	}*/
	DEBUGEND;
	return P;
}

void dessinePolygone (Polygone P,int x,int y){
	glBegin(GL_POLYGON);
		for(int i = 0;i < P.nbpts-1;i++){
			glVertex2i(P.pts[i].x + x,P.pts[i].y + y);
			glVertex2i(P.pts[i+1].x + x,P.pts[i+1].y + y);
		}
  	glEnd();
}

void dessinePolygoneZoom (Polygone P,int x,int y,float zoom){
	glBegin(GL_POLYGON);
		for(int i = 0;i < P.nbpts-1;i++){
			glVertex2i(P.pts[i].x*zoom + x,P.pts[i].y*zoom + y);
			glVertex2i(P.pts[i+1].x*zoom + x,P.pts[i+1].y*zoom + y);
		}
  	glEnd();
}

Point new_Point (int x,int y){
	return (Point) {x,y};
}

Point_2f new_Point_2f (float x,float y){
	return (Point_2f) {x,y};
}

Ligne new_Ligne (int x1,int y1,int x2,int y2){
	return (Ligne) {new_Point(x1,y1),new_Point(x2,y2)};
}

char* Point_tostring (Point p){
	return str_format("Point( x=%d , y=%d )",p.x,p.y);
}

char* Point_2f_tostring (Point_2f p){
	return str_format("Point( x=%f , y=%f )",p.x,p.y);
}

char* Ligne_tostring (Ligne l){
	return str_format("Ligne( %s , %s )",Point_tostring(l.p1),Point_tostring(l.p2));
}

char* Polygone_tostring (Polygone p){
	char* ret = malloc(sizeof(char));
	ret[0] = '\0';
	int size = 1;
	size += strlen("Polygone {\n");
	ret = realloc(ret,size*sizeof(char*));
	strcat(ret,"Polygone {\n");
	for(int i=0;i<p.nbpts;i++){
		char *pt = Point_tostring(p.pts[i]);
		size += strlen(pt)+2;
		ret = realloc(ret,size*sizeof(char*));
		strcat(ret,"\t");
		strcat(ret,pt);
		ret[size-1] = '\0';
		ret[size-2] = '\n';
	}
	size += 1;
	ret = realloc(ret,size*sizeof(char*));
	strcat(ret,"}");
	return ret;
}