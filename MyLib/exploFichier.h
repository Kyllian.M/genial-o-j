#define TEMPORISATION_MENU 0
#define TEMPORISATION_ASSOCIATION 1
#define TEMPORISATION_MEMORY 2
#define TEMPORISATION_PICTIONNARY 3
#define TEMPORISATION_RESULTATS 4
#define SHOW_CONTENT true
#define AFFICHAGE_MENU 0
#define AFFICHAGE_ASSOCIATION 1
#define AFFICHAGE_MEMORY 2
#define AFFICHAGE_PICTIONNARY 3
#define AFFICHAGE_RESULTATS 4

#define TYPE_BMP 0
#define TYPE_BMPT 1
#define TYPE_DATA 2
#define TYPE_FOLDER 3
#define TYPE_INCONNU 4



typedef struct pts{
	int x,y;
	float t;
}pts;

typedef struct fichier{
	char* fullInfo;
	char* nom;
	char* taille;
	char* time;
	int typeFichier;
}fichier;

typedef struct exploFichier{
	fichier* F;
	int nbChaine;
	bool isShown;
	bool isRefreshed;
	char currentDir[80];
	int selection;
	int nbChMax;
	Image* img;
	DonneesImageRGB* imgTypeFichier[5];
	int nbPage;
	int page;
	DonneesImageRGB* openedImage;

	fichier selected_file;
	void (*callback) ();
}exploFichier;

void afficheExploFichier (exploFichier EX);

//fonction qui permet d'identifier le type d'un fichier a partir de son nom
void comprendTypeFichier (fichier *F);
//cree par Kyllian MARIE

//fonction qui return true qi la chaine donnée contient un '.' false sinon
bool contientUnPoint (char* ch);
//cree par Kyllian MARIE

//fonction qui si on lui donne un chemin type "/home/yncrea/monDossier" va retourner la meme chaine en remontant d'un cran soit "/home/yncrea"
char* removeLastFolder (char* ch);
//cree par Kyllian MARIE

//fonction permettant d'interpreter la chaine retournée par la commande "ls -i [emplacement]" et d'en extraire les informations
void InterpreteLSargumentL (char* C,char* nom,char* taille,char* timeFull);
//cree par Kyllian MARIE

//fonction permettant d'initialiser la structure ExploFichier a partir d'un chemin type /home/
exploFichier initExploFichier (char* chemin);
//cree par Kyllian MARIE

//fonction de gestion de l'explorateur de fichiers
void gereExploFichier (exploFichier* EX);
//cree par Kyllian MARIE

//fonction de liberation de memoire de l explorateur de fichier
void freeExploFichier (exploFichier* EX);
//cree par Kyllian MARIE

//REMPLIS UN TABLEAU DE CHAINES DE CHARACTERES AVEC LES NOMS DES FICHIERS PROVENANT DU CHEMIN DONNE
void lisListeFichier(fichier** lsFile,char* chemin,int* nbChaine,int* nbChMax);
//cree par Kyllian MARIE

//fonction qui enleve le dernier charactere d une chaine
char* enleveBSn (char* C);
//cree par Kyllian MARIE

//fonction qui remplace les espaces d une chaine par \[ESPACE]
char* corrigeChaineEspace (char* C);
//cree par Kyllian MARIE

//fonction qui rajoute des guillemets au debut et a la fin d une chaine
char* putInQuotes (char* C);
//cree par Kyllian MARIE
