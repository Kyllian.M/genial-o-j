#define LargeurFenetre 800
#define HauteurFenetre 800
#define LF largeurFenetre()
#define HF hauteurFenetre()
#define ifkeypressed(x) if(keyboardState[x])
#define ifkeyup(x) if(keyboardState[x] == 2)
#define MOUSE_NOTHING 0
#define MOUSE_LEFT_UP 1
#define MOUSE_LEFT_DOWN 2
#define MOUSE_RIGHT_UP 3
#define MOUSE_RIGHT_DOWN 4
#define MOUSE_SCROLL_UP 5
#define MOUSE_SCROLL_DOWN 6

extern double runtime;
extern unsigned char keyboardState[255];
extern int mouseState;
extern bool handleSouris;
extern int AS;
extern int OS;
