#ifdef _WIN32
	#include <windows.h>
#endif
#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif

#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <math.h> // Pour pouvoir utiliser sin() et cos()
#include "../GfxLib/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "../GfxLib/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "../GfxLib/ESLib.h" // Pour utiliser valeurAleatoire()
#include "stdbool.h"
#include <string.h>
#include "Timer.h"
#include "consoletools.h"
#include "KyllianToolsLib.h"
#include "KToolsLibForGfxLib.h"
#include "images.h"

//fonction qui applique resizeDonneesImageRGB a chaque frame d'un gif
void resizeGif(gif* G,int largeur){
	for(int i=0;i<G->numberFrame;i++){
		G->img[i] = resizeDonneesImageRGB(&(G->img[i]),largeur);
	}
}
//cree par Kyllian MARIE

//ALGORITHME DE CROPPING D IMAGE DU "PLUS PROCHE VOISIN"
//cette fonction sert a redimensionner une image en fonction d'une nouvelle largeur (l'image va garder le meme ratio largeur/hauteur)
DonneesImageRGB* resizeDonneesImageRGB (DonneesImageRGB** img,int newLength){	
	DonneesImageRGB* Nimg = (DonneesImageRGB*) malloc(sizeof(DonneesImageRGB));		//nouvelle image
	float coefX;
	coefX = 1./( (*img)->largeurImage*1./newLength );		//coefX represente combien de fois l'image va etre grossie
	int newHeight = (*img)->hauteurImage * coefX;			//newHeight sera la nouvelle hauteur de l'image
	Nimg->hauteurImage = newHeight;							//initialisation manuelle d'une structure DonneesImageRGB
	Nimg->largeurImage = newLength;
	Nimg->donneesRGB = (unsigned char*) malloc(sizeof(unsigned char)*3*newLength*newHeight);
	RGB** T;										//T est un tableau bi-dimensionnel de RGB qui sera utilisé pour stocker l'image initiale(il est plus facile de manipuler un RGB** que Unsigned char*)
	T = (RGB**) malloc(sizeof(RGB*)*(*img)->largeurImage);
	for(int i=0;i<(*img)->largeurImage;i++){
		T[i] = (RGB*) malloc(sizeof(RGB)*(*img)->hauteurImage);
	}
	convertUnsignedCharInRGB(T,(*img)->donneesRGB,(*img)->largeurImage,(*img)->hauteurImage);
	RGB** T2;												//T est un tableau bi-dimensionnel de RGB qui sera utilisé pour stocker l'image convertie
	T2 = (RGB**) malloc(sizeof(RGB*)*newLength);
	for(int i=0;i<newLength;i++){
		T2[i] = (RGB*) malloc(sizeof(RGB)*newHeight);
	}
	int a1;		//abscisse du pixel plus proche voisin
	int a2;		//ordonnee du pixel plus proche voisin
	for(int i=0;i<newLength;i++){
		for(int i2=0;i2<newHeight;i2++){
			a1 = (i*1./coefX);
			a2 = (i2*1./coefX);
			T2[i][i2] = T[a1][a2];
		}
	}
	convertRGBinUnsignedChar(T2,Nimg->donneesRGB,newLength,newHeight);	//reconversion en Unsigned char de la nouvelle image
	//FREE
	for(int i=0;i<(*img)->largeurImage;i++){
		free(T[i]);
		T[i] = NULL;
	}
	free(T);
	T=NULL;
	//FREE
	return Nimg;
}
//cree par Kyllian MARIE

//fonction de conversion d'une structure RGB** en unsigned char* pour le stockage d'une image
void convertRGBinUnsignedChar (RGB **T,unsigned char* BVR,int largeur,int hauteur){
	int i=0;
	for(int y=0;y<hauteur;y++){
		for(int x=0;x<largeur;x++){
			BVR[i] = T[x][y].b;
			i++;
			BVR[i] = T[x][y].g;
			i++;
			BVR[i] = T[x][y].r;
			i++;
		}
	}
}
//cree par Kyllian MARIE

//fonction de conversion d'une structure unsigned char* en RGB** pour le stockage d'une image
void convertUnsignedCharInRGB (RGB **T,unsigned char* BVR,int largeur,int hauteur){
	int i=0;
	for(int y=0;y<hauteur;y++){
		for(int x=0;x<largeur;x++){
			T[x][y].b = BVR[i];
			i++;
			T[x][y].g = BVR[i];
			i++;
			T[x][y].r = BVR[i];
			i++;
		}
	}
}
//cree par Kyllian MARIE

gif initGif (char folderName[20],char commonName[20],int numberFrame,int fps,bool backExist,couleur color){
	//CETTE FONCTION SERT A DEFINIR UNE SEQUENCE D IMAGE POUR POUVOIR L UTILISER PLUS TARD
	//CES SEQUENCES D IMAGES NE SONT PAS DES .GIF MAIS ONT LES MEMES PROPRIETES ET BUT QUE LES .GIFS
	gif G;
	char base[40];		//BASE DU CHEMIN D ACCES AUX FICHIERS IMAGE
	char a[4];			//CHAINE OU EST STOCKé LE NUMERO DE L IMAGE A CHARGER
	char chemin[50];	//CHAINE CONTENANT LE CHEMIN D ACCES COMPLET A UN FICHIER
	// CREATION DE LA BASE DE LA CHAINE
	strcpy(base,"Ressources/");
	strcat(base,folderName);
	strcat(base,"/");
	strcat(base,commonName);
	//ALLOCATION DE LA MEMOIRE NECESSAIRE A UN TABLEAU CONTENANT LE NOMBRE D IMAGE REQUIS POUR L ANIMATION
	G.img = (DonneesImageRGB**) malloc(sizeof(DonneesImageRGB)*numberFrame);
	for(int i=0;i<numberFrame;i++){
		//CREATION DE LA CHAINE CHEMIN D ACCES COMPLET ET LECTURE DE L IMAGE ASSOCIEE A CHAQUE FRAME DU "GIF"
		sprintf(a,"%d",i);
		strcpy(chemin,base);
		strcat(chemin,a);
		strcat(chemin,".bmp");
		G.img[i] = lisBMPRGB(chemin);
	}
	//INITIALISATION BASIQUE DE LA STRUCTURE GIF
	G.numberFrame = numberFrame;
	G.backgroundExist = backExist;
	G.background = color;
	G.currentFrame = 0;
	G.fps = fps;
	G.pas = ((20*0.001))*fps;
	G.lastTime = 0;
	G.isFrozen = false;
	G.boucle = 0;
	return G;
}
//cree par Kyllian MARIE

void afficheGif(gif *G,int x,int y,bool sens,timer T){
	//CETTE FONCTION SERT A AFFICHER UN GIF IMAGE PAR IMAGE 
	if(G->currentFrame >= G->numberFrame){G->currentFrame=0; (G->boucle)++;	} 	//SI ON A ATTEIND LA DERNIERE FRAME ON REBOUCLE A LA PREMIERE
	if(G->currentFrame < 0){G->currentFrame = G->numberFrame-1; (G->boucle)++; }
	int currentFrame = G->currentFrame;
	ecrisImageSansFond(		//AFFICHAGE DE L IMAGE
		x,
		y,
		(G->img[currentFrame])->largeurImage,
		(G->img[currentFrame])->hauteurImage,
		(G->img[currentFrame])->donneesRGB,
		(RGB) {G->background.r,G->background.v,G->background.b});
	if(G->lastTime != 0 && G->isFrozen == false){
		if(sens == true){
			G->currentFrame = G->currentFrame + G->pas*(((tempsReel()-T.t0) - G->lastTime )*1./(20*0.001));
								//PASSAGE A LA FRAME SUIVANTE 
		}
		if(sens == false){
			G->currentFrame = G->currentFrame - G->pas*(((tempsReel()-T.t0) - G->lastTime )*1./(20*0.001));
		}
	}
	G->lastTime = tempsReel()-T.t0;
}
//cree par Kyllian MARIE


void ecrisImagePixels (DonneesImageRGB* I,int x,int y){
	unsigned char* img = I->donneesRGB;
	for(int i=0;i<I->largeurImage*I->hauteurImage*3;i+=3){
		int posx = x + (i/3)%I->largeurImage;
		int posy = y + (i/3)/I->largeurImage;
		couleurCourante(img[i+2],img[i+1],img[i]);
		rectangle(posx,posy,posx+1,posy+1);
	}
}


static int *BVR2ARVB(int largeur, int hauteur, const unsigned char *donnees){
	const unsigned char *ptrDonnees;
	unsigned char *pixels = (unsigned char*)malloc(largeur*hauteur*sizeof(int));
	unsigned char *ptrPixel;
	int index;
	
	ptrPixel = pixels;
	ptrDonnees = donnees;
	for (index = largeur*hauteur; index != 0; --index){ /* On parcourt tous les pixels de l'image */
		ptrPixel[0] = ptrDonnees[0];
		ptrPixel[1] = ptrDonnees[1];
		ptrPixel[2] = ptrDonnees[2];
		ptrPixel[3] = 0x0FF;
		ptrDonnees += 3; /* On passe a la premiere composante du pixel suivant */
		ptrPixel += 4; /* On passe au pixel suivant */
	}
	return (int*)pixels;
}
//Fonction créée par Ghislain Oudinet pour GFXLib

DonneesImageARGB* lisBMPARGB (char* path){
	DonneesImageRGB* intermed = lisBMPRGB(path);
	DonneesImageARGB* ret = malloc(sizeof(DonneesImageARGB));
	ret->width = intermed->largeurImage;
	ret->height = intermed->hauteurImage;
	ret->zoom = 1;
	ret->static_height = intermed->hauteurImage;
	ret->static_width = intermed->largeurImage;
	ret->donneesARGB = BVR2ARVB(ret->width,ret->height,intermed->donneesRGB);
	libereDonneesImageRGB(&intermed);
	return ret;
}

DonneesImageARGB* convertFromDonneesImageRGB (DonneesImageRGB* img){
	DonneesImageRGB* intermed = img;
	DonneesImageARGB* ret = malloc(sizeof(DonneesImageARGB));
	ret->width = intermed->largeurImage;
	ret->height = intermed->hauteurImage;
	ret->zoom = 1;
	ret->static_height = intermed->hauteurImage;
	ret->static_width = intermed->largeurImage;
	ret->donneesARGB = BVR2ARVB(ret->width,ret->height,intermed->donneesRGB);
	printyellow("%d %d\n",ret->height,ret->static_height);
	return ret;
}

void setZoomDonneesImageARGB (DonneesImageARGB* I,float factor){
	if(I != NULL){
		I->zoom = factor;
		I->static_height = factor*1.*I->height;
		I->static_width  = factor*1.*I->width;
	}
}

void freeDonneesImageARGB(DonneesImageARGB **structure){
	if(structure != NULL){
		if(*structure != NULL){
			free((*structure)->donneesARGB);
			free(*structure);
		}
		*structure = NULL;
	}
}

int glWindowPos2i(int x,int y);

void betterEcrisImage(DonneesImageARGB* I,int x,int y){
	if(I != NULL){
		glPixelZoom(I->zoom,I->zoom);
		couleurCourante(255,255,255);
		if(x > -I->static_width && x < LF+I->static_width && y > -I->static_height && y< HF + I->static_height){
			int largeur = I->width;
			int hauteur = I->height;
			int* pixels = I->donneesARGB;
			glWindowPos2i(x, y);
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
			glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
			glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
			glDrawPixels(largeur, hauteur, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV,pixels);
		}
		glPixelZoom(1,1);
	}
}

void sauveDessinBMP(char* nomFichier,unsigned char* img,int width,int height){ //Nouvelle methode : exportation du dessin vers un fichier bmp
	FILE *f;
	int w = width;
	int h = height;
	int filesize = 54 + 3*w*h;

	unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0}; //Documentation sur les bmp recuperees sur la doc
	unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};//Le bmp necessite un header puis le contenu de son fichier
	unsigned char bmppad[3] = {0,0,0};

	//Informations du header - obtenues en ligne
	bmpfileheader[ 2] = (unsigned char)(filesize    );
	bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
	bmpfileheader[ 4] = (unsigned char)(filesize>>16);
	bmpfileheader[ 5] = (unsigned char)(filesize>>24);

	bmpinfoheader[ 4] = (unsigned char)(       w    );
	bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
	bmpinfoheader[ 6] = (unsigned char)(       w>>16);
	bmpinfoheader[ 7] = (unsigned char)(       w>>24);
	bmpinfoheader[ 8] = (unsigned char)(       h    );
	bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
	bmpinfoheader[10] = (unsigned char)(       h>>16);
	bmpinfoheader[11] = (unsigned char)(       h>>24);

	f = fopen(nomFichier,"wb");	//Ouverture en mode binaire car le bitmap est une matrice binaire de points
	fwrite(bmpfileheader,1,14,f);
	fwrite(bmpinfoheader,1,40,f);
	for(int i=0; i<h; i++) //On trace les points dans le fichier avec une boucle
	{
		fwrite(img+(w*(h-i-1)*3),3,w,f);
		fwrite(bmppad,1,(4-(w*3)%4)%4,f);
	}

	fclose(f);
}

void retourneImageByte (unsigned char** img,int width,int height){
	long len = 3*width*height;
	long biglen = 4*width*height;
	unsigned char* bgr = malloc(sizeof(unsigned char)*len);
	int cpt = 0;
	for(int i=0;i<biglen;i+=4){
		bgr[cpt] = (*img)[i];
		bgr[cpt+1] = (*img)[i+1];
		bgr[cpt+2] = (*img)[i+2];
		cpt+=3;
	}
	unsigned char* reverse = malloc(sizeof(unsigned char)*len);
	for(int i = 0;i<len;i+=3){
		if(i+2 < len && len-1-i-1 >0){
			reverse[i] = bgr[len-1-i];
			reverse[i+1] = bgr[len-1-i-2];
			reverse[i+2] = bgr[len-1-i-1];
		}
	}
	unsigned char* rev = malloc(sizeof(unsigned char)*len);
	for(int i=0;i<height;i++){
		for(int i2 = 0;i2 < width*3;i2++){
			if((i+1)*width*3-i2 < len && i*width*3+i2 < len)
				rev[i*width*3 + i2] = reverse[(i+1)*width*3-i2];
		}
	}
	betterFree(&bgr);
	betterFree(&reverse);
	betterFree(img);
	*img = rev;
}

void screenshot (int x,int y,int width,int height,char* nomFichier){
	unsigned char* pixels = calloc(width*height*5,sizeof(char));
	glReadBuffer(GL_BACK);
	glReadPixels(x,y,width,height,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
	retourneImageByte(&pixels,width,height);
	sauveDessinBMP(nomFichier,pixels,width,height);
	betterFree(&pixels);
}

//void betterEcrisImage(DonneesImageARGB* I,int x,int y){
	//if(x > -I->largeurImage && x < LF+I->largeurImage && y > -I->hauteurImage && y< HF + I->hauteurImage){
		//if(x < 0 || y < 0 /*|| x+I->largeurImage > LF*/){
		/*	double t0 = tempsReel();
			DonneesImageARGB* I2 = malloc(sizeof(DonneesImageARGB));
			I2->largeurImage = I->largeurImage;
			I2->hauteurImage = I->hauteurImage;
			int offset = 0;
			int offsety = 0;
			int offsetx2 = 0;
			int offsety2 = 0;
			if(x < 0){
				I2->largeurImage+=x ;
				offset-=x ;
			}
			if(y < 0){
				I2->hauteurImage+=y;
				offsety-=y;
			}
			//if(x+ I->largeurImage > LF){
			//	offsetx2+= x+I->largeurImage - LF;
			//	I2->largeurImage-=offsetx2;
			//}
			I2->donneesARGB = malloc(sizeof(int)*I2->largeurImage*I2->hauteurImage);
			int size = I->largeurImage*I->hauteurImage;
			int cpt = 0;
			int cpt2 = 0;
			//println("kqsk %d",4*I2->largeurImage*I2->hauteurImage);
			for(int i=I->largeurImage*offsety + offset ; i<size ; i++){
				//144400
				//println("%d / %d",cpt,I2->largeurImage*I2->hauteurImage);
				//printred("%d / %d    %d\n",i,size,I->donneesARGB[i]);
				I2->donneesARGB[cpt] = I->donneesARGB[i];
				//println("a");
				//println("d");
				cpt++;
				cpt2++;
				if(cpt2 == I2->largeurImage){
					i += (offset) + offsetx2;
					cpt2 = 0;
				}
			}
			//printred("%f\n",tempsReel()-t0);
			ecrisImage3(offset+x,offsety+y,I2);
			libereDonneesImageARGB(&I2);
		}
		else{
			//ecrisImage3(x,y,I);
	//	}
	//}
//}*/