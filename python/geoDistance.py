import sys
from geopy.distance import geodesic

x1 = float(sys.argv[1])
y1 = float(sys.argv[2])
x2 = float(sys.argv[3])
y2 = float(sys.argv[4])

loc1 = (y1,x1)
loc2 = (y2,x2)

dist = geodesic(loc1,loc2).km

file = open("../Ressources/geolocator.txt","w")
string = str(dist)
file.write(string)
file.close()
