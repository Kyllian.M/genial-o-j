#ifdef _WIN32
	#include <windows.h>
#endif
#ifdef __APPLE__
//	#include <OpenGL/gl.h>
//	#include <OpenGL/CGLMacro.h>
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "../GfxLib/GfxLib.h"
#include "../GfxLib/BmpLib.h"
#include "../GfxLib/ESLib.h"
#include "../GfxLib/commonIO.h"
#include "../MyLib/KyllianToolsLib.h"
#include "../MyLib/KToolsLibForGfxLib.h"
#include "../MyLib/geometry.h"
#include "../MyLib/Cwing.h"
#include "genialOJ.h"
#include <unistd.h>


void gestionEvenement(EvenementGfx evenement);
int main(int argc, char **argv){
	initialiseGfx(argc, argv);
	prelaunch();
	prepareFenetreGraphique(processname, processwidth, processheight);
	lanceBoucleEvenements();
	return 0;
}

//FONCTION DE GESTION DES CLICKS ET DEPLACEMENTS DE LA SOURIS
void gestionSouris (int button,int state,int xa,int ya){
	switch (button){
		case GLUT_LEFT_BUTTON :
			//LEFT CLICK
			switch(state){
				case GLUT_UP:
					//PRESSED
					mouseState = MOUSE_LEFT_UP;
				break;
				case GLUT_DOWN:
					//UNPRESSED
					mouseState = MOUSE_LEFT_DOWN;
				break;
			}
		break;
		case 3 :
			//SCROLLING UP
			mouseState = MOUSE_SCROLL_UP;
		break;
		case 4 :
			//SCROLLING DOWN
			mouseState = MOUSE_SCROLL_DOWN;
		break;
		case GLUT_RIGHT_BUTTON :
			//RIGHT CLICK
			switch(state){
				case GLUT_UP:
					//PRESSED
					mouseState = MOUSE_RIGHT_UP;
				break;
				case GLUT_DOWN:
					//UNPRESSED
					mouseState = MOUSE_RIGHT_DOWN;
				break;
			}
		break;
	}
	AS = xa;
	OS = HF - ya;
	handleSouris = true;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DE MOTION DE LA SOURIS 
void motion (int xa,int ya){
	AS = xa;
	OS = HF - ya;
	mouseMoved();
}
///CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DE PRESSION CLAVIER
void keyDown (unsigned char a,int x,int y){
	keyboardState[a] = true;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DE RELACHEMENT CLAVIER
void keyUp (unsigned char a,int x,int y){
	keyboardState[a] = 2;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

int nbframe = 0;

void gestionEvenement(EvenementGfx evenement)
{
	

	//EXECUTES A CHAQUE BOUCLE 
	if(first){
		runtime_t0 = tempsReel();
		first = false;
	}
	runtime = (tempsReel() - runtime_t0);
	
	// SWITCH EVENEMENT
	switch (evenement)
	{
		case Initialisation:
			//FONCTIONS CALLBACK GLUT
			glutMouseFunc(gestionSouris);
			glutMotionFunc(motion);
			glutPassiveMotionFunc(motion);
			glutKeyboardFunc(keyDown);
			glutKeyboardUpFunc(keyUp);
			glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
			for(int i=0;i<255;i++){
				keyboardState[i] = false;
			}
			demandeTemporisation(0);
		break;
		
		case Temporisation:
			if(nbframe == 3){
				initialize();
				nbframe++;
			}
			if(nbframe > 3)
				update();
			else{
				nbframe++;
				rafraichisFenetre();
				sleep(1);
			}
		break;
			
		case Affichage:
			// RENDERING FRAME
			if(nbframe > 3)
				render();
			else{
				effaceFenetre(0,0,0);
				couleurCourante(255,255,255);
				epaisseurDeTrait(5);
				printGfx(LF/2 - tailleChaine("Loading...",80)/2,HF/2 - 80/2,80,"Loading...");
			}
		break;

		case Inactivite:
			// PAS D ENTREE
		break;
		
		case Redimensionnement:
			// REDIMENSIONNEMENT DE LA FENETRE
			if(nbframe > 3)
				windowResize();
		break;
			
		case Clavier:		/*UNUSED UTILISATION DE KeyboardState*/	break;
		case BoutonSouris:	/*UNUSED UTILISATION DE handleSouris*/	break;
		case ClavierSpecial:/*UNUSED UTILISATION DE KeyboardState*/	break;
		case Souris:		/*UNUSED UTILISATION DE handleSouris*/	break;
	}
	if(nbframe > 3){
		if(handleSouris){
			switch (mouseState) {
				case MOUSE_LEFT_DOWN :
					leftClickDown();
				break;
				case MOUSE_LEFT_UP :
					leftClickUp();
				break;
				case MOUSE_RIGHT_DOWN :
					rightClickDown();
				break;
				case MOUSE_RIGHT_UP :
					rightClickUp();
				break;
				case MOUSE_SCROLL_DOWN :
					scrollDown();
				break;
				case MOUSE_SCROLL_UP :
					scrollUp();
				break;
			}
			handleSouris = false;
		}
		ifkeypressed(27){ //ECHAP
			termineBoucleEvenements();
		}
		for(int i=0;i<255;i++){
			if(keyboardState[i] == 2)
			keyboardState[i] = false;
		}
	}
}
