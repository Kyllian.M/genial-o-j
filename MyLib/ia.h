#ifndef IA_H
#define IA_H

typedef struct ia_Tuple{
    Maillon* prs;
    int score;
    Mariage* prop;
}ia_Tuple;

/**
 * @brief : Renvoie les informations les plus probables sur un individu stockées dans un tableau d'individus
 * 
 * @param1 : *ptrTete -> pointeur de tete de la liste chainée de maillons
 * @param2 : *prs -> Maillon que l'on veut completer
 * @param3 : *ptrTete -> pointeur de tete de la liste chainée des maillon de mariages 
 * 
 * @return : tableau d'individu 
 */
Individu* getPropostion(Maillon *ptrTete, Maillon *prs,MaillonMariage *ptrTeteM);

/**
 * @brief : Renvoie un tableau de String contenant les prénoms les plus probables
 * 
 * @param : *prs -> Maillon que l'on veut completer
 * 
 * @return : tableau de String contenant le nom le plus probable
 */
char** propPrenom(Maillon *prs);

/**
 * @brief : Renvoie un tableau de String contenant les Noms les plus probables
 * 
 * @param : *prs -> Maillon que l'on veut completer
 * 
 * @return : tableau de String contentant le nom le plus probable
 */
char** propNom(Maillon *prs);

/**
 * @brief : Renvoi le lieu de naissance le plus probable pour l'individu concerné
 * 
 * @param : *prs -> Maillon que l'on veut completer
 * 
 * @return : structure contenant le Lieu de naissance le plus probable
 */
char** propLieuNaissance(Maillon *prs);

/**
 * @brief Fonction à base de statistiques/moyennes qui cherche à déterminer la date de naissance d’un individu. (Voir documentation pour plus d'informations)
 * 
 * @param prs 
 * @return int* 
 */
int* propDateNaissance(Maillon *prs);

/**
 * @brief Fonction à base de statistiques/moyennes avec un système de score qui cherche à déterminer le lieu de mort d’un individu. (Voir documentation pour plus d'informations)
 * 
 * @param prs 
 * @param poids 
 * @return char** 
 */
char** propLieuMort(Maillon *prs, int poids[8]);

/**
 * @brief : Renvoi la date de mort la plus probable pour l'individu concerné
 * 
 * @param : *prs -> Maillon que l'on veut completer
 * 
 * @return : tableau de int: [0] 1er quartile |[1] moyenne |[2] 3e quartile |[3] dateMin |[4] dateMax 
 */
int* propDateMort(Maillon *prs);

/**
 * @brief Renvois un talbeau contenant les peres les plus probables
 * 
 * @param1 Maillon* prs
 * @param2 Maillon* ptrTete
 * 
 * @return Tableau de peres potentiels
 */
Maillon** propPere(Maillon* prs, Maillon* ptrTete);

/**
 * @brief : Retourne un tableau d'entier 
 * 
 * 
 * @return : tableau d'entier contenant les dates (la dernière valeur étant 9000)
 */
int* propDateMort(Maillon *prs);

/**
 * @brief : Renvois un tableau contenant les informations les plus probable sur le genre de la personne
 * 
 * @param : *prs -> Maillon que l'on veut completer
 * 
 * @etrun : pointeur sur boolean
 */
bool* propGenre(Maillon *prs);


/**
 * @brief : Renvoie un tableau de maillon contenant les maillons les plus proba ble pour la mère
 * 
 * @param : *prs -> Maillon que l'on veut completer
 * 
 * @return : tableau de maillon 
 */
Maillon** propMom(Maillon *prs,Maillon *ptrTete);

/**
 * @brief : Renvoie un tableau de maillon contenant les maillons les plus proba ble pour le père
 * 
 * @param : *prs -> Maillon que l'on veut completer
 * 
 * @return : tableau de maillon 
 */
Maillon** propDad(Maillon *prs,Maillon *ptrTete);


/**
 * @brief Fonction à base de statistiques/moyennes avec un système de score qui cherche à proposer un mariage à un individu. (Voir documentation pour plus d'informations)
 * 
 * @param prs 
 * @param ptrTete 
 * @return Mariage** 
 */
Mariage** propMariage(Maillon *prs,Maillon* ptrTete);

#endif 