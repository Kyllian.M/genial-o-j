typedef struct couleur{
	unsigned char r, v, b;
} couleur;

typedef struct gif{			//CECI N'EST PAS REELEMENT UN "GIF" C EST UNE IMAGE ANIMEE QUI EST SIMILAIRE A UN GIF
	DonneesImageRGB **img;	//FRAMES DU GIF
	int numberFrame;		//NOMBRE DE FRAME DU GIF
	couleur background;		//COULEUR DU FOND A RETIRER
	bool backgroundExist;	//Y A T IL UN FOND A RETIRER ?
	float currentFrame;		//FRAME AFFICHEE A UN INSTANT T
	float pas;
	int fps;
	float lastTime;
	bool isFrozen;
	int boucle;
}gif;

typedef struct DonneesImageARGB{
	int width;
	int height;
	float zoom;
	float static_width;
	float static_height;
	int* donneesARGB;
}DonneesImageARGB;

void resizeGif(gif* G,int largeur);

//ALGORITHME DE CROPPING D IMAGE DU "PLUS PROCHE VOISIN"
//cette fonction sert a redimensionner une image en fonction d'une nouvelle largeur (l'image va garder le meme ratio largeur/hauteur)
DonneesImageRGB* resizeDonneesImageRGB (DonneesImageRGB** img,int newLength);
//fonction de conversion d'une structure RGB** en unsigned char* pour le stockage d'une image
void convertRGBinUnsignedChar (RGB **T,unsigned char* BVR,int largeur,int hauteur);
//fonction de conversion d'une structure unsigned char* en RGB** pour le stockage d'une image
void convertUnsignedCharInRGB (RGB **T,unsigned char* BVR,int largeur,int hauteur);
gif initGif (char folderName[20],char commonName[20],int numberFrame,int fps,bool backExist,couleur color);
void afficheGif(gif *G,int x,int y,bool sens,timer T);
DonneesImageARGB* lisBMPARGB (char* path);
DonneesImageARGB* convertFromDonneesImageRGB (DonneesImageRGB* img);
void setZoomDonneesImageARGB (DonneesImageARGB* I,float factor);
void freeDonneesImageARGB(DonneesImageARGB **structure);
void betterEcrisImage(DonneesImageARGB* I,int x,int y);
void sauveDessinBMP(char* nomFichier,unsigned char* img,int width,int height);
void retourneImageByte (unsigned char** img,int width,int height);
void screenshot (int x,int y,int width,int height,char* nomFichier);